<DOC>
<DOCNO>1070626_calcutta_story_7972015.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Growth pangs

 Schoolchildren from Drass Public School, Kargil, during their visit to Calcutta. A Telegraph picture

 Spending Sunday afternoons arguing with your father about monopolising the TV remote and pestering him for extra allowance are normal for most of you. But what about those whose fathers brave the icy chill manning the border in far away Kashmir, while they are tucked cosily in their beds.

 For children with parents in a risky profession, the growing-up years are punctuated with bouts of anxiety and uncertainty. 

 While the mother provides emotional support at home, it is often the father who gives a sense of security to an adolescent. Though I am used to it now, I still miss papa. Its difficult to cope with the void every time he leaves home for work, said Ali Mohammad, a Class IX student of Army Public School. His father Jagam Abbas is a havildar in the army and is posted at Baramullah.

 The pressure of being prepared for bad news all year round often forces these children to grow up soon and shoulder responsibilities they would not otherwise have been burdened with. Rujda Parveen is one such girl. She is the support system and a source of strength for her mother, who looks after the family.

 I help my mother and also look after my young brother as she has to take care of the household chores, said Rujda, a Class IX student of Army Public School. Her father has been posted in Hissar for the past three years. 

 Students of The Heritage School take part in an inter-house drama competition at the school auditorium. Picture by Sanjoy Chattopadhyaya

 Taslema Nasrins father has been working in Leh for the past two years. She too helps her mother with household tasks and shopping besides taking care of her own studies. I get to see my father for about 20 days in a year. It is scary at times, said the 11-year-old. 

 In a highly competitive world, the mental pressure usually piles up over academics. As a result, studies are often hampered. The situations these children face are complicated. We try to support them as much as we can, said R.S. Sharma, principal, Kendriya Vidyalaya, Ballygunge. 

 Rujda recalled the time when her father went to the Kargil war in 2000. I was quite young then. But I remember the anxiety and the fear of my family members. I shudder when I think that my father was fighting in the war.

 Deepti Dhapola knows a lot about wartime, even though she had not been born when her father was away in the battlefield. My mother was pregnant with me when my father was posted in Jammu and Kashmir. She had to be rushed to the hospital when my father was not with her. I know it must have been tough for her, the student of Kendriya Vidyalaya said. 

 Children of defence personnel face another problem that comes with their parents job. Every time their parent gets transferred, the family is shifted to a new place. The kid finds it difficult to repeatedly adjust to new surroundings.

 After shifting from Coimbatore to Berhampore to Calcutta, Siddharth Iyer has a first-hand idea of the itinerant life. I have studied in 11 schools in different parts of the country till date. I dont know where I will be next year, rued the Class XI student of Kendriya Vidyalaya, Ballygunge.

 The effect is often reflected on the report card. My marks deteriorated from Class X to Class XI. Sometimes I feel it would have been better had I studied throughout in the same school, he wondered. 

 The parent who stays with the child must also be equipped to handle additional duties. My kids are more attached to me than their father. But it is a tough job to play the role of both parents, admitted K. Prema, mother of Sandeep, who is in Army Public School. 

 Hardiness comes naturally to many children, as does pride. Many choose to follow in the footsteps of their fathers despite being aware of the comforts they might have to give up.

 It feels good that my father is among those whom the entire country is proud of. Even I want to join the defence forces, summed up Ali Mohammad. 

 Nabamita Mitra 




</TEXT>
</DOC>