<DOC>
<DOCNO>1070328_nation_story_7574728.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Grand trunk road

 - Rescued elephant calves take first steps in second shot at life in the wild

 The elephant calves with a keeper at the rescue centre in Panbari, Kaziranga, days before they were shifted to Manas National Park. (Courtesy: Wildlife Trust of India)

 Two-and-a-half years is no age to be leaving home and striking out in the wilderness for a life of independence.

 But Rupa and Pori are no ordinary babies.

 On their young shoulders rests the burden not only of saving scores of lives in their corner of Assam but of leading an entire species to safety and freedom.

 Which is why, as the day of separation draws near, Hemanta Das in his early thirties is already experiencing the pangs of a father marrying off his daughters.

 Looking across the kahuan grass, poetically inseparable from the elephant in Assam, he hums: Tumra gaile ki ashiben mor mahout bandhu re (you are going away, will you return my dear mahout)?

 Pratima Pandey Baruas folk song, portraying the delicate relationship between the elephant and its mahout and the pain of parting, strikes just the right chord for Das.

 For, Pori and Rupa are two of a group of six orphaned elephant calves that the keeper had brought up at Kaziranga, and who have now been brought to Manas National Park to be released into the wild in an experiment.

 At stake is a possible new way of saving human and elephant lives, on which eyes are peeled not only in India but in Southeast Asia and Africa, too.

 Programme Wild Rescue will attempt to demolish the belief that elephants separated from their original herds cannot be released in the wild after rescue, says Wildlife Trust of India (WTI) programme director N.V.K. Ashraf.

 Success would mean there is a sustainable, if costly, solution to the man-elephant conflict, more pronounced in Assam than elsewhere in the country. The experiment will create space for both and build opinion on the translocation of other endangered species, such as the Asiatic lion of Gujarat.

 Across Thailand, Myanmar, Laos and Cambodia, there are huge tracts of forest from where elephants have vanished. Yet, an estimated 16,000 are now in captivity across Asia, and in countries like Thailand, they are found begging on the streets.

 If the Manas experiment works, relocating captive elephants to re-stock the forests may not be far off.

 Rupa and Pori, along with the eldest male, six-year-old Mohan, and the younger Brahmaputra, Numal and Pinku will be showing the way.

 Im sad these little ones will be going away from me, but Im sure they will be safe in their new world. It would be great to see them challenge this great forest and be free of their past struggles, Das says.

 Well be watching if each one joins a different herd at Manas, or the orphaned calves form a family of their own. It will be the animals own decision, Ashraf says.

 Jungle rescue

 The calves had been rescued over the past two years in Kaziranga either from jungle ditches or, like Mohan and Brahmaputra, from swirling floodwaters.

 Das and six other keepers looked after them at the WTIs Panbari rescue centre, Kaziranga National Park. We cared for them as if they were our own children, Das says.

 Last February, the calves were radio-collared and brought in special trucks to Manass Doimari range, straddling the Indo-Bhutanese border, where space is ample and food plenty.

 Although Das, who has accompanied the calves to Doimari, will miss the mud baths, the animals frolic and their familiar petulance once they leave, its he and fellow keeper Tarun Gogoi who will make the translocation happen on the ground.

 For now, the two of them are taking care of their babies at Doimari, where a makeshift enclosure has been built in the heart of the forest that is home to elephant herds and tigers.

 Das and Gogoi wake up before dawn and prepare a mix from elephant milk powder, five tonnes of it donated free by Canada-based company Grober. After feeding their wards at sunrise, the keepers see the animals off as they enter the jungle and watch them anxiously through the day from the bush.

 The elephants return every evening and for now, are spending the night in the safety of the enclosure till they learn to find a family for themselves or start one of their own.

 Learning for the elephants, however, began at their old home.

 One evening here, the calves had returned in the evening accompanied by two full-grown wild elephants, a keeper at Panbari said.

 It seemed as if they were already remembering their early childhood with their original herd.

 One of the calves there were seven at the beginning joined a group of wild elephants from the surrounding forests and left.

 Challenges

 The reason why Manas and not some spot in Kaziranga itself was chosen for the experiment is that Doimaris core area is far away from human settlement. There are an estimated 500 elephants in Manas, who roam across forests in India and Bhutan.

 One source of worry is the genetic difference between elephant populations on the north and south banks of the Brahmaputra, says elephant expert Raman Sukumar, associate professor of ecology with the Centre for Ecological Sciences, Indian Institute of Science. Mohan and his friends have been moved from the south to the north bank.

 So, even if the calves are ready for a life in the wild, will the herds accept them?

 But forming a herd by themselves will bring its own challenges. Can Mohan, the eldest male, lead the surrogate family? Can they wait till Rupa is old and mature enough to take up the matriarchs role?

 We dont know, but I hope theyll break away one by one and join the other herds, says Ashraf.

 The WTI, which conceived the programme along with Assam forest department officials, and the International Fund for Animal Welfare (IFAW), the funding agency, are keeping their fingers crossed.

 The WTI has formed a three-man team to watch the elephants every movement. It will continue to monitor them as they begin their life of freedom.

 It might take anything from two to five years to reach a conclusion, Ashraf says.

 Similar experiments have succeeded in Sri Lankas Udawalave national park. But it was in Africa that the novel conservation effort began a few decades ago.

 The David Sheldrick Wildlife Trust in Kenya has released several orphaned African elephants in the Tsavo national park. Before the Manas experiment, an official from the Bodo Territorial Autonomous District, Khampa Borgiary, visited the Kenyan facility run by Daphne Sheldrick.

 Before this experiment, we had released two rhinos in Manas, Ashraf says.

 Wild Rescue may serve as a beacon and put pressure on those politicising conservation of the lion. Translocation for the lion was opposed by Gujarat, apparently because the state doesnt want to give up its pride.

 The lions only home in India is Gir near Junagadh, where the king is fighting a shrinking habitat and the danger of an epidemic while experts worry about the animals constant in-breeding.

 NISHIT DHOLABHAI IN NEW DELHI




</TEXT>
</DOC>