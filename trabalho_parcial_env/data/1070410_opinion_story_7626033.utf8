<DOC>
<DOCNO>1070410_opinion_story_7626033.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FINE BALANCE

 A tussle among the three organs of the State does not augur well for Indian democracy. Yet the warning note that the prime minister, Mr Manmohan Singh, sounded to the judiciary is an indication that such an unseemly controversy is not beyond the realms of possibility. It cannot be denied that the seeds of the controversy are actually embedded in the Constitution itself. Indian political and parliamentary practices are derived from Great Britain, where this kind of controversy never arose. In Great Britain, the sovereignty of parliament was never challenged by the judiciary, which actually acted to protect the rights of parliament and the individual from attacks by the monarchy. In the United States of America, some of the basic amendments were carried through to protect the individual from the State. There, too, the judiciary was not forced to act to protect the democratic rights of the individual. In India, some of the amendments to the Constitution have, in fact, encroached on individual rights and the judiciary has rushed in to protect those rights. This is how the tussle between the legislature and the judiciary began. A landmark judgment in this context was the one given by the Supreme Court in the Keshavananda Bharati case in 1973. The apex court argued that while parliament, as the elected body of the people, had the right to amend the Constitution, it did not have the right to alter the basic structure of the Constitution. The apex court, by what has come to be called the basic features doctrine, set itself up as the guardian of the Constitution. From this has also flowed the idea of judicial review, which is often viewed as a challenge to the legislatures power and sovereignty.

 The matter is thorny, since the parliament represents the collective will of the people. The judiciary does not represent the people, neither is it accountable to it. The pitfall before parliament is that it might be driven by the majority and by the political agenda of the party that commands the majority on the floor of the House. This may not always be the best option for a democratic polity. To check this tendency, the judiciary, by taking recourse to judicial review, has often gone beyond its remit. Judicial activism is good neither for society nor for the judiciary. The argument that the judiciary is often forced to compensate for the dysfunctions of the other two organs fails the axiom that two wrongs do not make a right.

 At the heart of the matter is a question of balance. The judiciary has rights of review and of suo motu pronouncements. But these are residual rights which should be exercised only in extraordinary circumstances and with a great deal of caution. The judiciary might well pause to ponder if its haste to review and correct is not a cause of long-term regret. Mr Singhs warning has more to it than a politicians ire.




</TEXT>
</DOC>