<DOC>
<DOCNO>1070805_entertainment_story_8148972.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Death of a star

 Hollywood frequently succumbs to lsquo;selective

 amnesia rsquo; when it comes to celebrities and their vices...

 Its a story as old as the Hollywood Hills a celebrity is caught in flagrante, drunk or high on drugs. He or she is charged with a crime and enters a rehabilitation facility. Meanwhile the world watches, waiting to see whether this time that heady cocktail of fame and addiction will result in the death of a star.

 Of course, in most cases it doesnt. As veteran Los Angeles image consultant Michael Sands puts it, Hollywood frequently succumbs to selective amnesia when it comes to celebrities and their vices. Just look at Robert Downey Jr, Drew Barrymore, even Mel Gibson.

 But everyone has limits hence the obsessive speculation coursing through LA this week about how devastating Lindsay Lohans latest drink-fuelled act of self-destruction will prove. Few were surprised to be woken on Tuesday morning by the sound of the 21-year-old actress crashing from the wagon again, despite an alcohol-monitoring anklet that she had been showing off on Malibu beaches.

 According to police, Lohan whose Hollywood nickname is Blow-han (on account of her alleged drug habit) was apprehended while driving erratically in pursuit of the mother of her former personal assistant. She failed a sobriety test and back at the station, officers found a packet of cocaine in her pocket (she denies it was hers).

 The arrest came only a week after the actress turned herself in to police in connection with another drink-driving arrest, a hit-and-run accident in May, six weeks before she reached the legal drinking age.

 There had been extensive discussion of disruption she caused on the set of Georgia Rule, where she would arrive late, blaming heat exhaustion. In a letter, producer James Robinson lambasted Lohan as discourteous, irresponsible and unprofessional, likening her to a spoiled child. Some therefore predict that this latest arrest could prove the final straw for the troubled star. How many times can you trust a girl who says It wont be happening again right before she slips up, as she told The Daily Telegraph in May just days before her hit-and-run arrest?

 I think shes in quite a bit of career trouble, says an executive at one major talent agency. People will be reluctant to hire her for two reasons: one, because shes no longer convincing in the wholesome, likeable roles shes made a living off; and two, because she cant be counted on to turn up on set as shes probably being scraped off the floor of a club somewhere.

 Its all very sad as she does actually have talent. But she seems to have lost her identity. She was the cute, bubbly redhead with the winning smile. Now shes another emaciated blonde.

 Commentators have been swift to compare Lohan to River Phoenix, who died of a drug overdose on Sunset Boulevard, Judy Garland and Marilyn Monroe or the unfolding tragicomedy of Britney Spears.

 Like Garland and Phoenix, Lohan was thrust into the limelight early, getting her first showbusiness gig as a Ford model aged three. Her breakthrough came with a role in a remake of The Parent Trap. The critics raved. After winning plaudits for Freaky Friday and Mean Girls, Lohan became Hollywoods hottest newcomer. She hit Los Angeles and began to unravel.

 This weeks arrest comes just days before the release of her latest film, the low-budget thriller I Know Who Killed Me. But as she was shuttled back to rehab, and the interviews scheduled to promote the release were cancelled, backers of the film were not pleased.

 I hope they put her in jail for as long as they can, Bernie Brillstein, whose agency has represented John Belushi, told the New York Times. Maybe shell realise how serious it is. I believe shes uninsurable. And when youre uninsurable in this town, youre done.

 Legal consensus is that a jail sentence is probably inevitable if Lohan is convicted. Shes in big trouble, says Steve Cron, a Santa Monica-based lawyer. 

 But not everyone is convinced that this is the end for Lohan. She even has support in the most unlikely quarters Robinson expressed sadness at the arrest and said he would hire her again in a heartbeat.

 The bottom line is that Hollywood is very forgiving, says Michael Sands, and Lohan is a tremendously talented actress. Over the long run, I think she will probably be just fine. 

 CATHERINE ELSWORT (THE DAILY TELEGRAPH)




</TEXT>
</DOC>