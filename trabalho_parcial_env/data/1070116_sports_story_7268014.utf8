<DOC>
<DOCNO>1070116_sports_story_7268014.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Fish swallows No.4 seed Ljubicic, Roddick struggles through

 - AUSTRALIAN OPEN

 Federer overcomes Phau; Mauresmo, Serena advance 

 Mardy Fish after beating Ivan Ljubicic on Monday 

Melbourne: Players always worry about a little bout of the jitters in the first round of a Grand Slam tournament even Roger Federer. 

The top-ranked Federer uncharacteristically dropped serve three times in the first set against Bjorn Phau before rallying to win 7-5, 6-0, 6-4 and kick off the defence of his Australian Open title on Monday. 

Amelie Mauresmo, who claimed her first Grand Slam title last year here, also started a bit nervous and Andy Roddick had to survive a slow start against a wild-card entry ranked only 212th. 

The bottom line in the first days of the two-week event is getting through without injury and finding form after the off season, often while facing unfamiliar opponents with nothing to lose. Fourth-seeded Ivan Ljubicic didnt make it, falling 4-6, 7-6 (2), 6-4, 6-4 to American Mardy Fish, who has climbed back into the top 50 after plummeting to 341 in the wake of two wrist operations in 2005. 

Federer feared losing the first set, which helped him regain his focus in a stuttering first set. 

I got broken in the first set three times, and that makes you a little bit nervous, said Federer, who went 92-5 with 12 titles last season and is on the cusp of breaking Jimmy Connors record of 160 consecutive weeks atop the mens rankings. You try to stay cool, but I got a little bit nervous. 

Im happy Im through because it looked like it was definitely going to head for a first-set loss, but I came through. Thats the most important. 

That was Roddicks mantra, too, after Jo-Wilfred Tsonga, riding a powerful serve in only his sixth match in a top level ATP event, took the first set in a marathon tie-breaker and served for the second set. Roddick wasted four set points in the but came through with a 6-7 (18), 7-6 (2), 6-3, 6-3 victory. 

I thought it was a pretty ordinary performance, the sixth-seeded American said. I think I got a little first-round jitters. But it got better as I went along. 

In a marathon last match of the day, 2005 champion Marat Safin beat Benjamin Becker of Germany 5-7, 7-6 (2), 3-6, 6-3, 6-4, demonstrating his renowned temper while continuing his comeback from a left knee injury that dropped him to 104th in the rankings last year. 

Benjamin Becker of Germany had the 26th-seeded Safin muttering to himself early with a strong serve and stinging groundstrokes. The Russian slammed his racket to the ground after missing an easy forehand in the second set. Then, minutes after a fan shouted Get angry! in the third, Safin smashed the racket and broke it, earning a warning for racket abuse. 

He was on his best behaviour the rest of the way. Becker made two errors that gave Safin the last break he needed at 12:30 am as thousands of fans stayed to the end of the three and a half hour match on a chilly night. 

The second-seeded Mauresmo dropped serve early with three errors against American Shenay Perry, then righted herself and took control to advance 6-3, 6-4. 

Serena Williams, seeking her eighth Grand Slam title but unseeded after a rash of injuries, started slow, too, but picked up her game to oust 27th-seeded Mara Santangelo 6-2, 6-1 in an error-plagued night match. Santangelo had only six winners to 23 unforced errors, while Williams ripped 10 aces in an otherwise lackluster service effort. 

Im feeling pretty good, Williams said. I came out and finally did what I was supposed to do. There is only one way I can move, and thats up. 

Federer next plays Jonas Bjorkman of Sweden, who rallied to beat Olivier Patience of France 5-7, 4-6, 6-0, 6-1, 6-2 to advance beyond the first round at Melbourne Park for the first time since reaching the quarterfinals in 2002. 

Federers opponent in the final here last year, Marcos Baghdatis, also advanced, backed again by a vocal crowd from Melbournes large Greek community. 

While the Australian Open has traditionally welcomed boisterous national followings, rivalries boiled over on Monday. Police and private security guards ejected 150 people from the Melbourne Park venue after Croatian and Serbian spectators kicked each other and used flag poles as weapons during brief scuffles that each side blamed on the other. 

Croatian Mario Ancic was among the seeded players advancing on the mens side, along with No. 18 Richard Gasquet and former world No. 1 Juan Carlos Ferrero, who only played three games before his first-round rival retired. 

Amer Delic, Zack Fleishman and Vince Spadea were the other Americans joining Roddick in the second round. 

Among the women, third-seeded Svetlana Kuznetsova beat Australian Jessica Moore 6-2, 6-0 while fellow Russian Elena Dementieva, seeded seventh, advanced with a 6-1, 6-2 victory over Stephanie Foretz of France. 

French Open semifinalist Nicole Vaidisova, seeded 10th, beat American Jill Craybas 6-4, 5-7, 6-1 and 11th-seeded Jelena Jankovic had a 6-3, 6-3 win over Canadas Aleksandra Wozniak. 

 (AP) 




</TEXT>
</DOC>