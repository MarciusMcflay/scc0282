<DOC>
<DOCNO>1070826_opinion_story_8237239.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ANOTHER LIGHT

 Brilliance has no religion, even if it is disguised in the habit of faith. Mother Teresa was an extraordinarily brilliant human being, who could only travel the path she knew without doubt to be her own, and who could move only when she had created her own order. This could not have been easy: it is difficult to imagine the kind of hard work, persuasion, determination and confidence that must have been needed for the Missionaries of Charity to be formed. There was a quality of genius in not just her vision but also in her ability to create and sustain the organization, her equal energy in caring for the poor as well as in raising funds to ensure that they would always be cared for. 

 No such person gets away without critics and Mother Teresa had many, some very famous people among them. But with the publication of the letters that she had written to her colleagues and superiors over 66 years, called, Mother Teresa: Come Be My Light, due on September 4, critics and admirers may be confounded alike. At least for some time. The extracts expose a painfully sensitive person in unspeakable agony, yearning for a god and a faith that have left her. And this sense of emptiness does not last only for a phase, as in the lives of many saints, but haunts her almost throughout her life in Calcutta. In a way, the irony seems too sharp to grasp. Her life was dedicated to the poor, the homeless, the old and the sick of the city; it is what she did for them that made her what she was. Yet it is here that she doubts the existence of the soul, of god, of Jesus, and feels that saving souls has no attraction. She is the first to call herself a hypocrite and her own smile a mask, for she speaks of gods love continually without finding it. It is almost as if the suffering of the poor makes her question the existence of god; yet her deep faith forbids her mind the terms of such a question.

 There is also some irony in the publication of the letters that the writer herself had wanted destroyed. The church wanted these preserved, in anticipation of her sainthood, and the volume has been edited by a proponent of her canonization. It may be that agonized doubts are an essential part of a saints life, but Mother Teresas dark night of the soul, spokespeople from the church admit, cannot be so easily contained in the traditional structure. By making them public, the church too is opening a window, even if for a moment. For lay people and unbelievers, Mother Teresa can now be seen as a hero like many other heroes, a great human being who struggled tirelessly for others, with a trust in life and hope that could not bank on faith, and for whom spiritual suffering was the greatest spur. She is great but familiar, for a moment shorn of the rhetoric of the church. A secular heroism is easier to understand. Those Calcuttans who resented the fact that a nun should become famous by saving souls among the poor in their city may feel differently now.




</TEXT>
</DOC>