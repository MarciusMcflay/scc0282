<DOC>
<DOCNO>1070227_sports_story_7446819.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Countdown to Crickets biggest show

 2003

This will go down as Indias most memorable World Cup since 1983. Almost written off before the start, Sourav Gangulys boys proved the pundits wrong with a superb display and upset many calculations. They showed tremendous grit, discipline and dedication. But for the final, where the mercurial Australians had the last laugh, the Indians not only won on the field, they were also winners off it. 

After a shaky batting performance in their opener against Holland and losing to Australia in their second match, the Indians faced a barrage of criticism back home. That, perhaps, ignited the fire in their belly. 

The side showed character in the hour of crisis and put together a winning streak of eight matches. A last-minute decision to restore Sachin Tendulkar to his favourite openers slot proved inspiring. He ended up with a staggering aggregate of record 673 runs. 

However, Indias challenge ended in the final where the Australians proved insurmountable. Ricky Pontings men deservingly won the World Cup. In winning all their 11 matches, they repeatedly displayed their professionalism and hunger for success. Not since the West Indies in the 70s, had any side shown such consistency. 

 Ricky Ponting 

The nearest Australia came to losing was in their final group match, at Port Elizabeth, when England had them at 135 for eight, needing 205. 

But Michael Bevan and Andy Bichel saw them through with great tenacity. They also faced trouble in their next two games against New Zealand in the Super Six and Sri Lanka in the semi-final but their focus and perseverance saved them. 

As was evident in the final at the Wanderers, Ponting showed the way. 

Australia, put in by India, ran up 359 for two in their 50 overs. Ponting hit a record unbeaten 140 from 121 balls, that included eight sixes and four boundaries. Tendulkar fell in the first over, and India, despite a spirited batting display, were never in the game. 

All of Australias ploys succeeded. The idea to play Andrew Symonds proved right after a crucial 91 not out in the semi-final. He had also cracked an unbeaten 143 off 125 balls in their opening encounter against Pakistan to carry his side through from a precarious 86 for four to 310. 

It set up a resounding victory in a game that began with the Aussies at their most vulnerable. 

Hours earlier, Shane Warne, their match-winner in the semi-final and final of 1999, had returned home after failing a drug test conducted during the tri-series back home in previous month. 

 Sourav Ganguly

Warnes samples showed he had taken two banned diuretics, hydrochlorothiazide and amiloride. A fortnight into the tournament, he was handed a one-year ban. The Australians, however, remained unfazed and the crisis, if there was one, never showed in their performance. 

Their miseries didnt end though. Before the second phase, Australia lost Jason Gillespie due to injury. 

Nathan Bracken was the second addition to the party (after Nathan Hauritz had replaced Warne), but Gillespies place in the team had already gone to Bichel. 

Brett Lee, however, showed the depth in their resources with a devastating display of pace and movement that resulted in 11 wickets in three games in the Super Six stage. He added three more in the semi-final against Sri Lanka and two in the final. 

The lead-up to the tournament saw problems concerning safety and security issues both in Kenya and Zimbabwe. 

Delegations, comprising senior ICC officials and board representatives from teams due to play there, were sent for inspection. Six matches had been scheduled for Zimbabwe and two in Kenya. 

New Zealand refused to play in Nairobi, and England didnt go to Harare. 

The ICC appeals committee, predictably, rejected their cases and awarded victory to the hosts. The teams had to pay heavily for this. 

Had England gone to Harare and avoided defeat, they would have reached the second phase instead of Zimbabwe. By winning four of the five pool matches, New Zealand reached the Super Six, but the forfeiture enabled Kenya to go through. 

There was more to follow as ICCs hopes of keeping cricket out of politics suffered when Andy Flower and Henry Olonga took the field against Namibia in Harare wearing black armbands, after issuing a statement mourning the death of democracy in our beloved Zimbabwe. 

After the ICC asked them to desist from making such politically motivated statements, they wore black wristbands in their next match against India. 

South Africa buckled under pressure and failed to come up with any memorable performance on their home soil. Allan Donald conceded that the pressure had been unbearable. They failed to cross the first hurdle. 

The surprise package was Kenya, who made it to the last four. 

Coached by Sandeep Patil, they made everyone sit up and take notice. They defended a score of 210 to beat Sri Lanka in Nairobi. They beat Bangladesh to confirm a Super Six berth. 

The points from the New Zealand win was carried forward, which meant they began the Super Six second only to Australia, and sealed a semi-final spot with a victory over Zimbabwe. But India put paid to their hopes. 




</TEXT>
</DOC>