<DOC>
<DOCNO>1070131_opinion_story_7325880.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 DANCE, THEN DESTROY

 Jamie Loyd

 Ka mate Ka mate/Ka Ora Ka Ora/ Ka mate Ka mate/ Ka Ora Ka Ora/ Tenei te tangata puhuru huru/ Nana nei i tiki mai/ Whakawhiti te ra/ A upane Ka upane/ A upane Ka upane/ Whiti te ra/ Hi !

 These are the words that have spread fear in all international rugby teams for years. There has never been a sight like it in rugby and there never will be.

 Having spent a month at the charity, Future Hope, in Calcutta, it is easy to see the love of rugby that the kids have in the city. This adoration is best personified by their keenness to know about the All Blacks and the Haka, a Maori war dance that has been made famous by the New Zeland rugby team. Recently, this pre-match ritual, which is performed by the All Blacks, has come under fire. Critics have also asked whether the Haka should remain a part of the rugby world. Frankly speaking, the fact that questions have been raised about this decades-old institution shows what a deep hole the world of rugby is in today. 

 Faltering steps

 The controversy started during the final autumn test match between Wales and New Zealand. Just before the game started, the Welsh Rugby Union informed the All Blacks that they would not be allowed to perform the Haka directly before kick off as has been the custom. Instead, they had to perform the Haka before the Welsh national anthem. This was taken as an insult, as it was felt that such a move was meant to put the All Blacks out of their stride. However the All Blacks, rather than being affected, performed the Haka in the changing room, came onto the pitch and outplayed their opponents in every aspect of the game. The result was a beating that is unlikely to be replicated ever again in Welsh rugby.

 This is not the first time that the Haka has run into problems. England prop, Richard Cockerill, is notorious for being one of the few men to have stood up to the Haka, face to face with the opposition, as if to say bring it on. This was the wrong thing to do, as he never played for his country again. Maybe, he simply lost form, but it is not wholly unreasonable to say that this gesture was seen as offensive and unsporting and Cockerill was dropped for gross misconduct.

 The story of the Haka is an odd-yet-typical extract from Maori folklore. Tanerore, the son of the sun god, Tamanuitora, is credited with the origin of the dance. Tanerore, according to Maori legend, represents the trembling of air on hot days. This, in turn, is symbolized by the quivering hands during the dance. The dance was used by the tribes before a battle. Now, it is performed by the All Blacks before matches. 

 Let it stay

 Originally, they would do the dance before a rugby match in full warrior attire. This idea was soon abandoned for obvious reasons. Over the years the Haka has become an established institution in rugby. The ferocity put into every action is a personification of what the game is all about. It would indeed be a travesty if it were to die out from rugby.

 In all its simplicity, the Haka is a Maori war dance that, apart from the accompanying gestures, should have no intimidation factor whatsoever. It is certainly not the sort of thing that nightmares are made of. The only reason it is so effective is because the opposition choose to believe it is intimidating. But any team that intends to fight fire with fire will have a chance of beating the All Blacks, the Haka notwithstanding. 

 Unfortunately, opponents allow themselves to be dominated psychologically by the show of aggression. By the time of the first kick-off, they have lost half the battle. The Haka is a fantastic spectacle that highlights Maori traditions. For this reason alone, it should not be erased from the annuls of rugby heritage. 




</TEXT>
</DOC>