<DOC>
<DOCNO>1070425_opinion_story_7690798.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ON A HOSTILE TERRAIN

 Sumanta Sen

 The attempted rape of two women at Rajauri by jawans masked as militants is sure to strengthen the claim that this is a regular practice among armed forces to discredit freedom fighters. The incident will also bolster the demand that the army be withdrawn from Kashmir, a demand which is now being examined by a three-member committee.

 Neither the claim nor the demand is unjustified. Deployed in any hostile terrain, soldiers do tend to behave as occupation forces. Things get worse when the forces are subject to sniper attacks, which make them even more ruthless as in Kashmir, where citizens are seen by the outside world as either militants or their allies. Such a perception has no basis, but the jawan operating in an atmosphere of suspicion and tension has no problem in sharing that belief. So he is always on the lookout for a chance to strike back after taking care to ensure that his identity is not revealed. In Rajauri, the jawans were unlucky.

 Yet a question remains. Should such incidents be reason enough to bow down to the demand for the withdrawal of troops? The Peoples Democratic Party, which has gone to town over the issue, is clearly guided by prospects of electoral gains. With the Congress making quiet overtures to the National Conference, the PDP has chosen to be populist, with an eye on the next elections. What the committee of three will decide is not known, but it would do well to keep this factor in mind. 

 The PDP should not forget that when it fought the last election, it had promised progress and peace through negotiations with militants. It had not spoken about withdrawing troops, and not a single step in that direction had been taken by Mufti Mohammad Sayeed when he was the chief minister. Its current stand has nothing to do with the welfare of the masses.

 Promises to keep

 In far away Manipur, on the other hand, curbing the men in uniform had been very much an electoral promise made by the Congress. Over a month has elapsed since then, but the army continues to enjoy special powers. Admittedly, a month is not time enough to decide on the future of an act which has been in place for nearly half a century. Yet, one cannot but feel uneasy.

 There is reason to suspect that the act will continue. Recently, two Manipuri youths were arrested in Calcutta on the charge that they were out to recruit students from that state to work for the underground Peoples Revolutionary Party of Kanglei Pak or Prepak. The organization has denied any link with the arrested men. But the arrests may be seen as reason to continue with the Armed Forces (Special Powers) Act.

 It will be sad if that happens. Militants by no means rule the roost in Manipur. It is the citizens who protest against the army presence, and with good reason. It is common knowledge how the army operations make a mockery of all democratic norms. The rape and killing of Thangjam Manorama cannot be dismissed as just one incident. It is in keeping with a pattern that has become a way of life in Manipur. The people, on the other hand, have always tried to protest in a constitutional manner. Withdrawal of the special powers may encourage the militants, but it is difficult to believe that law-keeping operations will be humane. But if the wishes of the Manipuris are not fulfilled, then who knows what may happen?

 Manipur is not Kashmir. There is no Pakistan across the border, and Myanmar has no interest in stirring up troubles in India. Kashmiris living in the fear of the armys olive green may feel their alienation, but Manipuris do not. The army, of course, will not agree to give up its special powers. But then, the Congress ought to remember that it has promises to keep in Manipur, unlike in Kashmir.




</TEXT>
</DOC>