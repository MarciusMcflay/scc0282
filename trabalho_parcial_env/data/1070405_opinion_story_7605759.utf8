<DOC>
<DOCNO>1070405_opinion_story_7605759.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 GIVE BIRTH AT A PRICE

 China Diary - Neha Sahay

 Its the Year of the Pig, and everyone is busy trying to get pregnant. This is supposed to be a lucky year to be born. 

 Most Chinese arent too happy with the countrys strict one-child policy, and some of them had found a relatively easy way of getting round it: having their babies in Hong Kong. This automatically confers Hong Kong citizenship on the babies, with its accompanying advantages. However, the Hong Kong authorities have suddenly put a brake on this practice. 

 Now couples from the mainland, wishing to deliver their babies in Hong Kong, must fulfil many conditions. First, they will be allowed entry only if the women are not more than seven months pregnant. This is to avoid last minute arrivals, which have been quite common. Second, they must show a confirmed booking in a Hong Kong hospital. Or else, they will be sent back. Third, the price of delivering a baby has suddenly shot up for mainlanders. 

 These conditions do not apply to foreigners. The irony is that Hong Kong is part of China, although it is a Specially Administered Region. To add to this, its low birth rate has made authorities urge citizens to have more babies. In effect, this means that the Hong Kong authorities want more babies, but not those born to their compatriots from the mainland. 

 The reasons are many. The influx of mainlanders had become so high that they accounted for one-third of all births in public hospitals in Hong Kong in the last few years. Hong Kongs own mothers are being turned away by overcrowded public hospitals. More importantly for the authorities, many mainlanders have left without clearing their bills, and its been impossible to trace them once they return to the mainland. 

 Mixed feelings

 At the heart of this issue are reasons more complex than accessibility to public services Hong Kong citizens rightfully consider their own. Its no secret that Hong Kong has mixed feelings about being part of China. The mainland is seen as politically backward, its citizens are seen as country bumpkins, boorish, smelly and uncouth. The only thing Hong Kongs citizens like about China is the cheap shopping just across the border in Shenzhen, the money the mainlanders throw around when they visit Hong Kong and the vast profits Hong Kong businessmen can make by shifting their business to the mainland where labour is cheap. So, to have this backward species fill up Hong Kongs public hospitals, enough to edge out bona fide citizens, is beyond tolerance. 

 The percentage of Chinese couples who leave without paying their bills is unknown. Some Hong Kong residents admit that for the misdeeds of a few, too many are paying the price. And its quite a price. 

 Under the new rules, a Chinese couple wanting to deliver its baby in Hong Kong must pay 39,000 Hong Kong dollars for the two-night-three-day package in a general ward almost twice the normal fee of 20,000 Hong Kong dollars. Those who manage to turn up at emergency wards without a booking will be charged 48,000 Hong Kong dollars, to be paid as soon as the woman approaches the hospital. The authorities have justified this huge rise by comparing it to the costs of deliveries in private hospitals. They hope that these rates would encourage mainland mothers to opt for private hospitals. However, the latter have backed off, saying they were already too overcrowded. 

 Some residents, especially men married to mainlanders, have begun to question the fairness of the new system. Is concern for the rights of locals just a mask for class prejudice, they ask? A few Chinese too have begun to express disgust at the craze for all things Hong Kongese among some of their fellow citizens. It is time perhaps to take a second look at the one-child policy.




</TEXT>
</DOC>