<DOC>
<DOCNO>1070422_sports_story_7680083.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Brian Lara fascinating, even in defence

CHRISTOPHER MARTIN-JENKINS

Brian Laras retirement from all international cricket deprives the game of the greatest batsman of his generation little more than three months after saying goodbye to its greatest bowler. 

In the case of Shane Warne, it was arguable that there has never been a more effective match-winner. Lara has been at once a dazzling star and a relentless record-breaker, but his misfortune has been to be the outstanding West Indies player in an era when other countries were stronger. 

There is no doubt that he is among the best batsmen in any era and of any country. In the West Indies he continued a line of genius started by George Headley and maintained by Walcott, Weekes, Worrell, Sobers and Richards. Laras special talent has been to combine remorseless efficiency with an ability to be constantly entertaining. Even in defence he was fascinating. 

He was on the winning team in only 32 of his 131 Tests and West Indies won 13 per cent more games ten out of 27 when he was not playing. That he started his career just as they were running out of fast bowlers, after nearly 20 years when bowling prowess made it so much easier for the likes of Viv Richards, Gordon Greenidge and Desmond Haynes, is one of the reasons that Lara has been for so long the subject of controversy and gossip. 

In stronger teams the limelight would not have been so constantly upon him, although it was in his three periods as captain that it burned most brightly and, despite a sunny outward mien, uncomfortably. Like Gary Sobers before him, he was not sufficiently shrewd, sympathetic or responsible to be a good team captain. 

He resigned from Test cricket, having announced his retirement from one-day International cricket, only because he was about to be pushed. 

His 131 Test matches stretch back to one in Lahore in 1990. He had been made to wait for his first game in a strong team, even though everyone had known of his prodigious talent since the little left-hander from Santa Cruz in Trinidad began to score centuries in youth tournaments. He went on to score 34 for the West Indies in his glittering career, finishing with 11,953 runs at an average of 52. 

I saw every run of his 400 not out against England in Antigua in 2004 and every run of his 375 against England in Antigua in 1994. Not on these or any other occasions was he ever dull. One marvelled and always will at the full backlift, the timing, the placement, the wristy brilliance and hidden ruthlessness with which he dominated attacks. 

Only against India were his figures anything like modest: despite a mastery of spin based on twinkling footwork, he averaged only 34 in 17 Tests against them. 

But, like all opponents, they felt the power of his blade in limited-overs games. He scored more than 10,000 one-day International runs, the fifth-highest aggregate, but all but Sourav Ganguly of those above him have played significantly more innings. 

In time, I hope the gossip will leave him and he will be revered like the other great players of the Caribbean because no one has given greater pleasure to spectators. Only Sachin Tendulkar has matched him during his career, but the Indian has been less consistent. 

For all the partying he is said to have enjoyed, the fact is that he has remained remarkably fit. In other words, he has worked harder than his detractors have been prepared to admit. 

 THE TIMES, LONDON 




</TEXT>
</DOC>