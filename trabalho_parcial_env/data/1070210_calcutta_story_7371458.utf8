<DOC>
<DOCNO>1070210_calcutta_story_7371458.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Keeping it spam and short

 Only Connect

Abhijit Gupta 

Brevity is a much admired quality, but notoriously difficult to practice in certain fields. Take literature. It is all very well to be told that brevity is the soul of wit (of all people by that garrulous fool Polonius, in Hamlet), but try saying that to a Proust or Dickens in multi-volume flow. 

 Publishers too are apt to look askance if an author were to fob them off with a slim 10-page manuscript in lieu of the telephone directory they were expecting. Not for nothing did the Argentine author Jorge Luis Borges fail to win the Nobel Prize he disdained to produce a single novel despite being the greatest short story writer of all time.

 But all is not lost. The rise of the Internet in recent years has given a new lease of life to brevity in writing. I am not referring to the rapidly spreading pollution of sms-speak, but to more creative engagements within the limits of brevity. 

 Last year, the admirable Wired magazine asked 33 sci-fi writers to submit short stories in no more than six words. The results were most gratifying. William Shatner came up with Failed SAT. Lost scholarship. Invented rocket, while Margaret Atwood offered the pithy Longed for him. Got him Shit. Mr William Gibson, who has been haunting this column lately, went for the jugular: Bush told the truth. Hell Froze. While Gregory Macguire reminded us: In the beginning was the word. For more of the same, see www.wired.com/wired/archive/14.11/sixwords.html.

 Nearer home, I am happy to recommend the blog titled 55 word fiction (http://55-words.blogspot.com/) whose purpose is self-explanatory. Among others, the stories by Gamemaster G-9 and Anand are warmly recommended though unfortunately I cannot quote them here.

 But perhaps the most random assortment of short fiction can be found in a certain category of spams. Of course, the addresses of spam senders are works of art in themselves, and they have recently been written up as characters in a short story called Spamayana (see http://rimibchatterjee.net/livelikeaflame/). But the message bodies of such spams often generate word sequences which read like James Joyce on a bad day. Random example: Downhill abridgment their sofa sigh storey of either and as ice-cream of paradigm to appropriation. Dadaists would probably purr with satisfaction at such offerings but the rather prosaic explanation is that these words are put together in order to bypass spam filters. 

 Finally, I leave you with a few samples of spam haikus, another brand-new form which reduces life, the universe and everything into 17 syllables, no more and no less. Is SPAM Kafkaesque / Or was Franz Kafka Spamesque? / That is the question, asks Haiku master John Nagamichi Cho. But the final word must rest with Anonymous: Shakespeare invented / SPAM. Recipe in witches / Scene in play Macbeth. For more, see http://stuff.mit.edu/people/jync/spam/literature.html.




</TEXT>
</DOC>