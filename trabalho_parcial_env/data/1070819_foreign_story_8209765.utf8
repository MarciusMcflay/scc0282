<DOC>
<DOCNO>1070819_foreign_story_8209765.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 In Peru, a grim parade amid quake rubble

 MONTE REEL

 Residents sit in front of a coffin in Pisco, Peru, during a funeral. (AP)

 Pisco (Peru), Aug. 18: Even the cemetery was in ruins, the tombstones cracked and the mausoleums skirted with piles of rubble.

 Carlos Zuiga and his granddaughter, Maria, 17, blankly watched as another parade of polished coffins passed by about the only objects in this city not filmed in a thick dust.

 Two days before, Maria had been chatting on the second floor of her home with her mother and grandmother about buying a new dresser. The next thing she knew, she recalled, she was being pulled from under a sheet of concrete and everyone was telling her how lucky she was. But she didnt feel fortunate. Her mother and grandmother were dead under the same rubble, among the estimated 510 people killed by the earthquake that shook Perus coast on Wednesday.

 Yesterday, she and her grandfather followed their relatives coffins into Piscos main cemetery, completing a grim journey that continues to be repeated here as more bodies are pulled from the debris, more coffins are filled, and more families try to restore some semblance of normality to their upended lives.

 The family below us all died, Maria said, while a man with a pickax continued to dig her mothers grave. After a couple of seconds, she added: Of asphyxiation.

 The Zuigas are like a lot of families in this working-class city of 116,000, living together in part of a simply constructed, boxy building. Carlos Zuiga, 67, spends his workdays to the north in Lima, enduring a 250-mile round-trip bus ride that a modest salary makes bearable. His ride home on Thursday, after he was stranded in the capital overnight, gave him a vivid glimpse of what had become of the arid strip of coastline most affected by the 8.0-magnitude quake.

 The evidence was subtle in Lima, but his bus windows soon provided a glimpse of progressive destruction. About halfway into the four-hour ride, he saw people lining the road with plastic buckets, hoping for water trucks. Brick walls painted with political slogans from this years election were half-collapsed, scrambling the letters of candidates names. The bus slowed as it neared Pisco; the quake had ripped 10-foot-deep ruts into the convulsed asphalt.

 He said he had to walk the final five miles to what was left of his house. Only then did he understand what it looks like when 70 per cent of a citys buildings are reduced to rock and dust. I dont know how this could ever be rebuilt, he said. What can be done when suddenly there is nothing?

 Like most people here who lost homes and family members, he was drawn to the central plaza. In the days following the quake, the flat expanse of relatively empty space has become a hospital, a morgue and a lifeline for those desperate for food and water.

 LOS ANGELES TIMES- WASHINGTON POST NEWS SERVICE




</TEXT>
</DOC>