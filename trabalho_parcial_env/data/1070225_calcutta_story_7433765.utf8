<DOC>
<DOCNO>1070225_calcutta_story_7433765.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Ladies who lunched

 A womens club in the city turns 100, to find the good life changed. Anasuya Basu reports

 A photograph of a programme put up by NIAW in 1957, from the album of Aarati Dutt; (below) a recent picture of Mira Chaudhuri, the oldest member of NIAW 

 They were the smart set wives of expatriates, corporate officers, army wives. A trifle bored, maybe, as housewives, but definitely not desperate. Rather they were genteel, timid, shy, a little uncertain, but willing to come out of their homes and mix with the last of the memsahibs in town, learning how to sip on a fine brew of Darjeeling tea delicately while balancing paper thin cucumber sandwiches. 

 They met once a month, on a Thursday afternoon. With husbands at work and children in schools, sometimes twice, during the winter months. Members of National Indian Association of Women (NIAW), Calcutta, the well-heeled ladies of the society, spent their afternoons on the sprawling lawns of Calcutta Club to discuss the next fund-raiser or ways to help the needy girl child. 

 Cut back to 2007. A hundred years after the ladies club was founded, the 300-odd members can hardly fit into the Calcutta Club Crystal Room. A cacophony of voices, mostly in Bengali, drowns the rather esoteric topic of the day: Life pauses after menopause.

 Members lick their fingers after a sumptuous high tea of dhoklas and gulab jamuns. There is no dearth of funds that pour in in aid of Bustee Welfare Centre or Nari Seva Sangha.

 NIAW, the Calcutta chapter, was founded to allow women from different communities to socialise, with social service thrown in as added activity.

 It was founded to bring women out of the purdah, says Mira Chaudhuri, 86, the oldest member of the association. She remembers how the visiting Maharani of Darbhanga stepped out of her phaeton and into a corridor of curtain made for her so she could step into the annexe of the Calcutta Club where the meetings were usually held. The ladies huddled into the club from the side entrance on Gokhale Road and the band played from behind a screen at the afternoon parties. For once, the men were in purdah, she quips.

 Today, the agenda is largely altered. You no longer need to bring women out of their homes, for one, laughs Chaudhuri. Then, social service has largely been relegated to cheque book charity. How much are we really helping these poor needy women? wonders Chaudhuri, who has been a member since 1944 and served as president during the 1960s.

 The governors wives would be presidents as a rule at the beginning. Lady Ranu Mukherjees mother-in-law Lady Jadumati Mukherjee was the first Indian president, she recollects. Luminaries graced the members list: Lady Ranu to Aarati Dutt, Gurusaday Dutts daughter-in-law and Shireen Tata. 

 The cosmopolitan character of the club seems to have given way to a predominantly Bengali membership, if Bachoo Parakh is to be believed. Another old-timer, the Parsi lady joined the club first in the 1940s and then rejoined in the 1960s. I was new to Calcutta, and my husband, who was with ICI, was posted here. I joined to make new friends, she says.

 A life member of the association, Parakh, who lives alone in Gurusaday Road, makes it to the meetings, still held on Thursday afternoons. Some things dont change and we have resisted change in the rules, says Sreemoyee Ganguly, the current honorary general secretary.

 The association still has competitions for knitting, cooking, essay writing among its members and the winners are awarded huge silver cups, donated and named after their members. The Lady Jadumati Mukherjee Cup for Knitting Competition is a huge silver cup that requires a large locker to fit into. While there are still entries for singing and essay writing, very few are really interested in knitting and the cups cant be awarded unless there are at least nine competitors, says Ganguly. 

 For its centenary celebrations, the club hosted a grand dinner at ITC Sonar Bangla on Saturday followed by a fashion show hosted by Anjali Jewellers. Here was another change. Earlier, it would be the members themselves who would don greasepaint and put up shows for their entertainment. Now, as with all else, everything comes sponsored.




</TEXT>
</DOC>