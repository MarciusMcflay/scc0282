<DOC>
<DOCNO>1070111_nation_story_7246194.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Right to expel, with window for court

 - Legislature can sack members, but SC keeps door open

 OUR BUREAU

New Delhi, Jan. 10: The Supreme Court today recognised the collective wisdom of Parliament and legislatures to judge their members but left open a window for it to intervene if any citizen feels his fundamental rights have been violated.

 In a 4:1 ruling upholding the power of the legislature to sack members whose conduct lowered the dignity of the House, a Constitution bench said there was no bar on courts interfering with parliamentary proceedings if there were allegations of substantive illegality or unconstitutionality.

 Chief Justice Y.K. Sabharwal, Justice K.G. Balakrishnan, Justice C.K. Thakker and Justice D.K. Jain, however, clarified that the courts could not go into allegations of procedural irregularity or into the adequacy of the material before the legislature. But any citizen, member or non-member, could approach the court if his fundamental rights are violated.

 The court said it has to be initially presumed that the legislature had discharged its functions reasonably, but the presumption was rebuttable. It noted that no mala fide or ulterior motive could be attributed to Parliaments expulsion of 11 MPs in the cash-for-questions scandal in 2005.

 Justice R.V. Raveendran, in his dissenting judgment, however, said the action of both Houses of Parliament in expelling its members was violative of constitutional provisions and, therefore, invalid.

 Unlike the UK, Parliament in India was not sovereign and had to act in accordance with the Constitution, he said. 

 Justice Raveendran said there can be cessation of membership of either House of Parliament only in the manner provided in Articles 101 and 102; and that cessation of membership by way of expulsion is alien to the constitutional framework of Parliament.

 The majority, however, stressed that the court will not lightly presume abuse or misuse, giving allowance for the fact that the legislature is the best judge of such matters.

 The Constitution bench also cleared the way for bypolls to 10 Lok Sabha constituencies, which fell vacant after the December 2005 expulsions. The court had, during the pendency of the proceedings, given the go-ahead for filling up the vacancy in the upper House. 

 The two Houses of Parliament, through their respective secretariats, had chosen not to appear in the matter but the Centre had defended the action before the court. 

 The court accepted the Centres contention that though Articles 101 and 102 were not exhaustive in respect of termination of membership, Parliament could expel a member for conduct that lowers the dignity of the House, which may not have been necessarily known at the time of election.

 With legislatures getting corresponding power, fears of misuse have arisen as the question whether a member has lowered the dignity of the House has been left to the judgement of the majority of members of the House. 

 There have been precedents to show that the power exercised by Speakers to disqualify members under the anti-defection law has often been misused to favour one group or the other.

 Schedule shield

 The Supreme Court will tomorrow deliver its verdict on whether Parliament can validate a law, struck down by courts for violation of fundamental rights, by inserting it in the Ninth Schedule of the Constitution.

 Article 31B of the Constitution provides that no act or regulation specified in the Ninth Schedule shall be deemed to be void on the ground that it was inconsistent with or takes away or abridges any of the fundamental rights.




</TEXT>
</DOC>