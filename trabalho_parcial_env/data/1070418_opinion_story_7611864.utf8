<DOC>
<DOCNO>1070418_opinion_story_7611864.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ALL IN THE NAME

 STEPHEN HUGH-JONES

 We all do it, I noted two weeks ago: we call other nations places by our own languages version of them. Your Kolkata to most Britons, even now, is still Calcutta. My London is an Italians Londra. And things can get odder still: Germanys Aachen to the French is Aix-la-Chapelle. How come? The reason is at times political. What Britons call the Falklands, Argentinians call the Malvinas (which, unhelpfully to both, refers to St Malo, home port of some early discoverers of the islands from France). Many towns have changed names since 1900, as borders or regimes altered. Greek Smyrna is now Turkish Izmir; Leningrad is back to being (in English) St Petersburg. But more often, there is no visible reason at all: thats just how we name our neighbours geography, and we all find it entirely natural.

 In fact, its very odd. If your name is Chatterjee, you may be perfectly happy to know the Singh family next door. But would you be so to learn that, among themselves, they called you Arora? Yet among cities that is common form. Does language have special rights over geographical names that (mostly) it lacks with personal ones?

 In Europe, certainly. To French or Spanish friends, my home town is Londres. We all three spell Frances capital Paris, but pronounce and stress it differently. And wed all get lost in Belgiums spiders-web of towns with two names, one Dutch, one French: I once spent hours looking for Mons (Latin for mountain) wondering why all the road-signs pointed to Bergen (mountains in Dutch).

 I may go to Munich, in Bavaria, albeit I call its football team Bayern Mnchen just as its inhabitants do. They, in turn, go to Mailand or Venedig, not Milano or Venezia, while the Italians go off to Grecia, a country known to most of Europe by variants of that name, even though its citizens for 2,500 years have called it Hellas. And the Swiss know their own country by four names none of which is much like Switzerland.

 National sentiments

 The Germans claim to live in Deutschland (which the French know as Allemagne); the Dutch in Nederland (no, certainly not Holland; one might as well use UP for the whole of India); the Swedes in Sverige, the Finns in Suomi and the Albanians in somewhere beginning Shq... which no one else has ever heard of.

 And those are just the places I can vouch for. Though Im also told that the Chinese cheerfully dont live in China, nor the Japanese in Japan. Newer countries are more touchy about their names; as are the Chinese indeed about their newly-transliterated cities, with the typically Chinese assumption that they can decide not just their own city names but how the West shall write those names in the Wests own script.

 Thats where my hackles rise. My late employers at The Economist, mindful of its global standing, used to go instantly along with every tinpot governments changing of names. Were they right? Maybe. It is at least courteous, a rare Western response to small-country sensitivities. And British habits are now shifting: we still write Dunkirk for the battle, but take ferry tickets to Dunkerque. So why not (now already re-changed) Kampuchea?

 Yet I wonder. Im happy to live in Angleterre, Inghilterra or indeed Inglistan. No Spaniard says I must write Sevilla, no Portuguese insists on Lisboa. So, are some Burmese brasshats really entitled to dictate that we all write of Myanmar and Yangon? Certainly no one can stop me eating Bombay duck, drinking Ceylon tea (wisely still so named even by the Sri Lankans) or Hollands (sic) gin, or wearing Madras cotton. And if ever I adopt a silly, small, fluffy dog, it will not be a Beijingese. English is what it is, and it belongs to its users.

 thewordcageyahoo.co.uk




</TEXT>
</DOC>