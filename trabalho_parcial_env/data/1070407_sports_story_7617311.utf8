<DOC>
<DOCNO>1070407_sports_story_7617311.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Bobs death hasnt hit me yet: Gillian

IVO TENNANT

 Gillian Woolmer

Gillian Woolmer is no closer now to knowing how or why her husband, Bob, died in mysterious circumstances in Jamaica nearly three weeks ago. She spoke about the handling of the inquiry and the future she faces the day after his memorial service, when old cricketing friends had returned to Johannesburg, England or the World Cup. 

While Professor Tim Noakes and other speakers were unable to mask their emotions during the service, Mrs Woolmer, flanked by her sons, Dale and Russell, remained composed. She had done, too, when television cameras were trained on her after her husbands death. 

I was shaking like a leaf underneath, she said on Thursday at her home in Cape Town. So much has been happening since Bob died. When everything becomes quieter, that will be when this will hit me. 

The television crews and police have moved away now from her attractive thatched home, with its swimming pool and a garden cottage in a quiet street in the suburb of Pinelands, but the telephone continues to ring. Friends have offered her sanctuary at seaside retreats in the Cape, but she does not want to leave home, or, indeed, move in the future. In her husbands study, the cricketing paraphernalia he accumulated remains undisturbed. 

Mrs Woolmer has no intention of travelling to Jamaica. She has not been told when her husbands body will be returned for a private family cremation, but is in regular contact with the police in Kingston. 

Obviously I have not met Mark Shields (who is leading the investigation), but he seems to be doing a good job. It is not the case that Scotland Yard are being called in because he is incompetent. He is under stress and tired and requires help because there are so many things to look into. We need to bring this to a speedy conclusion. 

The police have returned Woolmers mobile phone and personal possessions that were in the Pegasus Hotel where he died, but still have his laptop. They have looked at the emails he sent me and others, but there is not even a hint in them of his being scared, or of anything to do with match-fixing. It is only personal stuff, and there is no way on Gods earth that I am going to let the public read this. 

Mrs Woolmer was not keen for him to take on coaching Pakistan in the first place. 

Bob told me he would never coach another international side after finishing with Dr Ali Bacher and South Africa. I said to him, I cannot believe you are thinking of doing this. He knew Pakistan fluctuated in form, but he liked a challenge and no one else tried to prevent him taking the job. 

If he had been offered the sort of salary Duncan Fletcher (the England coach) is on, much more than he was receiving from the Pakistan Cricket Board, he would probably not have been able to turn down the England job, either. 

The players treated him like a father, calling him Dad or Bubbly, which apparently means soft and cuddly in Urdu. Danish Kaneria, Younis Khan, Kamran Akmal and Asif all telephoned me when Bob died. We called our garden the prayer lawn after all the Pakistan side prayed before eating with us during their tour of South Africa earlier this year. 

Bob had his differences with one or two players, but that would be the same in any dressing room. It annoyed him that Shoaib Akhtar was such a good cricketer and yet would always have a niggling injury. Bob made a comment at Port Elizabeth in January and Shoaib went off at him. 

Bob got on with Inzamam-ul Haq, whom he did not dislike, but there could be moods. Inzamam would not speak to him for a few days but then would be OK again. The captain had a presence about him when he walked into a room and the younger members of the side revered him and would tread on eggshells around him, she said. 

The executors of Woolmers will now have the task of ensuring his contract with Pakistan, due to have expired in June, is paid in full. Mrs Woolmer will have need of an income the family is not especially well off and there is a possibility that fundraising dinners will be organised. A trust has been formed to ensure that the Bob Woolmer Cricket Academy goes ahead. 

Something positive will come out of this, she said. 

Since Woolmers death, Dale, the elder son who works in England as a promotions and party planner, has got engaged to Pippa Thornley, a schoolteacher at Epsom College in Surrey. Bob knew this was going to happen, Mrs Woolmer said. A ring has been bought here in Pinelands, but the wedding will not take place for another year. 

Among the many messages, as well as flowers, received by the family was one from Woolmers old nanny when he was growing up in Kanpur, India. We had not heard from Susheela Karintikal for 10 years, and she said how sad she was to hear of the death of my Bobby. 

In addition to the calls from old cricketing friends Mike Denness, the former England captain, telephoned Thursday before returning home after his visit for the memorial service there have been ones from people Mrs Woolmer hardly knows. 

Someone who used to see Bob walking the dog called, as did the manager of our local supermarket. Unlike most professional sportsmen, Woolmers home number was not ex-directory. He did not hide it from the media or cricket supporters. 

As to his plans for the future, Woolmer, in addition to his hopes of acquiring sponsorship for his proposed cricket academy near the Kruger Park, had been offered a consultancy role at Dubai Sports City, where Abdul Rahman Bukhatir has been planning a stadium. His coaching manual with Professor Noakes of Cape Town University will still go ahead. 

But he was not keeping a World Cup diary. He used to work away on his computer every day, but I did not even know he had started to update his autobiography. I was aware Bob was planning to write an account of his time with Pakistan, but that was intended to be after he had finished coaching them, Mrs Woolmer said. 

It is best if that book never appears now. If it is going to cause upset, it is not worth publishing. She reiterated that he was not intending to write anything about match-fixing or bookmakers. 

Mrs Woolmer had hoped that her husbands memorial service on Wednesday would be a form of closure. But she is all too aware that, even though the memorial service is over and the assorted gawpers have left her locked garden gate, she will not find any sort of peace till her husbands death has been resolved and perpetrators are brought to justice. 

 THE TIMES, LONDON 




</TEXT>
</DOC>