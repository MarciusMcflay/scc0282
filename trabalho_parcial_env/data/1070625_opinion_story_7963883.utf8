<DOC>
<DOCNO>1070625_opinion_story_7963883.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 JUST A GAME 

 OF CHICKEN 

 FIFTH COLUMN

 Gwynne Dyer

 The 

 United States of America is off the hook: China has overtaken 

 the US to become the worlds biggest emitter of carbon dioxide. 

 From now on, China will be the main target of the criticism 

 that used to be directed at the US for refusing to accept 

 binding limits on its greenhouse gas emissions.

 Whats striking is the speed with 

 which China has passed the US. In 2005 its carbon dioxide 

 emissions were two per cent lower than that of the US; in 

 2006 they were eight per cent higher. Yet China only has 

 four times the population of the US, and the average Chinese 

 is nothing like a quarter as rich as the average American. 

 In fact, the vast majority of Chinese dont even own cars. 

 So why does China produce so much carbon dioxide?

 One reason is cement. The pace 

 of building in China is so intense that the country produces 

 44 per cent of the worlds cement (the US produces 4 per 

 cent), and cement production is a major source of greenhouse 

 gases. The main culprit, however, is coal, which accounts 

 for 70 per cent of Chinas energy consumption.

 China already burns more than 

 twice as much coal as the US, and almost as much as everybody 

 else in the world combined. In the race to keep up with 

 soaring energy demand, it is building 550 new coal-fired 

 power stations, and nobody has the time to experiment with 

 clean-burning coal technologies that are still new even 

 in the West. So Chinas emissions will continue to race 

 ahead of everybody elses.

 But climate change will 

 affect the lives of ordinary Chinese people, and the government 

 and the experts know it. One government study last year 

 predicted a 37 per cent fall in crop yields within the next 

 fifty years if current trends persist. Since we may assume 

 that climate change will have comparable effects elsewhere, 

 and that even a rich China will be unable to make up the 

 shortfall by importing food, that prediction implies mass 

 starvation. Dont they care?

 Pay them or suffer

 Of course they care, but they 

 are in a high-stakes poker game and they cannot afford to 

 blink. There is going to have to be a global agreement on 

 curbing greenhouse gas emissions within the next five to 

 ten years or the world faces runaway climate change. 

 Put yourself in Chinas shoes. 

 Five hundred years ago, average incomes in Europe, India 

 and China were about the same. Then the Europeans got the 

 jump on everybody else technologically, grew unimaginably 

 rich and powerful, and conquered practically the whole world. 

 They also industrialized, and for two hundred years it was 

 their industries, their cities, their vehicles that poured 

 excess carbon dioxide and other greenhouse gases into the 

 atmosphere. Now the rich countries are willing to curb their 

 emissions but they can easily afford to, because they 

 are already rich and bound to remain so.

 Whereas if China imposes the same 

 kind of curbs on its emissions, then it will not become 

 a country where most people are prosperous and secure in 

 this generation, or perhaps forever. So the deal must be 

 that they get to keep on growing fast, and the rich countries 

 take the strain.

 There are two main ways for the 

 developed countries to take the strain. One is to cut their 

 own emissions, leaving some room for the developing countries 

 to expand theirs. The other is to pay directly for cuts 

 in the emissions of the developing countries: pay them to 

 adopt clean-burning coal technologies, pay them to build 

 renewable energy sources, pay them not to cut the rain-forests 

 down. Pay them quite a lot because otherwise we all suffer. 

 The developing countries will 

 never get that deal unless they demonstrate that they are 

 unwilling to curb their emissions without it. That is what 

 they are doing at the moment, and its not actually a poker 

 game at all. It is a game of chicken.




</TEXT>
</DOC>