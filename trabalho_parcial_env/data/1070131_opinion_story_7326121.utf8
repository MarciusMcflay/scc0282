<DOC>
<DOCNO>1070131_opinion_story_7326121.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ENGLISH TEST

 For Indians, to be told by Australians, of all people, that their English is not good enough for most respectable professions in Australia should be a very serious thing indeed. Of course, the telling off is not confined to Indians but to all overseas students, and comes from a report published by Monash University, Melbourne. It says that a third of all former overseas students given permanent residence last year was unable to read, write, listen and speak English at a professional level despite having completed undergraduate and post-graduate degrees at Australian universities. Students from South Korea and Thailand struggle most with English, and then those from Nepal and China, followed by students from Bangladesh, Pakistan, India and Singapore (in that order). There are two ways of looking at these findings. First, although India seems to be faring best, there are still about 17 per cent of Indian students failing to reach the required level of proficiency in English a band six in the IELTS ranking. Obviously, even students from privileged sections of Indian society, who can pay their way through an Australian education, are not receiving a proper grounding in English. Inadequate English-teaching is, therefore, not simply a rural and suburban government-schools problem and has to be addressed more comprehensively.

 But the seriousness with which such a survey has been conducted and received in Australia also says something about the countrys anxieties about its national identity and place in the world. With the British queen still its constitutional head, yet increasingly looking towards Messrs Bush and Blair for framing its aggressive foreign and immigration policies, Australias attitude to its ethnically muddled, but thriving, economy is not entirely free of unpleasant ambivalence. With troops in Iraq, and laws requiring mandatory detention of asylum-seekers, a country that wants its aboriginal population to become assimilated into the white mainstream is also anxious, at various levels, about the presence of a diverse range of outsiders. Its long-reigning prime minister, Mr John Howard, a conservative monarchist, is proud about his unequivocal stance regarding the entry of outsiders: We decide who comes into this country and the circumstances in which they come. It is, therefore, no surprise that such a country would take the Monash report very seriously.




</TEXT>
</DOC>