<DOC>
<DOCNO>1070904_opinion_story_8270027.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ELUSIVE VOICES 

 - The lives and letters of Anandibai Joshi 

 Malavika Karlekar

 In 1886, the year Kadambini Ganguly 

 became a GBMC (Graduate of Bengal Medical College), a 21-year-old 

 Maharashtrian woman also qualified as a doctor in faraway 

 Philadelphia. When Anandibai Joshi died in 1887, she left 

 behind a rich body of correspondence that she had had with 

 her husband, Gopalrao, as well as with those who had helped 

 her go to America. These provided grist for the biographical 

 mill, beginning with one by an early American feminist, 

 Caroline Healey Dall, a year after Anandibais death. Dall, 

 who had met Anandibai, aimed to make available the life 

 and motivation of this young Indian woman for the American 

 audience. Womens education often at the behest of missionaries 

 took centre-stage, Anandibai being a prime example. In 

 Crossing Thresholds: Feminist Essays in Social History, 

 the historian of 19th-century Maharashtra, Meera Kosambi, 

 points out that although the biography is influenced by 

 Dalls Orientalism, it nevertheless iconizes that little 

 brown baby whose future no one suspected. 

 Kashibai Kanitkars 1912 biography, 

 the first Marathi one in this genre to be written by a woman, 

 also relied on letters, information given by Gopalrao, and 

 some family friends. Kosambi feels that despite the limitations 

 of her work, Kashibai did manage to bring Anandibais voice 

 into focus by quoting extensively from her letters. On the 

 other hand, the fictionalized Anandi Gopal (1962) 

 by S.J. Joshi, which follows her life very closely, projects 

 Anandibai more as a victim, a helpless recipient of all 

 Gopalraos depredations and untrammelled ambition. In doing 

 so, Kosambi adds, he subverts the earlier two books, both 

 by women. Published originally in Marathi and adapted for 

 the stage, Joshis novel was immensely popular, an English 

 translation appearing thirty years later. 

 Though Anandi is the heroine, 

 in Joshis version, the postmaster Gopalraos life-consuming 

 obsession with womens education makes the reader focus 

 on him even in anger. Abuse of his child-wife, violence 

 towards her all in the name of making sure that she had 

 a single-minded interest in education are described in 

 detail. So is a cringing, dominated Anandi. One day, when 

 she was found helping her grandmother in the kitchen, Gopalrao 

 flew into an uncontrollable rage and beat the young girl 

 with a bamboo stick. The neighbourhood was agog: husbands 

 beat wives for not cooking but whoever had heard of a 

 wife being beaten for cooking when she should have been 

 reading? Soon after, a son was born to the couple but 

 died shortly thereafter. He had been treated by the local 

 doctor, as the one who was trained in Western medicine was 

 a Christian and an outsider; neither Anandi nor her child 

 could be seen by him, lamented Joshi.

 Gopalraos fixation with educating 

 his wife grew exponentially, and he decided that with the 

 help of a Mrs Carpenter, a Philadelphian missionary, he 

 would send Anandibai to America to train to be a doctor. 

 Before she sailed for New York from Calcutta (where her 

 husband was then employed), Anandibai addressed a full hall 

 at a public meeting. This was in 1883, not long after Kadambini 

 and Chandramukhi Basu had graduated from Bethune College. 

 Anandi spoke of the lack of women doctors and added, I 

 volunteer to qualify myself as one. She went on to point 

 out that existing midwifery classes were not sufficient, 

 and in any case, the instructors who teach the classes 

 are conservative and to some extent jealous. Brave words 

 from a mere slip of a girl who, Joshi writes, hid timorously 

 behind her husband as loud applause broke out. But did she 

 indeed do so? Or was she smiling proudly at the audience? 

 Anandi survived the long sea voyage 

 in the company of a missionary couple and was met in New 

 York by Mrs Carpenter who instantly bore her off to her 

 family home in Roselle, a three-hour train ride away. On 

 a family picnic, a photographer was sent for and Anandi 

 mailed the visual back to Gopalrao to whom she wrote diligently 

 every week. Gopalrao was not pleased; who was the man she 

 was smiling at (the photographer, presumably), and why was 

 her sari not covering her breasts adequately? Anandi 

 was crushed; but overcame her sorrow by burying herself 

 once again in her books at the Womens Medical College in 

 Philadelphia. By now the strain of a different culture, 

 the cold and damp had affected her and she developed a persistent 

 cough. 

 To add to it all, Gopalrao decided 

 to come to America. Latterly, Anandi had felt even more 

 estranged from him, his sarcastic barbs about her having 

 become at heart one of them, unbearable. By the time Gopalrao 

 arrived in Philadelphia, he was met by Dr Anandibai Joshi. 

 It was time to go home, and a visibly sick Anandi boarded 

 the ship with her husband. Soon after returning to a heroines 

 welcome in Bombay, consumption claimed yet another victim, 

 and the 21-year-old died without a chance of practising 

 in her country. Her ashes were later sent to Mrs Carpenter 

 who had them interred in her family cemetery at Poughkeepsie.

 Kosambi finds agency in Anandibais 

 tragically short life an agency missing in S.J. Joshis 

 account as he had chosen to look mainly at Gopalraos dictatorial, 

 and later unnervingly self-abnegating, letters. She quotes 

 letters where Anandibai speaks openly of her husbands violence 

 (I had no recourse but to allow you to hit me with chairs 

 and bear it with equanimity) as well her own motivation 

 to study medicine. Different Anandis fashioned by different 

 authors so much so that Kosambi muses candidly, has the 

 real Anandibai Joshee eluded us? Here is the biographers 

 ultimate conundrum: presented with a cornucopia of raw data 

 (that is, the letters), how are they to be read? Whose voice 

 is to be presented? Given that it is not always possible 

 to reproduce entire letters, what parts are significant? 

 The novelists concentration on those of the husband served 

 to highlight the worldview of patriarchal Marathi Brahmin 

 society. Joshi portrays Anandibais emotions, a deep anguish, 

 in the third person; her words are rarely heard. On the 

 other hand, Kosambi gives a voice to the young woman who 

 nevertheless felt that she owed everything to her husband, 

 tyrannical though he may have been. She is able to do so 

 by her choice of letters and her interpretation of their 

 relationship. 

 Was Anandi a victim or did she 

 intelligently make space for herself? The truth clearly 

 lies somewhere in between. Perhaps as biographers struggled 

 to deal with or ignore Jane Austens one instance of fragility 

 her fainting at hearing that the family had decided to 

 move to Bath from the home at Steventon where she had been 

 born there are defining moments (apologies to Cartier-Bresson!) 

 that determine how a subject is to be viewed. Such moments 

 grow or diminish, depending on the orientation of the biographer. 

 For, biography-writing involves a messy, often contradictory, 

 mixture of approaches writes Hermione Lee in Body Parts: 

 Essays on Life-writing. (Lee is an Oxford don and author 

 of two recent well-received biographies of Virginia Woolf 

 and Edith Wharton.) The game of inclusion and exclusion 

 is further complicated if the subjects own writings are 

 also part of the mlange. How does one make sense of the 

 mess? How does one avoid being hagiographical, or super-critical 

 and merely objective? Or does this much-maligned word 

 have absolutely no space in contemporary biography-writing? 

 Anandibai Joshis life has been 

 dissected from several perspectives, unlike that of Kadambini, 

 about whose life there is little available to dissect. Both 

 women were amazing and, interestingly enough, both were 

 married to widowers appreciably older than themselves. Widowers 

 committed to educating their wives. But was Dwarakanath 

 as autocratic as Gopalrao? Did he quail when he felt that 

 his wife was escaping from the mould he had carefully constructed? 

 Was he involved in the minutiae of his wifes intellectual 

 life and barely concealed his jealousy at signs of any 

 other existence? As we have no way of knowing the answers, 

 we are free to dream them up. Ultimately, it is up to the 

 reader to form her private word-image of Anandibai and 

 fantasize endlessly about Kadambini who escaped being at 

 the receiving end of a biographical venture.




</TEXT>
</DOC>