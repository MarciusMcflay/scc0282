<DOC>
<DOCNO>1070926_nation_story_8362344.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 High amp; low of celebration

 G.S. MUDUR AND CHARU SUDAN KASTURI

 New Delhi, Sept. 25: The euphoria triggered by Indias win in the ICC World Twenty20 final may translate into higher national self-esteem and even bolster motivation for other feats outside sports, but only if victories are sustained, social scientists have said. 

 The celebration is collective energy. In the same way that a mob works negatively during a riot, collective energy can also work positively in other fields at the level of motivation, said Anuja Agrawal, reader in sociology at the University of Delhi. 

 Social scientists who have been studying the impact sporting events can have on societies have contended in the past that success on the field can contribute to a nations self-worth and national identity.

 Sport has the potential to make a difference and help bring about change, according to Grant Jarvie, professor of sports studies at the University of Stirling, Scotland, who has said sports can play a role in civil renewal.

 The potential of sports to hoist national self-worth is evident with football in Brazil, ice hockey in Canada, or athletics in Kenya, Jarvie said.

 Researchers also said sporting events can facilitate social integration... and enable collective action.

 However, Agrawal cautioned that a meaningful social impact would require more than isolated victories. Such victories will have to happen repeatedly if the collective uplift of motivation and feeling of higher self-worth are to have an impact. A loss in the future may nullify any good that were feeling today, he added. 

 Social scientists said although celebrations help, they ought not to be prolonged. Its okay to celebrate on the day, but its crucial to remember that this celebration is only a free achievement (for most of the country), said Satish Deshpande, professor of sociology at the University of Delhi. 

 But victories or losses notwithstanding, sociologists said, sporting events promote contacts across different ethnic groups and work as unifying agents. 

 At sporting events, ethnic rivalries seem to be forgotten and people identify with the national team, supporting players from different backgrounds, according to Nico Scheulenkorf, a researcher from Germany studying social impact of sports. 

 Not all sociologists agreed that ethnic rivalries would vanish through sports. Differences between communities are unlikely to be bridged through sports achievements alone, said Agrawal.




</TEXT>
</DOC>