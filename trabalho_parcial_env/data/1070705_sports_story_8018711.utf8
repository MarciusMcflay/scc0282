<DOC>
<DOCNO>1070705_sports_story_8018711.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 A champion who lost the plot

 - Amelie Mauresmo has always been mentally fragile 

Sue Mott

 Amelie Mauresmo 

So this is what it was like in those flea-pit cinemas half a century ago when the film kept breaking down. One moment high drama, the next a horrible hiatus while abuse was hurled at the projectionist. Of course, this is Wimbledon. There is no abuse. No voices are raised, only umbrellas. But the principle is the same. Watching Amelie Mauresmo, the reigning champion, lose in four rain-interrupted parts was a hugely frustrating experience, not least for the loser herself. 

Are you very disappointed or just disappointed, the miserable Frenchwoman was asked in her post-match interview. 

What do you think? 

From your face I see disappointed, not very disappointed. 

Well, Im a good actress then. 

Actually, she isnt. Not nearly as good as Serena Williams, who had hobbled and limped like Richard III against Daniela Hantuchova before coming out of character long enough to wallop her wilting Slovak opponent 6-2 in the final set. Shakespeare left that bit out. 

Drama is the thing at Wimbledon. It is what makes the viewing so compelling and this year the dramatic has been replaced by the aquatic. Poor Mauresmo. On and off court, in and out of the locker room, her rhythm destroyed, her confidence lost, she played a third set like a parks court bluffer. 

Her rival, 14th seed Nicole Vaidisova of the Czech Republic, had a shockingly easy time of it. Mauresmo has always been mentally fragile. Here she was breaking again. 

Even when 1-3 down in the final set, she seemed to sense the way the script was going. Failing to control a ball at the net, she slapped her thigh and let out a cry of irritation. Two games later she was trying to thump a ball over the rim of the stadium, so furious was she at shovelling a routine forehand volley into the net. 

She is an emotional woman but does not normally go in for violence. This was her Kill Bill moment. Suddenly the awareness that she was surrendering her title tamely was more than her snapping nerves could stand. She has been a proud champion for 12 months. Losing her crown and her temper were understandably overlapping experiences. 

The end came swiftly. 

For a champion Mauresmo is hugely self-effacing. She is not the type to deploy wiles and read little self-motivating notes during changeovers. 

It was time for stoicism as the weary watering continued. The British weather, globally warmed or otherwise, has been as disruptive as a pupil with attention deficit disorder. 

It is a nightmare out there. Thunder, lightning, a shortage of prawn baguettes. Rafa Nadal played the same match for four days. Roger Federer hasnt played any since last Friday. 

You had to be grateful to the women. At least some of their matches finished. Perhaps this is why Wimbledon finally awarded them equal prize money. In honour of the shortness of their matches. The mens five-setters have turned into Test matches. 

That, though, was no consolation to Mauresmo. Who do you think is going to win the championship now, she was asked. 

Frankly, my dear, I dont give a damn, she said. Or something very like it. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>