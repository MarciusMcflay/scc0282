<DOC>
<DOCNO>1070506_frontpage_story_7740886.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Got a spare plane out of Iran

 - Ordeal of Indian passengers forced to stop over in Tehran

 Vivek Nair, The Telegraphs special correspondent in Mumbai, was among several Indian passengers on a Turkish Airlines plane that made an emergency landing at Tehran, not once but twice in a day. His account, despatched from Mehrabad International Airport at Tehran, follows.

 Approaching Iranian airspace, I was not thinking about Operation Eagle Claw, the mission that Jimmy Carter ordered to end the hostage crisis in 1980 but ended in disaster because American aircraft broke down and crashed into one another.

 But forced to land in Tehran twice in a day without notice and fenced out of one of the most enigmatic capitals of the world, I had little option but to marvel about the strange effect the Iranian sky seems to have on flying machines.

 Turkish Airlines Flight 1074 carrying over 120 passengers, among them many Indians like me had taken off from Istanbul on Friday at 7.10 pm local time (9.40 pm IST). The plane was on its way to Mumbai, where it was scheduled to land at 3.45 am on Saturday.

 After a few uneventful hours in the air, the plane began descending following, we learnt later, a technical snag. It was then that I realised we were about to land in Tehran, a stopover that was not on my itinerary when I set out from Mumbai to Istanbul to cover an automobile launch.

 I was not immune to the alarm that sets in when a plane makes an unscheduled landing but there was also a hint of excitement at the prospect of touching down in a city that is at the centre of an international nuclear stand-off. 

 But once we landed at the Mehrabad airport in Tehran at 10.30 pm local time (12.30 am IST ), the excitement evaporated. Turkish Airlines officials told us we have to spend indefinite hours at the lounge since we didnt have the papers to step out and check in at a hotel.

 Thus began an ordeal that dragged on for over 17 hours, which some of my fellow passengers described as the most harrowing flying experience of their lives.

 We spent the night at the airport lounge, the exhausted passengers, who included children, trying to catch some sleep. I tried to kill time by exploring the airport, which falls between mountains on one side and the city on the other. 

 It is also adjacent to an air force base. I could see a few helicopters and fighter jets near the runway.

 The Mehrabad airport is, however, not Tehrans main airport. The principal airport is said to be Imam Khomeini International Airport, some 80 km away. International flights do take off from Mehrabad but it pales in comparison with even airports in India in terms of ambience and comfort.

 The airport has no air-conditioning for most part of the day but has two Internet kiosks, which can be used free. It also has a prayer room each for men and women.

 During the day, we saw some women at duty-free and other shops hawking wares that included books about Iranian history, digital cameras and local sweets. Some were managing the airports communication facilities.

 From whatever little we could make out from the lounge and the sky, the city did not look too different from Mumbai, but for the massive white streaks on the mountains. 

 Nine hours after landing and the self-guided tour around the airport, hope sprang as we were told that the flight would take off soon. 

 Around 10 am local time, we were airborne again but only for 45 minutes. Word was out then that we were turning back because of you guessed it a technical snag.

 This time, the airline tried to be more accommodative, saying transit visas could be arranged and we could be put up in hotels.

 But we had only one demand: another aircraft. We have pushed our luck far enough with a plane an Airbus A 310-300 made in 1987. 

 The Turkish officials eventually agreed to ask Istanbul to send another aircraft. The new aircraft was scheduled to reach Tehran at 4.45 pm local time. 

 If it does come and take off at the promised time, we should be in Mumbai by 11 pm on Saturday, clocking a delay of about 20 hours. 

 At least, unlike those who perished on the doomed mission of Carter because of a collision in the air, we are waiting for a spare plane out of Iran.

 The plane did take off and complete the journey. Vivek Nair and the other passengers landed in Mumbai safely after 1 am on Sunday.




</TEXT>
</DOC>