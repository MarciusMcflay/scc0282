<DOC>
<DOCNO>1070907_sports_story_8287052.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Roger dodges Andys right way

LIZ ROBBINS

 Roger Federer jubilates after winning the second set against Andy Roddick during their quarter final on Wednesday. Federer won 7-6, 7-6, 6-2. (AFP) 

Andy Roddick shook his head on just a few helpless points Wednesday night, repeating a motion he has often been seen making over the last four years whenever Roger Federer is on the other side of the net. 

Roddicks only victory against Federer came in the 2003 Montreal final, the same year that he won the US Open. 

But this time, when Roddick walked off the US Open court that radiated pure electricity, he said he had no more questions. About himself or about Federer. 

Dressed in fierce black to counter Federers fancy black attire in the spell-binding quarter final on Arthur Ashe Stadium court, Roddick drove aggressively through his groundstrokes, like an 18-wheeler bent on delivery. He blasted his faithful serve, returned with precision and barrelled, unafraid, to the net. 

And why not? Roddick had lost 13 of 14 career matches to Federer, the No. 1 ranked player in the world for 187 consecutive weeks. If Roddick was going to leave the Open early instead of in the final as he did last year to Federer he was going to make this man figure him out. 

As usual, Federer did. 

Chipping back Roddicks 140 mph serve on instinct for a backhand winner and striking one of his 15 aces in the first two tie-breakers, Federer upended Roddick 7-6 (7-5), 7-6 (7-4), 6-2 to charge into the semis against Nikolay Davydenko. 

The first two sets, played in an electric night atmosphere buoyed by celebrities, featured no service breaks. Federer finally found his opening in the sixth game of the third set and the match came to an abrupt conclusion. 

Although fiercely disappointed, Roddick was defiant in defeat. Im not walking with any questions in my head this time, he said. I played the right way. 

He added: I thought I made him play as well as he could play. 

But Federer, who is attempting to become the first man in the Open era to win four straight US Open titles, was not about to call it a perfect match. I dont know if it was my best match or not, he said. 

Federer has already moved on, thinking about his 9-0 match record against Davydenko. He improved to 14-1 against Roddick. 

But although Roddick won a set off Federer in last years final, he seemed to push Federer even more Wednesday night. I felt pretty good from the first ball, he said. I had a game plan I felt like I executed it pretty well most of the time. 

Roddick had topped the charts at the Open this year for the fastest serve with 145 mph, two miles faster than the young American, six-foot-nine John Isner. But unlike with Isner, whose high bounce and kick caught Federer off guard for a first-set loss, he was ready for Roddick. 

In the second set tie-breaker at 4-4, Roddick served 140 mph to Federers backhand. Federer chipped it back to Roddicks feet, on the baseline, and Roddick was stuck. No chance. And no surprise. No, Ive seen it done too many times, he said. 

No matter how many chances Roddick took, they seemed in vain. When he charged the net (winning 50 per cent of the time), Federer would send the ball whizzing by his ear a millisecond later with his passing shots off his one-handed backhand. 

And although Roddick staved off one match-point, on the second attempt, Federer wasted no time. He coolly blasted an ace right down the middle at 122 miles an hour. 

Roddick shook his head for the first time, but not the last. 

Roddick, seeded fifth, still has not been able to solve him, even after more than a year with his high-profile coach Jimmy Connors. Roddick lost to Federer at this years Australian Open semis, suffered a shocking first-round upset at the French Open, and a five-set quarter final loss to Richard Gasquet at Wimbledon. 

Day X Results 

Mens singles, Quarter finals mdash; Nikolay Davydenko (4, Rus) bt Tommy Haas (10, Ger) 6-3, 6-3, 6-4; Roger Federer (1, Sui) bt Andy Roddick (5, US) 7-6 (5), 7-6 (4), 6-2; Womens singles, Quarter finals mdash; Anna Chakvetadze (6, Rus) bt Shahar Peer (18, Isr) 6-4, 6-1; Svetlana Kuznetsova (4, Rus) bt Agnes Szavay (Hun) 6-1, 6-4; Venus Williams (12, US) bt Jelena Jankovic (3, Scb) 4-6, 6-1, 7-6 (4); Mens Doubles, Semi-finals mdash; Simon Aspelin (Swe) amp; Julian Knowle (10, Aut) bt Julien Benneteau amp; Nicolas Mahut (Fra) 7-6 (2), 1-6, 6-3; Lukas Dlouhy amp; Pavel Vizner (9, Cze) bt Paul Hanley (Aus) amp; Kevin Ullyett (5, Zim) 6-4, 6-2; Womens doubles, Quarter finals mdash; Nathalie Dechy (Fra) amp; Dinara Safina (7, Rus) bt Katarina Srebotnik (Slo) amp; Ai Sugiyama (3, Jpn) 7-5, 6-3; Kveta Peschke (Cze) amp; Rennae Stubbs (6, Aus) bt Corina Morariu amp; Meghann Shaughnessy (13, US) 6-2, 6-2. 

 NEW YORK TIMES NEWS SERVICE 




</TEXT>
</DOC>