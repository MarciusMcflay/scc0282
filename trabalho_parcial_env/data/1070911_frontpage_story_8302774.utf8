<DOC>
<DOCNO>1070911_frontpage_story_8302774.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Indias Paris Hilton link

 K.P. NAYAR

 Washington, Sept. 10: While the number two man in the US State Department, John Negroponte and his chief aide on South Asia, Richard Boucher, are running around like headless chicken in Islamabad and Kabul this week to retain Pakistan as the 51st state of the Union, India is smugly sitting back in the knowledge that it will be a winner whatever the outcome of the current power struggle in Islamabad.

 One member of the Manmohan Singh government, who is usually ahead of his cabinet colleagues because of his experience and political instincts, is largely responsible for this confidence.

 Pranab Mukherjee met Pakistans freshly-deported former Prime Minister Nawaz Sharif at Paris Hilton the hotel, not the celebrity heiress on September 5, 2006.

 Mukherjee explains away that meeting as a chance: insiders say it was as much of a chance as President George W. Bushs accidental drop-ins on Indian ministers who used to have meetings fixed with then national security adviser Condoleezza Rice in her White House office.

 Insiders on Raisina Hill also say their meeting last September was so carefully planned that Mukherjee and the Sharif family would have suites on the same floor of the Hilton hotel.

 That meeting was to have been secret, but in India, such encounters have a way of leaking. Since then, India has regularly maintained back channel contacts with Sharif and his exiled entourage, sources in Dubai who are privy to such meetings told The Telegraph.

 On the other hand, Indias contacts with Benazir Bhutto have been more open and wide-ranging, often providing her legitimacy and diplomatic respectability at a time when only very low-ranking officials of the Bush administration would meet her in Washington for fear of offending their virtual quisling in Islamabad, General Pervez Musharraf.

 And of course, New Delhis contacts with Musharraf have been uninterrupted, with the Generals National Security Adviser Tariq Aziz regularly spinning a rosy scenario on behalf of his boss in recent weeks for the benefit of Azizs Indian counterpart, M.K. Narayanan.

 Mukherjees meeting with Sharif took place around the time Prime Minister Manmohan Singh was becoming increasingly disenchanted with the road map some of his aides had drawn up for India-Pakistan rapprochement, falsely promising a breakthrough on Kashmir and other disputes.

 The Research and Analysis Wing (RAW), Indias external spy agency, had, however, a vastly different assessment on Pakistan at that time. 

 It had compiled a report on Pakistan that boldly predicted that Musharraf may not last beyond the end of 2007. It is assumed that Mukherjee saw that report.

 Around the same time, although Mukherjee was still defence minister, he also had some extensive conversations with Shiv Shankar Menon, then Indias high commissioner in Islamabad, who was preparing to move to New Delhi as Foreign Secretary.

 India may be a winner in Pakistan, but that win may turn out to be short-lived because Pakistan is most likely to return to increased export of cross-border terror to India for reasons of political exigencies,never mind who is the winner in the current power struggle there.

 Besides, given the current state of power play in Islamabad, there is not much that is tangible by way of a win in Indo-Pakistan relations. The field is likely to be barren in the foreseeable future.

 Sharif was effusive about India in his conversation with Mukherjee, sources who are knowledgeable about their encounter said.

 Since then, Sharif has unfailingly stated his unwavering respect and affection for former Prime Minister Atal Bihari Vajpayee to Indians who have met him.

 In a way, that attitude is a pointer to Sharifs strength and his weakness. Unlike Bhutto or Musharraf, Sharif is steadfast in his loyalty and in his enmity.

 It is not that the Americans have not approached Sharif to do a deal with Musharraf similar to the one they are trying to solemnise between Bhutto and the general.

 But Sharif would not give up old allies, who are uncompromisingly opposed to a US role in Pakistan. Nor would he condone Musharraf for the 1999 coup.




</TEXT>
</DOC>