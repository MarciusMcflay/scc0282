<DOC>
<DOCNO>1070812_sports_story_8182899.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Fletcher was 

 brilliant... Cant see how much wrong he did: Hussain

 - Former England captain opens up 

 LOKENDRA PRATAP SAHI

 A TELEGRAPH SPECIAL

 London: 

 The Chennai-born Nasser Hussain needs no introduction. A 

 quality batsman and a fantastic captain, among the best 

 of all time. Hussain, who is now a highly respected media 

 personality, spoke to The Telegraph for over half-an-hour 

 on Saturday morning. 

 The following are excerpts

 On whether there were moments, 

 after abruptly quitting in the summer of 2003, that he missed 

 being an England captain/player

 I was pretty much done as a player, 

 more mentally than physically... Id played with a 

 lot of fire and I felt the fire had begun to go out of my 

 belly... I wasnt the type to just stroll out there... 

 Go through the motions... With time, then, the obvious thing 

 was to leave... Id enjoyed my career and one moves 

 on... (After a pause) I suppose the only time I wished 

 Id been there was when we won the Ashes at home (2005)...

 On what made the fire go 

 out of his belly

 Because Id been pretty fiery 

 from the age of eight... My father (Jawad, who has played 

 Ranji Trophy) pushed me first and, then, there were others... 

 I was 35 when I retired, so Id spent a lot of time 

 being passionate about the game... Cricket was important, 

 yes, but there were other things as well... I had a young 

 family... All good things do come to an end.

 On whether the crisis over 

 Zimbabwe with England refusing to play in Harare during 

 the World Cup earlier that year took a lot out of him mentally

 Drained me... I fell out with 

 the Establishment... I dont think the ICC handled 

 it well... They still arent handling it well... The 

 ECB too... That World Cup was disappointing as well, for 

 wed got a decent side... Bottomline is that you have 

 just so much energy in your bones and no more. Dont 

 forget Id also been the captain for four years... 

 Had tried to turn Englands fortunes around... Itd 

 been tough.

 On whether hes disappointed 

 that Zimbabwe remains an issue on the ICCs table

 Yes... I accept its not 

 an easy one... Its said that politics and sport shouldnt 

 mix, but we know theyre often intertwined... The ICC 

 worries about the smaller things in life and ignores the 

 major issues. The ICC hasnt been strong enough on 

 Zimbabwe.

 On whether captains have 

 a shelf life

 It varies... Depends on how successful 

 the team has been... Depends on the effort the captain has 

 had to put in... Because one had to take England cricket 

 up, a lot of my energy went in convincing the ECB to award 

 central contracts... A lot of energy went in trying to change 

 our style of cricket... Michael (Vaughan) has also done 

 it for four years, but he could do it longer as hes 

 had breaks because of injuries... Hes also been more 

 successful... If the team is winning, the captain lasts 

 longer. However, you cant have one across the board 

 standard.

 On Vaughan as captain

 Hes been brilliant... In 

 his calmness... In the tactics hes employed... I never 

 had a doubt that hed be a fine captain and Ive 

 not been disappointed... Id always known him to be 

 calm, but I hadnt realised how astute he could be 

 tactically. Some of his field settings... Bowling changes... 

 Theyve been outstanding. Michael and Duncan (Fletcher) 

 forged a good partnership and worked out some fantastic 

 tactics during the 2005 Ashes... I made England difficult 

 to beat, but Michael has taken them to another level in 

 Test cricket. 

 On Englands poor show 

 in the ODIs

 We dont have the ingredients 

 thats available in India or Pakistan or Australia... 

 One-day cricket is about talent and flair... In England, 

 were too orthodox in our batting... We dont 

 take the aerial route much and, perhaps, our batters have 

 been over-coached... We also dont have the wicket-takers... 

 Line-and-length bowlers arent enough.

 On England going in for 

 separate Test and one-day captains

 I dont disagree with the 

 move... Michael had his chance, but couldnt get the 

 team to win with any degree of consistency... Then, his 

 body is falling apart... Bad knees and... Hes a fantastic 

 Test captain and let him devote his energies to the longer 

 version... This move, Im sure, will extend Michaels 

 career and he ought to be around for the 2009 Ashes... Im 

 happy with (Paul) Collingwood getting the one-day captaincy... 

 Hes a strong person, but must bat up the order. I 

 wouldnt have troubled Kevin Pietersen with the captaincy... 

 Pietersen splits people... Some love him, some dont. 

 On whether Fletcher over-stayed 

 as coach

 No... (Adds laughing) In 

 my view, he under-stayed his time... I cant see how 

 much wrong he did in those eight years... England felt things 

 had to move on after the last Ashes and the World Cup, but 

 Duncan was a brilliant coach... Of course, he did make mistakes, 

 but we all do...

 On what made Fletcher special

 He knew the weaknesses of the 

 opposition before I knew them or any of my bowlers knew 

 about them... Probably even before the batsmen themselves 

 knew about their problems... 

 On the captain-coach relationship

 Its critical... (Sourav) 

 Ganguly and John Wright had a nice partnership... Ganguly 

 was aggressive... Made the Indians tougher... Made them 

 into a feisty side... Ganguly was, I believe, a fantastic 

 captain... India needed him at that stage... Wright was 

 placid, but obviously effective. 

 On whether the profile of 

 a coach is important

 In India, perhaps, you dont 

 want him to be high profile... Personally, I was a fan of 

 Greg Chappell... He tried to take tough decisions... I dont 

 know whether or not it was right to take on Ganguly, but 

 the moment you do something like that, you lose half the 

 vote... Frankly, the players should take responsibility... 

 I mean Fletcher didnt lose England the Ashes, the 

 players did. (After a pause) The best combinations, 

 I suppose, have been the ones where the coach has been a 

 quiet behind-the-scenes man working with a strong captain. 

 Like John Buchanan and Steve Waugh or Buchanan and Ricky 

 Ponting or Fletcher and Vaughan...

 On the one batsman who could 

 soon make it big for England

 Watch out for Ravinder Bopara, 

 who is yet to play a Test... Sachin Tendulkars his 

 idol and he does bat a bit like him. Im not building 

 him up too much, but you must watch him with interest... 

 Of course, I have high hopes on Alastair Cook, but the one 

 to challenge the greats is going to be Pietersen provided 

 he stays fit.

 Finally, on whether fingers 

 pointed at the Asian community in the past couple of years 

 have made him feel uncomfortable

 Except on the Zimbabwe issue, 

 Ive stayed off politics... So, I dont want wish 

 to comment... Im one hundred per cent English, but 

 love my Indian roots... Because of my father, Ive 

 always had a soft spot for India.




</TEXT>
</DOC>