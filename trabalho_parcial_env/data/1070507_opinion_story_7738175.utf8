<DOC>
<DOCNO>1070507_opinion_story_7738175.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 A NASTY LITTLE WAR

 Gwynne Dyer

 Respected people of Helmand, the radio message began, The soldiers of the International Security Assistance Force and the Afghan National Army do not destroy poppy fields. They know that many people of Afghanistan have no choice but to grow poppy. ISAF and the ANA do not want to stop people from earning their livelihoods. It was such a sensible message that it almost had to be a mistake, and of course it was.

 The message, written by an ISAF officer and broadcast in Helmand province, last week, on two local radio stations, was immediately condemned by Afghan and American officials, including President Hamid Karzai. So does that mean that ISAF really is going to destroy the farmers poppy fields?

 Well, not exactly. The latest plan is that it will be civilians who spray the farmers fields with herbicides, while the Western soldiers just stop the farmers from retaliating. That should win lots of hearts and minds in Helmand and other opium-producing provinces of Afghanistan, where the former taliban regime is making an armed come-back attempt.

 The soldiers of ISAF do not want to be seen as destroyers of the poppy crop because that would get lots of them killed (for the farmers can turn into taliban fighters overnight). It was allegedly a Territorial Army (reserve) officer, newly arrived from Britain, who got a bit carried away with the language and sent the offending message to local radio stations in Helmand. But most other army officers in Afghanistan, whatever their nationality, privately agree with him. You cannot fight a war against taliban and a war on drugs successfully at the same time.

 That was clearly understood at the time of the invasion in 2001. Taliban, austere Islamist fanatics that they were, had eradicated poppy-growing entirely by 2000, by the simple expedient of hanging anybody they caught growing poppies.

 High hopes

 Then talibans house-guests, Osama bin Laden and his al Qaida friends, carried out the 9/11 attacks against the United States of America. Bin Laden probably didnt mention this to taliban in advance because Afghanistan was bound to get invaded as a result. In fact, he almost certainly wanted the US to invade Afghanistan, imagining that it would result in a long guerrilla war and ultimate humiliation for the US, just as it had been for the Soviet Union in the Eighties.

 The US dodged that bullet by not really invading Afghanistan. It simply contacted the various ethnic warlords who were already at war with taliban, gave them better weapons and money, and left the fighting on the ground to them.

 However, the US now depended on those warlords to keep Afghanistan quiet without flooding it with American troops. The warlords needed cash flow, which meant poppies: opium and refined heroin account for over one-third of Afghanistans gross domestic product and almost all of its exports. 

 So the US turned a blind eye in 2002 while its warlord-allies encouraged farmers to replant the poppies, and didnt object when they were elected to parliament and joined Karzais cabinet. The war on drugs lobby in the US insists that something be done about it, so the US and allied armies end up trying to destroy the farmers crops. 

 We cannot fail in this mission, said John Waters, head of the White Houses Office of National Drug Control Policy, last December, as if wishing would make it so. Next year, of course, Afghan farmers would plant twice as many poppies, so the costs of the operations would rise over time. 

 Nothing will stop the flow of heroin into the West. Even if it is suppressed in Afghanistan, it will happen somewhere else . Buying up the opium crop is about the only thing that would give ISAF a chance of winning its increasingly nasty little war.




</TEXT>
</DOC>