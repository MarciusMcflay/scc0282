<DOC>
<DOCNO>1070406_sports_story_7613048.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Boparas heroics go in vain 

DEREK PRINGLE 

Ravi Bopara narrowly missed out on becoming an England hero on Wednesday when he was bowled off the final ball of the match as Sri Lanka won a pulsating Super Eight encounter by two runs. 

Chasing 236 for victory, England lost the Super Eight match by two runs when Bopara was bowled by Dilhara Fernando off the last ball of the match for 52, after giving himself room to hit the boundary needed. 

But there was no dishonour in such a defeat until he and Nixon added 87 runs for the seventh wicket, Englands hopes of winning had looked remote. 

The cruel nature of the result will not be lost on England, who bowled well to restrict Sri Lankas booming artillery to less than 250. They batted well, too, in patches, though the overall picture was flattered by Bopara and Nixons brilliance at the end. Once again, crass errors and poor application cost them dear. 

Until he missed his last ball, ironically in the slot to be hit for four, Bopara played with nerveless aplomb, an incredible quality for a 21-year-old. Some of the shots he played were outrageous, and if he acquires a garish taste in jewellery he might give Kevin Pietersen a run for his money. 

His only mistake, bar the one that cost him his wicket, was in not insisting Sajid Mahmood take a second run from the fourth ball of the final over. 

Mahmood, who hesitated, might have been run out, but Bopara would have retained the strike. As it was, Mahmood got a single from the penultimate ball. 

Nixon, 37, was equally impressive, making 42 off 44 balls. Until his reverse hits for six and four off Muttiah Muralidharans final over, the 48th of the innings, Englands chances of winning the game were about 30-70. That it got to about 50-50 was testament to his bottle and eye for an opening. 

Earlier, England had bowled well after Michael Vaughan won the toss and put Sri Lanka in. Mahmood and Andrew Flintoff took four and three wickets respectively, Mahmood removing the dangerous Sanath Jayasuriya for 25 when the left-hander chopped the ball on to his stumps. 

Since their belated success in Australia, Englands game plan has been mostly predicated on batting first, but a bizarre-looking pitch, streaked as it was with condensation marks from the plastic covers, persuaded Englands captain to bowl first. 

It proved turgid enough that even Sri Lankas batsmen struggled, though Mahela Jayawardene still managed to make 56 from 61 balls. Jayawardenes plan seemed also to include waiting for Englands fifth bowler, a combination of a Paul Collingwood suffering from a stiff neck, and Vaughans off-breaks. 

It seemed a reasonable tactic and worked well until his first attempt at a big shot, a smear to deep midwicket off Collingwood, ended up in Ed Joyces hands. 

Englands batsmen set off needing 4.72 runs per over and soon hit trouble when Vaughan was dismissed by Chaminda Vaas for a duck in the third over, judged by umpire Billy Bowden to have been caught behind down the leg side. 

Joyce followed three overs later, lbw to a ball from Lasith Malinga that skidded quickly into his pads. Englands top scorer against two of the minnows in their group games, Joyce appears to have lost form exactly at the same time as his captain, a problem that hardly helps Englands already flimsy top order. 

The put-upon look worn by Englands one-day supporters was soon replaced by something sunnier after Pietersen joined Ian Bell. Using his nous, Pietersen quickly worked out that his best options lay on the front foot, hitting through Malingas skidders and working Vaas through the on side. 

It worked a treat, too, and with the score reading 69 for two after 15 overs, England were on top. Sensing this, Jayawardene suspended the third power play until he had ended the partnership, which came when a brief moment of sloth by Bell gave him the breakthrough he craved. 

It was a crass piece of cricket. Batsmen are often run out backing up when a partner drives the ball back at the bowler, but Bell was not far down and the ball was not struck hard as Jayasuriya deflected it on to the stumps. His bat was even behind the crease, though not grounded, a fact that required TV replays to discern. 

The dismissal signalled a shift in the games dynamic, despite Pietersen reaching his fifty soon after and taking 12 runs off the first over of Sri Lankas final power play. Pietersen hit across a doosra from Murali, a stroke that resulted in him giving a tame return catch to the bowler. 

When Flintoff followed soon after, spooning a slower ball off Fernando, and Collingwood lbw two balls later to the same bowler, the game looked up for England. But for Bopara and Nixon it would have been, though their glorious failure has not advanced Englands cause in this World Cup. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>