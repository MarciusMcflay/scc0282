<DOC>
<DOCNO>1070926_entertainment_story_8363407.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Everyman superstar

 Must-have movies

What a difference a decade makes. In 1998, the critics agreed: The Truman Show was offbeat, outrageous, startlingly original or (for the few negative reviews) far too preposterous to accept.

 How could we believe that telly addicts across the world would sit glued to a round-the-clock programme recording every last mundane detail of a mans daily life? That such a show would dominate front-page headlines and generate the GNP of a small country, thanks to merchandising and crass product placement? Could popular culture really be that bad?

 Yet Truman, as he leads his sitcom-perfect existence on the island of Seahaven, differs from the attention-seeking stars of real reality shows in one particular. He has spent the first 29 years of his life blissfully unaware of the 5,000 hidden cameras and massive set that maintain and contain his illusion.

 This role signalled a new direction for Jim Carrey, previously famed as the gurning, irritatingly cocky star of Mask and Ace Ventura: Pet Detective. As Truman, hes vulnerable enough to make us care, yet charismatic enough to be a believable Everyman-superstar.

 Gradually, though, hes assailed by doubts. A lighting rig falls out of the sky; he stumbles over a camera crew. His wife and friends start seeming phoney, like bad actors. Soon hes making a break for freedom, despite scare stories of plane crashes, shipwrecks and nuclear disasters. Seahaven, after all, is a nice place to live why leave?

 The director, Peter Weir, has always been fascinated by claustrophobic, oppressive worlds, from Picnic at Hanging Rock through Witness to Master and Commander. 

 Here, he chillingly evokes the ultimate gated community, and creates a witty alter ego, the obnoxious, beret-wearing conceptual artiste, or televisionary, who controls The Truman Show. Its Ed Harris, who gets to deliver the immortal line: Cue the sun!

 Sheila Johnston (The Daily Telegraph) 




</TEXT>
</DOC>