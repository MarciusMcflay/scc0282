<DOC>
<DOCNO>1070115_nation_story_7263189.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Learning to read, the barefoot way

 MONOBINA GUPTA

 New Delhi, Jan. 14: They work in areas where their city-bred cousins wouldnt even think of going.

 Like the barefoot doctors in Mao Zedongs China, these young men and women go from door to door to rip the veil of illiteracy that has shrouded the hinterland for long. And slowly, but surely, they are changing the countrys education map.

 Meet Indias barefoot teachers the band of volunteers who teach rural kids how to read and write.

 A recent countrywide survey of rural India done by Pratham, a leading non-government organisation that has trained these volunteers, shows that there has been a marked improvement in the learning skills of village kids.

 Prathams last survey in 2005 had revealed that a majority of rural children were barely able to read simple sentences or do simple math even while enrolment in schools was on the rise.

 The turnabout is, to a great extent, being attributed to these barefoot teachers and their innovative methods. The basic manual for the volunteers explains why. It starts with this lesson: make sure you treat the children with respect. Talk politely. And make sure that the children enjoy themselves.

 The volunteers are picked from the local community and then trained by Prathams instructors. But the NGO first makes sure that an instructor has been through the grind. It follows a simple rule: nobody can become a trainer unless he has successfully enabled at least 25 children to read.

 The NGO devised its own model as the traditional way of starting with alphabets, vowels and then moving on to words and sentences was clearly not working. And it produced results, though traditionalists would dub the method subversive.

 In Maharashtra, Pratham volunteers started the first lesson with a story based on Shivaji. A small storybook based on the warrior king was created. Shivajis childhood is part of the state governments history textbook for Class III. Tales about him are recounted in every Maharashtrian household, the NGO says.

 In Gujarat, Pratham used the movie Lagaan, which was set in Kutch. Both the attempts led to more assured results, Pratham says. But it required a lot of effort and there was no visible change overnight. For north India, the NGOs approach was different. The children started with simple words and then recognised the alphabets.

 Pratham had its first positive feedback when Zubeida, a volunteer in Delhi, reported that out of 108 children who could not read, 32 per cent had started reading after being taught in the new way.

 It meant the barefoot teachers had succeeded in their mission, going from door to door like Maos army of community doctors local people with minimal formal training who provided part-time medical service, promoted basic hygiene and counselled on family planning as they went from village to village.

 For a Pratham volunteer, there are two crucial tasks. It begins with projecting oneself as a role model. Second, they should work towards developing a clear pronunciation, diction and reading style.

 After the initial success of its methods, Pratham persuaded the Maharashtra government to adopt them in state schools. Teachers, parents and children were thrilled with the visible and rapid progress, it says.




</TEXT>
</DOC>