<DOC>
<DOCNO>1070620_sports_story_7947761.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Eyeing fresh challenges 

TIM RICH

 Jose Mourinho 

This would be a bittersweet summer for Jose Mourinho. Back in Portugal, he could reflect on his ninth and tenth trophies as a manager and perhaps ponder why, for the first time since he took control of Porto in 2002, he had not finished the season as a champion. 

Like any manager in June, Mourinho is restless, already sketching out a new campaign and attempting to absorb the lessons of the last one. Principally, he appears to have understood that not even Chelsea, with their vast financial resources, can succeed in winning everything. 

We target everything but we know the reality of modern football, Mourinho said in an interview. We have the experience of the small details making the big difference. It is difficult to always have those details on your side and so it is very difficult to win every competition. But I dont understand why people put this kind of pressure on Chelsea and not on the others. 

People ask Chelsea, Last season you won two trophies; why did you not win four? But not many journalists ask Rafael Benitez and Arsene Wenger, Why zero? And not many people ask Manchester United, Why only the Premiership? 

Is it about money? If it is, I hope that next season the media put pressure on the big spenders because the big spenders for sure will not be Chelsea. The spenders will be Liverpool, Manchester United, Tottenham and maybe Arsenal. I dont know, but it wont be Chelsea for sure. So maybe at the beginning of next season they will say that Chelsea are not the favourites any more because the club did not spend so much money. 

If the era of Roman Abramovich funding a stream of world-class footballers is over, Mourinho would back himself to cope. Managers are best judged when their resources are limited. Just as Sir Alex Fergusons greatest triumph was probably overcoming Real Madrid to win the 1983 Cup Winners Cup with Aberdeen, Mourinhos is still the Uefa Cup and Champions League he lifted with Porto. 

When asked about the home-grown talent at Stamford Bridge that is slowly emerging with the likes of Scott Sinclair, Mourinho gave an answer reminiscent of Eric Cantonas comparison of newspapermen to seagulls. 

Young players are a little bit like melons only when you open them up and taste the melon, you are 100 per cent sure that the melon is good, he said. Sometimes you have beautiful melons but they dont taste very good; other melons are a bit ugly and when you open them the taste is fantastic. The way Scott Sinclair played against Arsenal and Manchester United, we know the melon we have. 

Next season, whatever the budget or quality of his melons, Mourinho appears determined to put regaining the Premiership and the Champions League at the forefront of his ambitions 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>