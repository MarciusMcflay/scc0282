<DOC>
<DOCNO>1070222_opinion_story_7421593.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 WEBBED FEET

 - Watching newspapers adapt to the Web is fascinating

 Mukul Kesavan

 mukulkesavanhotmail.com

 The Web has changed the way we consume newspapers. One of the main changes has been a shift in the balance between opinion and news. In a world where nearly every newspaper has an online avatar, news has become generic: the same event can be read described in a hundred newspapers. Google News, a site that consolidates the headlines of the hour under various heads (politics, sports, technology, business and so on) makes this redundancy explicit: Iraqis take Basra Command is the lead story as I write this piece, sourced from Reuters. Below it is a short list of four other news sources that have treated the same story (The Guardian, the Financial Times and two television news sites) and the item ends with a bold-faced link which says all 409 news articles.

 All 409 news articles! Most of these will be regurgitations, with minor variations, of news agency reports from AP or Reuters or some similar organization. So if youve read one of them, youve read them all. What differentiates online newspapers from one another is no longer the speed at which they get the news to you or even their ability to break a story. The news on the web is updated so ceaselessly that an exclusive, while important, becomes general pretty quickly. Increasingly, newspapers are differentiated by the opinions they serve up on the days news.

 I navigate my way to the independent.co.uk, not because Im generally interested in its contents as a news source but solely to read Robert Fisk on the Iraq war or the Lebanese crisis or the baroque hypocrisies of the Bush administration in matters Middle Eastern. I turn up at hindu.com to read Mike Marqusees weekly column. I bookmark the home page of the Times, London, to read David Aaronovitch, whose journey to the right has a certain horrible fascination of its own while also serving as a symptom of the souring of Western liberalism after 9/11. Even cricket: when I want to read a game described in the grand manner, I bring up The Sydney Morning Herald and contentedly read Peter Roebuck go on about character and destiny as if it was Kurukshetra he was describing, not a cricket match.

 Its not that people dont read hard news online: its just that they dont associate news coverage with newspapers in the same way as they used to. I get my news from news consolidation sites like Google News, from the BBCs sites and other, more specialized sources. When I want hard news on a cricket match I go to cricinfo.com. If the news in question has to do with matters technological, Im more likely to go to a tech news site like the Register or Ars Technica than a newspaper. Because every newspaper and television news channel feels obliged to have a Web edition, the news itself has become commodified since it is so easily and repetitively available. What helps brand a newspaper are the names who write opinion for it, who give readers a particular take on the news.

 You can see this most obviously in the proliferation of opinion mongers in every newspaper. The Guardians web-page is set up so that a news headline is nearly always followed by a link to a leader or an op-ed piece on that event. Sometimes you do go to a newspaper for reportage, especially when that paper employs a distinguished correspondent whose stories have more colour, insight and integrity than the wire services or run-of-the-mill reporters. Typically, this happens when youre trying to follow the course of a war. Robert Fisk, already mentioned, is a case in point. War correspondents like Fisk and Patrick Cockburn have spent decades reporting on civil strife and war in particular regions. They have a reputation for being free spirits which readers value. The importance of experienced, opinionated correspondents as news sources was highlighted by the course of the second Iraq war, because their despatches were so completely at odds with the gelded reporting done by the embedded eunuchs who did their tours of the war as camp-followers of the American army.

 Ironically, the commodification of the news via the Web might mean a revival in the fortunes of the foreign correspondent. Newspapers had begun to cull correspondents because they were expensive and besides the expense, editors had begun to assume that people preferred to get their news from television correspondents like Christiane Amanpour. But if the Iraq war has demonstrated anything, it is that readers seed out the knowledge that first-hand reportage brings.

 Interestingly, Fisks newspaper, The Independent, acknowledges the commercial importance of Fisks reporting in particular and its columnists in general by limiting access to their work to readers who buy subscriptions to the paper. The New York Times follows the same route: all the content in the paper is free except for work of its star columnists: Paul Krugman, Maureen Dowd, Nicholas Kristof, and so on.

 As online newspapers and news sites struggle with the challenge of differentiating their product from that of a thousand competitors, the famous correspondent, the celebrated columnist have become more and more important because they supply an individual voice and a human face to the anonymous vastness of news on the Net. Online newspapers, in their struggle to attract readers, have begun to explore the interactive potential of web-browsing. The Guardians opinion pieces are becoming blog-like in form if not intention because the online edition allows comments that show up at the end of the column. These comments are light years removed from the gentility of Letters to the Editor: theyre quarrelsome, abusive, frequently obscene. In the jostling, short-attention-span world of the web, to keep browsers loyal, you sometimes have to invite the mob in.

 Watching newspapers adapt to the Web is fascinating: like witnessing a case study in evolution unfold in front of you. From their online editions merely being digital duplicates of their newsprint sibs, their content and their form increasingly reflect the technological possibilities of the new medium. Newspapers have reporters doing blogs, online journals which represent a novel hybrid of news, opinion and argument. Its like watching a landlubber becoming amphibious, first getting used to another medium, then adapting to it. Because of the Web, the news-gathering function of newspapers is ceding ground to its opinion-forming role. In their patronage of blogs, in their willingness to allow readers in, you can see newspapers grow webbed feet and gills as this earth-bound creature learns to swim in a more fluid medium.




</TEXT>
</DOC>