<DOC>
<DOCNO>1070129_calcutta_story_7315624.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

Life

 Sarod, sons and sandesh

 RESHMI SENGUPTA

 Passing Through 

 Amjad Ali Khan at his Ballygunge apartment. Picture by Aranya Sen 

 The dry Delhi climate suits his sarod the most, so Ustad Amjad Ali Khan loves to live there more than any other city in the world. Theres too much humidity in other places which affects the steel, explains the maestro, bending over his sarod to set the strings. 

 Concert ke din thora tension hota hai (I am a little tense on the day of a concert). Every concert is new for me. Thats how I take it, he says softly, squatting on the floor of his Sunny Towers apartment.

 Amjad Ali Khan was the concluding artiste of the 55th edition of Dover Lane Music Conference, where he first performed with his father and guru Haafiz Ali Khan.

 That was in 1958-59. I have played at Dover Lane at least 25 times since. In fact, my maximum concerts have been in Bengal, shares the torchbearer of the Senia Bangash school.

 And Calcutta, for him, is next only to Delhi. Its where he still sources his instruments from; its where he believes the discerning listener still lives; its also where he took the stage for his first major solo recital at age six at the Sadarang Music Conference in 1958.

 My father used to stay in music director Rai Chand Borals house in Bowbazar and I have lived there too, he recounts, admitting that he would try and emulate his fathers style in his youth.

 Now 62 years old and the father of two musicians trying to make their mark, he feels it necessary to let the boys explore, experiment and chart their own course. 

 I have never allowed Amaan and Ayaan to practise the sarod in the same room. When you do that, you start sounding alike. Often, musicians of the same gharana end up sounding like copies of each other, he says.

 I havent tried to create another Amjad Ali Khan... Both my sons have different approaches to the music they play But one needs to be multi-dimensional, a complete musician. Being a sarod player is only one aspect of it. So when they walk the ramp or do fusion music, I think its good that they are exploring all their facets.

 Ustadji himself has dabbled in the non-conventional, composing music for ballets, ghazals and symphonies. And with good reason. Theres a slight difference between tradition and convention. Tradition allows you to innovate and think. Those who are conventional soon reach a dead-end.

 What about connecting with todays audience? We have to tailor our presentation. I see the audience, I see the ambience and then I decide whether I would play for half-an-hour or more Sense of proportion is vital. You must know where to end. The artiste is to blame if listeners walk away.

 Its been five years since his sons penned Abba: Gods Greatest Gift to Us. Now, Amjad Ali Khan is scripting his own story. Its been such a long journey, he says, offering nalen gurer sandesh from Nakur. I love this sandesh and also ilish machh, he smiles.




</TEXT>
</DOC>