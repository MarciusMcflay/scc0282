<DOC>
<DOCNO>1070704_nation_story_8013768.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Beautiful smile that ended in a suicide

 SUJAN DUTTA 

New Delhi, July 3: A lady officer who won a prize for her beautiful smile at an army ball and who hanged herself near Jammu yesterday defies the textbook logic of what leads soldiers to desperate measures. 

 Captain Megha Razdan of the 113 Engineers hanged herself in her quarters at Ratnuchak while her husband, also a captain, was shopping for a party the couple were planning to host. 

 Defence minister A.K. Antony today reviewed the case because of the exceptional circumstances of Razdans death.

 He took a meeting with the vice-chief of army staff, the defence secretary and the adjutant general. Antony asked for an expeditious probe and also directed ministry officials to send a request to the Jammu and Kashmir government to speed up the police inquiry, defence spokesperson Sitanshu Kar said. 

 There have been 53 soldier-suicides in 2007 alone. Another soldier, rifleman V. Pratap of the 13 Rajputana Rifles, shot himself with his service weapon last evening shortly after speaking to his mother over the telephone. Prataps family lives in Morena, Madhya Pradesh. 

 But it is the case of the lady officers suicide that occupies more mind space in the defence establishment because of the suspicion that the armed forces are gender-insensitive. That was the case with the suicide by Lieutenant Susmita Chakravorty in July last year. 

 Chakravortys death led to a furore and a review of service conditions for women in the armed forces. The study confirmed suspicions that many women were incompatible with the overwhelmingly and aggressively male culture in the forces. But it also threw up startling perceptions such as women often bypassed the chain of command, an anathema in the rigid hierarchy of the military. 

 Chakravorty was frustrated by menial jobs such as carrying drinks or giving bouquets of flowers in official functions and by the fact that she was not trusted with responsible assignments. 

 Army officers who probed the background of Razdan assert that there is little evidence that she was treated poorly in her unit. They even refuse to confirm that it is a case of suicide. They point out that Jammu police have not ruled out murder. 

 Razdan is said to have left a suicide note behind. The note is a computer printout but is reported to have been signed by her. Information received suggests she had complaints against the army.

 Razdan was not staying alone. She was with her husband, Captain Chaitanya B. Wadekar. He was also in the same unit. The two met early last year during a Young Officers course in Pune and married in September. 

 Her parents also lived nearby in Pathankot and she was often in touch with them. This shows that unlike many soldiers, Razdan had pro-active social and familial support, one officer observed.

 Another officer who briefly met Razdan said: I saw her mother telling a television channel that she often complained of the hard life, of the fact that she had to get up early in the morning and do PT (physical training). 

 The records show that Razdan was not an exceptional performer at the Young Officers course. Her husband, a year younger and six months junior to her in service, fared better. But instructors at the course have reported that they appeared to have got along well. 

 Back in her unit, Razdan was given the responsibility of IO Intelligence Officer. The commanding officer of a unit usually gives such an office to an officer he trusts, a major general said. 

 We do not understand why she should be frustrated.

 Moreover, she was also rated high on the social ladder of the military. At the armys traditional Northern Command May Queen Ball in Jammu two months back, Razdan was one of the nine finalists. She didnt get the top prize but won one for Miss Beautiful Smile.




</TEXT>
</DOC>