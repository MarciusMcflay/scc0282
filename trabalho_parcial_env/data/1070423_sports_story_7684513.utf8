<DOC>
<DOCNO>1070423_sports_story_7684513.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Pietersen spoils farewell party

Scyld Berry

 Kevin Pietersen celebrates his century in Bridgetown on Saturday. (Reuters) 

It was the plate final: that sounds better than a play-off for fifth place out of eight. It was the first match of this World Cup in front of a capacity crowd, crossing the line between a cricket match and a social occasion. It was also Duncan Fletchers last assignment in charge of England. And most notably of all, it was Brian Laras last match for West Indies. 

Lara entered to rapturous applause and a guard of honour by the England players, and scored 18 before he was run out by Kevin Pietersen, unabashed at stealing Laras limelight. About 27,000 spectators had turned up to see the most gifted batsman of his generation for one final time, and were treated to one of Laras magical cover-drives, before an era ended all too suddenly. 

Lara walked back towards the Sir Garfield Sobers pavilion. Although the younger man came from Trinidad, the older from Barbados, they have had so much in common. They were both great cricketers, almost divine talents, when they had a bat in their hands. The strokes they played and the aesthetically pleasing poses they assumed were beyond the bounds of mortal batsmanship. The high back-lifts, the majestic balance, the wristy followthroughs: Sobers and Lara will be celebrated as long as the sport is played. 

They have not been, however, great men. Great men span two eras, taking people with them from dearth to plenty. But neither Sobers nor Lara left West Indian cricket better off than when they started. Sobers oversaw West Indies as they went from good to great to bad; Lara from great to bad. They were geniuses, not leaders. Laras record as a Test captain speaks for itself: 10 matches won, 26 lost, and a multitude of quixotic decisions never explained. And it is not as if the Caribbean cupboard has been entirely bare: that much was apparent on Saturday alone from the superlative hitting of Chris Gayle and Marlon Samuels. 

Lara turned round halfway to the pavilion and saluted the crowd: the first capacity crowd of this tournament, turning it from a cricket match into a social occasion. Lara saluted as he had saluted the Antiguan Recreation Ground when he set the world Test record for the first time by making his 375, with bat in one hand and helmet in the other. He walked on further, towards the West Indian players who lined the steps, turned and saluted a second time, and was gone. The king of cricketers of his generation; yet Saturdays result was somehow apt. 

For Fletcher it was not a great day either until Pietersen and Paul Nixon came together. A load seemed to have been removed from his shoulders as he organised the warm-ups for the final time as Englands coach. He smiled several times as he, left-handed, dabbed slip-catches then nothing if not thorough swapped places with Phil Neale, so that Englands slip fielders were given edges off a right-handed batsman as well. 

Michael Vaughan decided to bowl first on winning the toss. He had done right on Tuesday by batting first against South Africa on this same square only England had batted horribly. This time Chris Gayle and Devon Smith tucked into one belter of a pitch and raised 62 off the first eight overs. Anderson began with three wides in his first over, Liam Plunkett with two wides, and England unravelled from there. Perhaps no amount of coaching can stop young pace bowlers spraying nervily under pressure (Shaun Tait did exactly the same here). 

Gayle, dormant all tournament, exploded to hit 79 off 58 balls. On Thursday, when West Indies played Bangladesh, Gayle and Lara had stood side by side in the slips without speaking. On Saturday, Gayle batted as if liberated, another with a load off his shoulders, and pummelled Plunkett for two consecutive straight sixes. Gayle hit the ball so hard that Paul Collingwood, when bowling later, kept his right hand out of the way of a return hit and Collingwood was up for fielding because he took a right-handed catch at point with a salmon leap. 

Flintoff was superb as usual with the ball. Vaughan was next best with his slow off-breaks wobbling in the breeze. Stuart Broad had an interesting first Cup appearance: Ian Bell, Sajid Mahmood and Monty Panesar were replaced by Jamie Dalrymple, Plunkett and Broad. A baby face hides a spikey personality and in his first over, Broad got into Devon Smiths face and received warnings from the umpire and Paul Nixon. 

England were left with a much higher total to chase than any team had successfully done in this World Cup, Vaughan, for once, led the way with his 79, his first competitive fifty for England for 17 months. Glorious strokeplay it was, as Vaughan did not try to hit the ball too hard. When he was run out by Dwayne Bravo, Pietersen was left to carry the show. 

England needed 88 off the last 10 overs, and 47 off five. It was none too serious stuff, but it was a game worth winning. Pietersen went to his fifth one-day hundred off 90 balls, with a mega-pulldrive. 

 THE SUNDAY TELEGRAPH 




</TEXT>
</DOC>