<DOC>
<DOCNO>1070402_business_story_7595658.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Business

 Apex bank gives bourses the jitters

 If there were any doubts that the Reserve Bank of India was dead serious about slowing the economy down, Fridays actions have dispelled them completely. And now that the Congress has pinned the blame for its recent electoral reverses on inflation, the finance minister, unlike his earlier stance, is fully endorsing the RBIs actions.

 After blithely ignoring monetary tightening for long, the stock market has become concerned about the impact of the rate hikes. Thats one reason why the Indian market has been one of the worst performers this year. 

 Interest-rate sensitive sectors such as banks and autos have fared even worse, with the BSE Bankex falling 7.8 per cent and the BSE Auto index down 14.3 per cent since the beginning of the year. Thats compared to a 5.5 per cent drop in the sensex. The Bankex is likely to fall further as the market digests the implication of the CRR hike, the repo rate increase and the lower interest on CRR balances.

 But the big question is is this the last of the tightening measures? The market was in any case expecting a rate hike during the monetary policy statement in April and the RBIs action has caught it unawares. That could indicate that the RBI will not raise the repo rate again in April.

 HSBC economist Manas Paul, however, points out that while the chances of a further hike in April have been reduced, there are enough indications for further monetary tightening ahead, if inflation continues to sustain above tolerable levels.

 But lets turn to the RBI for the reasons for the hike. The central bank has pointed out that growth is continuing at a sizzling pace, inflation is high, money supply continues to grow rapidly and globally the European and Japanese central banks have raised rates.

 According to an analysis by JP Morgan, headline inflation is likely to come down soon, although manufacturing inflation will remain strong. And since the RBI has substantially tightened monetary policy, it may not increase rates in future. But then even JP Morgan didnt expect Fridays hike. 

 The consensus is that inflation is going to come down soon. One reason is the base effect. Last year, after going up from 196.2 in January to 196.7 till 18 March, the WPI suddenly soared to 203.5 within the next three months. Given the higher base, the inflation rate should be coming down. Add the impact of the monetary tightening, the higher wheat crop and the oilseeds imports and we should see a clear fall in headline inflation. Nevertheless, many observers expect the RBI to continue tightening to effectively kill inflationary expectations.

 So far as the equity markets are concerned, data show that inflows into income funds were at a record in February. And with fixed deposit interest rates approaching double digits, some money may be diverted away from stocks to the fixed income segment. At the same time, everybody agrees that growth is likely to remain above 8 per cent the ADB has recently forecast GDP growth at 8 per cent for 2007 and 8.3 per cent for 2008.

 Whether the RBI will pause depends on the data. Investors would do well to keep a lookout for signs of a slowing down in the economy, for signs of lower inflation and for a deceleration in credit and money supply growth. That will signal the RBI is finished with tightening and its time for the markets to shake off the blues.

 EMCEE




</TEXT>
</DOC>