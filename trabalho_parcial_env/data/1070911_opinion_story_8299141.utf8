<DOC>
<DOCNO>1070911_opinion_story_8299141.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 VALUE THE PROPERTY

 MALA FIDE 

 - Malvika Singh

 Indian embassies abroad and Raj Bhavans in India are, more often than not, great residences and valuable real estate. Those who procured government properties in the early days after India became independent had a well-honed sensibility and they understood the imperatives of investing in such buildings. Those days, many stalwarts in the civil service, men and women who operated the government and its complicated machinery, did up these official homes in a style that celebrated our fine cultural and aesthetic traditions. 

 Whether in London, Cairo, Brussels, Paris, Kathmandu, Colombo, Kabul or Cyprus, embassies were at great locations and many were the envy of other nations. They were intended to showcase a new republic, stand at par with the developed world both as an ancient civilization and equally, as a resilient, contemporary society that had given birth to satyagraha, an extraordinary, modern political tool. Our early leaders were proud men and women who understood the complexities of India, admired and respected the cultures and traditions that kept our plural society rooted despite the international turbulence, the wars and revolutions and the coming of the atomic bomb. They did not feel the need to ape the worst of Western culture, a frightening characteristic of today. To initiate a revival of what had been degraded or lost during colonial rule, particularly the textile sector, was tough but it had remained a priority for governments through the first three decades. 

 The rot sets in

 Then, gradually, a dilution of such national priorities began to take root and instead of absorbing new influences that had meaning for us, we discarded all that was indigenous and familiar for a mediocrity that was culturally alien. There was no thoughtful fusion of ideas. This was the attitude of the political administrators who determined our lives, intruding into them at all levels under the rule of the command economy. The socio-cultural fabric began to fray, anarchy raised its ugly head, and insecurity laced with discomfort overwhelmed a fast urbanizing India. Unpleasant, unnecessary contortions brought much unease. This malaise has been aggravated. 

 The landscape of post-independence India has been scarred by the most wretched state-sponsored architecture designed and developed by the PWD, the urban development authorities and suchlike that have pockmarked a subcontinent of great beauty, forever. The concrete overhead water tanks that tower above urban India symbolize the lack of aesthetic sense. Had our governments adopted traditional building techniques and appropriate, time-tested skills, infused them with modern structural amenities like proper drainage, et al, India would have been a veritable paradise of splendid styles of building and sensitive taste. Alas, that was not to be. Courtyards, cooled by the shadows cast by the four high walls, the mandatory high ceilings to beat the suffocating heat, the lime wash on walls to organically disinfect the interiors, and much more, have been abandoned. They have been replaced by sheets of chrome and glass that attract the scorching rays of the sun. Low ceilings constrict the flow of air and there are hardly any open spaces. Plastic emulsion paint, cold, inorganic, and sterile, is slapped onto dwarfed walls. 

 Government offices and institutions are a disaster. Embassies and Raj Bhavans have been vandalized by inappropriate aesthetics and a lack of care. The dirt and filth are inexplicable. The maintenance is non-existent. The grand properties are decaying. Millions of rupees are going down the drain. Only when there is an incumbent who understands or is well-versed in the traditions of this land, and who cares, does restoration happen. The showcase leaps to life and begins to breathe again. Someone needs to stem the rot.




</TEXT>
</DOC>