<DOC>
<DOCNO>1070722_sports_story_8091282.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Spaniards are smiling at Open 

 CHRISTOPHER CLAREY

 Miguel Angel Jimenez 

Carnoustie: The idea that this British Open could turn into a Seve Ballesteros memorial tournament gathered pace on Friday, as the bushy-tailed Sergio Garcia and the pony-tailed Miguel Angel Jimenez continued to attract crowds and avoid big trouble. 

Amid talk about the newly retired Ballesteross enormous legacy, Garcia and Jimenez are the only two Spaniards in the British Open field. But either one could still become the first Spaniard since Ballesteros to win the tournament. 

Garcia has clearly benefited from his recent decision to switch to a belly putter, a tool of the trade that has usually been reserved for bellies softer than his own. Pressure putting has kept Garcia from fulfilling all the ball-striking promise he displayed as a 19-year-old. It held him back in the final round of last years British Open at Royal Liverpool, where he went out in the final group with Tiger Woods and missed clutch putts. 

But in the midst of a disappointing season, he has swallowed his pride and his lifelong habits, reaching for what many professional players consider a crutch. 

Even Colin Montgomerie, the 44-year-old Scot who has made frequent use of long-handled putters in the last six seasons, once said he would support a ban because they provided an unfair advantage. 

Long putters be they anchored to the chin, the chest or belly all give the player the three pivotal points of two hands and the body rather than just the two hands, he wrote in his 2002 autobiography. But golf officials, despite considerable pressure from stars like Ernie Els, have declined to act, as belly putters made a tricky game easier for recreational players and for a few bankable personalities who played for a living, too. Was Garcia ever among those who would have liked to see a ban? 

No, he said, twisting away from his questioner and flashing a grin as his audience chuckled. 

I guess it did cross my mind when there was all that talk and everything, and I was happy with my short putter, he conceded. I dont know. I guess things change and whatever helps the game. I think the better the guys play, the more exciting the game is. If it helps, like I think its helping me, Im not going to change it. 

Garcias affair with the belly putter is still a bit fragile, however. At the 15th green on Friday, Garcia smacked a 30-foot birdie putt well past the hole. It was a mediocre stroke with any putter, and it earned a long glare from Garcia, who still managed to roll in his 5-foot par putt. 

Still shaking his head over the first putt, he flipped the belly putter, put the blade of his new toy in his mouth and pretended to bite it in anger. The belly putter feels miles better under pressure than the short putter has felt, Garcia said. I just misread a couple of putts today. Other than that, it was pretty solid. 

Jimenez was also solid Friday. It takes a bold 43-year-old to stick with a pony tail, particularly with his temples already graying. But Jimenez relishes playing the bon vivant. He is nicknamed the mechanic because of his attraction to fast cars, and he likes a midround cigar, too. 

He is also an excellent golfer, and his best shot Friday was a 2-iron off the 16th tee that nearly turned into a hole in one. Instead, he had to settle for converting a short birdie putt and for a round of 70 that put him three shots behind Garcia. 

 Garcias talent was identified early, but the 

 majors have eluded him. Asked if he got bored or frustrated 

 by the question of when he might win his first, Garcia answered 

 swiftly: Never. The first time I heard it. 

 NEW YORK TIMES NEWS SERVICE 




</TEXT>
</DOC>