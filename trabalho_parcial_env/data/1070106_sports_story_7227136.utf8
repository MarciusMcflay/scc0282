<DOC>
<DOCNO>1070106_sports_story_7227136.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Life hasnt changed, Im still a normal guy

 - It was difficult to choose between soccer and tennis: Nadal 

OUR CORRESPONDENT

Chennai: For someone who has achieved so much so early in life, Rafael Nadal is remarkably humble and down to earth. Dizzying achievements like back-to-back French Open titles, breaking every conceivable claycourt record, a Wimbledon final appearance, repeated conquests of the impregnable Roger Federer, amassing over $8 million in prize money besides millions more in endorsements and appearance fees havent gone to his head. 

Rafa, who hails from Manacor, the second-biggest city in the island of Mallorca, still maintains strong family ties, looks to spend time with friends on Tour and off it, loves watching soccer like any sports-minded Spaniard, enjoys the beach and movies, goes fishing. As normal as any 20-year-old. 

And yes, he also wants to put on his best behaviour when meeting hordes of strangers in different countries tennis takes him to. In Chennai, he couldnt satisfy everyones request for a one-on-one interview (the list was too long), but he did speak to a select group of journalists over pasta on Thursday night. The Telegraph was there to throw some questions, which ranged from Roger Federer to fashion and personal life. He answered patiently, turning to the ATP communications manager if he didnt understand a query. 

The following are excerpts 

Q You beat Federer in the French Open final and lost in the Wimbledon final. But would you agree that you played better at Wimbledon? 

A They were different types of matches. I had to change my game on grass. I would say my two best matches with him were the Rome Masters final which I won and Shanghai Masters Cup semis which I lost. 

No one has beaten Federer as many times as you have since he became No. 1 in 2004. Whats the secret of your success? 

I dont know really. Theres no secret. I just try my best on the court, just as I do against all opponents. 

Who would you rate higher, Federer or Pete Sampras? 

(Pausing awhile) For me, Roger is better. I think he is a more complete player than Sampras. Sampras may have more Slams, but look at other things Roger has achieved. He has over 8000 ranking points, while Sampras best was 5000-something. 

Which of the French Open triumphs was more memorable? 

The second one (in 2006) was very special because I was coming back after a lay-off... I had missed the first six weeks of the year because of a leg injury. Of course, I wont be able to forget my first Grand Slam also (2005). 

Is winning Wimbledon a big goal for you? 

I love playing at Wimbledon, it is a great tournament. I always try to do well there because I want to be a very good player. Yes, I prepare well on grass every year before the Wimbledon championship. Like all tennis players, I also want to win there. 

Which areas do you think your game needs improvement? 

Im just 20... Im still learning. I have to work on my winners, my volleys, the serve can get better. You can always improve. 

You come from a soccer-crazy country, your uncle played World Cup for Spain. Werent you tempted to take up that game? 

I played soccer till I was 13, I was very good at it. It was very difficult to choose between soccer and tennis when a decision had to be made. 

How has life changed after you became a rich and famous tennis player? 

Life hasnt changed for me... I live with my family whenever I get the chance, I have the same friends, I still love watching soccer and going to movies. I get recognised more these days, so I try to be nice when I meet new people in different countries, just like any normal guy would do. 

What are your other interests outside the tennis court? 

I like playing golf and going fishing. 

Is it easy to make friends on the Tour? 

I am friendly with all the players, but am closer to the Spanish guys. Carlos (Moya) is my best friend on Tour. He is 10 years older than me, but he helped me a lot when I first came to the Tour. Our friendship developed from those days. 

You have big arms, have you done anything special to develop them? 

(Smiles) My arms may be more defined, specially the biceps, but they are not the biggest. 

You are considered to be a sex symbol. Are you comfortable with that tag? 

(Rather dismissively) I dont think about those things. 

You are very fashion conscious... 

No, no nothing like that. I just like to dress well. 

What advice would you give to the budding Nadals? 

(Smiles sheepishly) Me? (After a pause) Just enjoy the game and the competition. 




</TEXT>
</DOC>