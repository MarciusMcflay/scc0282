<DOC>
<DOCNO>1070729_calcutta_story_8117300.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Among friends

 Mohua Das profiles a Bow Barracks resident

 Christopher Biswas in his room. Picture by Amit Datta 

 The grey and musty corridors within the red brick-walled houses dont draw much attention, but Christophers room in Block 20 on Bow Street with its neat and colourful interiors springs a pleasant surprise. Dressed nattily in hipsters and a fitting top, his coloured hair tied back in a ponytail and a hint of kajal in his eyes, Christopher Biswas, a.k.a. Chris, seems a little shy as he offers a warm welcome into his one-room flat. Now 30, Chris has lived here since childhood. 

 Chris in many ways seems to embody the spirit of Bow Barracks the subject of Anjan Dutts latest film, Bow Barracks Forever. The rows of houses in the heart of the city look somehow unreal in their generosity of spirit (everyone is welcome to the Christmas celebrations, which are robust); charm (its home to Goan families who make wine in many flavours) and music (Ella Fitzgerald, Nat King Cole, as well as FM). And truly cosmopolitan, for it is home to Anglo-Indians, Goans, Biharis, Bengalis, Christians, Muslims, Hindus, spread over seven blocks.

 And no one raises an eyebrow at Chris. He loves to put on make-up and dress up. He has a boyfriend. But he is not made to feel different. 

 Ive been like this since I was 12 or 13 years of age. No one has ever made me feel uncomfortable and I never want to move out of here, says Chris. The bustle of Chittaranjan Avenue does not dim the voices from the broad thoroughfare behind Bowbazar police station. The place was built as an army mess. Later Anglo-Indians from China Town were re-housed here nearly 90 years ago. The buildings are dilapidated and have been declared unsafe but are teeming with life. 

 The young play hockey or soccer while the older ones take a stroll in Nalanda Park, on the southern periphery. Chris prefers staying at home and watching television when he is not helping neighbours with their cooking or other chores. I left school when I was 12 and started working as a help in shops and peoples homes in the neighbourhood. But I just love cooking, so I joined a catering service as steward and cook, says Chris, who takes pride in dishing out Anglo-Indian delicacies like vindaloo, pork bhuni, sausage curry and tahari (a preparation of meat and rice). 

 About 130 families live here. As with many of the younger residents, getting a job has not been easy for Chris, unlike the older generation whose fluency in English fetched them jobs as secretaries or in teaching, railways and government services. Im not working now, so I help out my cousin upstairs with household work, says Chris, who loves to go out dancing at discos. I love to put on my grey lenses, some light makeup and wear black, brown and mauve!

 During Christmas, Chris is worth looking out for. He looks so different and nice when hes all dressed up, smiles Sandra Gomes, whom he calls auntie. 

 Chris is with the Bow Barracks residents who are ready to fight promoters eyeing the buildings. I will fight for Bow Barracks. They cant pull this place down, says Chris.

 Till then, he is comfortably tucked within his tenement, tidy and done up, the place that he calls my home. 




</TEXT>
</DOC>