<DOC>
<DOCNO>1070709_sports_story_8035004.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 THE KING KEEPS CROWN

 - World No.1 survives ferocious fightback from nadal to win in five sets 

 MARK HODGKINSON 

Seated in the front row of the Royal Box was Bjorn Borg, so famously an emotional flat-liner. And so when Roger Federer put away a smash to win his fifth Wimbledon title in a row, equalling the Swedes record, the Ice Man predictably allowed himself just a small smile. Federer, though, dropped on his back on the Centre Court grass and began to blub. 

And the Swiss, a five-set winner over the brilliant Rafael Nadal after a superb final of controlled violence, simply had no hold over his emotions and his tear ducts. 

He cried as he worked himself to his feet, wept as he hugged the Spaniard at the net, and wiped away tears after sitting down on his chair, and then carried on sobbing. After what has seemed like the wettest Wimbledon in living memory, Federer gave the All England Club lawns yet another watering. 

Surely even Borg felt a little emotional seeing Federer in that state? But, no. The Ice Borg declined to melt. 

Federer had cried after winning his first Wimbledon title, in 2003, but had then controlled himself the three summers after that. The wet-eyed reaction from Federer did not come from just the joy at winning a fifth Wimbledon, it was also that he had done it in front of the man he had described as a living legend, and had fended off the challenge of an opponent who, even when deep into the fifth set, refusing to wave a cheery Adios. 

Twice in the fifth set, at 1-1 and then at 2-2, Federer found himself at 15-40 down on his serve, and it seemed as though one man would be emulating Borg, but it would not be the Swiss. 

Nadal, also last years runner-up, had been attempting to become the first man since Borg to win the French Open and Wimbledon in the same season. But Federer staved off the danger, and then upped his level in the Centre Court sunshine for a 7-6, 4-6, 7-6, 2-6, 6-2 victory. 

After matching Borgs Wimbledon Big Five, which were achieved from 1976-80, when Federer was still not born, the talk will now shift to whether Federer can win six Wimbledon titles in a row next summer, and whether he can pass American Pete Samprass record of seven Wimbledon titles overall during his career. 

The 25-year-old also moved to 11 Grand Slam titles. That put him level with Borg and Rod Laver on the list of most Grand Slams won, in equal third place, with Australian Roy Emerson on 12 and Sampras on 14. 

Nadals tennis was outrageously good, including, during the second set, sitting on his backside on his baseline when he struck a backhand pass beyond Federer at the net. So he was inventing shots out there on Centre Court. This from a man who, although a triple French Open champion, was still meant to be relatively inexperienced on the lawns. 

Meanwhile, Jamie Murray became the first British winner at Wimbledon in 20 years when he and Jelena Jankovic defeated Jonas Bjorkman and Alicia Molik 6-4, 3-6, 6-1 in the mixed doubles final. Second seeds Cara Black and Liezel Huber won their second womens doubles title in three years defeating Katarina Srebotnik and Ai Sugiyama 3-6, 6-3, 6-2. In the mens doubles, French pair Arnaud Clement and Michael Llodra upset champions Bob and Mike Bryan of the US 6-7, 6-3, 6-4, 6-4 in the final. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>