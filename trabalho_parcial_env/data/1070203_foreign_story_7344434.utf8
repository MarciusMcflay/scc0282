<DOC>
<DOCNO>1070203_foreign_story_7344434.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 Norahs many facets

 Norah Jones in New York. (AP)

 New York, Feb. 2 (AP): Its not as if Norah Jones had never written a song before.

 On her blockbuster debut, Come Away With Me, the alluring singer penned three tunes, two of which she wrote all on her own. But it was her interpretation of others music that really drew listeners her breakthrough Grammy-winning song, Dont Know Why, was written by pal Jesse Harris.

 Harris returns on Joness new CD, Not Too Late but as a guitar player. There was no need for another lyricist: Jones wrote or co-wrote each track on the 13-song CD, and has become a seasoned songwriter in her own right. 

 If you asked me (to describe her) four years ago, I would have said, Great singer-pianist, Harris said recently. Now you cant really say that shes just that. Shes a lot of things now. Theres another element that has come in theres different sides to her now.

 Jones reveals her many facets on Not Too Late, her most adventurous and arguably finest CD to date. Though she still croons the kind of slow, melodic tunes that turned her into a surprise multiplatinum sensation, the issues behind the songs have become more complex, and in some instances, biting and political.

 The albums first track, Wish I Could, invokes a soldier killed in war; the second assails the captain of a rudderless ship, with allusions to todays commander in chief. While that song is a bit subdued, My Dear Country is defiant and obvious, as she warbles about the past election day: Who knows maybe its all a dream, who knows if Ill wake up and scream.

 Jones, who turns 28 in March, says the increasingly troublesome political climate and her own maturation have made her more aware of the world around her and willing to sing about it. The most obviously political song on this album kind of sums it up for me. I really try to see both sides of things, and in the end, there are things I see very clearly. ... Right now for me its just hard to not question whats going on, she says. 

 However, Not Too Late, is hardly Living With War, Neil Youngs blistering anti-war musical tirade of last year. Though the politically tinged songs are the most buzzworthy, they do not define the album. Instead, the melancholy disc touches on various aspects of lament, from the status of our world to the troubles of a relationship.




</TEXT>
</DOC>