<DOC>
<DOCNO>1070923_opinion_story_8345036.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 HISTORY OF A SMILE

 There is something peculiarly inhuman about a face that can go on smiling without ever getting tired. One thinks of inscrutable geishas and hardhearted nurses. But there is also the smiley face that universal icon of (American) goodwill, and mother of all emoticons. Have a nice day! it seems to say to the world. But look at it for a while, and it begins to turn into something else. The niceness begins to drain away, leaving a beady-eyed blandness that quickly turns sinister. On September 19, the smiley completed 25 years in cyber-space. At 11:44 on that day in 1982, Scott Fahlman sent a message to the Carnegie Mellon University bulletin board proposing that a colon followed by a hyphen and a parenthesis, forming a sideways smiley face, be used as a joke marker after every message that is not to be taken seriously. He also proposed that a grumpy face like :-( should be used to mark things that are NOT jokes. Mr Fahlman thus claims to be the inventor of the worlds first two emoticons. His suggestion caught on extraordinarily fast and started a whole computer-based language of facially expressed feelings. The elaboration of this vocabulary is committed to enhancing as well as clarifying everyday communication in cyberspace. One can now smile a whole range of smiles on the computer: open-mouthed, tongue-in-cheek, confused, and blank. 

 But the prehistory of the cyber-smiley is even more interesting. In 1963, Harvey Ball, a Massachusetts graphic artist, was hired by an insurance company to design a logo to accompany its friendship campaign for cheering up disgruntled emplo- yees. Ball designed a bright yellow smiley face for company button pins. By the end of the Sixties, that stylized representation of the smiling human face had become a cultural icon. To be found on everything, from number plates and postage stamps to Wal-Mart uniforms and promos, the smiley came to stand for a whole range of benign possibilities, from universal peace to brain-dead optimism.

 The history of the smiley thus brings together two essentially American structures of feeling that have come to be regarded as natural emotions in the contemporary world earnestness and feel-good. These may look like contradictory tendencies, but the smiley proves that they are not so at all. Something alarming and sad is happening to human communication (and intelligence) when people need to be told, as part of common politeness, that what they are reading is a joke. The smiley was invented, in the Sixties, to spread corporate good cheer at a time of war and social upheaval. It was re-invented in the Eighties to prevent people from taking offence. But whats in a smile? Two dots and a curved line. Think of Mona Lisa and Julia Roberts. And then, think of Tony Blair.




</TEXT>
</DOC>