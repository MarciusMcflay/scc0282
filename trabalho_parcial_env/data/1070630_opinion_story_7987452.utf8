<DOC>
<DOCNO>1070630_opinion_story_7987452.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 WORDS AND DEEDS

 - Indian gentlemanliness versus Chinese hectoring

 Sunanda K. Datta-Ray

 Mukherjee and Lee 

 Its not often that Lee Kuan Yew, who has known every major Indian politician since Jawaharlal Nehru, admits to being impressed by a visitor from New Delhi. But after their meeting last week, he called Pranab Mukherjee a thinking man and a very able fellow, adding, Im impressed with his intellect. The caveat was entered by a European ambassador who asked Mukherjee a question about Indias neighbours and wondered afterwards if such a nice, quiet, polite gentleman could tackle Chinas hectoring.

 Neither view may fit Mukherjees image at home. That is a reminder that no one, least of all the most experienced of Manmohan Singhs colleagues, is one-dimensional. Everything that is whispered in Calcutta may be true. But those allegations need not detract from his great erudition and formidable intellect, or the pleasant personality that struck the European diplomat. Mukherjee may well be able to cope with China. 

 Lee would not have been similarly impressed by the Bengali minister whose six-page CV, sent before he arrived, boasted of the closures, gheraos, strikes and bandhs he had organized. In contrast, Mukherjees advance publicity stressed everything Singapore wants to hear on Indias policy on trade, investment, security, air links and relations with China, Myanmar and the United States of America. Prospects for the visit improved even more when just before setting out, he, in effect, reversed the Reserve Banks decision to lump together two officially-owned Singaporean enterprises Temasek Holdings and the Government of Singapore Investment Corporation under one banner.

 The RBI ruling may eventually have been struck down since the Comprehensive Economic Cooperation Agreement signed in 2005 gives Singapore investors, as well as third-party investors via Singapore, national treatment rights. It also treats Temasek and GIC as separate entities for the purpose of investing in India. Each is, therefore, entitled to a foreign investors full quota. But the RBI decision created a flurry in Singapore, and Mukherjee got the credit for setting things right.

 Two factors highlight the importance of this achievement. First, a country that equates diplomacy with investment and politics with economics sees India mainly as a land of opportunity. The GIC and Temasek, with a combined portfolio of about $200 billion, are seeking nearly 40 per cent of ICICIs offered equity. Second, the deep involvement of the highest in the land in both corporations makes them especially significant.

 Lee himself is GIC chairman. The two vice-chairmen are his son, Lee Hsien Loong, currently prime minister, and Tony Tan, a former defence minister and deputy prime minister, who now chairs the company that publishes The Straits Times and a host of papers and magazines. Set up in 1981 to handle Singapores huge provident fund deposits, the GICs portfolio of well above $100 billion makes it one of the worlds largest fund-managing companies.

 Wholly owned by the finance ministry, Temasek has a smaller $80 billion portfolio. But Fortune and Time magazines rank Ho Ching, the prime ministers bouncy cheerful wife, who heads Temasek, as one of the worlds most powerful women. Her greatest influence may be within the family circle: believing that Lee has great respect for his daughter-in-laws judgment, some attribute his buoyancy about India to her business sense. Lee justifies Temaseks Indian buying spree as evidence of faith in sustaining and building on the 9 per cent growth for three successive years that Mukherjee mentioned.

 Similarly, he saw Temaseks $1.88 billion purchase of Shin Corp from Thaksin Shinawatras family, without the sellers having to pay a penny in tax, as a mark of confidence in Thailands economy. That the deal may have given Thailands military and royalists the excuse they were looking for to oust Thaksin is another matter.

 A frequent if unobtrusive visitor to India, Ho Ching sat in the front row during Mukherjees speech. George Yeo, the foreign minister and Indias other best friend in Singapore, was on the dais. Yeo need not have come at all to the meeting organized by the Institute of South Asian Studies and the Rajaratnam School of International Studies. He volunteered to be chairman as a mark of his involvement with India and regard for Mukherjee. Often, the organizers have to plead with junior ministers to put in an appearance when there is a high-ranking Indian visitor. But sign of the times, Sitaram Yechury is an honoured state guest this week, prompting Gopinath Pillai, ISAS chairman, to recall that at one time Singapore incarcerated even Communist sympathizers.

 Mukherjees speech itself may not have been electrifying, but his finesse in handling questions conveyed the impression of a mature elder statesman from a country that is certain of its future. There was no boasting about world power. Neither did he waffle with philosophical abstractions. Best of all, he made no attempt to gloss over Indias 40 per cent illiteracy, low life expectancy and poor infrastructure. The modesty with which he acknowledged a reminder from the floor that his tenure as finance minister saw India break out of the Hindu rate of growth to achieve 5.6 per cent growth drew applause. 

 But foreign policy cannot be viewed only through an economic telescope. Islamic terrorism is not an economic phenomenon. Neither is Chinas insidious one-upmanship. When Mukherjee told a questioner that Hu Jintao had said there is space enough for India and China to grow together, apparently stressing the together, I could not but recall Singapores first foreign minister, Sinnathamby Rajaratnam an astute, London-trained, Jaffna-Tamil lawyer distinguishing between a foreign policy of words and the foreign policy of deeds. While the former is always infused with lofty morality, the foreign policy of deeds is a more reliable guide to the intentions of a country than its declared and invariably unexceptionable principles.

 There is every reason to take pride with Mukherjee in the fact that even in 1962, India adhered to and pushed, as in 1949, for the Peoples Republic of China to replace Taiwan in the Security Council. That was truly a noble synchronization of the foreign policies of words and deeds. But the question people will ask, as they do of Jawaharlal Nehrus voluntary surrender in 1954 of rest houses, military escorts and Tibets postal, telegraph and telephone services, is what did such magnanimity achieve? Listeners were baffled by Mukherjees contention that Indias consistent advocacy for Chinas Security Council seat has no bearing on Chinas present refusal to support Indias claim.

 With scant respect for the sanctity of words, China was nibbling away at the border and laying the Aksai Chin road within months of promising mutual respect for each others territorial integrity and sovereignty, mutual non-aggression and, most famously, peaceful coexistence in the 1954 Panchsheel agreement. Article VII of the Sino-Indian agreement of April 2005 on not advancing territorial claims to regions with settled populations has not prevented robust reiteration of the demand for Arunachal Pradesh with its 1.09 million people. Foot-dragging over the agreement on river management, signed during Hus visit last November, fuels concern about attempts to tamper with the Sutlej and Brahmaputra flows.

 Rajaratnams old boss, Lee, once contrasted the Chinese, very intense types, hard-driving, hard-striving, with Indian sadhus, holy men, people who abjure the world, who go around giving land away or begging from the rich to give to the poor. Far from admiring Gandhi saintliness, China adores the kind of hero who forms a robber band and kills off wealthy people. In China, said Lee, you dont go begging from the wealthy to give to the poor. You just kill the wealthy and take from them. 

 Perhaps I am being simplistic. Perhaps the European ambassadors nice, quiet, polite gentleman demonstrated a mastery of the sophistry of Rajaratnams two foreign policies to counter the guile and ruthlessness Lee implies. Speech, as a French statesman said, was given to man to conceal his thoughts. And Mukherjee might well be the Talleyrand of our times.




</TEXT>
</DOC>