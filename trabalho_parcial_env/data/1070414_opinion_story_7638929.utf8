<DOC>
<DOCNO>1070414_opinion_story_7638929.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 IN TWILIGHT ZONE

 Rita Datta

 Visual Arts

 Paresh Maitys Togetherness 

 Agony and ecstacy? Well yes, that is what Aakritis current show is called. For, arent artists over-sensitive beings, torn by such emotional polarities, soaring to peaks and sinking to the pits? But when creativity becomes a career and there is a market monster demanding indulgence, can the Arts escape the curse of the hollow age? And so, once artists discover an art that works, there is a reluctance, even a dread, to break out of the familiar.

 Not surprising, therefore, that you dont often see art that touches the deepest chords. But you do see fine craftsmanship, as you do in this show on till April 17 of 13 established artists.

 The low-profile Partha Pratim Deb, the oldest in the group, is the most stimulating. His spirit of adventure an impish, probing spirit, you could say drives him to zestful experimentation, whereby he continually re-invents himself. This sport with form is not so much about toning the style as about probing the semantics of the language of arts. What Deb shows here are black and white drawings that have both a gaunt strength and the amused detachment of the observer. These evoke sculpturesque planes by breaking down the figure into sturdy lines and a mosaic-like topography.

 With Tapas Konar, you come to figures that are spry, unformed, diaphanous, set adrift in space a la Chagall. But the space is anonymous, just a cloudy ground colour, usually beige. And though the figures may be rapt in some act that summons references, they are like random excerpts rather than a montage rooted in the dynamics of logic. Even when they overlap or trespass into each others territories, there may not be interpersonal communication. And hence, interestingly enough, narrative coherence is subverted.

 But the narrative dimension is vital to three of the artists as they harness their assured draughtsmanship to depict the photographic naturalism of, what is being called, the Bikash Bhattacharjee school. Prasenjit Senguptas acrylics are cinematic in composition and throw up a comment on contemporary urban life. However, the artists empathy for the subject turns into a sigh. Kanchan Dasguptas insipid Rajasthan series disappoints. And Monoj Mitras mixed media, She, is something of a calendar image.

 With Sekhar Roy, you intrude into a twilight zone of spectral beings that are frozen or floating, trapped in a kind of limbo. It is the craft that allows him to lend palpable dimension to body parts and then dissipate the illusion: the sheer skin webbed with cellular patterns like arid fields at places or lined with limp wefts and woofs like threadbare weave may be distended like a balloon, but it is made bereft of substance. The metaphor of threadbare weaves suggests the worn and weary and could be an effective strategy were it not for the artists concessions to unctuous, rather than unsettling, images.

 It is primarily the dark and turbulent paint that you note in Aditya Basak the brooding red of embers smouldering in a ground whipped by congealed tentacles of black in Animal I and II; vaporous blues mottling the surface in blotches and blisters in Sky I and II; the feverish cross-hatching and dashes and sutures scarring the luminous figures in the foreground that seem partly of wood and partly of bone. No, the technique isnt wanting; its only that the works get overheated. 

 Chhatrapati Datta, who has been a restless artist trying out both figuration and abstraction, reacts to Singur and Nandigram. Dehumanizing the human figure into a flat road map is an acerbic correlative in Brand Wagon is Catching On. But, ultimately, the emotions seem rather too ready, especially in Kaar Land, and the imagery too literal.

 Subtle water-colourist, Paresh Maity shows two oils for a change, both of them playing on a palette of resplendent reds. The artists figurative terms are pared down to terse outlines, especially in Nayika I, where the human profile merges into a landscape of flat and textured colour divisions.

 If Asit Mondals works, combining calligraphic strokes with elegant stylization, have a refreshing appeal, Ashok Mullick appears somewhat spent, while Sheikh Shahjahan, the youngest in the show, ends up with involved symbolism.

 But the radiant simplicity of child art is what Monoj Dutta strives to achieve. Shedding all decorative excess he has done drawings of inspired artlessness. The lines may remind you of someone senior, but the latters squint of satire is replaced by a disarming naivet. And that is, of course, a matter of remaining young at heart. 




</TEXT>
</DOC>