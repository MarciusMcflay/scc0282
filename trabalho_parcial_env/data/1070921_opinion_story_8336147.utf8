<DOC>
<DOCNO>1070921_opinion_story_8336147.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 Paperback Pickings

 Small like jailbirds into cages

Napoleon: The Path to Power, 1769-1799 (Bloomsbury, Rs 795) by Philip Dwyer is a critical biography in the true sense of the term. It talks, among other things, about how the French general was instinctively able to exploit [the nations] need for a hero. He had a number of people who helped him promote and dramatize his own role in the wars. Dwyer documents how popular lore constructed Napoleon as a conqueror greater than Hannibal or Scipio. On the other hand, he also quotes General Auguste Petiet on the coup dtat of the 18th Brumaire in the Orangerie of Saint-Cloud, where Petiet writes that things were the opposite of the official accounts published by the consuls. The truth about Napoleon, Dwyer points out, lies somewhere in between. The most interesting aspect of this volume is the way Dwyer pits Napoleons attempts at rewriting the history of events against other available versions many of which were themselves a distorted rendering of facts. One of the best examples of this involves the battle to gain control of Arcola, of which Napoleon, and the generals, Pierre-Franois Augereau and Louis-Alexandre Berthier, presented radically divergent accounts. The book throws up many other fascinating sidelights of Napoleons career.

 The Terrorist At My Table (Penguin, Rs 200) by Imtiaz Dharker is a collection of poems born in times ruled by terror and instability. The poems are sharp, stark, and they hurt. The silence of the graveyard drowns out the cacophony of the modern world in Text. 9/11 is the event that looms large over many of the poems. Firm begins with Here I am, standing at a window on the thirtieth floor,/ looking down/...No gusts of wind can shake it./ My feet are firmly planted here./ Secure. And it ends with I am on the thirtieth floor,/ fifteen windows/ from the left. Can you see me/ from up there, from/ that plane? The lines dividing the private from the personal, the local from the global, the real from the imagined, the terrorist from the freedom fighter from the child, are all irretrievably blurred. Future seems too distant and horrifying to contemplate: But today/ Today let me just/ live through this rickshaw ride (Tomorrow). Dharkers poetry will stand the test of time.

 Amber Dusk (Indialog, Rs 250) by Rajat Chaudhuri is subtitled a psychedelic collage of Myth and Memory. Calcutta is the city of myth and memory, a gigantic cauldron where Valence and Lopamudra, Pedro and Bishwa, Bengali and French, love and bombs, death and perfumes, journalism and surrealism cohabit and together produce a heady mix of experiences. Description is Chaudhuris strength, evidently: Only age with its summary rights had loosened the skin from the skull, and it hung in sleepy folds down beside the cheeks, below the eyes and rested with nonchalance over the grey eyebrows that disappeared long before the corner of the eyes...

A Very Strange Man (Women Unlimited, Rs 250) by Ismat Chughtai is not-so-loosely based on the tragic triangular love story involving Guru Dutt, Waheeda Rahman and Geeta Dutt. The three become Dharam Dev, Zarina Jamal and Mangala, but many of the characters Meena Kumari, playback singers Mohammed Rafi and Lata Mangeshkar and the composer Burman dada appear in their true identities. Even in translation done by Tahira Naqvi the novel is enjoyable, though some might find the lives of a self-destructive actor, his obsessive wife and his cold lover eminently depressing. One wonders why Bollywood has not zeroed in on the novel yet.




</TEXT>
</DOC>