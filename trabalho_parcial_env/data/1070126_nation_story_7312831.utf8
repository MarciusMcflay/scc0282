<DOC>
<DOCNO>1070126_nation_story_7312831.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Democracy gift hits hurdles

 - Design delay for Parliament House

 SUJAN DUTTA

 The Darulaman palace. A Telegraph picture

 Kabul, Jan. 25: A magnificent desolation in snow today marks the spot where India is to gift Afghanistan a fitting symbol of its democracy: its Parliament House. 

 External affairs minister Pranab Mukherjee the No. 2 in the present cabinet took a tour of Darulaman, 11 km south-west of Kabul, yesterday, two days before Jumhuriyat-e-Hindustan showcases its republic for the world to watch. 

 Manmohan Singhs Jumhuriyat-e-Hindustan Republic of India all of 57 years old today, is to build Afghanistans Parliament on a modern battlefield. 

 With a little help from friends, Karzais Afghanistan is itself fashioning a republic at gunpoint. The grounds of Darulaman witnessed some of the bitterest fighting in Afghanistan. It is now home to an Afghan National Army garrison. 

 Many Indians still feel insecure visiting the spot. But that is not all. Afghanistans democracy project and Indias effort to export democracy is mired in minor and major hassles despite best intentions. 

 New Delhi is struggling to get the building off the drawing boards while Kabuls fledgling democracy dithers over a house for itself. The Indian governments Central Public Works Department (CPWD) has finally worked out a budget for the project. 

 At Rs 337 crore, the Afghan Parliament 

 House will probably be the costliest single giveaway and 

 diplomatic investment in democracy that the Jumhuriyat-e-Hindustan 

 will make in this country. 

 Before Mukherjee, in August 2005, when Prime Minister Manmohan Singh visited the grounds of Darulaman palace, he called it the venue where the worlds largest democracy will meet the worlds newest democracy. 

 Even today, the Darulaman palace built by King Amanullah Khan in the 1920s manages to hold itself together, its Corinthian columns teetering dangerously, its walls pockmarked with bullets and shells, its roof blasted and its windows shattered. 

 More than a year and half after Singhs visit, Darulamans grandiose and ghostly structure supervises the plains, now covered in layers of freshly fallen snow. For the moment, that is all there is over here because Afghanistans Parliament House is still being conceived, its architects in New Delhi and Kabul have yet to agree on what it should represent. 

 The authorities in both New Delhi and Kabul refuse to show the plans. When the CPWDs chief architect was handed the project in 2005, the Indians at first conceptualised a structure based on Gandhara art. In history, the Gandharas settled on the banks of the Kabul river. 

 Buddhism, with its message of tolerance, was disseminated by them through their artisanship. 

 But this did not go down very well with Kabul when it was suggested. The subtle message from the Karzai government to the planners was that the building should represent more of Islam. The Parliament would, it was pointed out, house the members of an august body of the Islamic Republic of Afghanistan. 

 Sultan Ahmad Baheen, spokesman for Afghanistans ministry of external affairs, told The Telegraph: The designs for the building are being discussed and we are very close to approving them. 

 A CPWD source in New Delhi said: We are planning a structure that will represent much of Afghanistans heritage from the outside and would have all modern amenities inside. 

 The latest design doing the rounds shows a structure with three domes, the central dome larger than the other two on an elevated platform that will have a basement. The building is to accommodate both the Wolesi Jirga (Lower House) with 249 members and the Meshrano Jirga (Upper House) with 102 members, offices for the Speaker, Deputy Speaker and ministers, a dining hall and canteen, a mosque/prayer room and a library. 

 The final plan of the building is likely to adopt some characteristics of Humayuns Tomb in Delhi.

 Pranab Mukherjee has carried back to Delhi a message from the Speaker of the Wolesi Jirga, Yunus Qanooni, for the Speaker of the Lok Sabha Somnath Chatterjee that India and Afghanistan should float a joint forum of their Parliamentarians. 

 Apart from the design, the other problem dogging the CPWD is that of builders. The government outfit has decided not to engage consultants in the design and the architecture. But it has begun posting officials in Kabul to supervise the construction of the Afghan National Assembly and the Rs 42-crore Indian Chancery. 

 The threat from the Taliban and the acknowledgement by both the Indian and Afghan governments that there has been a resurgence of the fundamentalist outfit worry Indian companies. 

 An Indian company Shapoorji Pallonji rebuilt the Kabul Hotel in Kabul (now called the Serena) for a hefty Rs 350 crore. 

 But even that kind of money is apparently making Indian private builders of repute rethink ventures in Afghanistan after the kidnapping and killing of three Indians over the last year. 

 The Indian governments engineers expect to start construction in the spring. The water is freezing now and we cant build in this weather, is an official plea. That is about the time that Nato and Afghan officials are anticipating an offensive from the Taliban. 

 Summer 2006 was the most violent in Kabul since the 2002 war. The day Mukherjee landed in Kabul a suicide bomber killed 10 people in Khost province. The winter, too, is seeing blood in the snow.




</TEXT>
</DOC>