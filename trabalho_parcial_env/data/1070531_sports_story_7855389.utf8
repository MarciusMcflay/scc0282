<DOC>
<DOCNO>1070531_sports_story_7855389.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Mongolian princess holds court in Paris

Robert Philip

 Jelena Jankovic 

A new day dawned in the Bois de Boulogne and we finally had some fun; the sun managed to poke an occasional hole through our dark, rain-laden heavens albeit fleetingly and unconvincingly umbrellas were lowered, balls changed, rallies exchanged and the smile restored to the face of tennis. 

Whatever the weather, however, the delightful Jelena Jankovic spreads sunshine wherever she roams. In a sport heavily populated by pampered, preening, posturing prats, JJ is sensitive to the reality that few of her fellow-Serbians will ever experience. As the fifth-ranked woman in the world and winner of the Italian Open which took her career earnings to more than $2 million shes lucky. She knows it and really wants to show it. 

Now 22, Jankovic was 12 when she escaped the impending war which ravaged her homeland to pitch up at Nick Bollettieris tennis boot-camp in Bradenton, Florida, where she watched with awful fascination the nightly CNN reports of bombs dropping among her friends and family in Belgrade. It was a very difficult time for me, and I want to forget about it, she recalls with tangible melancholy. 

A straight-set winner over Stephanie Foretz of France in the first round, Jankovic is that rare breed of modern tennis player who relishes the prospect of a two-week sojourn in Paris and can be relied upon to spend as much time doing the tourist beat as she will perfecting her drills on the practice court. 

The world is fascinating; we travel it all the time, visiting all the great cities, all of which have a story to tell. When I look at many of the other girls, I see what opportunities they are wasting. 

They dont seem to want to learn about the place they are staying or the culture. They are happy to fill their minds with tennis. I am different from the rest in so many ways. 

Not least in facial features that evoke images of a Mongolian princess and which Jankovic un-selfconsciously describes as unique. While the sport in Britain is spearheaded by an injured Andy Murray, an ageing Tim Henman and the worlds 430th-ranked woman, Elena Baltacha, Jankovic is a member of what is called the Serbian Goodwill Tennis Army. 

Close friend Ana Ivanovic, 19, lies two places below her at No. 7, while Novak Djokovic is five rungs ahead of Murray at No. 6 in mens rankings. As a nation we should bow our collective head in shame, therefore, when Jankovic describes the facilities in her native Serbia. 

Financially we have had some problems, so our training facilities are poor, she said. We dont have a national tennis centre, for instance, and we dont have hardcourts. 

Well, there is one in Belgrade but its completely wrecked; there are hoops above your head and a goal behind you because it is also used for basketball and football. In winter, we practise without heating. But I think you learn better that way than when you have everything. It makes us stronger. 

When not travelling, playing, practising, socialising and sleeping, Jankovic has also found time to complete two years of a three-year degree in media management, economics and business at Belgrades Megatrend University. This career is quite short so there has to be life after tennis. I need to prepare for my future, she said. 

Jankovics immediate future may involve a third-round engagement with Venus Williams, whom she defeated at the same stage of last years Wimbledon. Followed by a Roland Garros title to go with her Italian Open win? Maybe, sometimes some things are meant to be...

DAY III RESULTS

 Men rsquo;s Singles Round I: Lleyton Hewitt (Aus)

 bt Max Mirnyi (Blr) 6-3, 6-1, 6-3; Igor Andreev (Rus) bt Andy Roddick (US)

 3-6, 6-4, 6-3, 6-4; Dmitry Tursunov (Rus) bt Alessio Di Mauro (Ita) 6-3, 6-4,

 3-6, 6-3; Fernando Verdasco (Esp) bt Jerome Haehnel (Fra) 4-6, 6-2, 6-2, 6-0;

 Gael Monfils (Fra) bt Olivier Rochus (Bel) 4-6, 6-4, 6-2, 3-6, 6-1; Olivier

 Patience (Fra) bt Jonathan Eysseric (Fra) 6-1, 3-6, 6-2, 6-4; Mikhail Youzhny

 (Rus) bt Jan Hernych (Cze) (Hernych retired); Edouard Roger-Vasselin (Fra)

 bt Marcos Daniel (Bra) 6-1 4-1 (Daniel retired); Michael Llodra (Fra) bt Nicolas

 Devilder (Fra) 6-4, 6-3, 6-3; David Nalbandian (Arg) bt Lee Hyung-Taik (Kor)

 6-2, 6-1, 3-6, 6-3; Albert Montanes (Esp) bt Robin Soederling (Swe) 7-6(4),

 4-1 (Soederling retired); Marcos Baghdatis (Cyp) bt Sebastien Grosjean (Fra)

 6-3, 6-2, 6-4; Juan Carlos Ferrero (Esp) bt Amer Delic (US) 6-7(2), 6-3, 6-3,

 6-4; Nicolas Almagro (Esp) bt Justin Gimelstob (US) 6-4, 6-4, 6-4; Stefan Koubek

 (Aut) bt Christophe Rochus (Bel) 6-7(7), 7-6(1), 6-4, 1-2 (C. Rochus retired);

 Juan Ignacio Chela (Arg) bt Fabrice Santoro (Fra) 6-7(5), 6-0, 6-3, 6-3; Werner

 Eschauer (Aut) bt Alexandre Sidorenko (Fra) 6-3, 3-6, 6-3, 5-4 (Sidorenko retired);

 Richard Gasquet (Fra) bt Nicolas Mahut (Fra) 6-3, 6-2, 6-2; Stanislas Wawrinka

 (Sui) bt Ruben Ramirez Hidalgo (Esp) 0-6, 5-7, 6-2, 7-5, 6-4; Kristof Vliegen

 (Bel) bt Danai Udomchoke (Tha) 6-2, 6-4, 6-3; Carlos Berlocq (Arg) bt Julien

 Benneteau (Fra) 6-7(5), 7-5, 6-2, 6-3; Mathieu Montcourt (Fra) bt Benjamin

 Becker (Ger) 6-3, 6-1, 6-3; Filippo Volandri (Ita) bt Paul Capdeville (Chi)

 6-3, 7-5, 6-3; Juan Monaco (Arg) bt Fabio Fognini (Ita) 3-6, 2-6, 6-1, 6-2,

 6-4; Ivan Ljubicic (Cro) bt Arnaud Clement (Fra) 6-1, 7-5, 7-6(2), Konstantinos

 Economidis (Gre) bt Chris Guccione (Aus) 6-7(6), 6-4, 6-4, 6-1; Roger Federer

 (Sui) bt Michael Russell (US) 6-4, 6-2, 6-4; Guillermo Canas (Arg) bt Victor

 Hanescu (Rom) 6-3, 6-1, 6-4; Tommy Robredo (Esp) bt Sergio Roitman (Arg) 6-3,

 6-4, 6-2; Nicolas Lapentti (Ecu) bt Alexander Peya (Aut) 6-1, 6-4, 2-1 (Peya

 retired); Thierry Ascione (Fra) bt Marin Cilic (Cro) 6-2, 6-2, 6-1; Simone

 Bolelli (Ita) bt Martin Verkerk (Ned) 6-1, 6-4, 6-4

 Women rsquo;s singles Round I: Catalina Castano (Col) bt Emma Laine (Fin) 1-6,

 7-5, 6-4; Andrea Petkovic (Ger) bt Jarmila Gajdosova (Svk) 4-6, 6-3, 6-3; Olga

 Savchuk (Ukr) bt Yaroslava Shvedova (Rus) 7-5, 7-5; Marion Bartoli (Fra) bt

 Aravane Rezai (Fra) 6-2, 6-4; Tzipora Obziler (Isr) bt Melinda Czink (Hun)

 6-3, 6-4; Sybille Bammer (Aut) bt Roberta Vinci (Ita) 6-4, 6-4; Francesca Schiavone

 (Ita) bt Yvonne Meusburger (Aut) 6-2, 6-4; Tamarine Tanasugarn (Tha) bt Casey

 Dell rsquo;Acqua (Aus) 6-3, 4-6, 6-4; Milagros Sequera (Ven) bt Virginie Razzano

 (Fra) 5-7, 6-3, 9-7; Timea Bacsinszky (Sui) bt Zheng Jie (Chn) 7-6(3), 6-0;

 Stephanie Cohen-Aloro (Fra) bt Anastasiya Yakimova (Blr) 7-6(3), 6-1; Akgul

 Amanmuradova (Uzb) bt Vania King (US) 7-6(5), 4-6, 6-3; Shenay Perry (US) bt

 Olivia Sanchez (Fra) 3-6, 6-4, 6-3; Pauline Parmentier (Fra) bt Mariya Koryttseva

 (Ukr) 6-4, 3-6, 7-5; Maria Kirilenko (Rus) bt Maria Elena Camerin (Ita) 6-3,

 6-0; Kveta Peschke (Cze) bt Nadia Petrova (Rus) 7-5, 5-7, 6-0; Nuria Llagostera

 (Esp) bt Anna Smashnova (Isr) 6-2, 7-5; Mathilde Johansson (Fra) bt Anna-Lena

 Groenefeld (Ger) 7-5, 6-4; Elena Dementieva (Rus) bt Angelique Kerber (Ger)

 6-3, 6-2; Mara Santangelo (Ita) bt Agnieszka Radwanska (Pol) 6-1, 6-4; Nicole

 Vaidisova (Cze) bt Emmanuelle Gagliardi (Sui) 6-4, 6-3; Jelena Jankovic (Serbia)

 bt Stephanie Foretz (Fra) 6-2, 6-2 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>