<DOC>
<DOCNO>1070107_calcutta_story_7228259.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Noticed, but not liked

 Subtlety and novelty often act as barriers to comprehension, defeating the basic aim of communication

 Ad Lib 

 Two things can be said about this advertisement with confidence. It is unusual and it has evoked a fair bit of debate. The new ad for Zen Estilo is certainly not run of the mill. It shows people moving around enclosed in cardboard boxes. The audio track has a song. Its lyrics, tune and style of singing make it sound like a limerick.

 The car makes its appearance in the ad, once in the beginning and once in the end. That too not in the way an automobile ad or a Hindi film glorifies its hero. It is possibly one of the rarest of cameo appearances. In the 60-second version, the product shots occupy about 10 seconds of screen space. These are reasons enough for the ad to attract the label of unusual.

 The net result is that heads are turning and people are noticing the advertisement. That is more than half the battle won in an era where between the hero proposing and the lady accepting there are no less than 20 ads. As a viewer, your natural inclination is to ignore them make your cup of tea or refill your glass of whisky. Makers of the Zen Estilo ad should certainly pat themselves on the back for achieving the difficult feat of catching attention.

 The story of this ad unfortunately does not end there. After watching the ad viewers are often plagued with a feeling of confusion. Amidst the blur in their minds, the core message however seems to register. Anyone watching the ad will know that Maruti has relaunched Zen (a familiar name) with a new model. Getting noticed and then communicating even such a gross message is much more than what many other ads accomplish. That surely is adequate enough?

 The debate about the ad centres on this issue of adequacy. Many feel that this ad is good because it does this much at least. The others are tougher task masters. They argue that not many have liked the ad. That is held against the creative execution. 

 For an ad to work they feel noticeability is basic hygiene. Communication is a necessity. What counts the most, they argue, is a positive feeling about the ad which then rubs off on the brand.

 This is the crucial area where the Zen ad has failed. It is interesting to analyse the source of viewer dislike for the Zen ad. The lack of fondness has possibly emanated from the inability of the viewer to decode the full message. He somehow can guess that more is being said in the ad than what he has understood. This causes unease and that in turn manifests itself as a negative sentiment for the ad.

 Indeed, repeat viewing and attention to the words of the jingle have completed the jigsaw puzzle for some viewers. They eventually got what the ad was saying rest of the cars are all look alike. They all resemble boxes. Come out of this boring and monotonous world and embrace the exciting new model of Zen. Those who did not get this message obviously resented their feeling of ignorance. Strangely, those who eventually got it also hated the effort it took them to decode. There possibly lies the kernel of learning. 

 Advertising today is consumed not in the leisure of a Sunday morning. The most frequent interaction between the consumer and communication happens in passing. You are watching the match or the movie and the ad in fact intrudes. In such a situation the moot point is, how subtle or how coded should an ad be?

 In the Zen Estilo ad, the main message was possibly over-encrypted. Foreign visuals and the dependence on lyrics for communication may have acted as barriers to comprehension. The viewer mind has been left hungry.

 The end result is predictable. The commercial has been seen as unusual in the sense of strange or weird. The tragedy is that the intention was exactly the opposite. Instead of the ad, Zen Estilo should have been perceived as a car that is unusual in the sense of distinctive or unique. 




</TEXT>
</DOC>