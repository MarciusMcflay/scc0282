<DOC>
<DOCNO>1070515_business_story_7779012.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Business

 Drug firms pin hopes on US elixir

 VIVEK NAIR 

 Booster dose

 Mumbai, May 14: Pharmaceutical companies in India have been hobbling in pain as pricing pressures ratchet up in the US but there is now a glimmer of hope that the manacles will come off.

 With the US Presidential elections looming in 2008 and healthcare turning into a hot-button issue on the electoral stump, pharmaceutical companies expect to see a number of initiatives and decisions that will advance the cause of the generics business and boost their fortunes. 

 Generics are basically clones of brand-name drugs and have often been reviled as cheap copy-cat drugs. 

 Industry experts have started to see a visible shift in the posturing against generics which have the potential to knock the bottom out of costly, patent-protected blockbuster drugs. All the available indications from the US point to a pro-generic mood, sources said. 

 Take the case of authorised generics. At present, generics players are allowed a 180-day marketing exclusivity of a drug. This exclusivity is given to the first generic company who challenges a listed patent by filing what is known as a paragraph IV filing under the provisions of Hatch-Waxman Act. A paragraph IV certification kickstarts a process whereby the courts are asked to rule whether a listed patent is valid and whether a drugmakers rights are infringed by a proposed generic product. 

 A pharmaceutical company which has a patent for a blockbuster drug that is nearing its end tries hard to fight off a generic drug. When it cant stave off the interloper, it attempts to botch up the market for the new entrant by authorising another generics player to market the same drug during the 180-day exclusivity period. Result: the price of the drug collapses and the stratagem works against the generics entrant in the US market. 

 A few months ago, a group of three senators John D (Jay) Rockefeller, Charles Schumer and Patrick Leahy introduced a legislation to stop this ambush marketing strategy.

 In a typical 180-day marketing exclusivity situation, there are two entities who sell the drug the one which has lost the patent protection and the generic drug company. But when the original patent holder authorises another entity to sell this drug, there are three companies in the fray resulting in a slump in prices. This affects the returns to the generic drug manufacturer, said a senior official from a domestic pharmaceutical company. He added that if the legislation of the three senators was passed, Indian pharmaceutical companies would stand to gain immensely. 

 Several Indian companies, including Ranbaxy and Dr Reddys, have been betting big on generic drugs. 

 Last year, Ranbaxy won a 180-day exclusivity for its Simvastatin 80 mg (indicated in the treatment of patients with coronary heart disease) which helped it earn $60 million during this period. 

 The legislation pertaining to authorised generics is not the only development that has got the domestic pharmaceutical industry agog. Sources said recently the US Supreme Court had ruled that a company could not seek a patent by combining two existing technologies. Many companies were seeking patents after combining two drugs. The ruling is, therefore, positive, the source added.




</TEXT>
</DOC>