<DOC>
<DOCNO>1070324_opinion_story_7556143.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 DANGER AND OPPORTUNITY

 - How Nandigram will set back the industrial clock

 Sunanda K. Datta-Ray

 sunanda.dattaraygmail.com

 The death of hope

 Calcutta was again in the grips of a bandh when I landed last Friday and drove home through deserted streets. When I had left in January it was also a furtive journey to the airport because of another bandh. The only redeeming feature is that bandhs nowadays are never total. Like all else, including a murderously bungled attempt at industrialization, they are half-hearted and ineffectual. 

 The retreat from Nandigram signalled the death of hope. It makes little difference whether uniformed thugs, Marxist cadres, Trinamool storm troopers or Naxalite outlaws caused the carnage. What matters is that another attempt to acquire land for industry is unthinkable in the near future. What is even more disheartening is that last Saturdays seminar on Globalization and Education: Problems and Prospects for India as part of Don Bosco schools golden jubilee celebrations indicated that even highly educated and otherwise discerning Bengalis are hostile to the notion of capital flowing into the state. Sumit and Tanika Sarkar may have returned their awards to protest against brutality, but the gesture will be widely interpreted as another high-minded indictment of globalization.

 Yet, Bengal desperately needs jobs that can only come from extensive manufacturing industries. That means investing far more money than the trading community that finances the Marxists is willing (or, perhaps, able) to put up. Much has been written about 90,000 land protests in China. What deserves stressing is that these eruptions did not prevent the authorities from decanting a million people to create the glittering new township of Pudong across the Huangpu river from Shanghai. Its steel and glass towers and globes proclaim Chinas rapidly growing wealth, hectic financial activity and robust confidence in the globalized future. 

 Someone at the Don Bosco seminar remarked that I was optimistic about globalization only because I was visiting. True, abroad one has a surfeit of India Shining, India Rising, Incredible India and India Everywhere. The Indian glass looks at least half-full. The moment one lands at Dum Dum, the same glass seems bleakly half (or more) empty. That is because people here are still haunted by the bogey of the East India Company. The Don Bosco seminar presentations confirmed that globalization is todays Foreign Hand. The assembly speaker, Hasim Abdul Halim, spoke darkly of schools for the rich, as if his governments inability to educate the poor is the fault of international capital. The Indian Institute of Managements Shitangshu Kumar Chakravarti blamed globalization for his grandsons hip taste in t-shirt slogans. 

 Not everyone was as hostile. Shyamaprasad Mukherjee, an eminent statistician, presented a balanced view of the cultural mosaic of globalization which, he said, can augment creativity instead of imposing regimentation. But people remember the criticism because it feeds into existing prejudice and provides moral ammunition to the patrons of groups like the Bhoomi Uchched Pratirodhi and Krishi Jomi Raksha Committees. A casual listener to the seminar should not be blamed too much for imagining that, in addition to bad schools and shocking sartorial taste, globalization is also responsible for the lack of female empowerment, internet abuse and exploitative non-governmental organizations. The speakers Swaraj Kumar Nath, director-general of the Central Statistical Organization, Bethune College principal, Manimala Das, George Thadathil, head of the Salesian College near Darjeeling, and a computer professor, Subhansu Bandyopadhyay did not condemn globalization outright. Barring the engaging young Swami Sarvapriyananda, of whom more later, they damned it with faint praise. 

 Nandigram makes it difficult to take a stand against their jeremiads. Objectively speaking, the criminal collapse of civilized norms is more than reason enough to dismiss the state government. Presidents rule has been imposed for far less. But, then, punishing an incompetent, power-hungry and neurotic regime cannot be the only purpose of invoking Article 356. The punishment must also somehow benefit Bengals long-suffering people. That means educating peasants in the optimum use of land, which is charged with profound emotion. The land departments chaotic records, archaic rules and corrupt and inefficient staff make it virtually impossible to carry out the tasks of identification, verification and acquisition without a measure of pragmatic compulsion. 

 It is doubtful if outsiders operating from Raj Bhavan can handle such a gargantuan job. Given the Central governments composition and temperament, its authority may not be more effective than the Left Fronts. Hooligans with powerful local protection will defy discipline. Central nominees certainly cannot revolutionize public thinking, neutralize troublemakers and galvanize the peasantry. That is the responsibility of politicians with a popular mandate. They alone can instruct, motivate and inspire to arrest the decline of three decades. 

 This weeks tragic drama highlighted the interests at stake in a situation that would never have arisen if Jyoti Basu had given a damn about public welfare during his long tenure. With his education, sophistication, worldly contacts and taste for good living, he could have done so much; instead, a visit to Samavayikas dusty confusion or the wilderness of Haringhata confirms how much of what Bidhan Chandra Roy created for Bengalis has been destroyed. Basus emphasis now on Left Front consensus means that economic programmes that might deter voters will not be pursued and that the chief ministers wings will remain clipped. Poverty is his price for continued power.

 The word crisis is spelt in Mandarin with two ideograms standing for danger and opportunity. So was Nandigram for an array of self-seeking politicians. Prakash Karat came rushing down from Delhi not out of any concern for Bengal or Bengalis but because of fears that the Left Front tail will not be able to wag the UPA dog if Calcuttas red citadel falls. Caught between an ally at the Centre that it dare not ignore and an adversary in West Bengal it dare not offend, the Congress remains paralysed. Lal Krishna Advani saw the massacre as an opportunity to try once more to remind indifferent Bengalis that an animal called the Bharatiya Janata Party still prowls the Hindi heartland. Mamata Banerjee must have viewed the broken heads, mutilated corpses and raped women with satisfaction: the more villagers are terrorized under Marxist rule, the greater the chance that Trinamool might one day climb to power on the rebound.

 Amidst all these calculations, Buddhadeb Bhattacharjee perhaps does seek to revive the economy. He is certainly the only public personality to deserve the benefit of the doubt. But his choice of investor for the proposed special economic zone prompts misgivings about his judgment and knowledge of the world as well as about the people who influence his decisions. His methods are far from transparent; and it seemed last week that he was not averse to exploiting the special economic zone plan to consolidate his partys stranglehold on the countryside. Either he acquiesced in the grim doings or he has no control over ruthless party apparatchiks.

 Jairam Ramesh rightly dismissed all this as part of Bengals political culture of confrontation and violence. But it is wrong to conclude it has no bearing on industrialization. The link is direct, for by discrediting the acquisition of land for SEZs, Nandigram will set back the industrial clock. As the Don Bosco seminar highlighted, many thinking people are looking for an excuse to condemn what they dont know and therefore blame for all the ills they do know. It took the unlikely person of a Belur Math monk with an MBA degree to remark on the generation divide. Swami Sarvapriyananda pointed out that the old might be willing to stay with the status quo but the young are reaching out to the world. He could have added that if we cant bring the world to them, they will go off in search of opportunities in Delhi, Bombay, Bangalore and beyond the seas. The killing fields of Bengal will be left with the likes of Naru Maity. They will offer nothing to ambitious, young, English-speaking people from institutions like Don Bosco.




</TEXT>
</DOC>