<DOC>
<DOCNO>1070727_opinion_story_8109364.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SAINT HARRY

 - The boy who lived happily ever after

 HARRY POTTER AND THE DEATHLY HALLOWS By J.K. Rowling, 

 Bloomsbury, 17.99

 It is possible that J.K. Rowlings real subject, all these years when we thought she was writing the classic childrens fantasy, was not magic but love. Selfless love alone could defeat the deadliest magic: thats how Harry Potter survived Voldemorts curse. Lily Potter died shielding her son from the Dark Lord. But a sons love could never recall his dead mother back to life, Harry had been told by Professor Dumbledore, before the Mirror of Erised. Young Harry had spent hours before the enchanted mirror, mesmerized by the illusory presence of his dead parents, even as they remained fixed on the threshold of dream and reality. 

 This led to the other theme of Rowlings story: revenge. We realized that hers was not a childrens tale, but a bildungsroman the growth of a boy into maturity, seeking revenge for the murder of his parents. (The latest, and the final, book of the Harry Potter series has, quite incredibly, an epigraph from Aeschylus The Libation Bearers, a story of matricide, which would confound many young readers if they cared to look up the reference!)

 However, the power of love, as also the futility of it, ran through the Harry Potter novels like a fissure, leaving behind its debris of grief in an otherwise charmed world of friends, foster-families, laughter, romance, happiness and dangerous adventures. In Harry Potter and the Deathly Hallows, this faultline, made by the trail of love and grief, deepens, leaving a significant portion of the dramatis personae dead. The ending of this 600-page tome is laced with pity and fear (until one turns to the epilogue, Nineteen Years Later), just as the previous book, Harry Potter and the Half-Blood Prince, had ended with the shocking murder of Albus Dumbledore by Severus Snape. In the final book, however, the twists begin early. 

 After the first chapter, where Voldemort and his cronies meet at the Malfoy Manor to devise their course of destruction (the scene, sadly, reads like an unintended parody of Satans conference with the devils, turned into snakes, in Paradise Lost, Book X), Rowling follows her favourite and, by now, most predictable narrative strategy: cut to Privet Drive. The first surprise comes here. Harry is separated from the Dursleys forever as he turns 17, the protective charm on the house expires, and therefore he has to be taken into the safety of the wizarding world but the parting, eagerly-awaited for long by both the Dursleys and Harry, is not easy. His cousin, Dudley, who has turned vegetative by the kiss of the Dementors, tells an incredulous Harry, I dont think youre waste of space, while Aunt Petunia sheds a few (crocodile?) tears. 

 It is possible to read the entire book as an extended denouement, with this strangely sentimental scene as the first resolution of one of many conflicts. After this, instances of tidying up abound. The scattered pieces are made to fit into the completed jigsaw puzzle, the happy ending. A moral order must be upheld, the good must triumph over the evil and die for a good cause. So Percy Weasley is united with his estranged family, Draco Malfoy is tormented by uncertainties, as is the traitor, Peter Pettigrew. Remus Lupin, his wife, Nymphadora Tonks (they quietly get married in this volume), Fred Weasley, Mad-Eye Moody and Colin Creevey are killed in the battle with the Death Eaters. 

 Each of these incidents, sad as they might be, is not as shocking as Dumbledores murder in the previous book. There is nothing inventive or incredible about killing off a few minor characters, but Rowling saves her best for Snapes death. The crux of the narrative lies in those final moments when Snapes life ebbs away and Harry collects the dying mans memories into a vial, then proceeds to pour them into the Pensieve in Dumbledores office. As he descends into Snapes remembrances, a whole new dimension to Dumbledores death, Snapes personality and Harrys destiny emerges. Rowlings creative ingenuity is most impressive in this section, her fiction most convincing. 

 However, the readers disbelief is stretched farther and farther towards the conclusion. Harry Potter is theoretically killed by Voldemort, but actually saved by the Elder Wand, which he had disarmed from Draco. (The wand is one of the three Deathly Hallows that bestow the power of invincibility, and thus immortality, on their possessor.) Between this brief spell of death and return to life, Harry spends some time chatting, first with his dead parents and the dead members of the Order of the Phoenix, and then, predictably, with Albus Dumbledore. It seems that they are all part of his consciousness (or whatever they call it in the wizard world). In fact, the resurrection of the dead, so useful in clearing up narrative mishaps and clutter, is not fully explained. But Saint Harry comes back to life to finish off Voldemort (this happens surprise, surprise! once again, due to magical intervention over which Harry has no control).

 As if this were not breathtaking enough, we are taken on a Dickensian tour of the lives of Harry, Ron and Hermione, 19 years after they had ended the regime of the Dark Lord. In what must be the most embarrassingly trite endings imaginable, Harry and Ginny are married, so are Ron and Hermione to each other, and there is a perfectly pointless, feel-good scene where they go to see off their children to Hogwarts from Platform 9 at Kings Cross. This, after ploughing through 600-odd pages, about a couple of hundred of which could have easily been edited out! 

 Being a narrow-minded Muggle, one cannot help but end on a mundane note, on the absurdly gargantuan size of the Potter novels from the third book onwards. The multi-volume 19 th-century novel Dickens in particular seems to have been the challenge, which Rowling had taken on. In February, this year, after finishing the final book, Rowling compared her feeling of an incredible sense of achievement and mourning to that expressed by Dickens in the preface to the 1850 edition of David Copperfield a two-years imaginative task. Rowlings rejoinder to Dickens, preposterous as it sounds, was I can only sigh, try seventeen years, Charles

 For Rowlings readers, Potter has been around, since the first book appeared in 1997, for a decade. In this time, it took Moaning Myrtle toilet seats, Harry Potter toys, video games, and several companion books penned by Rowling (including Quidditch Through the Ages) to sustain the craze. True, Harry Potter has become a landmark in the history of the book trade. Still it requires more than magic to outlive a Dickens, Barrie or Carroll.

 SOMAK GHOSHAL




</TEXT>
</DOC>