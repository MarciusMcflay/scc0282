<DOC>
<DOCNO>1070521_sports_story_7807302.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Rooney didnt deserve to lose

 Patrick Barclay

For the first time in three years, we were rescued from penalties by a far finer goal than most of the final merited. But even without the dreaded decider, which had confounded the better side in 2005 (Manchester United, who lost to Arsenal) and 2006 (West Ham, denied by Liverpool), there was still a trace of cruelty in that Wayne Rooneys performance deserved better than defeat. 

In the second half of normal time, Rooney raised the quality of entertainment and offered his Manchester United colleagues a path to victory. That they failed to take it was largely due to the fact that Chelsea possess, in Petr Cech, the worlds most accomplished goalkeeper. They also have an outstanding Ivorian centre-forward in Didier Drogba, whom Frank Lampard helped to seize the day and complete the third domestic cup double, emulating the achievements of George Grahams Arsenal in 1993 and Gerard Houlliers Liverpool in 2001. 

How Drogba fought. How he emphasised, even before his 33rd goal of the season, that, but for the existence of Cristiano Ronaldo, he would have made a convincing Footballer-Of-The-Year. How subtly, after all the buffetings the 24million striker had given and taken, he was to edge Lampards superb return past the Dutchman when the moment came. 

Jose Mourinhos Chelsea are all about prudence and patiently painting by numbers until inspiration strikes the likes of Drogba and Lampard as it did here, while Sir Alex Fergusons way is to have his men switch and flow and chance their arms. A fast surface would have helped them to settle, but instead Chelsea took control. The notion that they might prove over-reliant on Drogba for a goal was challenged by the excellent Lampard, who, after forcing a sharp save, was unlucky not to obtain even a corner when his drive clipped Wes Browns heel and spun just too high. 

Why Ferguson had put Ronaldo on the left was hard to tell maybe in order that the more defensively minded Darren Fletcher could limit Wayne Bridges forays but the Portuguese tricksters relative ineffectiveness was balanced by Rooneys post-interval verve. He had spent most of the first half protesting to Bennett, but now he concentrated on playing and it became a different game. 

After making Cech parry, Rooney embarked on a run of such boldness as to catch the breath of the entire stadium, sweeping out of his own half, past Shaun Wright-Phillips and Lampard, and accelerating ahead of Michael Essien into the Chelsea penalty area, where Bridge did extremely well to come across and curtail his progress. Within minutes, Ryan Giggs having volleyed over the crossbar, Rooney again took on the Chelsea defence only to be foiled by Cechs judiciously outstretched right arm. 

It was not, in all, a display in which the referee could take much pride. But Bennett and the inesmen were entitled to sympathy, however, when, from Rooneys glorious ball across the face of goal, Giggs claimed to have legitimately forced it over the line; congestion rendered the naked eye at a disadvantage. Anyway, replays indicated Giggs had fouled the keeper. It would not have been right for Rooney and his team to benefit from that and ultimately the contest between the two spearheads the wily Englishman and the dashing Ivorian went the other way. 

 THE SUNDAY TELEGRAPH 




</TEXT>
</DOC>