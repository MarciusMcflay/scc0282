<DOC>
<DOCNO>1070621_opinion_story_7949906.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 JUST NOT MARRIED

 It is not easy for the law to keep pace with changing social mores. Yet, for the past ten years or so, the courts in India have shown remarkable alertness in trying to address changing relationships and expectations. Dealing with a multitude of cultural codes is difficult enough, on top of which there are situations created by the application of the personal law of the various faiths. It is not surprising, therefore, that the wisdom of the courts does not always hit the mark the very first time; there are many situations that exhibit unexpectedly new facets after the first couple of rulings. The law is evolving as society evolves.

 A good example of this evolution is the recent ruling of the Calcutta high court that breaking a promise of marriage after living together for a few years cannot constitute a charge of rape. About three years ago, the court had ruled differently, and there were a number of charges of rape where, earlier, a breach of promise charge may have been made. It is laudable that the court is willing to correct the balance. A relationship of three or four years between two consenting adults cannot be reduced to rape, even if the relationship was based on the mans promise of marriage. This is a tough one. Most unmarried Indian women are more likely to live with or have a long sexual relationship with a man only when assured of marriage. And being a consenting adult, she cannot, in all fairness, charge the man with rape. Yet, he might have been a married man who never intended to marry the complainant in the first place. Should he then be tried for breach of promise alone? Among the many difficulties the law needs to address in such cases, one is the new one of live-in relationships or premarital sexual relationships. The other is the old one of the socially and culturally disadvantaged position of women. The question what is just? becomes deeply troubled in such cases. Part of the solution might lie in adjusting the penalty according to the weight of circumstances surrounding the breach of promise.




</TEXT>
</DOC>