<DOC>
<DOCNO>1070619_opinion_story_7939438.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 AMONG FRIENDS

 Aveek Sen

 Bharati Bera, around sixteen now, loves to read. But over the last year, Ive seen how reading has become more than just a pleasure for her. Every time she gets upsetting news from home, fails in her grim struggle with arithmetic or gets scolded, she sits down with her Tuntunir Boi and Thakumar Jhuli on the verandah. With a determined sullenness, she first turns the pages blankly, then words begin to form on her lips, and before long shes into the story, smiling and frowning in delighted absorption. 

 Bharati came to work in our house, four years ago, from Arjuni village in Medinipur. Her father is a vegetable-seller; her mother doesnt go out to work. Bharati is the eldest of five daughters, and the day, last year, her youngest sister was born, she wept because she knew another daughter was disastrous for her parents. That was one occasion her books gave her refuge. She had studied up to Class IV when she came; two of her sisters still go to school and another goes to the anganwadi. My parents started teaching her arithmetic, English and Bengali, for she refused to go to school with the cooks daughter, Rinku, who is the first girl in her class. Sums and English were a struggle from Day One, and my parents proved too fragile for it. But Bharati quickly developed two compulsive habits: reading Bengali books and solving Sudoku puzzles. She also likes drawing landscapes, keeps the accounts and makes grocery lists when my parents are away. 

 These days, when she goes home, apart from taking clothes and trinkets for her family, she takes her books. The village borrows them from her and reads them before she returns. Tuntuni is the favourite (even the village elders have read it), followed by Thakumar Jhuli, Ha Ja Ba Ra La and Leela Majumdars Shob Bhuture. Her mother, who writes letters for the village illiterates, particularly enjoys reading Bharatis books. In villages like Arjuni, more and more girls are going to school, but all they get to read are their textbooks, for there are no libraries. Yet there is an insatiable appetite for reading, in spite of TV and cinema. 

 Bharati also reads the Anandabazar Patrika. She avidly follows stories of elderly people being abandoned by their children or the crazed, old woman keeping vigil over the corpse of her daughter. She also talks about all the students who clear their board exams but cannot afford to study further. Sometimes, our part-time helps daughter, Mampi, comes to visit us from a slum beyond Garia. She too is an eager student in her local corporation school and wants to be a doctor. Bharati and Rinku sit with Mampi all day in the verandah and listen to her read from her books.




</TEXT>
</DOC>