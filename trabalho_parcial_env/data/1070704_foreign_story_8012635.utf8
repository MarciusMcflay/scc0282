<DOC>
<DOCNO>1070704_foreign_story_8012635.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 Changing face of terror suspects

 - Focus shifts from homegrown extremists with modest backgrounds to foreign professionals

 Asha (right) with Jordans Queen Noor in an undated picture released by his family. (AFP)

 London, July 3 (Reuters): The alleged role of Arab and Indian doctors in what British authorities are calling an al Qaida-linked plot marks a contrast with recent conspiracies led by homegrown radicals, often with modest academic backgrounds.

 Experts said the differences underline the impossibility of constructing a profile of a typical Islamist militant.

 All eight people detained so far in connection with two failed London car bombs and a botched attack on a Scottish airport are doctors or have links to the medical profession, security sources said.

 Two including a man detained in Australia are from India, and the rest from West Asia including doctors Bilal Abdulla, who qualified in Iraq, and Mohammed Asha, a Jordanian.

 Asha had been in Britain since 2004, according to his father, and newspaper reports said Abdulla arrived last year. None of those held has been charged with any offence, and police have up to 28 days to question them.

 The possible involvement of foreigners in a suspected Islamist plot in Britain would not be new in April 2005, for example, Algerian Kamel Bourgass was convicted of a plot to launch attacks with poisons and bombs. Six men of African origin are now awaiting verdicts in another terrorism trial.

 But other recent investigations have focused mainly on long-term residents or British citizens, like the four young Muslim men who blew themselves up on London underground trains and a bus two years ago this week, killing 52 people.

 The MI5 intelligence agency has focused intensively on homegrown radicals in a rapid expansion which has nearly doubled its size since 2001. It has opened a series of regional branches to keep tabs on an estimated 1,600 militant Islamists it suspects of plotting attacks in Britain or overseas.

 The current case showed how difficult it is to try to pinpoint suspects before they strike. 

 The pattern is, there is no pattern, said one security official, denying that the strengthened domestic focus had distracted authorities from looking at non-British nationals.

 We dont view individuals on the basis of their ethnicity or origins, we view them on the intelligence basis of what theyre up to.

 The focus on West Asian and Indian suspects may mark a break with other recent British cases which have repeatedly featured links to Pakistan. In at least three plots, leaders had travelled there to train in al Qaida camps or seek approval from Osama bin Ladens network for proposed attacks in Britain.

 No link to Pakistan has yet emerged in the latest case, which is also unique for another reason the medical background of all the suspects.

 But the source said medical expertise did not appear central to the plot, and it was entirely speculative for media to suggest they formed an al Qaida sleeper cell smuggled into Britain using their profession as the perfect cover.

 International experience shows a wide variety of people may be drawn to militant Islamism, from drifters and petty criminals to affluent individuals and those with university backgrounds.

 Its not a surprise that educated people become part of these networks. I think its a complete stereotype that these people are deprived, uneducated, people below the average income line and so on, said Peter Neumann, security analyst at Kings College, London.




</TEXT>
</DOC>