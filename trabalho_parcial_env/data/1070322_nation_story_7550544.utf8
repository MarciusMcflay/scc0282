<DOC>
<DOCNO>1070322_nation_story_7550544.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Poverty dip with pinch of salt

 OUR SPECIAL CORRESPONDENT

New Delhi, March 21: Caught in the middle of land unrest and rising prices, the government has come up with two sets of figures that suggest poverty is falling.

 The data, compiled by the National Sample Survey (NSS) and released by the Planning Commission, indicate the number of poor as a percentage of Indias population may have come down.

 However, lost in the percentages is a staggering fact in absolute numbers: as many as 238.5 million Indians still live below the poverty line.

 The figures have been released a month before elections to the Uttar Pradesh Assembly and could give fresh ammunition to the opponents of Mulayam Singh Yadav. The state still has the countrys largest pool of poor at 59 million by one count and 45.8 million by another.

 One set of data put together using a new methodology devised by the Planning Commission during the previous NDA regime shows the percentage of Indians living below the poverty line in 2004-05 has fallen to 21.8 per cent from 26.1 per cent in 1999-2000.

 These figures have been arrived at using consumption distribution data spread over a mixed recall period.

 Under this method, people are asked to recall five infrequently purchased items (clothing, footwear, durable goods and education and medical expenses) and the food items they consumed over a 30-day period.

 The NSS has also used an older method to calculate the poverty figures. 

 Under that method uniform recall period data is collected for 30 days consumption with stress on food items.

 Figures compiled under the older method show poverty has come down but to a different level 27.5 per cent in 2004-05 compared with 36 per cent in 1993-94.

 Since this method was not used in 1999, it is impossible to get comparable figures for the NDA period. We are trying to compare the incomparable, a plan panel adviser said. 

 The figures based on both models are being projected as proof that reforms have been a success and that the Congress has brought about more inclusive growth.

 The statistics suggest that the decline in poverty was steeper in rural areas where the percentage of people living below poverty line fell to 21.8 per cent from 27.1 per cent under the new method. In urban areas, the percentage of the poor fell to 21.7 per cent from 23.6 per cent, according to the NSS estimates.

 A former Planning Commission member questioned the figures, asking how some among the 27.5 per cent (in the old method that stressed on food items) who could not afford a square meal can purchase the other five items and bring down the poverty level to 21.8 under the new formula.

 Its how one interprets the data. I feel the figures suggest poverty has deepened, S.P. Gupta, who was a plan panel member in 1999, said.

 However, plan panel economists contend that the number of poor have come down, if not in absolute numbers, at least in percentage terms. It shows targeted programmes like the job guarantee programmes have been successful in reducing poverty. 

 Statewise data shows that in absolute numbers, four states Uttar Pradesh, Bihar, Maharashtra and Madhya Pradesh have the maximum poor. Bengal is in fifth place. But in percentage terms, Bengal bettered the all-India figure.




</TEXT>
</DOC>