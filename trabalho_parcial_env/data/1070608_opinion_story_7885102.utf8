<DOC>
<DOCNO>1070608_opinion_story_7885102.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 Paperback Pickings

 Learning to play solitaire

 Memories Come Alive (Penguin, Rs 450) by Manna Dey is the veteran singers autobiography, which has been elegantly translated by Sarbani Putatunda from the original Bengali. To readers who are accustomed to think of Dey solely as a singer, this book might offer a few surprises. For one, as a little boy, Dey had little feeling for music, but was more inclined towards wrestling with [his] friends and picking up fights. It was much later that he was initiated into music by his uncle, Krishna Chandra Dey, a famous singer himself. With chapters describing the singers childhood days in Kolkata, his flourishing career in Mumbai and the women in [his] life, this is much more than an account of a musical journey. The many delightful anecdotes, reminiscences and portraits of friendships bring into this book a feeling of expansive nostalgia. Additionally, there are valuable appendices with catalogues of all the songs that Dey has either sung himself or set to tune. Finally, the book comes with a CD carrying a selection of thegreatest songs sung by Dey. Although the selection in itself is not comprehensive, the CD, nevertheless, lends a certain charm to the volume.

 TILLED EARTH (Penguin, Rs 195) by Manjushree Thapa is a collection of quirky, but excellent, short stories. Set in Nepal, these tales are written with the right touch of poetry that turns the mundane into the magical. The brevity of some of the narratives, running barely half a page, recalls the aphoristic little tales of Franz Kafka. In the opening story, an elderly woodworker, Heera Maharjan, loses his way in a posh neighbourhood of Kathmandu. He trudges through the alien locality bemused and bewildered, until an obscure sense of purpose brings him back to his senses. In another haunting story, a bureaucrat, immersed in the drudgery of his daily chores, discovers the pleasures of playing solitaire on his computer. The longer stories are more expository, even sociological, chronicling a wide range of emotions experienced by a gallery of memorable faces.

 THE SUGAR BARONS DAUGHTER 

 (HarperCollins, Rs 250) by Loveleen Kacker 

 claims to be a feisty novel of ambition and lust. The 

 eponymous protagonist, Nagina, is beautiful but unscrupulous, 

 who sets out, determinedly, to inherit her fathers sugar 

 mill. Anmol, her ex-lover, succumbs to her fatal attraction 

 and becomes her accomplice in this pernicious enterprise. 

 Although the pace of the novel somewhat flags, the prose 

 is brisk, the sections joined together by the first-person 

 narratives (by the main characters) are without frills and 

 flourishes. The range of historical and social references 

 from the riots initiated by the Mandal Commission Report 

 to middle-class-life in Delhi lends a social realism to 

 the narrative.

 OUTLOOK TRAVELLER GETAWAYS: 

 Rajasthan (Rs 295) is indispensable for prospective 

 visitors to this north-western state of India. It begins 

 with a comprehensive history of the region, moving on to 

 extensive details about the various towns: where to stay 

 and what to eat, and more importantly, the costs of planning 

 a tour of Rajasthan. The text is embellished with beautiful 

 photographs and supplemented with elaborate maps. One can 

 hardly imagine a better travel guide.

 ENGAGING INDIA(Penguin, 

 Rs 295) by Strobe Talbott is an exploration of 

 Diplomacy, Democracy and the Bomb by the former US Deputy 

 Secretary of State. It reveals the course of diplomatic 

 negotiations between India and the US following the 1998 

 nuclear tests in Thar desert. Interspersed with anecdotes 

 and human details, this is not just a historical narrative 

 but also one that bristles with the energy of a good thriller.




</TEXT>
</DOC>