<DOC>
<DOCNO>1070504_opinion_story_7729018.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 THREE FACES OF THE RIGHT

 - Awaiting the fates of Nicolas Sarkozy, David Cameron and UP

 Swapan Dasgupta

For an obsessive election-watcher, May is calculated to be a gripping month. By the afternoon of May 11, the outcome of the interminably long Uttar Pradesh election will be known. However, prior to that, there are two other elections which are fascinating for the issues they have thrown up. There are, first, the elections to the Scottish Parliament and English local bodies on May 3 the results will be known by Friday evening. Finally, on Sunday, the French people whose reach extend beyond the geographical frontiers of France will be voting to choose between Nicolas Sarkozy and Sgolne Royal.

 The direct Right-Left face-off for the Elyse Palace has been enthralling for two reasons: the astonishingly high 85 per cent turnout of voters in the first round last month and the ability of the 52-year-old Sarkozy to set the terms of the national debate. The two seem directly related. Although the first round of presidential elections is used by fringe groups of all varieties including all the 57 varieties of Trotskyism to make a statement, the incredibly high level of voter participation owed a great deal to the fact that this is an election where the people are being offered a real choice.

 The Sarkozy election campaign is reminiscent of the robust choice offered to the British electorate by Margaret Thatcher in 1979. Just as Thatcher promised to break the mould of the stultifying post-war consensus that contributed to Britain being labelled the sick man of Europe, Sarkozy has succeeded in addressing the two themes that have been agitating the French for far too long: economic stagnation and national identity.

 That Sarkozy threatens to destroy the cosy French consensus the romantics call it the French way of life, marked by a bloated state, pampered welfarism and over-regulation is undeniable. In arguing for radical tax cuts, trimming of the public sector and flexible labour laws, Sarkozy has tried to match politics with the imperatives of a competitive market economy. At the same time, he has personified the popular disgust articulated in a rabid way by Jean-Marie Le Pen with unchecked immigration, Islamic terrorism and the loutishness of the underclass. 

 Sarkozys no-nonsense approach has, predictably, been perceived as divisive by those who claim to be custodians of French mentality. In an appeal published in the Liberation earlier this week, a clutch of household names from the arts world, such as Jeanne Moreau, Constantin Costa-Gavras and Francois Ozon, argued that to vote against Nicolas Sarkozy is to avoid the peril of a France at war with itself, in conflict and in crisis, divided and torn. Sarkozy, the appeal warned, embodies a hard radicalized Right, under the influence of the far-Right with all its fears and hates.

 Sarkozy, it seems to me, must be doing something right if his platform of reform and patriotism has drawn flak from such quarters. In the early Eighties, the whos who of economists in Britain put out an appeal warning that Thatchers economic reforms would lead to the end of civilization as we know it. Like the anything but Sarkozy campaign in France, Britain too saw a wave of puerile indignation, including suggestions that the Iron Lady was fascist, racist and a complete ignoramus. Ronald Reagan, the most successful and charismatic of the American presidents of the 20th century, encountered similar liberal opprobrium and was the butt of cruel jokes.

 If, despite this hysterical onslaught, Sarkozy manages to carry the day, he would have reinforced a few ground rules for the Right. First, that polarization along ideological and, occasionally, community lines is both inevitable and a great motivator when something truly radical is on offer. Second, that shunning ideological confrontation including echoing Reagan and describing the Left as an evil force for the sake of goody-goody consensus is okay if politics is all about serving time but undesirable if the end is to nurture meaningful change. Third, the electorate turns increasingly cynical if a spade is consistently referred to as an excavating instrument. Sarkozy drew a lot of flak for describing last years riots in Paris as the handiwork of scums. Would it have helped his campaign had he emulated bleeding hearts and described them as socio-economically challenged, alienated multiculturalists? Finally, the Right cannot forge policies with an eye to securing the endorsement of the liberal establishment. Apart from liberals never voting for the Right (as the Bharatiya Janata Party found out in 2004), they derail the need to address those who have a stake in the system. The Right is temperamentally inarticulate and runs on instinct. But this should not prompt a tilt towards those who are naturally loquacious but have a different social vision.

 It is not surprising that Sarkozys in-your-face style has not appealed to the British Conservative Party and its 41-year-old leader, David Cameron. In trying to regain lost ground after a decade of Labour dominance, Cameron has jettisoned abrasive Thatcherism in favour of a touchy-feely, compassionate and contemporary Toryism. This is slowly yielding results and May 3 will demonstrate the extent to which Conservatives have edged Labour out of England. The positive feature of Cameron is that he has enlarged the Tory agenda to include green issues and discarded the traditional Conservative disdain for alternative lifestyles. At the same time, core Tory issues such as tax cuts and immigration control have been kept in abeyance.

 While these innovations have been dictated by Tony Blairs positive economic legacy and the realities of confused cosmopolitanism in Britain, it has led to the Conservatives shunning of hard questions like British Muslim radicalism and the defence of the Union in Scotland. Camerons preoccupation seems to be to avoid the Tories being labelled the Nasty Party, even if this involves yielding political space to the secessionists in Scotland. The virtues of Unionism a defining feature of British nationhood has been allowed to go virtually unaddressed in both England and Scotland.

 The divergent approaches of Sarkozy and Cameron have been dictated by national realities France needs a bigger shake-up than Britain. However, both approaches have been preceded by detailed deliberations of what constitutes the appropriate Right response to specific situations. This, unfortunately, is what is most lacking in the Indian Right.

 Despite glaring imperfections and a tendency of the Rashtriya Swayamsevak Sangh to propound confused neo-Gandhian economics, the BJP remains the sole custodian of the Indian Right. It is, for example, the only organization that squarely confronts the assaults on nationhood and identity. These attributes are likely to help the BJP stage a recovery of sorts in Uttar Pradesh. Yet, its campaign has been marked by stupendous non-application of mind. Whereas the situation called for a radical programme aimed at fighting criminalization, reducing the stranglehold of caste and promoting economic modernization, the BJP has been content to play safe. The Uttar Pradesh election should have been a dress rehearsal for the 2009 national election by throwing up new issues for the electorate to mull over. It has, quite unthinkingly, left the modernizing agenda to a dynasty-obsessed Congress which, to its credit, is positioning itself for a generation change. The BJP has not been able to decide whether electoral success depends on addressing issues robustly la Sarkozy or, like Cameron, banking on a combination of evasion and innovation.

 The country may have to wait for the Gujarat assembly election, due in December this year, before Indians get a taste of real political choice. For the moment, it is best to digest the experiences of three different faces of Right politics.




</TEXT>
</DOC>