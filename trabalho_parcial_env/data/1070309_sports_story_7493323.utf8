<DOC>
<DOCNO>1070309_sports_story_7493323.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Pietersen ready to resume hostilities 

Derek Pringle

The excruciating pain of a broken rib will not prevent Kevin Pietersen from giving Glenn McGrath the charge during this World Cup, should the need arise. Pietersen missed the last three weeks of Englands Ashes tour after McGrath halted his advances with a short ball to the midriff during the opening one-day match. The pair will meet again in Fridays warm-up match here. 

I go and express myself on a cricket field and I go and bat accordingly, said Pietersen. I pick situations, I pick bowlers and I pick areas I want to hit the ball, thats how I play. If running down the pitch is the way Im going to score off McGrath, then I might have to do it again. 

I felt upsetting his line and length in that game in Melbourne would provide me with a scoring option, but I didnt believe he could break my ribs. Hes just so accurate that it was my attempt at being positive. 

The incident brought a double break, one involving bone, the other a short respite from the most difficult period in Pietersens cricket career. 

The pain was excruciating, and I knew it was a problem when I couldnt breathe, said Pietersen. The injury gave me a break which might prove handy, but Id have preferred to have been in Australia. I was really enjoying my batting and would have liked to continue that and then taken that into the World Cup. Instead Ive had a break and had to start again. 

Ironically, given that his batting was the brightest aspect of Englands forlorn Ashes defence, his absence was not missed. After a few more defeats, England began to win and win well, a frustration surely for a glory seeker such as Pietersen. 

Theres nothing you can do about injuries, said Pietersen. I know how hard the winter was. I was out there two-and-half months and didnt see victory at all. Its frustrating to watch the team lose but not to watch them win, that was fantastic. 

Theres a definite difference in the team, confirmed Pietersen. To win a trophy against Australia and then have a few weeks to recharge the batteries, well the boys just had smiles on their faces. 

Australia, by contrast, have been soul searching with Ricky Ponting undertaking one-on-one meetings to see if any had been damaged by their six successive one-day defeats (three to England, three to New Zealand). The answer coming from the favourites is a unanimous no, though Brett Lees absence is bound to hurt them. 

Losing Lee is a massive hole in their attack, said Pietersen. Brett has matured into one of the great fast bowlers. 

We had our vulnerable moments when we didnt have Vaughan, Trescothick, Simon Jones and Freddie last year. The Aussies have lost Lee and teams become vulnerable when they lose big players. 

Good fast bowlers can always have a leading role though many feel that with so many newly laid pitches, spin might dominate. England obviously feel it will, and have called up Leicestershires captain Jeremy Snape as assistant coach for the competition. 

Snape, an off-spinning allrounder, was unlucky not to be picked for the last World Cup and has vast experience of one-day cricket. He also runs a sports psychology consultancy, which adds another dimension. Spinners are renowned fretters, especially when they are expected to be match-winners as well. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>