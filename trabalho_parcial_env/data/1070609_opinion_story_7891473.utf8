<DOC>
<DOCNO>1070609_opinion_story_7891473.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SMALL STATE, BIG STAKES

 - Goa has not really got the politicians it deserves

 Politics and Play - Ramachandra Guha

 ramguhavsnl.com

 Voters queueing up in Panaji, June 2, 2007

 Goas Raj Bhavan commands one of the loveliest views in the country. It is surrounded by water on all sides the estuary of the Zuari river on one side, the estuary of the Mandovi river on a second, the Arabian Sea on the third. The hillside on which it is located has a rich vegetation (all indigenous trees and shrubs none of that lantana or eucalyptus), and a bird-life to match. The building is suitably grand, with a charming (and still used) chapel tucked away at the back, and a grotto buried in the cliff below. 

 Now home to the representative of the president of India, for most of its history this building was the residence of the governor of the Portuguese colony of Goa. Till this Monday I did not know of its existence. On that day, I flew to Dabolim to attend a meeting of the University of Goa. From the airport I was driven to the Raj Bhavan where, to my great surprise, the meeting I had come for was to be held. As our hosts told us, it was in that very room that, later in the week, the new chief minister of Goa would be sworn in. 

 This was another and equally pleasant surprise. No sane man and few madmen either would turn down an all-expenses paid trip to Goa. When I had told the university I was free to come in the first week of June, I had no idea (nor, probably, had they) that my visit would coincide with the assembly elections. It is said that June a hot, sticky month is the worst time for the tourist to come to this part of the country. But for a political animal it was the perfect time. For here, as elsewhere in India, it is when elections are held that one gets the most intimate insights into the hopes and fears of the people. 

 The two main parties in the state are the Congress and the Bharatiya Janata Party. Before the elections, the singer, Remo Fernandes, remarked that the choice before the Goans was between a party that promoted communalism and corruption (the BJP), and another that promoted corruption and communalism (the Congress). But apart from these big, bad beasts there were a host of minor players in the fray. Other national parties who had put up candidates included Sharad Pawars Nationalist Congress Party and H.D. Deve Gowdas Janata Dal (S). Also active were two regional parties, the Maharashtrawadi Gomantak Party and the Save Goa Front, as well as a host of independents. 

 Goa is, by Indian standards, a prosperous and progressive state. In terms of human development, access to education and healthcare, for example, it ranks almost as high as Kerala. The economy is growing faster than Maharashtras. There is little crime and (thus far) little communal violence. While agriculture is declining, tourism and fisheries are thriving. Another important sector of the economy is mining, which generates money and employment but also degrades the environment. 

 Among the people I spoke to who included a writer, a scientist, a retired civil servant, and a taxi-driver I got the sense that the citizens of Goa had not really got the politicians they deserved. Given the high levels of education, and the relative social peace, the state could be in the vanguard of the knowledge economy. But instead of attracting software companies who would generate jobs and revenue, the politicians were more keen on cornering land to sell at a premium to real estate developers. 

 As a small and beautiful state, Goa feels itself peculiarly vulnerable. After it joined India in 1961, the principal threat appeared to be from the behemoth on its eastern border namely, Maharashtra. For several years, a merger with that state seemed a likely possibility. Through the Sixties, the ruling party in Goa, the MGP, campaigned in favour of joining Maharashtra. A referendum was held, which was narrowly won by those who wanted Goa to retain its autonomous identity. 

 In the decades since the option of joining Maharashtra was rejected, the uniqueness of Goa has been consolidated by an affirmation of a Konkani identity. How much of this identity is artificial and constructed, and how much solid and substantial, remains a matter of dispute. Konkani is not, of course, the only language spoken in Goa. Historically, the elite spoke Portuguese. As for the working people, along the coast most of them spoke Konkani, but in the interior many villages were Marathi speaking. 

 At the time of liberation, the three principal languages in Goa were Konkani, Marathi and Portuguese. Now the last has all but disappeared. But two new tongues have entered English and Hindi. These are the languages that the Goans use when conversing with visitors from other parts of India. But among themselves they speak either Konkani or Marathi. How many speak one, and how many the other? No reliable figures exist, but the sense I got was that, at least for public consumption, many Goans who were comfortable in Marathi would still say that their principal language was Konkani. It appears that this preference was related to the construction of Goa as, if anything, not-Maharashtra. 

 There is no fear anymore of Goa being swallowed up by Maharashtra. But there remain worries that the identity of Goa will be diluted or even destroyed by the wholesale migration of outsiders, whether pleasure-seekers from Mumbai and Delhi (and further beyond) or job-seekers from the adjacent districts of Maharashtra and Karnataka. The Goans I spoke to were concerned that this influx, if unchecked, would dilute their culture, destroy their environment, and corrupt their youth. 

 In the 450 years that they were under Portuguese rule, the Goans had never been allowed to choose their own leaders. But within a couple of years of coming under the rule of New Delhi they were able to do so. The political scientist, Benedict Anderson, has tellingly compared Indias treatment of Goa with Indonesias treatment of East Timor, that other Portuguese colony liberated by armed nationalists. As Anderson points out, Nehru had sent his troops to Goa in 1960 [actually, it was 1961] without a drop of blood being spilt. But he was a humane man and the freely elected leader of a democracy; he gave the Goanese their own autonomous state government, and encouraged their full participation in Indias politics. In every respect, General Suharto was Nehrus polar opposite. 

 Despite their cynicism with regard to politicians, the Goans have taken with relish to electoral democracy. In this, their latest tryst with the ballot box, they voted in very large numbers. Nearly 70 per cent of the electorate turned out, a higher percentage than in most other states of the Union. When I reached Goa I was told that the most likely outcome was a hung assembly, with neither the Congress nor the BJP getting a majority. By the time I had left that prediction had been invalidated. With the Congress and its partners scraping a narrow majority, at least no legislators would be bought and sold. Whether the Goans will now get the ministers they deserve is another matter altogether.




</TEXT>
</DOC>