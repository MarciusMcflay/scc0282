<DOC>
<DOCNO>1070706_opinion_story_8015254.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 PRODUCE THE WITNESS, PLEASE

 Against the Bombay sepoys

 The Indian Mutiny By Julian Spilsbury, Weidenfeld amp; Nicolson, 20

 Who else but a journalist and that too from the Daily Telegraph would have the effrontery to write a book on history without a single reference or footnote, indeed without even a bibiliography? This is to stretch the readers credibility a bit too far. 

 The blurb claims that Spilsbury has woven his narrative around eyewitness accounts. This could very well be true, but what are these eyewitness accounts? Who wrote them? What is their provenance? Where did Spilsbury read them? Have other historians/writers read these accounts before Spilsbury? If so, how does Spilsburys account differ from those previous readings? One looks in vain for an answer to any of these questions in Spilsburys book. He unashamedly violates every single established norm of constructing a historical narrative.

 Reading his racy narrative, written in the manner of G.A. Hentys novels for schoolboys when Britannia ruled the waves, a reader unfamiliar with the rich literature on the revolt of 1857 might get the impression that Charles Ball, John Kaye, T. Rice Holmes, G.W. Forrest, S.N. Sen and other historians have not written at all. The argument that a popular book, like the one that Spilsbury has written, need not be weighed down by footnotes and references does not hold at all. Christopher Hibbert and William Dalrymple have both shown that footnotes and references have nothing to do with readability. What about a bibliography? Doesnt Spilsbury owe his readers this much?

 Spilsbury commits too many egregious errors, some of them would not be passed by the Daily Telegraphs desk. To take just a few. He describes the talukdars of Awadh (Oudh) as village headmen. The king of Awadh, Wajid Ali Shah, who was deposed by Lord Dalhousie, is made into the Nawab of Awadh. Spilsbury describes with brio James Neills march from Benares to Cawnpore in the summer of 1857, and notes also the indiscriminate violence of that counter insurgency operation. What he completely fails to note is that by a series of Acts passed in May and June, 1857, individual Britons were given the power to judge and take the lives of Indians without recourse to the due processes of law. This permitted the hanging parties to go out into the North Indian countryside. Spilsbury misses this because he is writing solely on the basis of eyewitness accounts, and is therefore clueless about the overall policy which made such violence possible.

 Spilsbury doesnt even make an attempt at an analysis. The really big question is why a well-known publishing house like Weidenfeld and Nicolson chose to publish this book. Did it go through referees which is the standard practice for all books? The reasons for the callousness, authors as well as publishers, were probably two-fold. One, the 150th anniversary was too good an occasion to let slip. Second, in the world of British publishing, so far as Indian history is concerned, anything goes. 

 It is possible that all of Spilsburys account is factually correct and based on valid documents. But how is a reader to judge that, since the sources are not given? How does one trust an author who doesnt extend to his readers the common courtesy of sharing his sources and their origins?

 RUDRANGSHU MUKHERJEE




</TEXT>
</DOC>