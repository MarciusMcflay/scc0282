<DOC>
<DOCNO>1070804_opinion_story_8144557.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 BURNING QUESTIONS 

 Ananda Lal

 Theatre

 As Utpal Dutt did decades ago, 

 renaming the Little Theatre Group as Peoples Little Theatre, 

 Little Thespian may consider masking the diminutive in its 

 name. Calcuttas 14-year-old leading Urdu group has certainly 

 matured. Its career takes a firmly political turn with the 

 enigmatically-titled ? (picture), based on the 

 English play, They Burn People, They Do, by South 

 Africa-born London resident, Ismail Choonara. 

 He wrote it about Godhra, though 

 the structure may faintly recall Peter Weisss Marat/Sade, 

 set inside a madhouse. Four mentally challenged men, one 

 of them Muslim, live together amicably in a hospice under 

 a widows care. Each of them dreams of a specific person 

 or event to free them. What happens instead is not something 

 they could have anticipated. The haven of security they 

 knew becomes an inferno as the external world smashes in, 

 making their once-cloistered asylum look much saner than 

 the sheer insanity outside.

 Enactments of collective violence 

 invariably shake audiences. So the director, S.M. Azhar 

 Aalam, hasnt done anything particularly new in this area. 

 The plot is simple, too. But sometimes things need to be 

 put simply and directly, and Aalams abrupt concluding use 

 of a mannequin and Joy Sens lighting shock viewers with 

 an unforgettably horrifying image. 

 Little Thespian also runs a series 

 called Katha Collage concurrently, inspired by D.R. Ankurs 

 invention of kahani ki rangmanch in 1975. However, 

 one fears this is not as forward-looking an exercise. Ankur 

 resorted to retelling fiction on stage at a time when original 

 Hindi drama had virtually died. Bereft of texts, he took 

 up short stories, not adapting them conventionally but relating 

 them in their intended form, therefore with substantial 

 narrative passages. I have never liked this method, whether 

 by Ankur or occasional followers like Usha Ganguli, because 

 it falls between two stools, neither theatre nor fiction, 

 and often upon weak narrators.

 In Katha Collage 1, Aalam 

 revives his Manto ne Kaha by adding Aulad 

 to the two Manto stories he had dramatized previously. Aulad 

 depicts the gradual mental disintegration of a childless 

 woman. Katha Collage 2 is a more innovative concept, 

 comprising Hindi and Urdu stories by Calcutta authors Shailendra 

 Srivastava, Sayeed Premi, Madhu Kankria and Anis Rafi. Three 

 of these are linked by their common focus on problems faced 

 by women. 

 Srivastavas Sapne ki Bat 

 questions the Hindu custom that disallows women from joining 

 funeral processions. Premis Rehai presents the social 

 exploitation of wives as male property. Kankrias File 

 examines the supposed rehabilitation of a prostitutes daughter, 

 the NGO file on whom constantly reminds her of her past. 

 It ends with a striking picture of the activist burning 

 the pages of the file. Rafis Polythene ki Diwar 

 criticizes well-off authors who choose not to write about 

 common people. 

 Uma Jhunjhunwala directs this 

 quartet, only partly solving the above-mentioned difficulties 

 of this genre. Little Thespians team of actors has definitely 

 come a long way, but requires one or two extra actresses 

 to strike a balance.

 Ankurs own new production from 

 Mumbai, Sambhavs Hamse Tumse Pyar Karega Kaun !, 

 formed the centrepiece of Ganakrishtis festival, low on 

 outside participation this year. As if to deliberately prove 

 my theory wrong, it vindicated his style for the first time. 

 Perhaps after retiring from the National School of Drama, 

 Ankur is able to shed his cares and devote his attention 

 exclusively to theatre.

 Actually Vijay Tendulkars first 

 story (from 1947, before he turned to playwriting), it begins 

 slowly but picks up and peaks at the right moment. Pressured 

 by mother and uncle, a son agrees to an arranged match but 

 reneges when he sees that the girl is dark-complexioned. 

 She, however, insists on knowing his reasons. Tendulkar 

 plays with social mores, as is his wont, and true to the 

 title, theres a sparkling satire of film romance and other 

 fantasies. The couple separate eventually, after realizing 

 the utterly wrong, conditioned notions that society puts 

 into our heads. 

 Why does HTPKK succeed 

 where others didnt? Ankur selects a story that has more 

 dialogue to begin with, and dramatizes it much more, relegating 

 the narratives to the margin. Also, the acting level is 

 high in the main roles (Sushil Bounthiyal and Sonamoni Jain).




</TEXT>
</DOC>