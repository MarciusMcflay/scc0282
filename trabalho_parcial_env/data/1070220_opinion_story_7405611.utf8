<DOC>
<DOCNO>1070220_opinion_story_7405611.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 UNQUIET FLOWS THE CAUVERY

 The final award of the Cauvery waters dispute tribunal has raised more critical questions than just the problem of water-sharing, writes M.R. Venkatesh

 Dissenting voices

 At a recent lecture, Amartya Sen passionately pleaded for public reason, goodwill and voluntarism over crude utilitarianism in the larger context of public health. But the politics of the Cauvery waters issue seems to preclude that possibility. The tussle, which has seen more litigation than conciliation, is said to have benefitted lawyers more than the distraught farmers in Karnataka and Tamil Nadu.

 Asking for a level playing field in so complex a dispute may amount to committing what philosophers would call a category mistake. The geography, ecology and political boundaries of the river do not lend themselves to mathematical formulae. However, the much-awaited final award of the Cauvery waters dispute tribunal does not hedge the issue. It has upheld the status quo that formed the kernel of its interim award of 1991. Nevertheless, it has also made some important modifications. 

 Taking the current total availability of water in the entire Cauvery basin at 740 tmcft annually, together with a realistic appraisal of the monsoons, the tribunal has done two things. First, it decreed the annual allocation entitlement of the Cauvery waters to the basin states. For this, the tribunal depended on the report of a technical team which studied all aspects, like the extent of agriculture, additions in acreage and expansion in cropped area, seepage and so on. Tamil Nadu now gets 419 tmcft (against its demand of 562 tmcft), Karnataka 270 tmcft (against a demand of 465 tmcft), Kerala 30 tmcft and Puducherry 7 tmcft. A small quantity of water has been left for environmental protection.

 The second big thing is to specifically direct Karnataka to make available 192 tmcft of water annually to Tamil Nadu through a prescribed schedule of monthly deliveries in a normal monsoon year. This is to ensure that the farmers of Tamil Nadu, where the bulk of the Cauvery delta is spread out, are ensured a reasonable and predictable supply of water. In water disputes the world over, the lower riparian state gets priority in the apportioning of the quantum to protect its already existing agricultural area. On its part, Tamil Nadu will have to allocate 7 tmcft to Puducherry from what it gets. 

 The final award has made a significant departure on where the water is to be delivered by Karnataka. The measuring point in the interim award was Mettur reservoir. But now, in a significant victory for Karnataka, which, over the years, has aggressively stepped up its area under cultivation, the water is to be delivered to Tamil Nadu upstream of Mettur at the Central Water Commissions gauge and discharge station situated in Billigundulu.

 For Tamil Nadu, the silver lining lies in the fact that the water yielded in the roughly 80 kms between Billigundulu and Mettur assessed at 25 tmcft in a normal monsoon year will automatically go to Tamil Nadu. This ensures, at least on paper, an assured availability of 217 tmcft of Cauvery waters to the state.

 Although the proposed regulatory authority is supposed to monitor the actual releases of water, the final award is rather vague on water-sharing in a distress year. It says that when the water yield in the Cauvery basin in a distress year is less than the estimated 740 tmcft, the allocated shares to the respective states shall be proportionately reduced. On the face of it, this seems a fair deal. But both Tamil Nadu and Karnataka are worried about it and want to take it up in their review petitions. Karnataka is also piqued over the month-wise deliveries when it will be hard-put to release waters in the absence of sufficient inflows into its reservoirs.

 The river water dispute, which goes back to the British era, and has been accentuated since the Fifties owing to the development needs of the upper riparian state of Karnataka, has seen a lot of bloodshed and consumed the energies of rulers and administrators for decades. The practical difficulties of water-sharing has been worsened by the spurt of linguistic chauvinism on both sides. Naturally, the problem has given political parties and even fringe outfits full opportunity to politicize the issue. There have been numerous rounds of talks since the Sixties. The Tamil Nadu chief minister, M. Karunanidhi, himself was involved in talks with at least 11 Karnataka chief ministers and six Union water resources ministers. Matters came to the tribunal when, acting under the apex courts directive, the then prime minister,V.P. Singh, referred the dispute to a tribunal in 1990. The tribunals prompt interim award in June 25, 1991, was greeted by large-scale protests, arson and riots. The developments have intensified the stand-off between the two states. 

 The irony is that in a year which sees good rainfall, as was the case during 2005-06, there is simply no dispute. Karnataka cannot hold the excess inflows in its dams and thus Tamil Nadu gets its water. But when the monsoon is poor Tamil Nadu saw three consecutive droughts from 2001-02 and acute farm distress Karnatakas refusal to release any water to the lower riparian state exacerbates the conflict to irrational levels.

 However, the tribunals final award, on which the farmers had pinned their hopes, makes it a no-win situation. Tamil Nadu, still dependent as much as before on Karnatakas magnanimity in its hours of crisis, has not gained much despite the allocation. Karnataka, given that it is being seen as the loser for having to release 419 tmcft of Cauvery waters to Tamil Nadu, is already thinking of ways to duck responsibilities. 

 The apprehensions, in fact, have complicated matters. For instance, Tamil Nadu has found its permissible paddy cultivation area reduced from 29.30 lakh acres to 24.71 lakh acres. It is also shocked that the 1924 agreement, which it still considers valid, will be superseded now. For Karnataka, there is more bad news apart from the water it will have to part with. Its summer crop has been disallowed by the tribunal. Kerala too is unhappy with the quantum allotted to it. Now all the three chief ministers are up in arms against the disadvantageous aspects of the award.

 Sadly, the political leadership in the basin states hardly seems to have realized what the noted economist and civil servant, S. Guhan, had warned of in his monograph, The Cauvery River Dispute. Unlike the other major river disputes in India, including those related to Narmada, Krishna and Godavari, the one about Cauvery differed in a fundamental respect. While those disputes were mainly about the inter-state utilization of hitherto untapped surplus waters, the dispute in the case of the Cauvery relates to the re-sharing of waters that are already being almost fully utilized in their totality, Guhan pointed out. 

 This basic dilemma was echoed after the award was announced not so much by politicians but by leading farmers like S. Ranganathan, the general secretary of the Tamil Nadu Cauvery Delta Farmers Welfare Organization. Given the dwindling total water availability in the Cauvery basin, it was a bit of divine work that enabled the tribunal, after 17 years, to arrive at an overall pragmatic distributive formula among the basin states, said Ranganathan.

 The Cauvery waters dispute is no longer just a numbers game. The final award has brought to the fore more critical issues, such as prudent management of available water resources, sustainable agriculture and impact of global warming on rainfall patterns. Above all, it has voiced the urgent need for a convergent approach on the part of all the stake-holders. In considering the final award of the tribunal, the Cauvery basin states will have to keep in mind the fact that they have before them what can be called a give-and-take situation one which makes everyone slightly better off and none worse off.




</TEXT>
</DOC>