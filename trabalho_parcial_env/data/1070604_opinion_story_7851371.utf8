<DOC>
<DOCNO>1070604_opinion_story_7851371.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ECOCIDE IN THE OCEANS

 Fifth Column - GWYNNE DYER

 When the annual meeting of the International Whaling Commission opened in Alaska on May 28, Japan declared that it planned to kill fifty humpback whales as well as the usual minke and fin whales next year in its scientific whale-hunt (catch them, count them, and sell them as food). Humpbacks were heading for extinction when the IWC agreed a moratorium on all commercial whaling in 1986. Understandably, the place erupted in protests.

 Australian Environment Minister, Malcolm Turnbull, called it a highly provocative act, but it is also a carefully calculated one. Japans real goal is to get commercial whaling re-started, and it offered to drop the plan to kill humpbacks if the IWC approves a return to limited commercial whaling by four Japanese coastal villages. Just four little villages for now, and strictly limited numbers of whales but the 1986 moratorium on commercial whaling would have been broken.

 The pro-moratorium countries at the IWC understand Japans tactics and will not make that deal, reckoning the lives of fifty humpbacks are less important than the principle of no commercial whaling. The killing of fifty humpbacks is regrettable, but it will not endanger a species that has gradually recovered to sixty or seventy thousand since the moratorium was imposed.

 We care about whales now, but the fish of the oceans benefit from no such sentiment, and they are now going as fast as the whales once were. In fact, according to a report last year in Nature, the scientific journal, 90 per cent of the really big fish are already gone, and the middle-sized fish are following.

 Not an easy job

 The codfish are gone on the Grand Banks of Newfoundland, once the richest fishery in the world, and show little sign of recovery despite an absolute ban on cod-fishing for the past 15 years. They are declining rapidly in the North Sea, too. In the Eighties, the annual catch was about 300,000 tonnes. At this point, 29 per cent of fish and seafood species have collapsed; that is, their catch has declined by 90 per cent, explained Professor Boris Worm of Dalhousie University late last year. It is a very clear trend, and it is accelerating. If the trend continues, he predicted, all fish and seafood species that are fished commercially will collapse by 2048.

 Individual fishermen, up to their ears in debt for their high-tech boats and equipment, cannot reverse this trend because they have to go on fishing. Governments could cut the huge subsidies they give to their fishermen, and above all to the bottom-trawlers that are systematically turning the floors of the worlds oceans to mud, but they are unwilling to face the political protests of well-organized fishing lobbies. The systematic destruction of the worlds fisheries will continue unless some body equivalent to the IWC takes charge. And how likely is that?

 Not very. Or at least, an International Fisheries Commission with global regulatory authority is only likely to be accepted, as the IWC was, when all the commercial stocks have already collapsed. Yet fast-breeding fish can recover far faster than whales.

 A major human food source the principal source of protein for one-fifth of the human race is going to collapse in the next generation unless drastic measures are taken. The worlds fishing fleet needs to be reduced by at least two-thirds, bottom-trawling must be banned outright, and widespread fishing moratoriums for endangered species and even for whole areas need to be imposed for periods of five or even ten years. 

 Unfortunately, the minimum measures needed to prevent ecocide in the oceans would cause major short-term disruption and throw millions out of work. It will be much easier, politically, to ignore what is happening now and let the collapse happen later, on somebody elses watch.




</TEXT>
</DOC>