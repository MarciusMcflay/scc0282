<DOC>
<DOCNO>1070429_calcutta_story_7707381.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Amazing lightness of being

 Patrick Reyntiens speaks to Soumitra Das about the magic of stained glass that once captured the European imagination

 Patrick Reyntiens with Katayun Saklat, the only Indian artist taught by him. 

Picture by Pabitra Das; (below) a work by Reyntiens 

 Why is little heard of stained glass artists at a time when internationally the price of art work is going through the roof?

 Patrick Reyntiens, one of Europes most reputable stained glass artists who was here for five days till Sunday, says point-blank that since publicity is directly connected with profit, and stained glass is immovable and hence cannot yield gains it is never in the news. 

 Moreover, stained glass is not in metropolitan positions. It is spread all over the countryside. Stained glass tends to be attached to a historically proved mental-spiritual situation. It cant be taken out like sculpture of Hindu temples. Once it is fixed it is there for the next 2,000 years, says Reyntiens, who was born in 1925 and lives in the countryside in Somerset. He is travelling the world and has been invited for teaching assignments by American institutions. 

 Reyntiens is immaculately dressed jacket and tie in this killing heat and has a physical sense of humour. He is thickset but goes spasmodic as he re-enacts his turbulent flight to the city.

 Sean Connery in his modelling days

Reyntiens remarkable accent is a throwback to the early decades of the last century. When his friend Sean Connery, whom he has known since the days the James Bond incarnate used to be an artists model, called recently, Reyntiens said: Artists, aristocrats and dogs do not retire because they are. Connery retorted: I understand, I am a bit of all three.

 Reyntiens says during the last war he had joined the army and then became an artist as if that was the most natural thing to do. He explained afterwards that I knew I was going to become an artist at six. I always painted and drew.

 When he decided to get married, he joined a stained glass studio at 3 a week. Before that he had been trained in drawing, painting and printmaking.

 I drew three hours a night five days a week. Anybody trained there became very good at draughtsmanship. Glasgow and Edingburgh were still in the 19th Century even after 50 years in the 20th Century.

 He joined the studio of E.J. Nuttgens, got married and started his own career. And he has been at it for 60 years. It doesnt seem to be like 60 but it is, he says resignedly. 

 Stained glass was a historical remnant of the Arts amp; Crafts movement and not my way of working. It is a major agent of producing colour in architecture. Most architecture is absolutely white. However, few modern architects use stained glass.

 Up to age 35 he created non-figurative stained glass but thereafter went back to human figure. Reyntiens is known for translating the paintings of celebrated English artist John Piper into glass. He is collaborating with artist Graham Jones on a church in Germany with eight windows, each 35 ft high and seven ft broad, and is creating a 40 ft figure of Christ. 

 Reyntiens, occasionally, talks unprovoked. He mentions in passing the non-figurative glass (mostly pattern) of German artists like Ludwig Schaffrath and Johanees Schreiter and the psychological effect of stained glass: If you surround people with brilliant light they are highly active. When you take the light down as in Egyptian temples and medieval churches with a wealth of stained glass, the eyes are under-stimulated and the brain becomes contemplative.

 It is useless to have stained glass in a football stadium.

 As a footnote Reyntiens adds: The best stained glass is in America in airports people experience these for 10 minutes. A fleeting glance. 




</TEXT>
</DOC>