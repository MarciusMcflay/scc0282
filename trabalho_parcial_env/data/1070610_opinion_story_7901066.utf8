<DOC>
<DOCNO>1070610_opinion_story_7901066.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FARM FATALE

 An excess of role-playing may create confusions in the soul about identity. But with an incensed Mayavati on the rampage, such spiritual confusions, especially when they translate into material transactions based on dubious paperwork, have little chance of being overlooked. Amitabh Bachchan is being made to feel the heat. He had been supporting his claim to a spot of agricultural land in Lonavala, Maharashtra, with his title as farmer owning farmland in Daulatpur in Uttar Pradesh. Only no one wants to believe that Mr Bachchan is a farmer; there is a tendency to dismiss his farmer identity as fraud. Which is such a pity. There is at least one solid claim he has to farmer-hood. The chief minister of West Bengal, Buddhadeb Bhattacharjee, has been insisting that farmers sons do not want to be farmers. Mr Bachchans son, therefore, is far from being a farmer. He is the husband of Aishwarya Rai. Could there be better proof? And all this apart from Veer-Zara.

 Perhaps the dream of being the star farmer had so overcome Mr Bachchan Seniors good sense that he lacked finesse in making the deals. India abounds in farmhouses and land belonging to his colleagues in films and politics. Aamir Khan has no problems going to town with his family identity as agriculturist. Perhaps Ms Mayavati should pause to consider the possibility of noble motives in Mr Bachchan Seniors self-identification too. Mahatma Gandhi had identified himself as farmer and weaver in court at his first trial in India. He was representing the poor. Mr Bachchan Senior may be following in the footsteps of the great.

 It is true that the lifestyles of Mr Bachchan and Mahatma Gandhi are rather different. The glittering worlds of the neo-agriculturist and the would-be farmer are so distant from those of the starving and suicidal farmers of, say, West Bengal or Andhra Pradesh, that it is quite difficult to discern a bridge between the two. In Mr Bachchan Seniors case, the bridge is proving to be the trap. There is a law in Maharashtra that agricultural land cannot be given to non-farmers. Evidently, that is to protect genuine farmers. But earlier projects in Lonavala have shown that the law can accommodate industrialists and builders in its generous sweep. And the land in Daulatpur, allegedly meant to help penniless farmers, had apparently been given to Mr Bachchan Senior by Mulayam Singh Yadav. As chief ministers changed, so did the fate of the transfer papers. And Ms Mayavati, the Taj corridor controversy neatly behind her for the moment, is full of vim in her search for corruption and fraud among her foes. In a top-ten rating, there are too many ties for first place in greatest hypocrisy. But there is a moral to the tangled tale. Instead of the age of industry, or information technology, or whatever, this should be called the age of farmers.




</TEXT>
</DOC>