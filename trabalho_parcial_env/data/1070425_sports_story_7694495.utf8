<DOC>
<DOCNO>1070425_sports_story_7694495.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Panache, verve and unbeatable Australians

St Lucia: From the moment they stepped on to the Caribbean soil in St Vincent, Ricky Pontings band of marauders has been on a mission. 

Even judged by the exalted standards of previous Australia captains, Ponting is a perfectionist. Over the past Australian season, he has forged a team in his own image. 

Before the summer had properly started, Australia captured the only major trophy missing from their cabinet by winning the Champions Trophy. 

Then they avenged their 2005 Ashes defeat by thrashing England 5-0. Still not satisfied, and with the added stimulus of upset losses to England in a tri-series final and then to New Zealand in the three Chappell-Hadlee matches, Australia targeted the World Cup to complete an unforgettable season. 

Their progress has been remorseless since they beat England in the warm-up week in St Vincent. 

England were beaten again, the West Indies, South Africa and Sri Lanka brushed aside and New Zealand humiliated by a record 215-run margin. The last three are the other semi-finalists with South Africa earning the unenviable right to play Australia in St Lucia on Wednesday. 

Statistics need to be treated warily in one-day cricket but in Australias case, they do tell a tale. 

Matthew Hayden has scored three centuries and compiled the most number of runs in the tournament so far with 580 at an average of 82.85. Four of the top 10 in the batting averages are Australian. 

One of them, Brad Hodge, has played as a substitute only when first-choice Andrew Symonds and then Shane Watson were injured. 

Australia were expected to have the best top-order batting at the World Cup. 

Three of their top five batsmen Ponting, Michael Clarke and Andrew Symonds also form the best infield, a trio of predatory prowlers within the circle on the off-side during the Powerplays. 

What will have given Ponting particular satisfaction is the performance of his bowlers. Glenn McGrath, 37, was believed in some quarters to be susceptible on slow Caribbean pitches. 

Tait, sensitively handled by Ponting, has visibly matured throughout the tournament into a strike bowler with the potential to match the giants of the past. 

Hogg, who endured five wicketless internationals against England and New Zealand, is finding Caribbean wickets ideal for his jaunty left-arm wrist spin. 

Hes bowled beautifully through the tournament, bowling as well now as he ever has and hes beating a lot of batsmen with his variations, Ponting told reporters recently in Grenada. 

Supporters of other sides and some neutrals have found Australias dominance in the West Indies oppressive, recalling the old saying about the New York Yankees in their pomp: Rooting for the Yankees is like rooting for US Steel. 

Others admire their drive to improve based on a relentless work ethic, noting that a typical Australian training session seems more exhausting than the average limited-over international. 

In contrast to just about everybody else, the entire team is always practising some facet of their game at full intensity. 

Its a very motivated team, Ponting had said after the match. A lot of champion players and you dont have to motivate champion players. 

 (REUTERS) 




</TEXT>
</DOC>