<DOC>
<DOCNO>1070908_frontpage_story_8291434.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Smells like Asian Nato

 SUJAN DUTTA 

 A crew member holds tow chains on the USS Kitty Hawk in the Bay of Bengal on Friday. (AFP) 

 Sept. 7: The five-nation Malabar war games are being conducted on rules and procedures compliant with the requirements of the North Atlantic Treaty Organisation, Indian naval and air force officers disclosed in interviews aboard the aircraft carrier today.

 The Malabar 07-02 war games, now into the fourth day, have raised concerns in Beijing of an emerging Asian Nato. But Vice-Admiral William Douglas Crowder, commander of the US Seventh fleet, insisted this was not gunboat diplomacy directed against China but an opportunity to share experiences in a multi-threat scenario. 

 The acceptance by India of the standard operating procedures (SOPs) proposed by the US in the lead-up to the exercise meant that the navies could draw up gameplans to exploit most skill-sets.

 For the first time, manoeuvres like air-to-air refuelling have been possible with US aircraft, officers from Indian ships and from an air force maritime strike squadron said. They were on board the USS Kitty Hawk to observe the arrested landings and catapult shots that launch and recover the US Navys aircraft.

 The common procedures for this exercise were worked out in four initial planning conferences between the participants. There are so many navies involved that it was important to ensure that the glitches be smoothened out, an officer explained. 

 The evolution and implementation of the Nato-based SOPs are not sudden but are a consequence of the 13 episodes of the Malabar series of exercises between the US and Indian navies. The exchanges intensified over the last five years. 

 The current war games are the second this year but the first in Indian waters involving 24 ships, a nuclear submarine and more than 200 aircraft from five navies.

 The SOPs could signal a paradigm shift for the Indian armed forces that have so far evolved their own practices.

 Those rules were traditionally influenced by the erstwhile Soviet Russia-led Warsaw Pact. It was logical because it came with the Russian hardware that has equipped the Indian army, navy and air force for decades. 

 Nato is essentially a military alliance led by the US against the erstwhile Soviet Russia-led Warsaw Pact. Since the end of the Cold War around 1991, Nato has repositioned itself as a coalition in Americas global war against terror and has itself shown eagerness to work with Indian forces. 

 In the current exercise Malabar 07-02 those efforts have begun to mature. 

 The common procedures meant that the participants were able to engage one another despite differing practices. 

 An example: in joint sorties worked out on the Nato-prescribed SOP for this exercise, US Navy F/A-18 Superhornets shooting off this carrier and also from the USS Nimitz refuelled Indian Naval Sea Harrier aircraft that flew out of Indias flagship, the INS Viraat. Between September 4 and today, there have been 20 such sorties.

 I think it was remarkable to see our Superhornets refuel the (Indian) Sea Harriers, said Crowder. We did not charge for the fuel, though, he joked. Just after he finished speaking, Indian Harriers and US Superhornets overflew the Kitty Hawk in a victory (V) formation. 

 Crowder this morning handed over tactical command of the exercise to Vice-Admiral R.P. Suthan, Indias flag officer in charge of the Vizag-headquartered Eastern Naval Command. 

 The other fleet commanders on board are Vice-Admiral Yoji Koda, the commander-in-chief of Japans self defence fleet, and Vice-Admiral Nigel Coates of the Royal Australian Navy. 

 The effect of the common SOPs has been extended to other sectors of the exercise as well in communications, anti-submarine warfare drills and in offensive and defence air manoeuvres. 

 In other scenarios, Superhornets from the Kitty Hawk and the Nimitz met at an RV rendezvous point to escort Jaguar maritime strike aircraft flying out of Car Nicobar to simulate an attack on the Viraat. 

 They were tasked to beat through the Combat Air Patrol of the Viraat-based Sea Harriers. Asked if the attack was successful, an officer said: They overflew the Viraat. 

 Similarly, the Nimitz and the Kitty Hawk were also designated as targets. Indian Sea Harriers, far behind in technology when compared to the Superhornets, tried to engage the US naval aircraft in close combat. The US aircraft relied mostly on BV (Beyond Visual Range) missiles and were guided by communication from the E2C Hawkeyes, the airborne early warning and control systems with distinctive rotating domes. 

 We managed to dodge, too, the officer said and overflew the Kitty Hawk. 

 (The USS Nimitz has completed its deployment for this exercise and is now headed to the Persian Gulf. It left the exercise area at 1 am on Friday).

 The designated exercise area is 150 nautical miles by 200 nautical miles and the airspace above it. This morning, the Kitty Hawk, where the fleet commanders had gathered, was sailing about 100 nautical miles west of Port Blair. 

 The US allowed the use of its Centrix system, a platform for battlegroup-networking that facilitates exchange of sound, pictures and data among participating ships, to the Indian Navy. 

 This is not so new for Australia and Japan which have been in a military alliance with the US, and not even for the Singaporean navy that believes in the virtues of working along with a military coalition.




</TEXT>
</DOC>