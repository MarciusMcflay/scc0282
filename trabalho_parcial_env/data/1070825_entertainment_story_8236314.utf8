<DOC>
<DOCNO>1070825_entertainment_story_8236314.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Battle the baby bulge

 Fitness pro Mrinalini MUkherjee guides you through the challenging process of getting back in shape after giving birth

 Motherhood doubles the joys of life, but at the same time brings with it the battle with post-pregnancy bulge. It does become harder to look your pre-pregnancy best, unless you do something about it. Just remember: there can be no quick fixes here as you and your body need time to recover from the childbirth experience. Also, a slow pace will ensure the weight stays off.

 For many mothers, having a baby seems to be a great excuse not to exercise. After all, youre legitimately busy, sleep-deprived, and probably a bit nervous about leaving your precious baby with someone else while you head to the gym. But remember that to raise your metabolism, exercise coupled with good nutrition is the only solution and that will help you gain more energy to enjoy your bundle of joy!

 Be realistic, judicious and consistent. There is no point in setting a programme that you will not be able to follow. Most women can begin a formal exercise programme within six weeks of giving birth, though it might be slightly longer for some women, including those recovering from a C-section. How soon you begin an exercise programme will depend on how you feel. A gentle exercise and good nutrition plan drawn up by professionals should get you started.

 Work out for at least 30 minutes, at least three to five days a week. If you werent active during your pregnancy, start with a 15-minute programme and gradually increase to 30 minutes. If you tapered off your fitness routine as your pregnancy progressed, begin at the level you stopped at and increase the intensity or time as you feel ready. Keep your system hydrated at all times. If you develop any aches or pains, slow down or stop and take adequate rest or better still, consult your doctor.

 TARGETED EXERCISE

 Kegel exercises, small contractions of the muscles at the vaginal wall and opening, are extremely beneficial to strengthen and repair the pelvic floor muscles. To identify these muscles, you have to try to stop the flow of urine mid-stream and then allow it to flow again. The muscles you use for this are the pelvic floor muscles. Try to contract them without urinating. If your stomach or buttocks muscles tighten, you are doing it wrong. Tighten the muscles for three seconds and then relax them for three seconds.

 For pelvic tilts, lie on the floor with your legs bent, feet resting on the floor. Slowly tighten your abdominal muscles and roll your pelvis towards you. Breathe evenly and try to initiate the movement with your abs. Dont forget to tighten the pelvic floor, as otherwise it may place pressure on it and stretch it further. Another option is isometric contractions you can do with your baby. Lie down with knees bent and feet flat on the floor, placing the baby on your belly. Inhale and lift the baby up, pulling your abs in and squeezing them like a sponge. Exhale and lower the baby back down. Repeat as many times as you can. (If you have had a C-section, you will have to give your body at least eight weeks before trying this.)

 AEROBIC EXERCISE

 Slow walks early on will not only help you feel youre getting back into a fitness routine, but will also help relieve tension. Dont push yourself work to establish a regular walking time and keep a steady pace.

 A full-fledged return to aerobic activity would be after six weeks or once the doctor gives the green signal and then you can take to brisk walking, swimming and low-impact aerobics. To get the most out of your workout, exercise at 60 to 80 per cent of your maximum heart rate. This means that you should feel a little out of breath but still be able to hold a conversation.

 Swimming is one of the best forms of exercise you can do, offering a relatively weightless environment which helps prevent injury whilst working out the large muscle groups and cardio-vascular system. Aqua aerobics classes can also prove beneficial.

 CORE STRENGTH

 Yoga/pilates/body balance exercises help with posture, strength, relaxation and flexibility. They are great for getting abdominal and pelvic floor muscles working again. The techniques used in pilates have been described as the ultimate body conditioning workout. Through a series of progressive slow measured movements, pilates focuses on developing core abdominal strength and relieving lower back pain. Pilates will not only benefit muscle tone and strength, but will also focus on breathing and postural alignment. 

 The practice of yoga helps you invigorate both body and mind. Yoga is designed to support your recovery from the physical and emotional stresses of pregnancy and giving birth. The practice strengthens and de-stresses muscles, helps to ease the stress of sleep deprivation and enables you to refocus both mind and body. Awareness of your own body is promoted and special emphasis is given to strengthening the abs and lower back, opening the hips and stabilising the spine. The workout will help you maintain balance while creating power in the legs and lower body.

 Shoot your fitness queries to t2abpmail.com 




</TEXT>
</DOC>