<DOC>
<DOCNO>1070219_calcutta_story_7403701.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 When art breaks the boundaries

 SOUMITRA DAS

 CROSS-FERTILISATION CURRENTS: Visitors at the exhibition at Victoria Memorial. Picture by Pabitra Das

 The statue of the Marquis of Wellesley stares at the blank wall. A hybrid creature with the head, torso and legs of a woman, equine rump and one hand resembling a donkey head stands next to the marble likeness of the man who once ruled this land. It was carved out of teak wood by Mumbai-based Karlo Antao.

 Next to them is a tableau of naked and winged chessplayers made by Santiniketan-based Pankaj Panwar.

 In the great central hall, a youthful Victoria Regina looks from her perch at the milling crowds gaping at what look like discarded and disconcerting Batman costumes hanging limply from the grand columns. These are by Luca Matti, who lives and works in Florence.

 At two ends of the hall above two entrances are hung two canvases printed with images of deities borrowed wholesale from Indian calendars. The intervention of Fabrizio Passarella is noticeable as Garuda has a huge erection, and the holy icons are juxtaposed with erotic miniatures. Passarella lives and works in Bologna.

 You discover similar copulating couples in the painted photographic images superimposed on Sachin Karnes take on Rembrandts Night Watch. Karne lives and works in Baroda.

 Victoria Memorial Hall, which was meant to be the Valhalla of the Empire, has turned into a temple of multi-culturalism. An exhibition titled On the Edge of Vision, New Idioms in Indian and Italian Contemporary Art, was inaugurated here last Monday evening by Italian Prime Minister Romano Prodi.

 This exhibition, with 12 Indian and 30 Italian participants, is curated by Rajeev Lochan, director, National Gallery of Modern Art, Delhi, and Lorenzo Canova. What viewers may find a little strange is that it is difficult to distinguish the works of the Indian artists from those of their Italian counterparts.

 True, globalisation is breaking down borders and indigenous cultures are finding it difficult to hold their own, but is cross-fertilisation really that prevalent? Or are aesthetic products being tailormade keeping the international consumer in mind? Is it a one-way traffic, with Western influence being equated with and dubbed as global? Can wannabe economic superpowers ever resist this juggernaut? These are questions that confront us while viewing these works from two cultures that have little in common. 

 And perhaps Indian artists should keep these concerns in mind, for what can be more syncretic than the culture of our country? But in the past even, when we appropriated idioms, icons and metaphors wholesale from other cultures, we never lost our identity.

 Yet, when one looks at the superb finish of the canvases of both Jayshree Chakravarty of Calcutta and Atul Dodiya of Mumbai hanging close to each other, one can discover close affinities between their work and that of Alberto Di Fabio, who lives and works between New York and Rome. 

 That is no crime, except that the works of both Chakravarty and Dodiya seem to have been born in the laboratories of their mind, at least once removed from the stress and strain of living from one day to another in the cities of India. They are not contaminated. It is not a question of quality but one of sensitivity to ones surroundings.

 On the other hand, Maurizio Cannavaccuiuolos very fine and delicate paintings two works as delicate as lace are fraught with anxieties and are overshadowed by intimations of death.

 Nonetheless, this is a great exhibition, for it demonstrates how artists in the West are using cutting-edge technologies and traditional skills to meet their own ends. Federico Solmi makes brilliant use of the cartoon strip format in his video on King Kong. Nicola Verlatos large painting could have been a photograph, except that it resembles the more lurid illustrations from Neil Gaimans Sandman graphic novels.




</TEXT>
</DOC>