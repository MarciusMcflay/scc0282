<DOC>
<DOCNO>1070713_opinion_story_8046760.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 VIEW FROM THE CONCRETE TERRACES, THEN AND NOW

 Genius in any name 

 Men in White: A Book of Cricket 

 By Mukul Kesavan, Viking, Rs 395

 It seems only appropriate that Mukul Kesavans taste in cricket started developing around the same time that he became fluent in English. It meant that AIR commentaries, Sport amp; Pastime, and BBCs Test Match Special came together with Billy Bunter stories, P.G. Wodehouse and My Fair Lady to shape a writer who, writing about cricket in 2007, can still make the reader use the words riveting and fun. 

 Surprisingly, for a person who looks back with so much wistfulness to those days and to the time when Doordarshan made television programmes, Weston made television sets and only real life happened in colour, Kesavan has an enormous amount of passion left for the modern game. This comes partly from the belief that the gentlemans game will work out its own ways of negotiating with the market and win back everything it has lost including the paying fan, or the stadium spectator. 

 Such faith is rare among old-timers, but then, Kesavan does not consider himself one. His interest in contemporary cricket can be compared to an urban Indian boys following of Manchester Uniteds progress in the Premier League. For proof, there is his analysis of the ICCs fifteen degree solution, which was rustled up with great technological fanfare after the governing body of cricket found itself in a soup, trying to outlaw the bowling action of Muttiah Muralitharan. The farce that was passed off as a great scientific breakthrough for cricket is summed up in a brilliant couplet: Chuckers are bowlers, they said in the end/ (Not that they chuck, they just hyperextend).

 The advantage of writing about a sport as a fan is that the author does not have his little finger tied to the muse of unbiased writing. But we live in strange times, in which expert comments and newspaper reports and columns reek of prejudice. Even at his most partisan (Unlike Tendulkar, he [Gavaskar] did not adjust his guard in public), Kesavan possesses greater objectivity and balance than the newspaper cricket-writer. Or at least, he has enough substance up his sleeve to back up an outlandish argument if he happens to make one.

 This is mostly the case, but there are one or two exceptions. One of them is when he bludgeons the late Hansie Cronje for cutting a holier-than-thou figure after burning his fingers in the match-fixing controversy. True, the South African cricket establishment, led by Ali Bacher, made a great song and dance about Cronjes devout Christian background, but for Kesavan all of it was hypocrisy of the first order. He insists that Bacher should have reminded his wards of the basic Calvinist ethic of individual responsibility: He could have had them coached on how to turn down without hurting their feelings. And the BCCI in India could have helped by supplying him (and the Australians) a list of Indian synonyms for no. That is all very well, and Australia, England and South Africa have indeed had an august tradition of hushing up match-fixing scandals involving their players. But this would still not explain why almost all the big betting scandals in modern cricket seem to originate in the subcontinent. 

 Just as Kesavan does not go into why Sachin Tendulkar fails to play long innings like Brian Lara. In fact, it seems that the writer has not yet made up his mind about Tendulkar. If he plays an attacking game, then the Gavaskar-worshipper in Kesavan finds his shots more like being heavily nudged by a barn door; and if he plays safe, then he has almost too much time to play the ball and he uses it to think and fret instead of using it to attack the bowling. In other words, Kesavan will not fight Tendulkars case for greatness in Indian cricket, let alone on the world stage. One reason for this is that Tendulkar has been pinged on the helmet many more times than Gavaskar (who did not wear one), and even Dravid. But Kesavan himself admits that once protective gear has been introduced, it cannot be held against batsmen for using them. And is it not possible to argue that a batsman who knows his head is safe under the helmet is likely to be less careful in facing the rising delivery than his helmet-less predecessors?

 One of the joys of reading Men in White is that such dissensions are encouraged. So much so that the author often provides the counter-arguments to his own theses. The other joy derives from the fact that Kesavan says beautiful things you have thought about cricketers and their styles but never quite managed to find the language to articulate them. All the elements of Rahul Dravids batting style (and his entire personality, one dare say) are, according to Kesavan, symptoms of his decision to sacrifice power to reduce risk. (Could you agree more?) Then there are the one-liners: Srinath is Vishwanathan Anand with a ball in hand, Indian fans arent just optimists, theyre connoisseurs of hope, (Bishen Bedi) was a classicists wet dream. Navjyot Sidhu has a long way to go before he can come up with a term like anecdotage, which is Kesavans chosen word to describe people like Sidhu, who keep referring to players they have never seen in action.

 There is no doubt that Kesavan spares more thought to the present and future of the game than most of its governors do. His is a relentless search to put his finger on what went wrong with the game as he knew it, and when. The trouble with cricket, he concludes pretty much accurately, is that it has moved from a system of supervision based on an acceptance of human (read umpiring) error and the alleged honour of gentlemen cricketer, to an umpiring regime dependent on (and second-guessed by) the all-seeing camera. 

 What Kesavan needs to do now is interpret the maladies that ail Indian cricket. He even hints at them when he says: Till the game was amateur, till the business of cricket was a matter of stipends and gate money, the members of cricket boards were patrons; once the game became thoroughly commercial, they became parasites. The problem is once he starts digging up the muck, the first casualty might be the child on the concrete terraces for whom the sight of Farokh Engineer swaggering down the steps of Willingdon pavilion to open the Indian innings was the doorway to heaven the child who partly wrote this book. 

 SREYASHI DASTIDAR




</TEXT>
</DOC>