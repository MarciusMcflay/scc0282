<DOC>
<DOCNO>1070717_opinion_story_8065516.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ALL LINED UP

 The premiership of the nation now entails the job of selling dreams. Perhaps it also requires this seller to be a dreamer himself. The prime minister of India, Manmohan Singh, has grand visions about transforming Indias borders, particularly the ones experiencing perennial disquiet. He had earlier expressed his desire to see Siachen being converted into a mountain of peace. While accepting his honorary doctorate at the Jammu University recently, Mr Singh enumerated his thoughts for the line of control as well. He wants it to be transformed into a line of peace with a greater interchange of ideas, goods, services and people. For a subscriber to the vision of a world without borders, the idea is a natural one. And, in any case, Mr Singh has shown more consistency in his policies towards hastening the lowering of guards on Indias troublesome border with Pakistan than the general next door whose radical change from a position of no-compromise to one of anything-goes has created its own set of complications. For the past few years, Mr Singh has earnestly tried to ensure that trade and people-to-people contact across the borders did not become hostage to any breakdown in the peace talks. Points along the line of control were opened up to ease communication, trade and tourism, and army presence was reduced at the borders to spread the message of goodwill. 

 However, as Mr Singh himself must be painfully aware, noble intentions on his part alone cannot transform reality. Much depends on the neighbours sincerity of purpose, and this has often been suspect. Cross-border interaction and the peace process itself have been jeopardized every time there has been an intensification of terrorist activity within the two countries. The lack of trust, which affects the response of the bureaucracy and the army establishment to peace pledges from the leadership (and this significantly colours public perception), has been found to be a stumbling block when giving plans a concrete shape. Also, the lessening of control along the LoC is hamstrung by some very practical problems to which neither country has found solutions. It necessitates an end to the dispute over Siachen, on which the army has had more say than political heads. Above all, peace requires the people to believe in themselves and in the sincerity of the leadership. Both seem to be tottering on the borderline.




</TEXT>
</DOC>