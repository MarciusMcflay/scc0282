<DOC>
<DOCNO>1070520_calcutta_story_7800571.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 For love of the game

 The Centre has slashed grants where they are needed most in football and hockey. Is there a point in carrying on? Yes, says the next generation

 Pahoon Biswas, 15, is one of the citys soccer-mad youngsters who wake up at the crack of dawn for practice. He attends the Russi Mody Football Academy and trains on the run-down grounds of Mohammedan Sporting Club on the Maidan. Pahoon appeared for his Madhyamik from Naktala High School in March. But with a practice schedule of three hours in the morning and another two or more in the evening, studies are not a priority. 

 Pahoons hard work has yielded results: the midfielder was selected for the under-14 Bengal squad to Chandigarh in 2005. He hopes to play for India under-16, and then the senior national team. 

 The youth affairs and sports ministry wasnt obviously thinking about Pahoon when it downgraded football from the general category to others this month, even as hockey was pegged down from priority to general. This means severe restrictions on funds for travelling outside the country. Football will suffer: the Indian Football Association will be poorer by Rs 2 crore annually.

 Dreams and demotion 

 But the demotion has not affected Pahoons dreams. Or the dreams of thousands of the citys young. It has not been able to pull the rug from under their feet, because there was no rug. 

 The game is everything for Rakesh Sen from Church Missionary School, Krishnagar. The 16-year-old travels with his 10-year-old brother Rajesh to Calcutta four days a week, often taking the first local train at 4 am to practise at Avenue Sammilani at the Rabindra Sarobar stadium. A student of Class IX and a die-hard fan of Cristiano Ronaldo Rakesh smiles when asked about the board exams next year. I cant miss practice, he says. I want to play in first division and then get a job. My parents expect me to do well as a footballer. They also want a better life for me and my brother. Rakeshs father sells vegetables at a market in Krishnagar. 

 Paresh Halder, 21, is a little closer to that goal: he is a custodian for Calcutta Leagues first division (group B) club Victoria Sporting. When hes not at practice, the higher secondary pass-out helps his father run a tea-stall at Taratala. I look up to East Bengal goal-keeper Sandeep Nandy and, of course, German goalie Oliver Kahn. Im working hard to improve and move on to a Super Division team, he says. Does the demotion of soccer mean anything to Paresh? Its an unfair decision, but it doesnt affect us directly. We dont even receive half the support that cricketers get, says Paresh. 

 24x7 school 

 The young players are resigned to crickets sovereignty. Only 10 to 15 countries play cricket seriously. Nearly 160 countries play football. Our lack of infrastructure becomes apparent when we are pitted against countries with better training schemes, says Pahoon.

 If anything, it is funds that the youngsters need. Says Nikhil Nandi, an ex-Olympic player and a FIFA-certified coach at the Russi Mody Football Academy: Many are from very poor families. They have bad diets that prevent them from becoming strong. Nasir Amir Hamed, who runs the academy with Nandi, feels that such schools need to work 24x7. We should have live-in arrangements for the boys, so that we can monitor their training and diet. This is impossible now with so little funding. 

 Nandi feels lack of uniform standards in training is a hindrance to the games development. In Brazil and Germany, all kids are expected to be at the same level in terms of fitness and skills. Here the standards and methods vary enormously, says Nandi. It is especially sad in Bengal, a state that had a very strong football tradition. 

 Hockey grounded

 If football is in bad shape, hockey is forgotten. The demotion in status means that grants will be slashed hugely for hockey. Players may have to bear their own travel expenses even while representing the state. 

 The worst ground for hockey players is in Bengal, literally too. Beat this: at the moment, theres no astro-turf (the surface hockey is played on) in the state. Every state capital has one; a small city like Ranchi has two. The Bengal Hockey Association only gets grounds from the three big clubs (East Bengal, Mohun Bagan and Mohammedan Sporting) for three months, from January to April. Despite that, Bengal ranks eighth among the 32 states and union territories.

 The astro-turf in the Sports Authority of India complex at Yuba Bharati Krirangan lies in tatters. The absence of astroturf matters. Players from Bengal tend to get exhausted very soon into the game. The speed and bounce of a ball on astroturf is very different from its movement on grass, says Khaled Hussein, assistant secretary of the hockey association. 

 Young hope

 There is a bunch of schools with active teams including La Martiniere, St James, Julien Day, Khalsa English School, Khalsa Model School (Dunlop) and Loreto Day School, where hockey is played as part of the curriculum. In the Calcutta league, over 60 teams from three divisions fight for league titles. And quite a few players in the Premier Hockey League hail from the state. 

 Getting a ground to practise on is a tough task for 14-year-old Shubhajit Das and his friends from Howrah DSA, which recently faced the first-division team Young Society from Entally at the Lagden Shield final. Does the grants slash bother them? We dont know much about the grants being taken away, smiles Shubhajit. But our coach keeps on telling us to get better. If we do, maybe the government will return the grant. He is echoed by 13-year-old Shubhodeep Das, 12-year-old Susmit Raina, and the tiny but sprightly and immensely talented 11-year-old forward Surmeet Singh. 

 Look at these kids: Provided the right infrastructure, theyre bound to shine, says Bijoygopal Adhikari, amid fervent shouts at the Lagden Shield game at the Mohun Bagan grounds. A former state player and senior manager of the Bengal squad in 2006, Adhikari has seen the golden days of the game. But he refuses to give up. The kids are our future: It doesnt make sense to take away whatever little help we can offer them, he says. 

 Talent and training

 The ministry says the Indian hockey squad is under-performing. Yes, the teams have been under-performing compared to, say, the Olympic or Asiad gold-winning squads. But that doesnt mean that you take away grants from a neglected game. That way, youre basically going to kill the enthusiasm of the next generation, says Hussein. There are many talented hockey players in the districts, but they dont get the exposure the players in Calcutta get. Most of the district boys cannot afford to travel to Calcutta for practice: the commute costs about Rs 20, adds Adhikari.

 It is the same for football players. Has the funds slash made them lose heart? Soccer is our identity; I dont want to be anything but a great footballer, smiles Rakesh. 

 Talent is nothing without training, nurture and refinement. Maybe the government should encourage them, instead of giving them a tough time.

 ARKO DAS AND PATRICK PRINGLE

 PICTURES BY BISHWARUP DUTTA




</TEXT>
</DOC>