<DOC>
<DOCNO>1070619_sports_story_7942659.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Hamilton turns season into a battle between giants

 Kevin Garside 

 Fernando Alonso 

Lewis Hamilton grows in stature by the hour. His second Grand Prix victory eight days after the first followed the fiercest examination of his fledgling career (as reported in Mondays City edition). 

For 73 heart-stopping laps he and McLaren teammate Fernando Alonso turned the US Grand Prix into the OK Corral on wheels, bullets flying in all directions as the defending champion trained the heavy weaponry on Hamilton. 

If this doesnt sell F1 to the Americans, nothing will. From lap 21 when Hamilton began the first round of stops, Alonso never left his teammate alone. The race build-up was fraught with tension, Alonso cranking up the pressure with talk of English bias. A team gathering on the eve of the race was said to have cleared the air. It cleared one thing up. On the circuit at least the gloves are off between Hamilton and Alonso. 

Seven races into his F1 career Hamilton has still to finish off the podium. He leads the championship from Alonso by 10 points. They have two wins apiece. In every sense Hamilton, the story of the British sporting summer by a country mile, has turned the season into a heavyweight battle of gargantuan talents. 

F1 has not seen a rivalry like this since Ayrton Senna and Alain Prost were at each others throats. Here we have the added dimension of the fairy tale. No rookie in history has announced himself in F1 like this. Hamiltons debut season is arguably as stellar as any in sport. 

Dotted around the bleachers, the Cross of St George and Union Jacks proclaimed allegiance to Hamilton, whom the locals had labelled the F1 phenom. White team shirts paired with fluorescent flame-red hats told of the brisk trade being done in the McLaren merchandising tent. Not yet a threat to Ferraris hegemony in the replica shirts business, but growing apace. 

Hamiltons second successive pole following victory in Canada raised the roof in qualifying and cemented his heroic status. The Hamilton aura was sufficiently compelling to attract the interest of American rapper Pharrell Williams. The fusion of pop and sporting idolatry provided photographers with a Champagne moment before the start, and shortly afterwards when he pumped his fists furiously as Hamilton held off Alonso through the first corner. 

Alonso was initially more circumspect than of late through turn one, deciding wisely no to press his case after drawing up alongside Hamilton at the end of the start-finish straight. That would come shortly. 

Hamilton started the ball rolling at the end of lap 21, rapidly followed by Felipe Massa and Nick Heidfeld. One lap later McLaren brought in Alonso. The world champions main chance of victory lay in getting out ahead of the Briton. He didnt. Hamilton hammered down the straight as Alonso rolled off his mark. 

At the start of lap 39 all hell broke loose. The champion drew alongside menacingly as the pair hurtled down the straight towards turn one. 

The guns were cocked. A failure of nerve here would have been fatal for both. Alonso fired. Hamilton fired back. Tyres almost touched. The duel continued through corners two and three. Stunning stuff. 

Hamilton was rock-like in defence of his position. Alonso continued the battery until lap 50 when he shot into the pits for his final stop. Hamilton followed suit a lap later to emerge 50 metres to the good when battle resumed with 20 laps to go. To the line they fought, hammer and tong. Alonso could find no way past. He might have to get used to this. 

 RESULTS 

1.Lewis Hamilton (McLaren) 1hr 31min 09.965; 2. Fernando Alonso (McLaren) +00:01.518; 3. Felipe Massa (Ferrari) 00:12.842; 4. Kimi Raikkonen (Ferrari) 00:15.422; 5. Heikki Kovalainen (Renault) 00:41.402; 6. Jarno Trulli (Toyota) 01:06.703: 7. Mark Webber (RedBull-Renault) 01:07.331; 8. Sebastian Vettel (BMW Sauber) 01:07.783; 9. Giancarlo Fisichella (Renault) 1 lap; 10. Alexander Wurz (Williams-Toyota) 1 lap; 11. Anthony Davidson (Super Aguri-Honda) 1 lap; 12. Jenson Button (Honda) 1 lap; 13. Scott Speed (Toro Rosso-Ferrari) 2 laps; 14. Adrian Sutil (Spyker-Ferrari) 2 laps; 15. Christijan Albers (Spyker-Ferrari) 3 laps; 16. r. Nico Rosberg (Williams-Toyota) 5 laps, 17. r. Vitantonio Liuzzi (Toro Rosso-Ferrari) 5 laps; r. Nick Heidfeld (BMW Sauber) 18 laps; r. Takuma Sato (Super Aguri-Honda) 60 laps; r. David Coulthard (RedBull-Renault) 73 laps; r. Rubens Barrichello (Honda) 73 laps; r. Ralf Schumacher (Toyota) 73 laps. 

 STANDINGS 

(Top ten) 

Drivers 1. Hamilton 58 points; 2. Alonso 48; 3. Massa 39; 4. Raikkonen 32; 5. Heidfeld 26; 6. Fisichella 13; 7. Kubica 12; 8. Kovalainen 12; 9. Wurz 8; 10. Trulli (Toyota) 7. 

Constructors 1. McLaren-Mercedes 106 points; 2. Ferrari 71; 3. BMW Sauber 39; 4. Renault 25; 5. Williams-Toyota 13; 6. Toyota 9; 7. RedBull-Renault 6; 8. Super Aguri-Honda 4; 9. Toro Rosso-Ferrari 0; 10. Honda 0. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>