<DOC>
<DOCNO>1070510_sports_story_7757672.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Laver lauds ego-free Federer

MARK HODGKINSON in ROME

 Roger Federer 

While most sportsmen would have their heads turned by being listed in Time magazine as one of the 100 most influential people in the world, Roger Federer has never been an egomaniac with a racket. He went quietly about his business on Tuesday, coming through his opening match at the Rome Masters with little show or fuss in beating Nicolas Almagro. 

Federer has become accustomed to the lavish praise, and to others making grand statements about the meaning of his ability to control a tennis ball. 

He has come to realise that, whatever anyone else is saying, he should just get on with the job of winning matches. 

Indeed, Federers idol, Rod Laver, has suggested that, remarkably for a leading athlete, the Swiss has no ego. 

The latest issue of the magazine also includes Kate Moss, Sacha Baron Cohen and Osama bin Laden in the 100, but George W Bush is omitted, and quite what Federer has been influencing was not made clear, unless much of the planet suddenly has dreams of becoming mild-mannered and outrageously talented Swiss tennis players. 

But you cannot argue with the fact that he is at his most influential when he is in full swing on court, and he dispatched Almagro, Tim Henmans Spanish conqueror 6-3, 6-4 to move into the third round. 

Every time I speak to Roger, I sense no ego on his part, Laver, the Rockhampton Rocket, has written in Time, and Federer could certainly teach the average Premiership footballer a thing or two about how to avoid becoming another jumped-up, puffed-up prat. 

He asks me questions about how I prepared for big matches Roger has a clear appreciation for the history of tennis. Plus, these days, I should be the one peppering him with questions. Hes the big star. 

When youre talking to Roger, he makes you feel important whether youre a fan, an opposing player or an old geezer like me. He is one of the most admirable champions on the planet. Thats something worth crowing over. The beauty is, Roger wont. 

Laver also likes Federers lack of entourage. The most impressive aspect of Rogers ascendancy to the top of the tennis world is the way he carries himself as a champion. 

Its quite unusual. He just lets his racket do the talking. He doesnt have a bunch of coaches and trainers micromanaging everything he does. 

 Roger has so much natural talent, they would just disrupt it if they muddled his mind. 

He exudes energy, and you just know he enjoys the camaraderie of all his competitors. Tennis had lost that positive vibe over the years. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>