<DOC>
<DOCNO>1070106_opinion_story_7225104.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 THE BEAUTY AND THE BEST

 Six years after she left Mumbai for the United States to marry an Indian surgeon there, Madhuri Dixit a la Mrs Nene is ready for her comeback 

 There are some who age with time; and then there are a few who time themselves with age. Madhuri Dixit four months to 40 belongs to the rare group of people who know what to do when. And, for her, there is no time like the present. The red carpet is being laid out, the clapboard given a polish and the film industry of Mumbai is all agog. For the doctors wife and mother of two is all set to return to cinema. 

 For the industry, and her legion of fans, this is good news. Dixit quit cinema for marriage and motherhood when she was at the peak of her career. Now, six years after she left Mumbai for the United States to marry an Indian surgeon there, Dixit a la Mrs Nene is ready for her comeback. 

 Madhuri is a very nice person, apart from being a good artiste, says veteran film maker Yash Chopra, who heads Yash Raj Films (YRF), the producers of Dixits comeback vehicle. I talked to her about working with her again. But she had to fit the story. And when she did, I approached her and she agreed, says Chopra. 

 Chopra knows what he is talking about. The year 2006 saw the return of another Mumbai heart-throb who had forsaken the arc lights for marriage. And actress Kajol came back with a bang with Chopras film, Fanaa. If you have to mark your return, as Kajols success proves, it has to be with suitable fanfare. 

 Dixits film promises to be a blockbuster, too, though not much is known about it, barring the fact that its being directed by cinematographer Anil Mehta, who shot such films as Lagaan and Devdas. Sources state that the theme is woman-centric, and that Dixit may essay multiple roles in the still-to-be-named film. What is known, though, is that she is looking good having lost five kilos working out in a Mumbai gym. 

 There is excitement in the air and all the more so because Dixit failed to make her mark in television after her reality marriage show, Kahin Na Kahin Koi Hai on Sony TV, sank without a trace. So will she, as many seem to think, return to reign, or has she, as TV demonstrated, lost her touch? 

 It will be sometime before the question finds its answer, but that Dixit is still the stuff fantasies are made of was more than evident in her last film. She played the role of a kotha dancer in Sanjay Leela Bhansalis 2002 extravaganza, Devdas. And that, in the fairly short-lived careers of Mumbais top heroines, was something, for Dixit had made her debut 20 years earlier in a TV series opposite Benjamin Gilani. The series never saw the light of day, and even her first big-screen film Abodh in 1982 did little to boost her image. Those were the days when critics were writing her off, and her secretary Rikku Rakeshnath was being warned that shed never make it big. 

 And then came Tezaab in 1988 with its hit song, Ek Do Teen and Dixit never had to look back. Madhuri has gone on to become the biggest star. Almost everyone in the film industry wants to sign her today, says Rikku Rakeshnath, who has managed her work for 24 years. 

 As far as Bollywood is concerned, the sluice gates have been opened generating a fair bit of optimism in the industry. Its really good news that Madhuri Dixit is coming back to movies. Shes an accomplished actress. In todays age and time, the industry does need real talent like Madhuri, says Raj Kumar Santoshi, who worked with her in his film, Lajja. 

 Director Prakash Jha, who deglamourised Madhuri for Mrityudand, is equally upbeat. She is surely an actress every Bollywood filmmaker is waiting to work with again, he says. 

 Clearly, there is a great demand for Madhuri even though she left Mumbai in 1999 to marry Denver cardiovascular surgeon Sriram Madhav Nene. Designer Vikram Phadnis, for instance, is trying to get Madhuri for his debut film. She fits the role. I have spoken to her. Lets see, says Phadnis. 

 But Dixit winner of several awards has never been short of offers. Director Santoshi wanted to cast her in a remake of Mother India. Shaji Karun approached her for his film, Suryamukhi. Rituparno Ghosh was keen to cast her, as was Pooja Bhatt. Zoya Akhtar wanted her for her film Luck By Chance. But Dixit turned down all the offers. 

 Her absence was felt so keenly that Ram Gopal Varma even produced a film called Main Madhuri Dixit Banna Chahti Hoon (I want to be Madhuri Dixit) in 2003. Says Antara Mali, who played the lead role in the film: This girl that I played didnt just want to be an actress. She wanted to be like Madhuri Dixit. Shes been a huge icon and a personal inspiration helped me perform better. 

 There are many in the industry who hope that todays cinema will be able to complement the older Dixit. Our cinema is evolving, emphasises actress Preeti Jhangiani who has worked in films such as Mohabbatein, Baaz and LOC Kargil. In Hollywood, aged actresses are always considered hot. In India, the trend is catching on. Nowadays, roles are being created for actresses who have crossed 40. Thankfully, Madhuri is making a comeback at the right time. I am sure shell rock as she always used to, she says. 

 The one waiting to help her rock is choreographer Saroj Khan, who gave Dixit a distinct profile by propping her up as an able dancer. Actually, Madhuri Dixit was not a great dancer. She was very thin and only knew kathak. But she practised hard for 17 days and rehearsed for 12 hours every day. Only then could you come up with such a good number. Thats why Ek Do Teen is in the history books today, she says. I am waiting to work with her again and I am sure her fans will want to see her do Dhak Dhak and Ek Do Teen once again, she says. 

 That, of course, will be in a while, for the film is yet to be launched though it is expected to be announced sometime this month. Right now, however, its time for countdown: Teen, Do, Ek




</TEXT>
</DOC>