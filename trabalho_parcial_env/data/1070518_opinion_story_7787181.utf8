<DOC>
<DOCNO>1070518_opinion_story_7787181.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 BIG STATE, BIGGER LOSS

 - The BJP is the only party that lost its core vote in the UP polls

 SWAPAN DASGUPTA

 The victory of the Bahujan Samaj Party in the Uttar Pradesh assembly election has served as an outlet for the pent-up angst of the radical Brahmin intelligentsia. Mayavatis success in winning a clear majority in a politically fragmented state owed as much to the exaggerated majorities of the first-past-the-post system as to the 10 per cent or so extra votes the BSP secured. Effective and politically-astute governance in Lucknow may well pave the way for the BSPs emergence as a formidable player in the rest of north India and even contribute to the weakening of the two main national parties. However, for the moment, the conclusion that securing 30.28 per cent of the popular vote heralds either a new dawn or a social revolution as contrite Brahmin commentators have suggested seems recklessly fanciful.

 To be sceptical of grandiose conclusions grounded in Brahminical penitence is not to underestimate Mayavatis success in using her unshakeable Dalit base to secure incremental votes from other castes and communities. Electoral success in UP has always depended on a partys ability to supplement its core vote with strategic additions. Charan Singhs clout in the Seventies and early Eighties was, for example, based on his ability to attract Muslim votes to supplement his solid following among Jats, Yadavs and Kurmis. Likewise, between 1991 and 1998, the Bharatiya Janata Party consistently polled more than 30 per cent of the popular vote and won a majority of Lok Sabha seats in UP by creating a Hindu combination of upper-castes, Jats and non-Yadav backwards.

 The novelty of Mayavatis victory this time lies in the fact that Dalits were part of the winning combination for the first time since the short-lived Samajwadi Party-BSP alliance of 1993. Earlier, of course, the Dalits were an indispensable part of the Congress system, which last tasted success in 1984. Equally significant, this is the first time in post-independence India that a winning social combination has been forged under the leadership of Dalits. By maintaining the political unity of the Dalits and, at the same time, reaching out to others, Mayavati has undone the fractious legacy of the Republican Party and posited an alternative to demands for a Dalit-Muslim-Christian alliance. 

 In social and cultural terms, the ability of those who were condemned to the bottom rung of the Hindu ritual hierarchy to lead the way is stupendous. However, in political terms, where the votes of individuals are equal in value, Mayavatis success seems less dramatic. Like others before her, she has successfully leveraged her own numbers to secure the support of others.

 Viewed in statistical terms, the UP election results are curious. That Mayavati was a clear winner is obvious. Yet, despite what has been said about the anti-incumbency wave against the Mulayam Singh Yadav government, the fact is that the SPs popular vote of 25.45 per cent was a modest statistical improvement on the 2002 figures. Disaggregated exit poll results suggest that the Muslim-Yadav alliance, on which the SP rests, remained intact. The BSP may have succeeded in securing the election of more Muslim members of the legislative assembly than the SP, but nearly half the Muslim voters preferred Mulayam Singh over the rest.

 Likewise, the 8.56 per cent vote for the Congress constituted a virtual no-change from 2002. As the most vocal opponent of the man who denied Sonia Gandhi the prime ministership in 1999, the Congress was not a beneficiary of any anti-incumbency, despite Rahul Gandhis energetic campaign. The suggestion that the Congress organization let the heir-apparent down lacks credibility. Organization matters, but it is not the last word in election strategy. In 1991, for example, the BJP hardly had any worthwhile organization in UP. Yet, its popular vote increased by nearly 23 per cent probably the largest swing in Indias election history on the strength of a fiercely aggressive campaign and effective social combinations. 

 Unfortunately for the BJP, very little of the 1991 magic was in evidence in 2007. Politically, it was the big loser in this assembly election its seats down to 51 and its vote share down by nearly three per cent to 18.50 per cent. Exit polls suggest that the party polled slightly less than 50 per cent of the upper-caste vote a fall from the good old days when it used to secure some 75 per cent and lost more dramatically among Jats and Kurmis.

 The decline in its share of the upper-caste vote was primarily responsible for the BJPs overall decline. All the major players in UP have one major bloc whose votes they can bank on even in adversity. In bolstering the electoral fortunes of the BJP, the upper castes and middle classes play a disproportionate role. Losing core votes, either through defection or abstention, crippled all hopes of a saffron recovery in the Hindi heartland. No wonder it proved incapable of gathering incremental support.

 The UP verdict was as much a censure of the BJP as it was an endorsement of Mayavati. To blame the debacle on half-hearted Hindutva, as the Rashtriya Swayamsevak Sangh weekly Organiser has done, is to skirt the awkward questions arising from the BJPs dubious political conduct. Since 2003, when Mulayam Singh assumed change in Lucknow, the BJP has suffered from a crisis of integrity. One section of the party had, frankly, been corrupted beyond belief by the SPs elaborate patronage network. Another section, desperate to regain power at the Centre, imagined that the SP offered a bridgehead into the vulnerable sections of the United Progressive Alliance dispensation. The cumulative result was a series of shoddy deals and bouts of shadow-boxing that left the party looking incredibly pathetic in the eyes of its core support base.

 The BJP lost the popular mandate in UP in 2002, and at the Centre in 2004. The electorate voted for it to play the role of an effective opposition. The party leadership chose to subvert this simple message. In attempting an impossible palace coup to dethrone the Congress in Delhi, it became a slavish instrument of SP manipulation in Lucknow. The BJP in UP was worse than a loyal opposition; it was an undercover ally of Mulayam Singh Yadav.

 Those who questioned this unholy nexus were abused and marginalized. For questioning the partys volte face on Natwar Singh and the public displays of fraternity with the SP, a former cabinet minister in the Vajpayee government described a colleague as an American agent in the presence of the party leadership. He was not rebuked. A functionary of the BJP once rued that Mulayam Singh threatened him with removal as the president of the state unit if he persisted with his obstreperous opposition; within a month the threat was carried out. During the BJP national council meet in Lucknow last December, the SP government elevated nearly 150 BJP leaders to the status of state guests, a gesture that went far beyond the needs of protocol.

 Last Friday morning, the duplicitous world the BJP leadership helped create collapsed in a heap. Mulayam Singh lost power but his core voters remained loyal to him. The BJP, however, lost more than votes and seats; the electoral debacle was a reflection of the loss of public trust in its leadership. Restoring that trust will take more than the deputation of ineffective full-timers from the RSS. It calls for a thorough post-mortem, which the leadership will try and avert in the name of unity and discipline. And it calls for a merciless purge of all the rotten apples. The alternative is slow extinction in Indias largest state and an end to all hopes of returning to power at the Centre.




</TEXT>
</DOC>