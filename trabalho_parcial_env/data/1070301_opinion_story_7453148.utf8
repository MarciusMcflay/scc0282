<DOC>
<DOCNO>1070301_opinion_story_7453148.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 LOST HORIZON

 It was an year of growth. It was a budget to dull expectations. Mr P. Chidambaram presented the budget for 2007-08 against a backdrop of an economy that has been exhibiting mixed signals. While the economy continues to grow at remarkably robust rates, inflation, particularly in food items, has been very high. The budget proposals have had to keep in mind the twin objectives of stimulating growth and containing inflation. In this, he has been helped immensely by the extraordinary buoyancy in tax revenues. This has enabled him to increase overall expenditure quite significantly without having to levy harsh new taxes. In fact, the overall impact of the budgetary proposals regarding indirect taxes is estimated to be revenue-neutral, while there will be a small increase in direct tax collections. Despite this, the finance minister hopes to be able to contain the overall fiscal deficit to 3.7 per cent of GDP. While new tax proposals typically attract more attention, initiatives taken on the expenditure side are equally important. The finance minister backed his statement on inclusive growth with significantly higher budgetary outlays in health and education, as well as in rural and urban employment schemes and insurance schemes for unorganized workers. Overall increases in education and health are as high as 34 per cent and 21 per cent.

 The Economic Survey points out that the current bout of inflation is owing to supply-side constraints. The most important constraint, it is argued, is the stagnant agricultural sector. Not surprisingly, the budget contains a slew of new policy initiatives to boost agricultural productivity. These include significant increases in farm credit, and in outlays on irrigation and production of certifiable seeds, the latter being an important requirement for increasing the production of pulses. These are deemed enough, somewhat controversially, for containing inflation.

 Mr Chidambaram has kept faith in the existing tax system. There has not been any dramatic change in taxes. He has effected a modest reduction in customs duties in order to bring them closer to East Asian rates. There have been no major changes in excise taxes. A slightly populist measure, aimed at the middle classes, is the increase in exemption limit for personal income tax. 

 Mr Chidambaram has built up an enviable reputation for being one of the most innovative finance ministers in India. The current budget falls short of the lofty standards set by him, even though the prevailing rates of economic growth offered him the best opportunity to bat with flourish. The budget may even be labelled pedestrian by his detractors. But this is an uncharitable interpretation. The policy initiatives taken in the social sectors may well have far-reaching consequences. To a large extent, he has left the corporate sector to itself. This could well be because of his confidence that the current system will deliver. This is not an unreasonable premise when the economy has been growing at rates which were unbelievable a few years ago.




</TEXT>
</DOC>