<DOC>
<DOCNO>1070904_sports_story_8273246.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Williams show goes on

LIZ ROBBINS 

 Venus Williams during her match against Ana Ivanovic on Sunday 

The most intense match involving a Williams sister so far in New York might have been the shopping expedition they took together last week. Venus Williams, the older, more relaxed sister, put on jeans and a T-shirt until she saw Serena decked out in a dress. Venus changed, she said, to do the sister thing. 

Off the rack, just like on the court, they try to balance compassion with competition. We try to run through the store and compete to find the best outfit, Venus said on Sunday. If it doesnt fit the other, then we switch. 

And on days off from shopping in Manhattan, like on Sunday, the sisters occupy the same side of the US Open draw. With convincing fourth-round victories at the Arthur Ashe Stadium that put rising stars in their place, Venus and Serena each found themselves only two challenging quarter finals away from facing off in the semi-finals. 

That would be awesome, Venus said, because it would mean that there is a Williams in the final. 

But which one? Serena started the season by winning the Australian Open, giving Venus the impetus to get on the Sister Slam scoreboard at Wimbledon. 

On Sunday, Richard Williams, their father and coach, slipped into the stadium for the last six points of eighth-seeded Serenas 6-3, 6-4 victory over Marion Bartoli, who was runner-up to Venus at Wimbledon. He had just gotten off the practice court with No. 12 Venus, who three hours later crushed French Open finalist Ana Ivanovic 6-4, 6-2. 

The sisters overwhelmed their opponents with booming serves. Serena led the ace tally with 10 to Venuss 1. But Venuss fastest serve registered at 126 miles an hour, compared with Serenas 124. 

Venus is moving extraordinarily well, Richard said. From what I can see, Venus is moving better than anyone that is playing right now. 

It has been five injury-filled years since the sisters, ranked No. 1 and No. 2 in the world, last played in the prime-time US Open final created with them in mind. Serena won in 2002 and 1999; Venus won in 2000 and 2001. 

Serena, 25, still leads Venus, 27, in Slam titles, 8-6. They like the titles more than the money, said mother Oracene Price. 

They were on track to meet in the Wimbledon final until Serena toppled to the court in the fourth round, seized by a calf cramp. She braced her fall with her left thumb, spraining it severely, but returned to lose to Justine Henin in the quarter final. 

Serena was in a splint for four weeks, which restricted one of her favourite activities. She couldnt text with her thumbs, Price said. Texting is very important. 

But until then, tennis seemed to have recaptured its importance in Serenas life. 

I felt like I was going to win Wimbledon, she said. I felt like I was just going to do big things in the summer.

SEEDS WHO FELL IN WEEK I

 MEN

 Fernando Gonzalez (Chi, 7) mdash; lost 4-6, 1-6, 6-3, 7-5, 4-6 to Teimuraz Gabashvili, Round 1

 Mikhail Youzhny (Rus, 11) mdash; lost 6-3, 4-6, 3-6, 3-6 to Philipp Kohlschreiber (Ger), Round 2

 Ivan Ljubicic (Cro, 12) mdash; lost 4-6, 7-6 (7-5), 6-2, 3-6, 3-6 to Juan Ignacio Chela (Arg, 20), Round 3

 Richard Gasquet (Fra, 13) mdash; walkover to Donald Young (US), Round 2

 Guillermo Canas (Arg, 14) mdash; lost 5-7, 5-7, 3-6 to Lee Hyung-taik (Kor), Round 2

 Lleyton Hewitt (Aus, 16) mdash; lost 6-4, 4-6, 4-6, 2-6 to Agustin Calleri (Arg), Round 2

 Marcos Baghdatis (Cyp, 18) mdash; lost 3-6, 5-7, 6-3, 6-7 (6-8) to Max Mirnyi (Blr), Round 1

 Andy Murray (GBR, 19) mdash; lost 3-6, 3-6, 6-2, 5-7 to Lee Hyung-taik (Kor), Round 3

 Juan Carlos Ferrero (Esp, 21) mdash; lost 3-6, 4-6, 4-6 to Feliciano Lopez (Esp), Round 1

 Paul-Henri Mathieu (Fra, 22) mdash; lost 6-3, 6-1, 4-6, 3-6, 3-6 to Fernando Verdasco (Esp), Round 1

 David Nalbandian (Arg, 24) mdash; lost 3-6, 6-3, 6-4, 6-7 (5-7), 5-7 to David Ferrer (Esp, 15), Round 3

 Marat Safin (Rus, 25) mdash; lost 3-6, 3-6, 3-6 to Stanislas Wawrinka (Sui), Round 2

 Jarkko Nieminen (Fin, 26) mdash; lost 7-6 (7-4), 6-7 (4-7), 6-7 (5-7), 4-6 to John Isner (US), Round 1

 Dmitry Tursunov (Rus, 27) mdash; lost 4-6, 3-6, 6-3, 4-6 to Tim Henman (GBR), Round 1

 Nicolas Almagro (Esp, 28) mdash; lost 5-7, 0-6, 5-7 to Nikolay Davydenko (Rus, 4), Round 3

 Filippo Volandri (Ita, 29) mdash; lost 3-6, 6-4, 4-6, 4-6 to Michael Llodra (Fra), Round 1

 Potito Starace (Ita, 30) mdash; lost 5-7, 6-7 (4-7), 3-6 to Ernests Gulbis (Lat), Round 1

 Jurgen Melzer (Aut, 31) mdash; lost 3-6, 1-6, 4-6 to Juan Martin del Potro (Arg), Round 2

 Ivo Karlovic (Cro, 32) mdash; lost 6-7 (5-7), 4-6, 6-4, 7-6 (8-6), 4-6 to Arnaud Clement (Fra), Round 1

 WOMEN

 Maria Sharapova (Rus, 2) mdash; lost 4-6, 6-1, 2-6 to Agnieszka Radwanska (Pol), Round 3

 Ana Ivanovic (Cro, 5) mdash; lost 4-6, 2-6 to Venus Williams (US, 12), Round 4

 Nadia Petrova (Rus, 7) mdash; lost 4-6, 4-6 to Agnes Szavay (Hun), Round 3

 Daniela Hantuchova (Svk, 9) mdash; lost 4-6, 6-3, 1-6 to Julia Vakulenko (Ukr), Round 1

 Marion Bartoli (Fra, 10) mdash; lost 3-6, 4-6 to Serena Williams (US, 8), Round 4

 Patty Schnyder (Sui, 11) mdash; lost 6-4, 4-6, 6-7 (1-7) to Tamira Paszek (Aut), Round 3

 Nicole Vaidisova (Cze, 13) mdash; lost 4-6, 6-3, 6-7 (5-7)to Shahar Peer (Isr, 18), Round 3

 Elena Dementieva (Rus, 14) mdash; lost 1-6, 2-6 to Sybille Bammer (Aut, 19), Round 3

 Dinara Safina (Rus, 15) mdash; lost 0-6, 2-6 to Justine Henin (Bel, 1), Round 4

 Martina Hingis (Sui, 16) mdash; lost 6-3, 1-6, 0-6 to Victoria Azarenka (Blr), Round 3

 Tatiana Golovin (Fra, 17) mdash; lost 4-6, 6-1, 2-6 to Ahsha Rolle (US), Round 1

 Sybille Bammer (Aut, 19) mdash; lost 4-6, 6-4, 1-6 to Jelena Jankovic (Ser, 3), Round 4

 Lucie Safarova (Cze, 20) mdash; lost 6-4, 2-6, 3-6 to Bartoli (Fra, 10), Round 3

 Alona Bondarenko (Ukr, 21) mdash; lost 1-6, 2-6 to Venus Williams (US, 12), Round 3

 Katarina Srebotnik (Slo, 22) mdash; lost 4-6, 3-6 to Maria Kirilenko (Rus), Round 2

 Tathiana Garbin (Ita, 23) mdash; lost 4-6, 3-6 to Dominika Cibulkova (Svk), Round 1

 Francesca Schiavone (Ita, 24) mdash; lost 3-6, 5-7 to Paszek (Aut), Round 2

 Mara Santangelo (Ita, 25) mdash; lost 2-6, 4-6 to Vera Dushevina (Rus), Round 1

 Sania Mirza (Ind, 26) mdash; lost 2-6, 3-6 to Anna Chakvetadze (Rus, 6), Round 3

 Vera Zvonareva (Rus, 27) mdash; lost 4-6, 6-7 (4-7) to Serena Williams (US, 8), Round 3

 Ai Sugiyama (Jpn, 28) mdash; lost 4-6, 6-4, 2-6 to Ekaterina Makarova (Rus), Round 2

 Samantha Stosur (Aus, 29) mdash; lost 3-6, 2-6 to Alize Cornet (Fra), Round 1

 Anabel Medina Garrigues (Esp, 31) mdash; lost 3-6, 1-6 to Svetlana Kuznetsova (Rus, 4), Round 3

 Michaella Krajicek (Net) mdash; lost 6-7 (4-7), 3-6 to Agnes Szavay (Hun), Round 

day vii 

 results

Men rsquo;s singles, Round III mdash; Ernests Gulbis (Lat) bt Tommy Robredo (8, Esp) 6-1, 6-3, 6-2; Novak Djokovic (3, Ser) bt Juan Martin Del Potro (Arg) 6-1, 6-3, 6-4; Carlos Moya (17, Esp) bt Philipp Kohlschreiber (Ger) 4-6, 7-5, 7-6(5), 4-6, 6-4; Stanislas Wawrinka (Sui) bt Robby Ginepri (US) 5-7, 6-4, 4-6, 6-4, 6-3; Juan Monaco (23, Arg) bt Agustin Calleri (Arg) 7-6(5), 6-2, 6-4; Rafael Nadal (2, Esp) bt Jo-Wilfried Tsonga (Fra) 7-6(3), 6-2, 6-1; David Ferrer (15, Esp) bt David Nalbandian (24, Arg) 6-3, 3-6, 4-6, 7-6(5), 7-5; Juan Ignacio Chela (20, Arg) bt Ivan Ljubicic (12, Cro) 6-4, 6-7(5), 2-6, 6-3, 6-3

 Women rsquo;s singles, Round IV mdash; Jelena Jankovic (3, Ser) bt Sybille Bammer (19, Aut) 6-4, 4-6, 6-1; Justine Henin (1, Bel) bt Dinara Safina (15, Rus) 6-0, 6-2; Venus Williams (12, US) bt Ana Ivanovic (5, Ser) 6-4, 6-2; Serena Williams (8, US) bt Marion Bartoli (10, Fra) 6-3, 6-4 

 NEW YORK TIMES NEWS SERVICE 




</TEXT>
</DOC>