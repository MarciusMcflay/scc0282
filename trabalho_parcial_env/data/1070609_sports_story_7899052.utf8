<DOC>
<DOCNO>1070609_sports_story_7899052.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Ivanovic pools her resources to make the big stage

MARK HODGKINSON in PARIS 

When Ana Ivanovic was growing up in Belgrade, she learnt to play tennis on a court marked out on the bottom of an emptied swimming pool. And yet she will, on Saturday, appear in her first Grand Slam final on one of the sports biggest stages, Court Philippe Chatrier, as she takes on defending champion Justine Henin for the Roland Garros title. 

The smiling, sunny Serbian certainly did not require any metaphorical armbands or flippers on Thursday as the 19-year-old gave the most assured performance of her career when she annihilated Maria Sharapova, the world No. 2 from Russia, 6-2, 6-1. 

But Jelena Jankovic was unable to join her for an all-Serbian final when she was stopped 6-2, 6-2 by Belgian Henin, who is attempting to become the first woman since Monica Seles from 1990-92 to win three French Open titles in a row, and also to score her fourth overall. 

Until this fortnight, Ivanovic had only reached only one quarter final at the Slams, and it had been supposed by some commentators that she had not quite developed yet into a genuine contender. Would she panic against Sharapova, the ferociously competitive Siberian who had been on the record here to say that she could not be both a tennis player and Mother Teresa? Not a bit of it. Ivanovic, the world No. 7, kept her nerve in glorious style, striking the ball hard and deep throughout as she out-Sharapovad Sharapova. 

It has all come together for Ivanovic at Roland Garros, and there have been plenty of Parisians wishing her well, as she is about as unaffected, sweet and demure a tennis player as you will find. She also the looks to keep her management team busy with off-court offers. 

Quirkily, it was Seles, also born in the Serbian part of the former Yugoslavia, who inspired Ivanovic to take an interest in tennis. 

The story goes that Ivanovic, then aged just five, was watching Seles competing on television and, during a commercial break, an advertisement came on for a local tennis club. Ivanovic learnt the number off by heart, and when her mother returned to the room, she pleaded to be allowed to visit the club. 

From there, Ivanovic was soon in that disused pool. When we were in the pool, it was impossible to play crosscourt because the edge of the court was so close to the wall. We had to hit down the lines, she said. 

And it hardly got any easier, as, when she was 11, she had to arrange her practice sessions around NATO dropping bombs on Belgrade. 

Those were tough times, especially 1999 during the bombings. I thought it would be impossible to continue, because we didnt know how long it was going to go on. And, also, after that, we had troubles to travel, because we had problems getting a visa to another country, said Ivanovic, now Switzerland-based. 

Sharapova, a double Grand Slam champion, was also not born with a silver racket in her hand, and believes that has made her hungrier to succeed. I think the girls from tough backgrounds, and didnt start their careers in such a pleasant way, they have to find ways to do it on their own, she said. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>