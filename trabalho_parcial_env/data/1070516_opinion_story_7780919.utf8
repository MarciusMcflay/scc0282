<DOC>
<DOCNO>1070516_opinion_story_7780919.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SMART MOVE

 Imagine Mamata Banerjee coming to power in Bengal and approving an expansion of the Tatas small-car project in Singur. For forms sake, though, she may change a thing or two about how additional land required for the expansion is to be acquired. Given the bitter rivalry between her and Bengals Marxists, such a scenario appears to be unlikely, at least for the moment. But this is precisely what the Leftist government in Kerala has done. It has approved a project that the preceding Congress-led government had proposed. The case of the Smart City project, an information technology venture promoted by a Dubai-based firm, has a significant political message. The chief minister, V.S. Achuthanandan, is better known as a trade unionist. He shares little of the reformist zeal of Pinarayi Vijayan, the secretary of the Kerala unit of the Communist Party of India (Marxist). As the leader of the opposition in the last legislative assembly, he had objected to the project. All that did not stop him from proceeding with a project that was the idea of his political rivals. Mr Achuthanandan must have come to terms with the reality that running a government is very different from scoring points with political opponents. He must have also awakened to the importance of the project in the promotion of the IT sector in Kerala.

 However, the episode has a larger message for the political class elsewhere in the country. For far too long, political parties have wallowed in petty, partisan squabbles. In following obstructionist strategies, governments have missed the big picture and big opportunities. The result has been a disastrously slow rate of economic development. But that is inevitable if every government scraps every project conceived of and approved by another government run by a rival. Different parties can have their alternative approaches to development issues. Mr Achuthanandan amended some of the proposals of the previous government before agreeing to the Smart City project. But the important thing is that his government continued with the project where its predecessor had left it. Political leaders can take two important lessons from the case in Kerala. One, political parties need to increasingly evolve common approaches to economic issues. Two, a change of government must not hold economic policies to ransom.




</TEXT>
</DOC>