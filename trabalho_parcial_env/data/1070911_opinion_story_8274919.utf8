<DOC>
<DOCNO>1070911_opinion_story_8274919.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FAMILY AMP; OTHER ANIMALS 

 Uddalak Mukherjee

 I remember the morning I had visited 

 the Calcutta zoo for the first time. It was bright and cold, 

 in early December. My sister and I, both seven years old 

 then, had sat up through the night, the quilt drawn up to 

 our chins, waiting for the first light. We had been told 

 that the zoo was an enchanting place, full of fat, happy 

 animals, living in brightly-painted cages. We impatiently 

 waited for the car to arrive. 

 It did, after a while, spewing 

 white smoke and carrying an uncle, a silent, gloomy man, 

 who, we believed, had smiled only once, just after his birth. 

 He had promised to take the two of us to the zoo, and we 

 had agreed after some hesitation. As soon as we heard the 

 car, we pushed away the poached eggs and milk, put on our 

 sweaters, and trooped into the waiting vehicle. 

 After some time, we stopped in 

 front of a domed structure, with a gate and a ticket counter. 

 We waited, while the uncle bought the tickets. There were 

 men around the gate, selling little packets of nuts, oranges, 

 chewing gum and straw hats. The air smelt strange, almost 

 wild. We pushed round a noisy, revolving gate and entered. 

 Now, almost twenty-three years 

 later, I have forgotten many of the things I had seen. But 

 I do remember a few of them: some of the birds and animals; 

 the taste of sweet, sticky candyfloss, and, most of all, 

 my uncles face. On a bridge over a pool, he pointed excitedly 

 to what looked like a white, moving, chirping carpet on 

 water. They were migratory birds, he told us, winged creatures 

 that flew in each year from distant lands. 

 Next, we stopped in front of the 

 big cats. The tiger stared at the uncle. He looked back, 

 his eyes narrow, lips curved almost in a smile. My sister 

 and I stood watching, our gaze moving between uncle and 

 tiger.

 The monkeys were a disgrace though. 

 They snatched away the packets of nuts and made faces. A 

 zebra first sniffed, and then ate up my sisters candies. 

 And the reptile house was full of dreaming snakes. 

 Then, we reached the elephant 

 enclosure. I remember it being smaller than it is now. There 

 were two elephants, with chains on their feet. A man, standing 

 beside us, threw a coin, urging the animals to pick it up. 

 The bigger elephant looked away. The calf, however, moved 

 its trunk, sniffed the ground, picked up some sand and blew 

 it over the low fence, onto the mans face. There was a 

 shocked silence, then a muffled noise, and finally, loud 

 laughter. It was the uncle. Moments later, two shrill voices 

 my sisters and mine joined in. 

 On our way back, we stopped at 

 a restaurant for lunch. We ate slowly, and made a lot of 

 noise. Then two cheerful children followed their radiant, 

 adult companion into the warm, winter afternoon.




</TEXT>
</DOC>