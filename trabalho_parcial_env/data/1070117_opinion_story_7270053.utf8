<DOC>
<DOCNO>1070117_opinion_story_7270053.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 TWO ENDS IN ONE

 If it marks the end of the old order, the Maoists participation in Nepals interim parliament also signifies the demise of the old Left. For over a decade, some thirteen thousand people were killed as the rebels rehearsed their violent uprising. In the end, democracy, not the gun, was the winner. True, the Maoists had set the tone for last years jan andolan against the monarchy. But only the democratic movement helped them and the mainstream parties achieve what the decade-old armed insurrection had failed to do. Thus, the historic change in Nepal should be seen as the peoples rejection of both the feudal order symbolized by the monarchy and the Maoists cult of violence. The prime minister, Mr Girija Prasad Koirala, had made precisely the same point when he insisted that the Maoists could join the interim parliament only after surrendering their arms. This understanding was also the basis for the agreement between the Maoists and the seven-party alliance in November, 2005. The negotiations for the interim government and the interim constitution also succeeded only after the Maoists agreed to abjure violence. 

 However, the changes in Nepal so far have only set the stage for the new order. The Maoists and the democratic parties have fought together to end the old system; they have to move together now in order to usher in the new one. It is possible that differences of opinion will cloud the new horizon at times. But they must ensure that the gun no longer takes the place of democratic debate. This is not the first time that the people of Nepal have fought for democracy. The pro-democracy movement of 1989-90 ended in a new constitution that gave the monarchy only a constitutional role and introduced the multi-party democracy. The utter failure of the parties to strengthen the democratic system resulted in the rise of Maoist extremism and of the dictatorial monarchy. Neither the parties nor the Maoists can afford to repeat that history of failure. Nor can they make democracy secure in Nepal if they remain ambivalent toward the monarchy. It is not only an anachronistic institution but also a permanent threat to democracy. It is not enough to take away its powers and privileges; its existence, in whatever form, could be a rallying ground for anti-democratic forces. Only an end of the monarchy and of the Maoist violence can ring in real democracy in Nepal.




</TEXT>
</DOC>