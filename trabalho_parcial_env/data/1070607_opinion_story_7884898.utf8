<DOC>
<DOCNO>1070607_opinion_story_7884898.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 HUNGRY, BUT AT HOME

 Banning women under thirty from going abroad to work as domestic help would curtail their right to work and freedom, writes Manjula Sen

 Safety first

 The recent incident of human trafficking by elected members of the parliament has not prompted any action from lawmakers to restrict or ban foreign visits by MPs and their family members. Indeed, it would be risible even if such a proposal was made along with certain riders such as restricting the eligible age for foreign travel to over 30 years. Significantly, lawmakers breaking the law quite so spectacularly did not cause the scale of outrage or concern that one would have thought was warranted by such brazenness. Instead, it was left to the legal apparatus to chart out its own course of action.

 This particular incident was in sharp contrast to two other recent events that also revolved around migrant workers. On one occasion, the minister of child and women welfare returned from a visit to Kuwait so distressed by the complaints of sexual and economic exploitation of Indian women migrant workers in the Gulf region that she, according to media reports, instantly announced a ban on women under 30 going abroad to work as domestic help. The ban, the press reported, would apply to 17 countries. The right of women to travel for work was thus swiftly curtailed by such a decree.

 It is unlikely that such a step can withstand legal scrutiny. But it is the spirit of the action rather than the wording of the decree that is more disconcerting. Moreover, such a measure also raises some uncomfortable questions. For instance, should a victim have to pay for a crime that was committed against her? Is this not an abdication of responsibility on the part of the government? What kind of disciplinary action is being envisaged to thwart criminal elements among recruitment agents? Or, for that matter, is there some sort of mechanism in place to ensure the security of female economic migrants in high-risk countries? One would also like to know whether the minister concerned conducted an empirical study before unleashing her off with their heads dictum?

 In the past, studies by the International Labour Organisation have chronicled the perils that Asian women migrant workers face in some countries that do not have adequate safeguards against exploitation. Interestingly, these studies have also warned that a policy of restricting female migration has limited effectiveness, and argue that such bans only increase illegal migration, thereby making the migrants more vulnerable.

 It is not as if the hardship, sexual exploitation and discrimination faced by women workers abroad is new, or has been insufficiently reported. Hostile working conditions are something that male migrant labourers face as well. However, the difference is that a ban has not yet been declared as a redressal mechanism in their case. Equally bewildering is the choice of an arbitrary age 30 years as the cut-off limit. Does this mean that sexual exploitation and economic abuse are to be graded differently? After all, economic abuse is not age-restricted and this form of exploitation can continue for a long period of time. These certainly are complex issues and a hasty announcement is uncalled for. 

 Indeed, there seems to be some confusion about the fine print. Is the decree a ban or a restriction? Which work categories and countries does it apply to and when would it come into effect?

 What is undeniable, however, is the fact that the right to work and freedom of movement for migrant women workers cannot be summarily dispensed with. Women workers abroad, such as nurses, maids and caregivers, do not usually belong to the affluent sections of society. Hence, a curtailment of their economic rights can have a devastating effect on them. 

 The Central government was not the only agency to take on the role of a patriarch. Around the same time that the ban on maids was announced, Bangalore, often touted as the infotech capital of the country, saw a vocal protest being launched by thousands of demonstrators. These protests ultimately led to the repeal of a notification that sought to apply an archaic law which would have endangered the employment rights of blue collar women workers. 

 The controversial law sought to ban women from working in night shifts in all establishments which came under the Karnataka Shops and Commercial Establishments Act, 1961. The chief minister had stated that the ban on night shifts for women workers had been prompted by the rising incidents of crime against women. However, information technology companies and upscale hospitality establishments were exempted from the ban. This was ironic, considering that some of the more widely reported crimes had been committed against women working in IT companies.

 The selective stance adopted by the Karnataka government was one of the reasons as to why it was forced to back-pedal on this particular issue. Another reason was that the employers demanded an immediate repeal of the ban.

 In both instances, the State took the view that in order to protect women, it was necessary to keep them indoors. In both cases, the women were a part of the labour market and not employed in the services sector which is perceived to be either better regulated, safer, or more organized. This approach is both paternalistic and condescending. And both measures are debilitating when it comes to womens empowerment.

 That such quick-fire decisions can be taken is explained by the lawmakers approach to an economic issue from a moralistic rather than analytical point of view. Perhaps the problem lies in the perspective of the lawmakers who are asked to judge such complex issues. One just has to look back at the discussion preceding the ban on bar dancers in Maharashtra. In this particular instance, legislators had commented that it was better for a woman to commit suicide than dance at a bar. 

 Little wonder then that the issue of right to livelihood and the States responsibility towards providing a non-discriminatory legal framework, which could be implemented in a safe and just manner, is becoming increasingly inconsequential. The forcible expulsion of former dance-bar women from a residential colony in suburban Mumbai by activists of the Shiv Sena is just another example of a stride in the wrong direction.




</TEXT>
</DOC>