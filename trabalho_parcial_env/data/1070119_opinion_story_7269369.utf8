<DOC>
<DOCNO>1070119_opinion_story_7269369.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 Paperback Pickings

 Why does the sky smell sour?

 The Tenth Rasa (Penguin, Rs 295) edited by Michael Heyman, Sumanyu Satpathy and Anushka Ravishankar is an anthology of Indian nonsense, where each editor contributes a section of the introduction. The untranslatability of nonsense is, as the editors admit at the outset, one of the biggest challenges facing such a project as theirs. Satyajit Ray is one of the few who managed to translate the register of Lears Jumblies in his Papangul, or of Carrolls Jabberwocky in his Joborkhaki. Unfortunately, these do not find a mention in the otherwise comprehensive and enriching introduction, which only has references to Rays introduction to The Select Nonsense of Sukumar Ray. Sampurna Chatterjees translations of Sukumar Rays nonsense verse and stories are quite well-known (although some of Sukanta Chaudhuris translations could also have been included, if only to let the readers compare). Shanta Shelkes Once... which goes Once, flowers became children/ And children became flowers!/ The children sat in the garden,/ And the flowers went to class... seems to have retained its original Marathi flavour through Anita Vachharajanis English rendering. The Oriya limericks translated by Sumanyu Satpathy are enjoyable too, as is the innovative section on Nonsense in Hindi film (which, of course, has the unforgettable Anthony Gonzalves speech from Amar Akbar Anthony). Perhaps the strikingly similar thoughts shared by the Tamil folk nonsense poem, Grandpas beard, with Lears limerick about an old man and his beard prove the universal brotherhood of nonsense poets.

 Iraq, Inc.: A Profitable Occupation (Earthcare, Rs 195) by Pratap Chatterjee is another book which provides the real story of the Iraq war and that is, obviously, miles away from the version handed out by the occupation forces. But again, the stakes of Halliburton, Bechtel and their likes in the destruction-followed-by-reconstruction of Iraq are quite well-known by now. Chatterjee, an investigative journalist, unveils one racket after another, and at the end, it is possible to see a sinister design emerging.

 The cricinfo guide to international cricket 2007 (Penguin, Rs 295) edited by Steven Lynch is a ready reckoner for todays cricket lover and sports journalist, just ahead of the World Cup. The profiles of individual cricketers are truly well-written. Not many handbooks will tell you that Mohammad Yousuf is particularly strong driving through the covers and flicking wristily off his legs, and has a backlift as decadent and delicious as any, although a tendency to overbalance when playing across his front leg can get him into trouble. The guidelines for using Statsguru are most helpful.

 The Life and Times of the Nawabs of Lucknow (Rupa, Rs 295) by Ravi Bhatt has a wealth of historical research behind it, but it does not intimidate the reader, nor is it dry and pedantic. For one, the chapters are short, although brought together under four broad sections: Palace politics, Art and culture, Begums and Life and living in Lucknow. For another, the language employed is closer to that of the storyteller than the historian. Sample this account of the eccentric classical singer, Haider Khans (popularly known as Sidi Haidary) encounter with Nawab Ghazi-ud-din: Once at the palace, the genius started singing his melodious compositions so well that the Nawab almost went into a trance. But then, Haidary suddenly stopped singing. When the Nawab requested that he continue, Haidary asked, The tobacco that you are smoking in your hookah is very good. From which shop did you buy it?




</TEXT>
</DOC>