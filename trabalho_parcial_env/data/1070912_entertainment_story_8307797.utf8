<DOC>
<DOCNO>1070912_entertainment_story_8307797.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Im dying here

 Must-have movies - Dog Day Afternoon

 Nineteen seventy-five was one of the all-time great years for the Best Picture Oscar, offering such a wealth of options for Academy voters that the winning film (One Flew Over the Cuckoos Nest) looks more and more like the least interesting of the five nominees. Spielbergs Jaws, Kubricks Barry Lyndon and Altmans Nashville are all movies for the ages, and so is Sidney Lumets Dog Day Afternoon, a masterly reconstruction of a Brooklyn bank siege on August 22, 1972, built around arguably Al Pacinos finest screen performance.

 The role of bisexual desperado Sonny Wortzik was Pacinos great chance to extend his range after making his name in the first two Godfather pictures (1972, 1974) and Serpico (1973). The temptation would have been to chew the scenery, so the real masterstroke was to let it chew him. Pacinos Sonny is fighting off a feeding frenzy: the authorities who want him dead, the demands of his loved ones, and the bright white glare of media scrutiny.

 Every line and gesture in the performance bespeaks a neurotic desperation, as Sonny drips with sweat Im dying here his slightly self-dramatising catchphrase and continues to rack his brains for a way out. Its amazing and moving work.

 Sonnys motive for the robbery is to fund a sex-change operation for his lover Leon (Chris Sarandon, persuasively effete, and nominated for Best Supporting Actor). They have one long dialogue together on the phone which was famously improvised. It is a beautiful scene, a still moment of reckoning in the midst of a baying three-ring circus.

 This was a career high for Lumet, a director whose journalistic approach to drama can make his movies preachy and sensationalistic at the same time. Here he closed right in on the human mess of a fraught, sad situation, and the movie is solidly buttressed by his lean docudrama style.

 It has proved very influential, too: hostage thrillers from Costa-Gavrass Mad City (1997) to F Gary Grays The Negotiator (1998) and Spike Lees Inside Man (2006) tip their hats to it. You cant rob a bank these days without Sonny Wortzik sending off warning signals in the back of your mind.

 Tim Robey (The Daily Telegraph) 




</TEXT>
</DOC>