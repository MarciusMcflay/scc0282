<DOC>
<DOCNO>1070416_opinion_story_7630737.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FORCES ON 

 REVIEW 

 - Why is the DRDO stagnating? 

 Brijesh D. Jayal

 For far too long, there has virtually 

 been no accountability within the defence management system 

 to ensure that our fighting forces are equipped with the 

 proper weapons and systems to handle the complex security 

 challenges facing the country. The resultant state of modernization 

 of our armed forces today is therefore cause for alarm. 

 Many major weapons system projects under the Defence Research 

 and Development Organization of the ministry of defence 

 have been stagnating for over a decade or more. Not surprisingly, 

 a recent report by the parliamentary standing committee 

 on defence has been highly critical of the functioning and 

 performance of the DRDO.

 Based on its recommendations, 

 the government has now formed a committee to carry out an 

 independent review of the DRDO. The unfortunate fact is 

 that the laudable aims of self-reliance and indigenization 

 have been so misused that, for years, the DRDO and defence 

 public sector units have always had the first call on any 

 operational requirement that the services may project. As 

 numerous examples have shown, these organizations have readily 

 accepted the commitments but rarely delivered. While the 

 armed forces continue to face the adverse operational consequences, 

 no one has been held to account. With 5,000 scientists, 

 25,000 other scientific, technical and support personnel, 

 51 laboratories and an annual budget of around Rs 5,000 

 crore, the DRDOs vision as spelt out on its website, 

 of making India prosperous by establishing world class 

 science and technology base and providing the defence services 

 decisive edge by equipping them with internationally competitive 

 systems and solutions has remained an elusive vision. 

 The genesis of the parliamentary 

 committees ire has been the DRDOs inability to deliver 

 on the many vital projects that are at hand and the absence 

 of any accountability for gross time and cost overruns. 

 While the composition of the review committee has been announced, 

 one is not aware of the terms of reference. In all fairness, 

 while there is much that the DRDO needs to answer for, both 

 to the armed forces and the tax-payer, it would be unfair 

 to limit the committees charter to just reviewing the DRDO. 

 If indeed the spirit of the exercise is to inject efficiency 

 and accountability into the entire system of modernizing 

 the armed forces, then every organization that plays a part 

 in the process needs to be reviewed for its contribution 

 to this sorry state of affairs. 

 It is understood that both the 

 MoD and the DRDO were firmly opposed to the concept of an 

 independent audit and review. That the alternative view 

 has prevailed indicates that, in keeping with the prevailing 

 spirit of transparent and merit-oriented decision-making, 

 the government is not willing to treat the DRDO as a holy 

 cow. In furtherance of this spirit, one hopes that the terms 

 of reference of the proposed committee will not be limited 

 to the DRDO alone but will extend to the other holy cows 

 that must also share the burden of this state of blissful 

 neglect of national security. 

 We need to ask ourselves why we 

 have allowed the DRDO to become an omnibus organization, 

 which is involved in activities as diverse as basic research, 

 at one end of the spectrum, to designing and developing 

 complex weapons systems like main battle tanks and light 

 combat aircraft, and on occasion even indulging in pre-production 

 activities. This, when there exist large defence production 

 units with integrated design and development departments, 

 whose primary task is precisely to undertake these latter 

 activities. In the event, the DRDO falls between two stools 

 and has been unable to fulfil, through research and development, 

 what should have been its primary function. That of ensuring 

 that the Indian armed forces are technologically prepared 

 and operationally relevant in the ever-evolving technological 

 and security environment. While the former would be a function 

 of the research being carried out and the advice provided 

 to the MoD and the armed forces, the latter would be through 

 applied research where technology can be developed, commercialized 

 and transferred to the defence industry, which would then 

 apply it to weapons system development. 

 This brings us to the defence 

 industry, which consists of both defence public sector units 

 and the ordnance factories. Here one must differentiate, 

 between the navy and the other two services, because the 

 former has been far more successful in indigenous design 

 and production; possibly because it still runs its own design 

 department, and shipyards have largely been headed by serving 

 or retired naval officers. The rest of the defence industry 

 has been more interested in keeping its production lines 

 going rather than aggressively contributing to design and 

 development of futuristic weapons systems with applied technology 

 inputs from the DRDO. The industry is far more comfortable 

 with licensed production, with no risks and assured production 

 orders. With the services as captive customers and prices 

 of products artificially fixed, the system is not conducive 

 to a competitive and dynamic culture, where providing the 

 armed forces with technologically current weapons systems 

 at competitive prices carries a premium. This culture has 

 several negative fallouts. It leads to the stagnation of 

 the industrys own design and development capability, thus 

 making it reliant on further licensed programmes. There 

 is no backwards push to the DRDO to come up with technologies, 

 which can be commercially applied, to future weapons system 

 designs or for weapons system upgrades. And finally, such 

 an industry becomes lethargic and is incapable of competitiveness 

 in the international arms market. 

 At the end of the day, it is the 

 armed forces that are the ultimate users of the final products 

 of indigenous research, development and production. In any 

 healthy commercial organization, the customer is king. It 

 is only in the existing defence management system that the 

 customer is actually the slave. He is made to feel apologetic 

 about futuristic requirements to meet his operational needs 

 and is often accused of aping foreign sales brochures. He 

 is dubbed as pro-import when he is not convinced that indigenous 

 claims are realistic. The ministry of defence sits in judgment 

 over technical and operational issues, for which it lacks 

 professional expertise. Often it rules in favour of claims 

 made by the DRDO or the defence PSUs, driven by the lofty 

 ideals of self-reliance and indigenization, but, one suspects, 

 also to take the easy route, as the alternative involves 

 imports and the bogey of arms dealers, et al. 

 The armed forces have only themselves 

 to blame for this pitiable plight. Service leadership, possibly 

 because of a false sense of patriotism, has found it politically 

 correct not to openly criticize these fatal systemic flaws. 

 The few that have voiced concern have done so either in 

 a muted fashion or on the eve of shedding their uniform. 

 In this age of rapidly advancing technology and equally 

 rapid obsolescence of weapons systems, the need is for integrated 

 teamwork across the spectrum of research, applied research, 

 development, testing, operationalizing and productionizing. 

 With so much at stake, the services have also not shown 

 any enthusiasm to establish functional technology and systems 

 commands with delegated authority to work alongside the 

 DRDO and the industry. Part of the existing problem is precisely 

 the absence of such a mechanism. 

 This brings us to the holiest 

 of holy cows, the MoD. It commands all the authority with 

 no attendant accountability. It stands as arbiter of disputes 

 between the services and the DRDO or the defence industry, 

 without possessing the requisite technical or operational 

 expertise. No incremental delay or cost overrun can be permitted 

 without the sanction of the MoD, yet no questions are asked 

 of it. If indeed the nation aspires to take its place in 

 the forefront of defence technologies, to become a force 

 to reckon with in producing weapons systems to equip its 

 defence forces and to compete in the international market, 

 then it is the entire defence management system that must 

 come under scrutiny, not just the DRDO. Scrutiny not only 

 of performance, but the charter, organization, decision-making 

 hierarchy, authority and accountability within each of the 

 organizations is vital. 

 Unless we are willing to broaden 

 the charter of the proposed committee to encompass the above 

 weaknesses, the spirit and purpose of our review will not 

 be served. A valuable starting point would be to task the 

 College of Defence Management to produce classified management 

 case studies on the main battle tank, the light combat aircraft 

 and the Trishul missile projects. These studies can form 

 the basis of the terms of reference for the proposed committee. 

 The committee will then have its work cut out.

 The author is 

 a retired Air Marshal of the Indian Air Force




</TEXT>
</DOC>