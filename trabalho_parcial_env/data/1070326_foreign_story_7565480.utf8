<DOC>
<DOCNO>1070326_foreign_story_7565480.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 Slow down, brave multi-taskers

 STEVE LOHR

 One thing at a time? 

 New York, March 25: Confident multi-taskers of the world, could I have your attention? 

 Think you can juggle phone calls, email, instant messages and computer work to get more done in a time-starved world? Read on, preferably shutting out the cacophony of digital devices for a while.

 Several research reports, both recently published and not yet published, provide evidence of the limits of multitasking. The findings, according to neuroscientists, psychologists and management professors, suggest that many people would be wise to curb their multi-tasking behaviour when working in an office, studying or driving a car.

 These experts have some basic advice. Check email messages once an hour, at most. Listening to soothing background music while studying may improve concentration. But other distractions most songs with lyrics, instant messaging, television shows hamper performance. Driving while talking on a cellphone, even with a hands-free headset, is a bad idea.

 In short, the answer appears to lie in managing the technology, instead of merely yielding to its incessant tug.

 Multi-tasking is going to slow you down, increasing the chances of mistakes, said David E. Mayer, a cognitive scientist and director of the Brain, Cognition and Action Laboratory at the University of Michigan. Disruptions and interruptions are a bad deal from the standpoint of our ability to process information.

 The human brain, with its hundred billion neurones and hundreds of trillions of synaptic connections, is a cognitive powerhouse in many ways. But a core limitation is an inability to concentrate on two things at once, said Ren Marois, a neuroscientist and director of the Human Information Processing Laboratory at Vanderbilt University. 

 Marois and three other Vanderbilt researchers reported in an article last December in the journal Neuron that they used magnetic resonance imaging to pinpoint the bottleneck in the brain and to measure how much efficiency is lost when trying to handle two tasks at once. 

 Study participants were given two tasks and were asked to respond to sounds and images. The first was to press the correct key on a computer keyboard after hearing one of eight sounds. The other task was to speak the correct vowel after seeing one of eight images.

 The researchers said they did not see a delay if the participants were given the tasks one at a time. But they found that response to the second task was delayed by up to a second when the study participants were given the two tasks at about the same time.

 New York Times News Service




</TEXT>
</DOC>