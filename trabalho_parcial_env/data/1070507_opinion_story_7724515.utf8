<DOC>
<DOCNO>1070507_opinion_story_7724515.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 HEAT AND DUST

 - The power sector suffers from a backward-looking mindset

 Commentarao -S.L. Rao

 The author is former director-general, National Council for Applied Economic Research

 In search of light

 Another hot summer is upon us and once again there are severe power shortages in every part of the country. Is there ever going to be a respite for the harried Indian consumer? 

 There are many things that must change. There are fundamental and till now insuperable issues, badly managed macro issues and issues of poor implementation. 

 The responsibility for power distribution in India is with the state governments because the Constitution placed electricity on the concurrent list. (In 1951, long-distance transmission was not possible; it became so soon after. That would have made electricity a Central subject, and the situation might have been the opposite of what it is today). The states since 1951 have added substantially to the capacity for generation and distribution. However, they have been unable to withstand public and political pressures to treat electricity as a public good. They have made it available to large groups even at a loss. This has resulted in huge losses to the state electricity boards. State government ownership of all distribution and intra-state transmission as well as a bulk of generation have also led to a bureaucratic style of management. This has added to inefficiencies, the tolerance of indiscipline and collusive theft by employees. 

 The lack of a political consensus in India prevents politicians from taking rational stands on issues. Paying for services like electricity is one such. Politicians and well-placed bureaucrats in state governments condone electricity thefts, prevent police action against thieves and plead for non-paying consumers. Politicians out of power do not support the initiatives they took when in power. The result is a diversion of public funds that could have been invested in fresh capacities. Instead, these funds support thieves and the well-connected. 

 It has taken the Central government many years to understand that private investment depends on the investor getting a decent return. That is why the early initiatives to tempt private investment into generation failed. The government amended the law in 1998 to allow private investment in transmission and in distribution. The Power Grid Corporation, a Central government company, had the monopoly on interstate transmission. State electricity boards monopolized intra-state transmission. Power Grid was against private participation and would allow private investment only in joint-sector ventures over which it would have control. So private investment in transmission is very recent. Thus, a Central government monopoly for nine years prevented the implementation of a law allowing private investment in transmission. The SEBs did the same with intra-state transmission. 

 Distribution has been an even greater catastrophe. Orissa privatized well over ten years ago. It was a sloppy and ill-thought out privatization. It has ended with one private company walking out and returning the company to the state which is running it inefficiently. The other private company is beleaguered by unreasonable demands from the state government that used up the profits it made from privatization and is now demanding fresh funds to flow from the private distributing company. 

 Delhi distribution was privatized five years ago. The Delhi Vidyut Board was plagued by huge losses, massive thefts and undisciplined staff, many of whom colluded in the theft of electricity. Its procedural mindset, which was not customer-oriented, was not focussed on efficiency and quality improvement. Delhi is short of electricity supply in peak season, largely because of limited investment in new generation and severe shortages in the whole of north India. Increased supplies were inadequate for the growing demand. 

 After privatization, thefts have come down sharply and quality has improved. The pampered, extremely vocal and influential Delhi citizenry protested against the new tamper-proof electronic meters which read their consumption properly, leading to some consumers paying more than before. The same meters in Mumbai did not evoke a murmur of protest. Neither the prime minister nor the Congress president supported the courageous chief minister who had introduced privatization against this agitation. The result: states like Uttar Pradesh and Gujarat that were ready to privatize distribution following Delhi, halted privatization. They did not want to face flak, having seen the inability of a Congress-led government at the Centre to support a Congress government in Delhi. There is no talk any more of privatizing distribution. This will prevent any significant investments at any point in the sector. 

 Nineteen ninety-eight also witnessed the passing of the Electricity Regulatory Commissions Act and the creation of the Central electricity regulatory commission and other state electricity regulatory commissions (Orissa had done it two years earlier). Chairpersons and members are almost all retired government servants. The staff is mostly deputed from the government. The mindset therefore largely conforms to the existing administration-oriented management of the sector, especially in the states. 

 There have been practically no innovative or commercial ideas. There was no base-line data, no monitoring and follow-up for implementation of orders issued. When the new act asked for open access to transmission lines on payment of a small surcharge, the commissions imposed high charges. Open access remained unused. 

 When the CERC was asked to set out rules for electricity trading, it set a minuscule trading margin as well, though this could easily have been left to the negotiations between buyers and sellers. The CERC introduced the availability-based tariff, a commercial mechanism to stabilize the frequency of flows in the system. There was a penal surcharge to be paid by a buyer or seller who caused frequency disturbance by missing forecasts. This penal surcharge is now regarded by many regulators as a price. Many SEBs starve their customers to make the large margins that this penal surcharge offers, with no punishment by the regulators. 

 A penal charge cannot be a tariff; a tariff must be discovered between buyers and sellers. Orders of commissions on similar issues were contradictory and inconsistent and have not enabled the development of regulatory law and precedents. Thus, independent regulatory commissions have played little role in moving reform of the power sector forward through improved efficiency and adequate new investments. 

 A major impediment is also that the price of electricity is regulated; profits are predetermined by regulators. The major cost element is the fuel, coal, domestic or imported, gas, naphtha and hydro-power. Except for the last, for which tariffs are determined by the electricity regulators, fuel prices are market determined or determined by the owner in the case of coal, the owner being the government. This has led to gas being little used and the few gas-based plants are working below capacity because of limited availability. 

 At the state government levels, funds are short, the administration is inefficient making poor forecasts the quality is unreliable, thefts are huge, and large and powerful voter- groups like farmers are heavily subsidized. Also, advantage is not taken of Central government funds through schemes like the Accelerated Power Reform and Development Programme. Many times, regulators orders are not obeyed, yet they do not punish such violations. 

 The crisis in the power sector is two decades old. Apart from regular meetings of state and Central governments, little action is visible. The Electricity Act, 2003, gave some hope for reform but it has been stymied by poor implementation. 

 In recent years, the pithead based ultra-mega power projects, backing by the Power Finance Corporation, private investments in transmission, hydro-power projects, impetus to captive generation, development of power trading and the impending Power Exchange, give hope of faster forward movement. They are inadequate for alleviating shortages given the growing demand, and confined to some parts of India. The macro issues, political problems and bureaucratic mindsets leading to poor implementation, show no signs of being changed.




</TEXT>
</DOC>