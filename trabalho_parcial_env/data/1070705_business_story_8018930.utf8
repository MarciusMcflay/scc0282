<DOC>
<DOCNO>1070705_business_story_8018930.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Business

 Blackstone to check in at Hilton 

 Los Angeles, July 4 (Reuters): US private equity firm Blackstone Group will buy Hilton Hotels Corp for about $20 billion plus debt, the richest in a series of recent private equity offers for hotel companies.

 Under the terms of the deal, Blackstone will pay $47.50 per share in cash, a 32 per cent premium to Tuesdays closing price, for one of the most prominent global hotel brand names.

 Ahead of the post-close announcement, shares of Hilton had risen 6.4 per cent to close at $36.05 on the New York Stock Exchange.

 Blackstone, which manages over $88 billion in assets, was recently in the news for its record initial public offering that raised over $4 billion, the largest ever. The firm is not using its shares to fund the Hilton acquisition. 

 The all-cash deal has received financing commitments from Bear Stearns, Bank of America, Deutsche Bank, Morgan Stanley and Goldman Sachs.

 Blackstone said it intended to invest in the Hilton properties and brands globally to grow the business.

 The hotel industry is enjoying a multi-year boom as robust demand has allowed hoteliers to steadily raise rates. The upbeat market environment, supported by limited construction of new hotels, has made lodging assets hot commodities.

 Hilton is worth $20.1 billion, based on 424 million shares on a diluted basis at the end of March, at the price Blackstone is paying. 

 Hilton reported more than $7 billion of debt during its January-March quarterly results.

 A spokesman for Blackstone said the group was not revealing the debt and equity breakdown of the deal at this stage. Many investors think the notoriously cyclical US hotel industry has more room to grow and could provide risk-adjusted returns that outperform other asset classes, according to Thomas Callahan, an analyst with hospitality industry tracking firm PKF Consulting in San Francisco. 

 The perspective is that the industry still has a few more good years left in this upturn that began depending on how you count it several years ago, Callahan said.

 Blackstone already has a significant portfolio of hotel and resort properties that includes more than 100,000 hotel rooms in the United States and Europe. Its properties include La Quinta Inns and LXR Luxury Resorts and Hotels.

 Hiltons brands include Hilton, Conrad Hotels amp; Resorts, Doubletree, Embassy Suites, Hampton Inn, Hilton Garden Inn, Hilton Grand Vacations, Homewood Suites by Hilton, and The Waldorf=Astoria Collection. The Hilton chain was founded in 1919 by Conrad Hilton, the great grandfather of US celebrity socialite Paris Hilton.

 Her grandfather Barron Hilton, co-chairman of the Hilton board of directors, owns 5.3 per cent of Hilton shares, according to the companys April proxy. 

 He is the brother of the late Conrad Nicholson Hilton Jr, called Nicky, who was the first husband of actress Elizabeth Taylor.

 The deal is expected to close during the fourth quarter of 2007 and was approved by Hiltons board of directors on Tuesday, the company said.

 Blackstone said it expected no significant divestitures as a result of the transaction.

 So far this year, several private equity groups have made multibillion-dollar offers for hotel assets.

 Investors believe the limited supply of new hotel rooms in key markets means they can achieve higher returns by buying and investing in existing hotels, said Joseph Toy, president of Hospitality Advisors LLC in Honolulu.

 The game here is acquire at a premium, then renovate and reposition, Toy said. And with capital as abundant as it is, its something were seeing across the market, including Hawaii. 

 Last month, Goldman Sachs real estate fund, Whitehall, said it would buy Equity Inns Inc, a mid-market hotel chain, in a deal worth $2.2 billion, including assumed debt. 




</TEXT>
</DOC>