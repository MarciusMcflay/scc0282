<DOC>
<DOCNO>1070721_nation_story_8087571.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Not afraid to speak up

 JYOTI MALHOTRA

 New Delhi, July 20: As the grainy images of Saddam Hussein, with the noose around his neck and a handful of Kurds taunting the former dictator, were flashed around the world, many Indians were appalled.

 The foreign ministry, at the time visibly warming up to the nuclear deal with the US, feebly announced that it was disappointed with the American move.

 But one former Indian diplomat, Hamid Ansari, who had earned his spurs in the same elite foreign service, was not afraid to speak up.

 Pointing out that the hanging had taken place on Id, Ansari said in an interview: Why has Saddam been executed today? This is not a measure of justice, this is a measure of revenge and celebration. The timing of Saddams hanging is vicious.

 The mild-mannered Ansari may be soft-spoken, but when he wishes he can make himself heard very clearly.

 Perhaps the fact that he was born in loquacious Calcutta in 1937 has something to do with it. Ansari travelled to Shimla for his schooling, but returned to Calcutta in the fifties to study in St Xaviers College. Then he went to Aligarh Muslim University (AMU), where he was to return later as vice-chancellor.

 Ansari is best known as a scholar of West Asia. After he joined the IFS in 1961, he specialised in the Arab world, where he spent years as ambassador on both sides of the Shia-Sunni divide. Ansari speaks Arabic and Persian, besides Urdu, Hindi and English.

 As chief of protocol (from 1980-85), he often interacted with Indira Gandhi. In 1983, when she organised what must still rank as one of Indias best conferences ever the Non-Aligned Summit (when Fidel Castro gave Mrs Gandhi a bear hug) Ansari was in minute-to-minute control of the event.

 As for suggestions that Ansari has not been in active political life, friends argue that he has had enough experience both as permanent representative at the UN and as vice-chancellor of AMU. If youve served in the academic council of AMU, said a friend, handling the sound and fury of the Rajya Sabha is a piece of cake. 

 Journalist Dilip Padgaonkar, a co-member of the minorities commission, recalls the time when he and Ansari went to Nandigram to inquire why 14 people had died in police firing. Were they real policemen, the two asked, or were they political workers in police uniform?

 When scores of villagers claimed that they were party workers, Ansari wanted to know how they were so sure.

 One man replied: The uniform on real policemen is usually very tight because they have such fat bellies. But most party cadres are thin, so the uniforms were hanging loose. Moreover, regular policemen wear leather shoes. These men were wearing rubber chappals.

 Their report found that the killings may have been the result of state failure but were not communal.




</TEXT>
</DOC>