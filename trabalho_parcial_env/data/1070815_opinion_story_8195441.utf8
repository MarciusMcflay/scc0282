<DOC>
<DOCNO>1070815_opinion_story_8195441.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 What did India learn from its wars 

 By Sujan Dutta

 At your Service: victorious Indian jawans in 1971; Sam Manekshaw (below) 

 The bullet tore into the fibreglass and tore out of it. Subedar Brahm Singh was holding in his hand the helmet given to him by one of his men. He stared hard at it, disbelieving what had happened. But the evidence was there. 

 In the crisp afternoon in Mushkohs beautiful valley on July 4, 1999, army surgeons were setting up a field medical station. On the slope in front, weary soldiers climbed down the southern spur of Tiger Hill. Many carried stricken comrades on stretchers and shoulders. Some of the wounded would be taken to the two-seater Cheetah helicopters flying untiring casevac (casualty evacuation) sorties. Kargil Indias last war had all but climaxed. 

 The night before, the ghataks forward party from the 18 Grenadiers battalion had fought it to the summit of Tiger Hill. The battle was still raging. Pakistans shells flew over the Valley and crashed into the soft hillside behind us. 

 The grenadier with the two bullet holes in his helmet was one of the stretcher-bearers. The helmet did the rounds from hand to hand. The bullet fired by a sniper had miraculously zipped through the centimetre of space between his scalp and the concave inside of the primitive headgear. 

 Thats luck for the Indian soldier. In every war, trust wraps him like a fibreglass helmet around the head. He trusts his superiors will have mapped out the battle that the country trusts him to win for it. But in each of Indias overt wars, barring 1971, he has been sent to the battle ill-equipped. That was the only occasion when the top brass, with Sam Manekshaw at the helm, earned authority for military counsel over political expediency. The nonagenarian field marshal, now in and out of hospital in Wellington, put his foot down and told Indira Gandhi in June that he would take six months to prepare. The result after the December war was a resounding and undisputed victory. In no other engagement has the military top brass exerted its authority in like manner. 

 But the military was often willing to be sycophantic, willing to tailor assessments to suit political ends. The trooper complied. Only in 1962, with not enough to clothe him in NEFAs wet Himalayan winter, and his magazines empty, did the jawan flee the battlefield. 

 Those two years mark the extremes of Indian military performance in six decades a victory that sundered Pakistan and created a new country and a debacle that is so deeply disconcerting even today that the government still thinks the Henderson Brooks report should continue to be classified 45 years after the event. 

 None of the other military operations match the ecstasy and agony for the defence establishment as those two. Not surprisingly, the photograph that represents the pinnacle of Indias military glory Lieutenant General A.A.K. Niazi of Pakistan signing the instrument of surrender in Dhaka in December, 1971 adorns nearly every military establishment. 

 But in each of the wars Kashmir (1947-48), Hyderabad (1948), Goa (1961), Siachen (1984 onwards), Sri Lanka (1987-90), Kargil (1999), Maldives (November 1988) and Parakram (2001-2002) there were lessons delivered. Some were learnt, but much was unlearnt. The funny thing about war is the fact that it is difficult to measure what militaries learn from the last one unless they get into the next. That is the grisly equivalent of asking: how many bodybags? 

 Indias war machine is currently going through a post 9/11 metamorphosis, the top brass would have us believe. It is dictated by economics and politics. Soviet-era hardware is being phased out. Indias de facto recognition as a nuclear power and its engagement with the US-led coalition have never been as intensive as it will be when the USS Nimitz and the USS Kitty Hawk carriers sail up the Bay of Bengal next month. Sixty years since 1947, India is dealing and engaging with a power and a military philosophy that it has always shunned. 

 Modern India went to war at birth. In 1947-48, both the Indian and Pakistani armies still had British officers and British hardware. Neither India nor Pakistan emerged with threats of external invasions. But both inherited territorial disputes from borders drawn at the whim of colonial map-makers. In 1947, when Pakistan came up with Operation Gulmarg the push of tribesmen across the border in Kashmir Nehru held out till Raja Hari Singh of Jammu and Kashmir signed the instrument of accession at gunpoint. 

 The gun was Pakistans. Indias most celebrated military hero, Sam Manekshaw, who was then in the Directorate of Military Operations, says their forces were about nine kms from Srinagar when Hari Singh signed Kashmir into India and Nehru ordered the airlift in Dakotas from Gurgaon. The first troops that landed on Srinagars pockmarked airfield were actually based in the Delhi suburb thats now a cyberia. 

 What this meant for the jawan is that he did not have adequate time to prepare for battle. Nehru and his government were almost certain that Hari Singh would sign in, but that certainty did not translate into a warning for war in good time. Riven by Partition, many of its soldiers having borne its brunt, India ordered the army into a war. The Indian army should have lost it but for the hungriness of its men and the reluctance of the Pakistani army to put its officers in front from the beginning. 

 It was a funny war. Both armies not only had some British officers, many from the officer cadre were batchmates and course-mates. It was like brother turning against brother, each understanding the others psyche. 

 To what extent that has changed is still suspect. Operation Parakram, the year-long stand-off, was actually the biggest military mobilization by the two countries. Military strategists will not admit it, but it was probably Indias biggest military failure since the 1962 China debacle. It was a military mobilization that did not lead to war but intensified skirmishes in Kashmir. 

 Vajpayees NDA government and its national security advisor, Brajesh Mishra, ordered one of Indias ablest generals, Sundarajan Padmanabhan, to move troops and armour from the eastern borders, that is from Secunderabad and Madurai to Ganganagar and Chhamb in the west and the north. In the meanwhile, Gujarat burned and there were not enough soldiers available to manage the Modi-government-sponsored pogrom. (Earlier in 1999, George Fernandes got the then director general of military operations, Lieutenant General N.C. Vij, to brief the BJP leadership in party headquarters). 

 The air force nearly emptied its fighter squadrons in the east opposite China and deployed them to forward bases in the West. The navy ordered its Eastern Fleet from the Bay of Bengal to the Arabian Sea. A year later, they were all ordered back because India concluded that Pakistan had enough to defend itself. In other words, India did not have enough to invade Pakistan. 

 The jawan complied, again. More than 300 soldiers and many more civilians were killed as the frontiers were at first mined and then de-mined. In a border outpost in Rajouri, because of a lack of warning systems, Indian soldiers hung beer bottles on a fence that would clink if an intruder chanced in the vicinity. Parakram was neither war nor a war-minus-the-shooting. It was a political game played by the regimes in both New Delhi and Islamabad. 

 In the Charge of the Light Brigade, Alfred Tennyson speaks for the soldiers, Theirs not to reason why. Like schoolchildren inspired by the poem, Indian army officers quote it, sometimes to their peril. Which modern army, for instance, will take the kind of officer-casualties that the Indians do? As the drive for modern equipment and doctrines gets into gear, the military establishment is increasingly caught in a cleft. On one side, is a brooding history of loyalty, valour, courage and politics. On the other is the footsoldiers hunger for information. 

 India has democratized in every aspect except its military. Next time the jawan is sent to the front, he might ask Why? The answer had better be prepared. And it shouldnt go through the head with telltale entry and exit marks. 




</TEXT>
</DOC>