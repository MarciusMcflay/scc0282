<DOC>
<DOCNO>1070106_calcutta_story_7226722.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

On amp; Off

Flop shows

Stage On amp; Off

 ANANDA LAL

 A moment from Bahudha

 I shall go mad! raves King Lear in Shakespeares tragedy, and I found myself echoing the sentiment upon watching The Shakespeare Enthusiasts production marking the classics 400th anniversary. 

 Given Bhaskar Guptas directorial credo that Shakespeare lives in his language, he need not have inflicted this apology for theatre on us. Wearing Napoleonic hats, redcoat regalia and auburn wigs (Utpal Dutt style, except for the women), the cast merrily forgot their lines too. 

 At least Subhajit Sengupta blustered heroically as the hero, but in his fluffy white beard and red outfit resembled Santa Claus in a Christmas pantomime, his lithe movements around the stage betraying that he is half Lears age. More intriguingly, his Fool affected effeminate airs and a falsetto, giving us food for scandalous thought. 

 Another collegiate production, by Lime Lights of Shaws Arms and the Man, was only marginally superior. The director, Kona Roy, made the typical romantic mistake of casting the handsome Iftekar Ahsan as Bluntschli, who should be enacted by a very ordinary-looking man. In contrast, the supposedly dashing Sergius evoked spontaneous laughter in his oversize coat that hid his hands. 

 The greatest mystery was why the mistresses of the house ever employed a hip-swinging, black-stilettoed Louka (Debarati Mukherjee) as maid, with such susceptible men in the family. 

 Both shows proved that enthusiasm is no substitute for experience in theatre. 

 Bahudha, a dance film produced by Ranan, will be screened at the 35th Annual Dance on Camera Festival in New York this month. Bahudha has also been nominated as one of the four films for the Jury Prize at the festival; the other three films are from France, Australia and New Zealand. 

 An audio-visual experiment on Kathak, Bahudha will be shown on January 12 at Walter Reade Theatre, Lincoln Centre, New York.

 With documentary film-maker Ranjan Palit behind the camera, Bahudha deals with the multiplicity of Kathak, focussing on sound, light, rhythm and movement. There is a juxtaposition of live and filmed dance, choreographed and performed by Debashree Bhattacharya and Vikram Iyengar.

 The film is part of the Kathak production Prathama (a series of Kathak duets interacting with other art forms) that was performed by Debashree and Vikram in 2002. 

 While the film goes to the US, Debashree and Vikram, founder members of Ranan, will be joining Suman Mukhopadhyays stage production Raktakarabi at the National Theatre Festival organised by NSD in Delhi. This will be the closing performance at the festival, on January 20.

 Ranan will also travel to the Nishagandhi Dance Festival in Thiruvananthapuram to present Vaichitra, a collage of Kathak pieces set to both live and recorded music, on January 25. The four-member team comprises Sohini Debnath and Oliva Talukdar, apart from Debashree and Vikram.

 Back home, Ranan is busy working on its forthcoming project Chitra, based on Rabindranath Tagores work. The performance piece weaving dance, music and drama will portray Chitrangada in modern-day Manipur. 

 In August, Badal Sircar, the celebrated yet reticent dramatist and founder of Third Theatre surprised Calcutta with the first instalment of his autobiography Purano Kashund. This month he came up with a second book Prabasher Hijibiji.

 It is not, however, a continuation of the biography, but a collection of letters, poems and random pages from his diary. The letters, mostly written to Monu (his sister) fell into his hands in 1989 said Sircar, and it was then that he realised that they together mirrored his development as a dramatist. 

 The writings encapsulate his experiences and thoughts during his stay in London (1957-59), France (1963-64) and Nigeria (1964-1967). We learn of his hardships infinding a toehold in London before joining London Universitys town planning faculty. We learn of his introduction to Joan Littlewoods theatre that grows from workshops independent of scripts in London. 

 We learn of his friends Rosemary, Ela his first attempts to write, his reading of Thomas Mann, Jean-Paul Sartre, Albert Camus and repeated visits to the art museums of Paris. The birth of Formula X, Boropishima, Ballavpurer Roopkatha, Evam Indrajit, Kavi Kahini, Bagh.. which to him is each a history in itself.

 His poems reveal his longings for Calcutta and his known circles, his artistic frustration, his constant search for meaning and logic. We learn of his restlessness with the proscenium form and his attempts at designing minimalist sets for Indrajit. 

 There are also lesser-known details like his foray into creating books for children and his ambition to start a childrens magazine full of puzzles and games on his return to Calcutta. Towards the end of the book, in a letter dated August 17, 1967, he notes the realisation that what he wants to write should be both Funny and Terrifying. Fun-terror. Robust laughter followed suddenly by a cold hand wrenching the heart. 

 The launch of the book was suitably accompanied by a Shatabdi performance of Khatt Matt Kring!, a black comedy bristling with wit amidst a collage of recurring instances of oppression and annihilation.

 The Ninth Lokkrishti Theatre Festival will begin at Laban Hradh Mancha on January 6, with the premiere of Mohit Chattopadhyays Aay Ghum presented by Samstab and the launch of a new monthly theatre magazine, Mancha Mukhar. The nine-day festival (till January 14) will also extend to Purbasree. Productions like Homapakhi (Cinetel), Page 4 (Lokkrishti) and Shuprabhat (Swapnasandhani) will be staged.




</TEXT>
</DOC>