<DOC>
<DOCNO>1070604_sports_story_7872708.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Ive been scoring since I was a tot: Owen

 ROY COLLINS

 Michael Owen with his daughter, Gemma Rose 

With Wembley predictably becoming a love-in for David Beckham on Friday night, Englands other prodigal son, Michael Owen, could have been dropped on to the pitch through the stadium arch wearing an Elvis suit and still gone unnoticed. During the match, like Beckhams, his first for England since the World Cup, he cut just as lonely and isolated a figure. 

Five games into his comeback, after three for Newcastle and one for England B, Owen is still waiting for the adrenalin rush of seeing the ball hit the back of the net from his touch. 

Not that he harbours any doubts about his ability to end Englands goal shyness, which has put in doubt their Euro 2008 qualification, as well as manager Steve McClarens job. 

On whether he still had goals in him, Owen snapped: Thats a stupid question. Ive been scoring all my career. Its a pathetic question. 

It was a bit like asking Beckham if he still had any more tattoo visits in him, though it is natural for doubts to surface about a player returning from serious injury, particularly one who has always relied so heavily on pace. 

He has scored just once in 14 games, against Jamaica in a 6-0 romp last June, the worst run of his 10-year career, though it is slightly misleading since it dates back to Boxing Day, 2005, and includes two long lay-offs, his World Cup anterior cruciate knee injury coming as he was recovering from a metatarsal break. 

The rest of his career statistics make happier reading for Owen, 27, and support his absolute belief that the goals will come, maybe as early as in Estonia on Wednesday, where England pick up their European Championship qualifying campaign. 

He has an astonishing return of 0.52 goals from 265 club appearances and at Real Madrid, where he was regarded as a flop, he scored 13 times despite only starting 20 games. 

For England, having scored 36 goals in 81 matches, he is still on course to break Bobby Charltons record of 49, especially with the likes of Estonia and Israel on his immediate radar. 

He says: Im looking forward to my first goal, but Id be lying if I said I was worried about it. Scoring is something Ive been doing since I was a little tot. Thats what I do, score goals. Judge me how you like. Im just pleased to be back, feeling no aches and pains and feeling fit. Goals are the least of my worries. Im sure they will come. 

Unlike Beckham, who has myriad opportunities to show off his talents at free-kicks and corners, , Owen needs someone else to make the bullets for him and, against Brazil on Friday, the armoury was pretty empty, strike partner Alan Smith admitting: There wasnt much support for Michael because my job was to drop back and help Frank (Lampard) and Stevie (Gerrard) in midfield. 

The defensive strategy was to protect McClaren and the team from a defeat that may have further undermined confidence before the game in Tallinn at the A.Le Coq Arena, named after a local beer. 

Englands players are expecting more froth in their game, but Owen warns it may not be as straightforward as supporters might believe it should be. 

People look at their record and say they havent got many points and they havent scored any goals, but they can defend all right, he says. They only seem to lose every game 0-1 or 0-2 maximum, even against the big guns, so its going to be very difficult. Weve seen it a million times before, travelling to these countries. 

 THE SUNDAY TELEGRAPH 




</TEXT>
</DOC>