<DOC>
<DOCNO>1070202_foreign_story_7338501.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 Police question Blair

 SAM KNIGHT

 Blair in London on Thursday. (AP)

 London, Feb. 1: Tony Blair has been questioned by police for a second time in the cash-for-honours inquiry, Downing Street said today.

 The Prime Ministers official spokesman said that Scotland Yard detectives interviewed Blair as a witness for less than an hour last Friday. Asked whether Blair expected to be questioned again, he replied: That is a matter entirely for them.

 In a separate statement, No 10 Downing Street said that the interview had been kept confidential at the request of the police. 

 It added: During the course of yesterday afternoon, the police contacted us to inform us that the requirement for confidentiality had been lifted. We are, therefore, informing you at the first appropriate moment..

 Blair became the first serving Prime Minister to be questioned as part of a criminal investigation when he was interviewed by detectives investigating the alleged sale of political honours last December.

 At the time he described the interview as perfectly natural because the complaint that sparked the police investigation, by Angus MacNeil, a Scottish Nationalist Party MP, was effectively levelled against him as the leader of the Labour Party.

 Blair explained that the nominations for peerages that prompted the inquiry had been made by him as party leader and not as a reward for public service.

 But Blairs account of how major Labour funders came to be nominated for peerages and knighthoods is believed to have differed slightly from evidence given by Lord Levy, his personal fundraiser, who was arrested for a second time two days ago. The Prime Minister told detectives that he did not have full knowledge of secret loans received by Labour or the subsequent nominations of those lenders for peerages, according to sources involved in the inquiry. 

 That contradicted a claim made by Lord Levy in a written statement that Blair knew all about his dealings with the lenders.

 The second questioning of the Prime Minister comes amid a flurry of police activity believed to be focusing on a suspected cover-up within Downing Street after the investigation began into alleged offences under the Honours (Prevention of Abuses) Act 1925.

 Lord Levy, the Prime Ministers envoy to the Middle East and tennis partner, was re-arrested on Tuesday, this time on suspicion of conspiracy to pervert the course of justice. Earlier this month, Ruth Turner, the third most senior official in Downing Street, was arrested on the same charge.

 The shift in the inquiry, which is being led by John Yates, a deputy assistant commissioner at Scotland Yard, is reported to have followed the inadvertent revelation by one of the witnesses interviewed by police of the existence of notes and e-mails that were not shown to detectives in the early stages of investigation.

 The BBC has reported that a note exchanged by Lord Levy and Sir Christopher Evans, a Labour donor and biotech entrepreneur who was arrested last September, contained the question: quot;Wd you like a K or a big P? quot;, referring to a knighthood or a peerage.

 The dragging-on of the inquiry, which Mr Yates originally suggested would be over last autumn, is said to have greatly frustrated Mr Blair and Gordon Brown, the Chancellor and his likely successor as Prime Minister.

 Mr Brown is believed to want the investigation well and truly over by the time he moves into No 10 in the early summer, while the Prime Minister is hoping for a moment of calm to allow for a dignified exit. Labour MPs have criticised Mr Yates for expanding the terms of the investigation and quot;smearing quot; party officials. Yesterday Dr Tony Wright, the chairman of the Commons Public Administration Committee, called for a swift conclusion to the inquiry, which he described as quot;immensely damaging quot; to Downing Street.

 The investigation into the alleged sale of political honours Britains first such inquiry since the 1920s began last March after media reports highlighted the nomination of Labour funders for peerages after they had secretly loaned money to the party. Police have also interviewed Conservative MPs, including Michael Howard, the former party leader, over similar allegations.

 Copyright 2007 Times Newspapers Ltd.

 THE TIMES, LONDON




</TEXT>
</DOC>