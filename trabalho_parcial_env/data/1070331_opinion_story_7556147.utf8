<DOC>
<DOCNO>1070331_opinion_story_7556147.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 PUBLIC OFFICE, PRIVATE GAIN

 - Will Indias scientific community let Mashelkar off?

 Politics and Play - 

Ramachandra Guha

 ramguhavsnl.com

 Matter of honour

 Public service is now a less-than-clean word, associated in the middle-class mind with corruption and nepotism. It was not always so. One of my abiding childhood memories is of opening the door on a winter evening to Bhawani Singh, a peon who worked in the Forest Research Institute. A Garhwali from the Pindari ghati, it was Bhawanis job to unlock in the morning the laboratory that my father headed, and to close it down at night. At 5 pm, the siren would sound to signal the end of office hours; fifteen minutes later, my father would come cycling into the driveway. Half-an-hour later, he would be followed by Bhawani Singh. Usually, it fell to me when he knocked to open the door. Beta, daftar ki chabi laya hoon, he would say, as he handed over the keys to me, Ye saab ko de dena. 

 During school vacations, I had to sometimes open the door for Bhawani Singh in the mornings as well. Beta, daftar ki chabi laoge? he would say, and take the keys, when I brought them, with the same loving care as he handed them over in the evening. I can recall, as I write, the tone of reverence with which he used the words, daftar ki chabi. 

 Another of my childhood memories concerns the staff car of the Forest Research Institute. My father was allowed its use rarely only when going out of town on official business (otherwise he cycled to work). It was a beautiful vehicle, a blue Dodge that, in those insular, swadeshi-oriented days, was the very height of exotica. The driver, a man named Mahanand, kept it in prime condition, spending hours polishing it. In keeping with his steed, and status, he himself wore a dashing blue uniform, complete with a peaked cap. 

 About four or five times a year, Mahanand and his Dodge would show up at our doorstep to convey my father to the Dehradun railway station. Each time I would ask my father to allow me to savour a sweet minute or two sitting inside the car. I knew it would be scandalous to accompany him to the station, so I would ask to come till the institute gate, or even to the end of the road. My wish was never granted. As the Dodge sped away, I would gaze longingly at its bright blue back. 

 Truth be told, both Bhawani Singh and my father were merely representative of the times. Among Indians of all classes then hung the clean, if somewhat antiseptic, air of the freedom movement. This was especially true of those in public service; whether an unlettered peon or a scientist with a PhD, to be in the employ of the government of India was recognized as an honour that, despite (or perhaps even because of) its lack of material reward, somehow elevated you above your countrymen. With this sense of honour went a sense of duty and responsibility. Hence the respect with which Bhawani Singh treated the laboratory keys placed in his charge; hence also the doggedness with which my father would refuse to allow me to sit in the Dodge that Mahanand drove. 

 In the Sixties, a majority of the cabinet ministers of the government of India were pure as driven snow, judges of courts high and low could not be bribed, journalists had not begun to take freebies, and cricketers would not have known how to fix a match. By the Seventies, things had begun to change. To quote another example from personal experience, my girlfriend at university came to college in her fathers official car. This, compared to what was the norm before, constituted a gross misuse of a public office, but, compared to what was to come afterwards, was the very epitome of Gandhian austerity. Through the Eighties and Nineties, officials and (especially) politicians, up and down the ladder, shamelessly exploited their position for personal gain. 

 Jorge Luis Borges once wrote, with regard to his own country, that the State is impersonal; the Argentine can conceive only of personal relations. Therefore, to him, robbing public funds is not a crime. I am noting a fact; I am not justifying or excusing it. This can certainly serve as a description of large sections of the Indian State today. My own impression is that politicians have been most flagrant in their abuse of public institutions and public funds, followed by officials, and last of all by scientists. 

 In holding this opinion, I do not think that I am being unduly swayed by the fact that I come from a family of scientists. I believe that most Indians would share this impression; that scientists on the rolls of the government of India are somehow more honest in their dealings than politicians or civil servants. Which is why the recent charges against Dr. R.A. Mashelkar have caused such discomfort. For more than a decade, Dr Mashelkar held the most important post in the Indian scientific establishment, which is, director-general of the Council for Scientific and Industrial Research. As the CSIR D-G, he was made chairman of an expert committee to look into patent laws. The report, submitted after Dr Mashelkar demitted office, was found by two eagle-eyed researchers to have lifted chunks, without attribution, from a report prepared by an Oxford researcher at the behest of multinational pharmaceutical companies. In the resultant furore, Dr Mashelkar withdrew his report while claiming that the error was accidental, and unprecedented. Soon it turned out that in a book on intellectual property, which he co-authored with another Indian in 2004, Dr Mashelkar had based some of his findings, again without attribution, on a paper written by a British academic in 1996. 

 In his time at the CSIR, Dr Mashelkar had a reputation for dynamism, for infusing life and energy into a somnolent organization. To be sure, he did things scientists were not supposed to do. For example, he was felicitated in a function hosted by the Rashtri- ya Swayamsevak Sangh. Again, Dr Mashelkar joined the board of Reliance Industries very soon after leaving office. 

 Breaking bread with the RSS, cosying up to corporate India these are things we have become accustomed to, from our journalists and social scientists at any rate. We should perhaps not be too judgmental about a scientist following the same route. However, the charges of plagiarism will be harder to wish away. For nothing can be more damaging to a scientist than to be told that his conclusions are stolen from someone or somewhere else. 

 As I write this, news comes in that Dr Mashelkar has resigned from the Technical Expert Group on Patent Law Issues. Although belated it comes several weeks after the charges of plagiarism were made public it is a welcome acknowledgement of error, if not negligence. With this, the controversy in the press will die down. However, Dr Mashelkar has still to withstand the proper scrutiny of his peers. I would be most interested in the reactions of the scientific academies of which he is a member, sometimes a leading member. Will they chastize him for violating the ethical code that mandates scientists always to scrupulously acknowledge the source of their data or analysis? Or, will they instead close ranks and let off the errant member of their community? This will be a test of their integrity, as well as their courage.




</TEXT>
</DOC>