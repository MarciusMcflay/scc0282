<DOC>
<DOCNO>1070709_opinion_story_8014865.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 UNITED STATES OF AFRICA 

 FIFTH COLUMN

 GWYNNE DYER

 Before you put a roof on a house, 

 you need to build the foundations, South African president, 

 Thabo Mbeki, reportedly told diplomats at the summit meeting 

 of the African Union in Ghana recently. Others were just 

 as quick to ridicule the summits declared goal of creating 

 a unified African government by 2015, and it certainly isnt 

 going to happen fast. It may never happen at all but it 

 might, and it would be a very good idea.

 The African Union was created 

 five years ago out of the wreckage of the discredited Organization 

 of African Unity with the goal of making Africas rulers 

 accountable. Now it is trying to revive the project for 

 real African unity, and there is no shortage of Africans 

 who argue that it is merely a distraction from urgent and 

 concrete problems like Darfur and Zimbabwe. Maybe they are 

 right, but what if those crises are just symptoms of a deeper 

 African problem?

 At the time most African countries 

 gained their independence in the Sixties, they had higher 

 average incomes and better public services than most Asian 

 countries. Kenyans lived better than Malaysians; people 

 in the Ivory Coast were richer than South Koreans; Zimbabweans 

 were healthier and better-educated than the Chinese. And 

 there were worse wars in Asia than in Africa.

 Now its all dramatically the 

 other way round. But why? Individual Africans are no less 

 intelligent, hard-working or ambitious than individual Asians, 

 so the answer must lie in the system. And the most striking 

 characteristic of that system is the sheer number of independent 

 states within Africa: 53 of them, in a continent that has 

 fewer people than either India or China.

 Theme for a dream

 This is where the discussion usually 

 veers off into a condemnation of the arbitrary borders drawn 

 by the old colonial powers, which paid little heed to the 

 ethnic ties of the people, but that is not the point at 

 all. The point is that at least half of the 53 African countries 

 have greater ethnic diversity within their borders than 

 all of China. A few, like Nigeria, approach India in the 

 sheer diversity of their languages, religions and ethnic 

 identities. 

 You cannot draw rational borders 

 for Africa that give each ethnic group its own homeland. 

 Even if you refused that privilege to groups of less than 

 half a million people, youd end up with over 200 countries. 

 So the old Organization of African Unity decreed that the 

 colonial borders must remain untouchable, because the only 

 alternative seemed to be several generations of separatist 

 ethnic wars.

 The problem is that quite a few 

 of the separatist ethnic wars happened anyway, and many 

 other African countries, to avoid that fate, became tyrannies 

 where a big man from one of the dominant ethnic groups 

 ruled over the rest by a combination of patronage and violence. 

 It was nobodys fault, but Africa needs to change this system.

 There are over 200 ethnic groups 

 in Africa that have over half a million people, and none 

 (except the Arabs of north Africa) that amounts to even 

 5 per cent of the continents population. Only three languages 

 Mandarin Chinese, Hindi and Japanese account for half 

 the population of Asia. Even in Europe, eight languages 

 account for 75 per cent of the continents population. Africa 

 is different, and maybe the national state is not the answer 

 there. 

 The African federalists imagine 

 a solution that jumps right over that problem: a single 

 African Union modelled on the European Union, but where 

 no ethnic group is even five per cent of the population. 

 Then politics stops being a zero-sum ethnic competition 

 and starts being about the general welfare. Also, in theory, 

 the continent starts to fulfil its potential. We will all 

 be a good deal older before the African Union becomes more 

 than a dream, but in the end it may. 




</TEXT>
</DOC>