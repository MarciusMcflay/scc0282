<DOC>
<DOCNO>1070618_opinion_story_7931927.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FOR THE RIGHT MIX

 CYRIL ARIJIT GHOSH

 George W. Bush desperately wants to push immigration bill, S.1348. On June 7, the Senate majority leader, Harry Reid, pulled the measure from the Senate in the face of opposition from Republicans who see the bill as an amnesty programme to legalize millions of illegal immigrants. But Bush has called the status quo unacceptable, and has been trying to persuade opponents of the bills merits. Fortunately for him, the Senate Democratic and Republican leaders have announced their intention to revive the bill.

 The bill proposes, for the first time since the mid-Eighties, a significant revamp of the US immigration policy, and makes it considerably easier for illegal immigrants to pursue legal status and/or citizenship. At present, the immigration policy is responsible for many gratuitous deaths among thousands of migrants who illegally cross over into America every year under breathtakingly dangerous circumstances. In the last two decades, the immigration rules have managed to transform what used to be a benign, seasonal pattern of illegal Mexican migration into settled populations across America that are living with the constant fear of being deported. 

 The recent immigration bill follows from Bushs call for a comprehensive restructuring of US immigration policy in 2003. The latest iteration of the bill proposes a guest worker programme, and opportunities for illegal immigrants to seek legal status and citizenship. 

 As with everything else concerning immigration, the provisions of this bill are patently controversial. For instance, it asks that temporary work visas be not granted until some very stringent border enforcement measures are in place. Some of these measures include the extension of the border fence, and a new, improved system of verification of legal status for all job applicants. According to some officials, it will take at least 18 months for this system to be up and running, which, of course, means that the guest worker programme, which the US badly needs in order to dissuade illegal immigration, will have to wait. 

 Play along

 The bill also allows for current illegal immigrants to apply for a Z visa, pay penalties, and, thereby, work legally in the US. Members of the immediate family of illegal immigrants already in the US would qualify for these visas. But those family members in other countries would not. The bill stipulates a road to full citizenship in about 13 years, given a period of permanent residence, a fee, and demonstration of English-speaking abilities. This provision is being widely criticized by conservatives as an amnesty programme that wrongly absolves those who broke the law and, instead, offers them American citizenship. 

 If signed into law, the guest worker programme would enable the issuance of 400,000 temporary workers visas a year, thus cutting down illegal border crossings, border deaths, and deportation. A guest worker would be allowed to come to the US to work legally for a two-year period and to then return to his country of origin for a year. He would be allowed to repeat this cycle two more times. According to some critics, this measure will prove far too costly for employers to make it feasible. 

 Obviously, these policy reforms cannot please everyone at the same time. But what is important is the recognition that reform is long overdue. And, for once, there is a semblance of bi-partisanship in the discussion surrounding immigration. A recent Pew Research Center poll indicated that 36 per cent of Republicans, 33 per cent of Democrats and 31 per cent of independents support the bill. Not everybody is happy, but a critical mass is. 

 Senate Republicans should make use of this opportunity and support the bill. The Senate will be wise to support the president instead of seeking a consensus. Consensus, in a plural democracy, is like Godot. One has to keep waiting for it.




</TEXT>
</DOC>