<DOC>
<DOCNO>1070828_opinion_story_8237215.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 TESTING HYPOTHESES

 - The mystery that is Manmohan Singh

 Writing on the Wall - Ashok 

 V. Desai

 Political astronomers have noted certain discrepancies in the behaviour of superstar Manmohan Singh in recent weeks. First, he chose The Telegraph to send a message to the stars of the Communist Party of India (Marxist) when he could have simply picked up the phone and told them. Second, he sent an uncharacteristically belligerent message that they could go ahead and do their worst. It was not simply that he thereby jeopardized the future of the Congress-communist alliance, but he abandoned his mild and emollient ways for once. Third, he accused unspecified Bharatiya Janata Party politicians of having conducted certain rituals meant to kill him, let alone depose him. There is no reason to doubt his claim, for he has two of the worlds wiliest secret services at his command, as any Pakistani would attest; for them to find out what stupid Hindutwit types were doing would have been childs play. 

 But it was impossible to discover the point of his statement. It could not have been to deter such twits from doing more deadly rituals, for millions of them do various rituals every day, and if some of them want to wish death to someone else however exalted he may be no one can even know what they are muttering under their breath. It could be to cadge some popularity out of a shocked public; but it is hardly popularity that the prime minister is short of just now. He is not known for making pointless remarks; they did not quite sound like him. 

 Political observers have been exclusively concerned with what these discrepancies imply for the future of the government and of the nuclear deal. This is understandable, for it is their job to worry about the affairs of the nation. But there are the perhaps less important but more interesting questions: which is the real Manmohan Singh, the old one or the new one? Were the discrepancies noted above temporary aberrations or has the course of the superstar permanently changed? In case it has changed, what are the new laws of motion the star will follow? And since the star has a strong influence on the horoscopes of Indians and not just those who mutter incantations what does its strange behaviour portend for them? 

 Manmohan Singhs public image is so consistently favourable that few are aware of the disdain, bordering on hatred, that he evokes amongst some people. That does not apply only to Hindutwits, who see him as a usurper to a kingdom that legitimately belongs to them since they made it shine. From my days in the government onwards, I have from time to time come across bureaucrats who worked with Manmohan Singh, who think they know him well, and who harbour an unflattering opinion of him. It is essentially that he is manipulative, an opportunist, subservient to the powerful and a man without convictions. I was unfamiliar with this strain of prejudice before I joined the government; when I first came across it, I was quite outraged. But my outrage cannot quell powerfully-held convictions, which I continue to encounter and be reminded of. 

 While I was in the government, I associated these unfavourable opinions with the opposition to the reforms. It did not come from Hindutwits, who were incapable of elementary rational reasoning, but from leftists. In the recent atmosphere of bonhomie, it is difficult to remember that when Manmohan Singh launched the economy rescue operation in 1991, the communists genuinely and strongly believed that he was a traitor, an American agent who had sold out and had overturned a venerable socialist legacy of 40 years. Now, of course, the reforms of 1991-92 have become a part of our landscape; no one remembers the profound impact for good or evil they have had on our destiny. But the animus that some communists felt towards Manmohan Singh then was too visceral to have completely disappeared. 

 But I must also admit that my correlation of hostility towards Manmohan Singh with communist convictions was facile and empirically unsound. Many of those who were hostile to him were competent and honest civil servants who had worked closely with Manmohan Singh and had reason to know him well. It would be empirically unsound to connect their unfavourable opinion with leftist political beliefs. 

 Nor can Manmohan Singh have been unaware of that opinion. It would not have been openly expressed to him. But it must have been reflected for decades in his colleagues remarks, body language and actions. How did he cope with it? 

 When it came to running the government, Manmohan Singh dealt with it by creating a coterie of civil servants who would be loyal to him. Montek Singh Ahluwalia is the most prominent example, but not the only one. It has been remarked how, since Singh became prime minister, he has chosen a small group of civil servants to work with him, often keeping them beyond superannuation. His government has from time to time put out proposals for depoliticizing the civil service and for putting in place firm, objective rules for promotion and tenure. But he himself has blatantly flouted them, to such an extent that senior members of the foreign service are suing the government over favouritism and all that the government has to say for such indefensible conduct is that it was born out of political necessity. 

 The political practices that have evolved after independence made favouritism in the management of the civil service easy for Manmohan Singh. But he could not use rewards and punishments when it came to politicians. He does not have a popular base that would ensure his survival in case it came to a conflict with his political colleagues. And the destiny that made him prime minister namely, Sonia Gandhis last-minute decision, dictated by an inner voice, to renounce the prime ministership also implied an arrangement that deprived Manmohan Singh of control over his cabinet. 

 This is why he has watched helplessly while P. Chidambaram introduced tax proposals in budget after budget that achieved nothing beyond acute public inconvenience, Arjun Singh spearheaded OBC reservations and planted sycophants across the academic world, and A. Ramadoss imposed a ban on public smoking that is flouted by millions every day. He has, in effect, become the first amongst equals in the cabinet. Ministers do the most egregious things; as long as they remain superficially on good terms with him, they can continue to run their own independent fiefs. 

 How is one to explain his acquiescence to so many policies that have caused him discomfort and embarrassment, policies that it is impossible to believe he believes in? The interlocutors from the civil service I have alluded to would have a perfect explanation for it: he was always like this. He had no convictions; he only pursued power. 

 But I cannot agree with them; there must be another explanation. If it is what Manmohan Singh has given, that it is his manifest destiny that he should have become prime minister, it must be his destiny that he does what he does. He must have his own inner voice; if only we could hear it, all our questions would be answered, all our doubts stilled. So be it.




</TEXT>
</DOC>