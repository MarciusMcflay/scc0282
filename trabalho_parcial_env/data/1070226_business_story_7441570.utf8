<DOC>
<DOCNO>1070226_business_story_7441570.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Business

 FM faces reform dilemma

 JAYANTA ROY CHOWDHURY

 Budget 2007-08 

 New Delhi, Feb. 25: Finance minister Palaniappan Chidambaram, the political face of Indias heady growth story, is often unfairly compared to his boss, Prime Minister Manmohan Singh, who ushered in reforms 16 years ago.

 Unlike Singh, Chidambaram is atop an economy which is growing at more than 8 per cent running for two years. This growth has emboldened India Inc to carry out successful takeovers abroad. But like Singh, a top-drawer economist who handled the finance portfolio between 1991 and 1996, the dilemma of reforms versus the needs of the common man has come back to haunt the Harvard-trained lawyer.

 Should Budget 2007-08 focus on the aam aadmi for votes in the crucial state Assembly polls or should it address the reform constituency one last time before the general elections force the Congress-led UPA to turn populist in next years budget? 

 The booming economy, which is likely to post a 9.2 per cent growth, and the soaring sensex, despite this weeks jitters, should normally have strengthened Chidambarams hands in pushing through reforms. However, this is not the case, and Chidambaram by all accounts is finding it difficult to force the pace of reforms. 

 The Left, the traditional bugbear of reforms, is no longer the party holding back change. It is the Congress workers who have opposed the carving out of special economic zones (SEZs) from farmland, the entry of global retail giants such as Wal-Mart and the takeover moves of foreign banks.

 The party is increasingly feeling the need to woo its traditional vote banks the poor, the minorities and the weaker sections of the society rather than address the urban middle class.

 The result: A budget focused on rural areas, with huge spending commitments to the farm sector, a populist farm credit package and curbs on forward trading in commodities is being bandied about by the North Block. The finance minister himself is on record on the need to invest more in agriculture. 

 The Planning Commission has rooted for special packages for the weaker sections scheduled castes and tribes and minorities which the budget makers, led by Chidambaram, are likely to endorse. 

 Another pressing area is infrastructure. The countrys roads, ports, rail and power networks are overloaded and in need of an estimated investment of Rs 6,000-7,000 crore a year. Chidambaram is likely to deliver on this count. While helping the masses, the boosters will also remove hurdles to growth. 

 On top of the agenda are also the welfare programmes for poverty removal, education and health, areas where both the Left and the Congress have been advocating for greater allocations in the budget. Elections apart, the inflation spiral has made the UPA jittery, and combating the price rise is a priority for the government.

 Excess money with companies and the middle class, easy credit, higher fuel prices and a short supply of food products have sparked the high inflation rate that has become an embarrassment to the government. Even President A.P.J. Abdul Kalam joined the diatribe against inflation in his address to Parliament on Friday. 

 India may be among the worlds two fastest-growing economies, but the inflation has raised fears of growth getting burnt out by higher prices. The Congresss political opponents have taken advantage of inflation to rail from street pulpits at the government. 

 Chidambaram obviously needs to tackle this in his budget. Prescriptions include cutting import duties drastically on food products and on fuel and goods whose prices have risen rapidly. The problem is the tough measures of the Reserve Bank of India (RBI) and his government have not affected the prices yet. 

 The question that begs an answer is whether reforms have been given a quiet burial.

 Analysts feel this is not the case. A budget, as Chidambaram has shown in the last three years, is as much a political document of the governments intentions as a statement of fiscal policy. But big ticket reforms need not be tied to its apron strings, waiting to invite the proverbial storm in the tea cup.

 This budget too may pass over, before the reformer in North Block unveils his plans on issues such as the role of foreign banks and FDI in retail. 




</TEXT>
</DOC>