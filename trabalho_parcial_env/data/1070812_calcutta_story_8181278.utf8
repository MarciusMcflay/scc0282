<DOC>
<DOCNO>1070812_calcutta_story_8181278.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Memorable trysts with destiny 

 Tilottama Tharoor and Krishna Bose at the launch of Great Speeches of Modern India. Picture by Pabitra Das

On the midnight of August 14-15, 1947, in the middle of Pandit Nehrus tryst with destiny speech, delivered at the Constituent Assembly, there was a loud honk. The bemused audience soon discovered the source of this interruption it was just one of the Hindu members trying to blow into a conchshell to mark the momentous occasion. 

 Historian Rudrangshu Mukherjee regaled a gathering of mainly school students with several delightful anecdotes such as this one at the launch of his book, Great Speeches of Modern India, at the Oxford Bookstore on August 10. He was joined by Krishna Bose, chairperson, Netaji Research Bureau, and Tilottama Tharoor, professor of humanities, New York University, who introduced and read out from a few speeches of their choice. 

 Mukherjee focused closely on two memorable speeches by Nehru (the other one made in December 1955, at the laying of the foundation stone of the Nagarjuna Sagar Dam). He addressed the young crowd on What the history books dont teach you, providing a human context to political landmarks in the history of modern India. History lessons in school dont tell us, for instance, about the odd meeting between Nehru, Rajendra Prasad and Lord Mountbatten a few hours after Independence was declared. After agreeing to become the first governor-general of independent India, Mountbatten opened a bottle of port and toasted to India, to which Nehru responded with, To King George VI! 

 Krishna Bose gave a moving introduction to Netajis seminal speeches, before moving on to his inspired address to the INA, Give me blood and I promise you freedom. She also read out the sequel to this speech, delivered by Netaji after the defeat of the INA on August 15, 1945. 

 Tilottama Tharoor selected Vikram Seths address at the Doon School, about two decades after he had been a student there. She could not have chosen better. The speech was devoid of nostalgia or pleasant memories. Rather, Seth was ruthlessly honest and unsparing in his criticism of his parents who left him with suspiciously reassuring strangers. In a democracy, speech-making comes not only with the freedom of self-expression but also bestows on the listeners the liberty to dissent, debate and carry on a dialogue. Which is why, Seths speech left the young students with questions they, too, ought to be asking about their schooling. 

 SOMAK GHOSHAL




</TEXT>
</DOC>