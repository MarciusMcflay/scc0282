<DOC>
<DOCNO>1070507_opinion_story_7738096.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SOOTHING STYLE

 The finance minister was suitably soothing on inflation in parliament on Friday. P. Chidambaram has no magic wand to bring down prices. He could order public sector enterprises to bring down prices; but then few enterprises are left that can give relief to the consumer. He could bring down taxes; but then his aides would complain that he is ruining the nations finances. In the absence of such direct instruments, he is reduced to using his voice. And it must be admitted that he has an amiable, avuncular style that beautifully matches the reassurance that he seeks to give on inflation.

 Mr Chidambaram is broadly right on the causes of the current inflation short supply of agricultural commodities. He is not entirely right on the impact of international prices: while some metals are going up, the trend of crude prices is definitely not upward. Nor does he have much scope to be wrong on the measures his government has taken. Where he goes wrong is on the likely impact of those measures. If wheat prices are rising despite expectations of a bumper crop, they cannot be brought down by importing a million tonnes at a time; the quantity is too small, and the procedure of the Food Corporation of India issuing tenders, identifying the winning bidder, negotiating terms, finding ships, finding ports to land the wheat, and reaching it to the market is just too bureaucratic and slow. The Indian entrepreneurial class is much nimbler than the ponderous FCI; consumers would be better served if Mr Chidambaram dropped all import duties on food products and let anyone import who will.

 Nor is the effect of monetary tightening by the Reserve Bank of India as certain as Mr Chidambaram thinks, even in the long run. Raising interest rates raises costs and prices, whatever the government may like to believe; the effectiveness of hard money depends on how far it manages to reduce total demand. Besides, the tightening of monetary policies is nowadays rendered ineffective by international capital flows; as seen in the past financial year, high interest rates in India promptly pulled in funds from abroad. Then the RBI is compelled to use direct controls such as the cash reserve ratio and detailed rules on whom the banks may lend to and on what terms. That raises squeals from government banks, and the RBI responds to them by giving lollipops like paying interest on CRR cash. In the end, policies are appended with so many exceptions and conditionalities that they lose their effectiveness. Mr Chidambaram is welcome to make comforting speeches. But if he wants to be credible, he should recognize that there are only three effective policies against inflation. First, he can abolish import duties on consumer goods. Second, he can ask the RBI to appreciate the rupee significantly. Finally, he can tax more, spend less, or do both, and thereby reduce demand pressures.




</TEXT>
</DOC>