<DOC>
<DOCNO>1070709_sports_story_8034627.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 THE KING KEEPS CROWN

 - World No.1 survives ferocious fightback from nadal to win in five sets 

Roger Federer with his trophy on Sunday. The top seed beat No.2 Rafael Nadal 7-6, 4-6, 7-6, 2-6, 6-2 to win his fifth successive Wimbledon title. He became only the second man after Bjorn Borg to do so. (AFP) 

London: Roger Federer survived a ferocious onslaught from Rafael Nadal to win his fifth consecutive Wimbledon singles title on Sunday, finally taming the inspired Spaniard 7-6, 4-6, 7-6, 2-6, 6-2. 

The normally serene Swiss looked about to crack under the strain when Nadal twice had chances to break serve in the fifth set but rediscovered his magic just in time. 

After smashing the ball away on his second match point Federer collapsed emotionally on Centre Court knowing he had equalled the five titles in a row of the legendary Bjorn Borg. 

Borg, who won from 1976 to 1980, watched with 14,000 other transfixed spectators as battle raged in the sunshine for three hours and 45 minutes, the perfect end to a rain-hit tournament. 

Only Federer, who now has 11 Grand Slam titles at the age of 25, knows how close he came to losing what will go down as the best Wimbledon mens final for many years. 

Nadal seemed poised to become the first man since Borg in 1980 to win the French Open and Wimbledon back to back when he stormed the fourth set and had a rattled Federer down 15-40 on two consecutive service games in the decider. 

Federer, who walked on court having won 53 consecutive grasscourt matches, began the afternoon with a whistling ace, the cue for a riveting contest. 

Claycourt king Nadal came under fire immediately, dropping his opening service game as Federer came out swinging. 

From 0-3 down, however, the Spaniard caught fire, rocking Federer with a series of fizzing winners to get back to 3-3, one searing backhand pass to break back the pick of the bunch. 

There was drama in the tiebreak when Federer served on his third set point at 6-5. A Nadal backhand was called out but he challenged the decision and HawkEye revealed it had clipped the line. Federer finally got his nose in front when he carved away a sublime backhand volley at 8-7. 

Nadal looked the more menacing player in the second set. 

Federer found himself 15-40 down again at 4-5 and this time there was no escape when the 21-year-old Nadal ripped a backhand past his despairing lunge. 

The final simmered in the third set. The atmosphere crackled as the third set moved into a tiebreak and there were even a few signs of needle, Federer shooting Nadal a dark stare when he challenged one call. Federer stayed calm to win the days second tiebreak but Nadal came out fighting in the fourth set. 

The Swiss seemed to lose his focus completely when, having already been broken once, a Nadal forehand at 0-2, 30-40 was called out but the decision was overturned on the evidence of the HawkEye screen. 

Nadal showed no obvious signs of discomfort when he carved out break points at 1-1 and 2-2 in the fifth set. But he was left to rue the missed opportunities, especially one forehand sitter, when Federer came to his senses to claim yet another title. 

Meanwhile, second seeds Cara Black of Zimbabwe and Liezel Huber of South Africa won their second womens doubles title in three years defeating Slovenian Katarina Srebotnik and Japans Ai Sugiyama 3-6, 6-3, 6-2. 

In the mens doubles, French pair Arnaud Clement and Michael Llodra upset champions Bob and Mike Bryan of the US 6-7, 6-3, 6-4, 6-4 to maiden Wimbledon doubles crown. 

 (REUTERS) 




</TEXT>
</DOC>