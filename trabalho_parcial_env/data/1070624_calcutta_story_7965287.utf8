<DOC>
<DOCNO>1070624_calcutta_story_7965287.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Distant north

 Children from north Calcutta are routinely refused entry into reputed city schools. The reason cited is distance. A report 

 Manisha Mohta, a resident of Bangur Avenue, had trouble getting her son admitted to a good school last year. She tried many schools. The first thing that the schools wanted to know was how I would be able to bring my child to school every day from such a remote place. I tried to convince them, but my child couldnt get admission to any of those schools, says the dejected mother.

 The children at the montessori my son went to were also turned away by the schools. Their parents had to face the same question, says Manisha, adding that her son had to be admitted to a school in Garia. I commute to the extreme south every day without much hassle and why wont I? Its natural to think of the pros and cons before applying to a school. 

 Bangur is much farther away from Garia than it is from the areas in which the schools that denied Manishas son admission are located. The schools, all of them in the south, have their reputation preceding them, unlike the Garia school. 

 Most good schools in the city are in south Calcutta. Children from the north often find it difficult to get admitted to these schools because of their addresses. This, the parents feel, is another fallout of the north-south divide that splits the city, socially, culturally and economically, with the south enjoying the position of privilege. 

 Alarm bell

 Though some parents feel that reputed schools reject their children because north Calcuttans are perceived to be downmarket parents from Behala are never asked how they would commute to a school in, say, Minto Park the official reason cited by the schools is distance. Many children living in the north have faced resistance at the interview when schools raise an objection on how a child will commute. I have a feeling that this is also used as an excuse at times to reject a student, says Madhu Haralalka who runs Rainbow Montessori House in Jatindra Mohan Avenue in north Calcutta.

 Schools insist that it is primarily merit that decides admission and a long commute is a deterrent to a childs well-being. Sunirmal Chakravarthy, principal of La Martiniere for Boys, says that there are no fixed rules with regard to distance but applications from those living as far as Howrah and Baguiati alarm the authorities. Theres no prejudice against the north, but I would be worried about their access to transport in order to come such a long way. We try and rationalise with parents since it definitely is an issue, says Chakravarthy. 

 The rejection can lead to desperation in parents. They have shown false addresses and claimed that they are willing to shift close to the school. Its easy to see how stressful a long commute can be for a three-year-old. When half a day is spent commuting, the childs health will be affected, says the principal of a reputed south Calcutta school. 

 The rat race to get a child admitted is absent in Mumbai and New Delhi. In both cities, good schools are not concentrated in one zone. In the Capital, however, schools have to give priority to children staying within a 3-km radius. But in Calcutta, almost all good schools, including La Martiniere for Boys (Loudon Street) and La Martiniere for Girls (Rawdon Street), Don Bosco, Park Circus, St Xaviers (Park Street), St James (AJC Bose Road), Modern High (Syed Amir Ali Avenue), Loreto House (Middleton Row), Ramakrishna Mission (Narendrapur) and South Point (Ballygunge Place) are in the south. Only a handful of schools in the north like Calcutta Boys, Calcutta Girls, Loreto Bowbazar and Loreto Dharamtala find favour with parents from the People Like Us (PLU) community.

 Rules and exceptions

 Children from Uttarpara and Chandernagore and also those from Howrah and Dum Dum get ruled out by schools in the south. Twenty minutes or 10 km can be crucial, though upmarket Salt Lake seems to be exempt from the rules. 

 We dont provide transport, so we ask at the interview how the child will commute. We usually keep a check on a childs mode of transport, especially for those applying from the north, says Ranjan Mitter, principal of Future Foundation School, Tollygunge. Its not official but it is one of the many factors that determine whether a student will be admitted or not. If we have two candidates with the same credentials, the one living in closer proximity will be given preference, he adds.

 Many school buses dont go to the north. Our buses go up to 9 km but dont ply in the north. For those applying from far off, we try to figure out how long the child will take to reach school. Whether the family lives close to a Metro station or has a car is important, says Bratati Bhattacharya, principal of GD Birla Centre for Education, Tollygunge. 

 Not all parents are given a reason. Aditi Paul, a resident of Amherst Street, says: I wasnt ever told why the schools rejected my children. I tried for Pratt Memorial, Gokhale Memorial and Our Lady Queen of the Missions for my daughter. Her daughter is now studying in Our Lady Queen of the Missions. 

 We try to avoid taking in those from across the river. The traffic is heavy and students will be late. We take in students from Salt Lake and Lake Town, but only a few, based on how they would travel and the time it would take. We prefer those who can reach school in 30 minutes from whichever direction they might be coming from, says Terence Ireland, principal, St James School. 

 There is a feeling though that Metro and flyovers have made a difference. Gargi Ghosh, a Howrah resident whose daughter studies in Gokhale Memorial Girls School, says: At the interview they asked how I would commute from so far away. I said that there are a lot of car pools and commuting would not be a problem. It just takes about 20 minutes for my daughter to reach school via Vidyasagar Setu.

 Not a problem

 Reeta Basu, principal of Disharee, a montessori in Ballygunge Circular Road, says: I dont think distance is a factor anymore. I know of children who attend schools in the south from as far off as Dum Dum and Barasat.

 Some people dont face a problem. Kasturi Banerjee of Harvard House, which has montessories in Alipore, Ballygunge, Tangra, Maniktala and Howrah, says: Students of Harvard House dont face this problem. Three to four years ago it might have been a problem but that was perhaps because of lack of the kind of training provided by us. Suvra Roy Chowdhury, a resident of Sovabazar, says: I had trouble admitting my elder daughter to Middleton Row Loreto, but that was in the 80s. I think the north is better connected now. 

 But the distance remains. Perhaps a new limit has been set. Says Shubhobrato Bhattacharya, assistant manager of operations of Kidzee, a pre-primary education centre, in Ho Chi Minh Sarani: Several students from our institute have not got through to south Calcutta schools because of a 4-km limit to commute. 

 MOHUA DAS WITH INPUTS FROM MALINI BANERJEE




</TEXT>
</DOC>