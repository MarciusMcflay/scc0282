<DOC>
<DOCNO>1070512_frontpage_story_7767081.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Inclusive experiment yields rare one-party rule, knocks out BJP 

 RADHIKA RAMASESHAN

 *One seat countermanded

 New Delhi, May 11: The Blue Elephant has sauntered like Gajgamini through Uttar Pradesh.

 Mayavati today gifted the state its first single-party government in 15 years and the Dalits their first majority dispensation in the country.

 In a performance worthy of the party symbol the elephant the Bahujan Samaj Party has romped home with 206 seats, four more than that needed to form a government on its own.

 The analogy with the mammoth does not end with the symbol.

 True to the fable of the stupid blind men and the elephant, Mayavatis party has confounded pollsters and politicians. Few fathomed the deftness with which Mayavati cobbled together an inclusive social matrix. 

 The BJP did not want the Muslims; the Samajwadi Party wanted the Mus- lims and the Yadavs; the Congress was obsessed with Rahul baba. But Gajgamini made room for all.

 Mulayam Singh did as badly as expected, ending up with 97 seats and stepping down from power. The Congress fell three short of the 25 last time and promptly devoted itself to deflecting fire from Rahul Gandhi.

 But the biggest blow was reserved for the BJP, which lost 57 seats and hopes of a comeback nursed by a series of Assembly poll victories in the run-up to the heartland battle.

 How did Mayavati pull off what was considered the impossible? The BSPs victory is a victory of voters across the caste and religious spectrum, said political scientist Zoya Hasan.

 Mayavatis experiment began two years ago when she tried to reach out to the Brahmins and later the other upper castes through her Brahmin jodo (integrate Brahmins) mission. The idea was to tap the more socially committed among Brahmins and, through them, appeal to the larger community. The strategy was backed up by sound electioneering tactics.

 Mayavatis first big breakthrough was the induction of Satish Chandra Mishra, an advocate-general in the coalition government she ran with the BJP. He was tasked with appointing district-level Brahmin mahasammelans (congregations). Then came other caste congregations to attract the Rajputs, Vaishyas and Yadavs.

 As jubilant Mayavati addressed the media today, by her side were Mishra, her upper-caste face; Nasimuddin Siddique, her most recognisable Muslim leader; and B.S. Kushwaha, an OBC leader. I am proud to have Mishra who has united the Brahmins with the Dalits, Siddique who brought Muslims along and Kushwaha who was responsible for bringing in OBC support, she said.

 To the upper castes, which resented the BJP for its dalliances with Mayavati, her gesture meant one thing: the BSPs rock-solid base of transferable Dalit votes was seed capital to build their electoral fortunes. Even Mulayam Singh could not take risks with his Yadav votes. But Mayavati could count on the Dalits.

 Mayavatis election machinery also performed well. The BSP divided each constituency into 25 sectors with 10 polling booths to a sector. Each booth, which covered around 1,000 voters, was supervised by a nine-member committee, which had at least one woman member, to motivate and mobilise the voters.

 Last, Mayavatis image as a no-nonsense administrator symbolised by her arrest of Raja Bhaiyya went down well with the urban middle class.

 As the marginalised BJP and the Congress spoke glowingly of her victory, there was worry about what the victory means for heartland politics. Congress sources admitted that the Dalits in states split between two parties (Himachal Pradesh, Madhya Pradesh, Rajasthan and Gujarat) would not remain untouched and may start considering Mayavati as an option. 

 With the value of the BSPs votes increasing, her role will now have a bearing on the President polls. Sonia Gandhi has already congratulated Mayavati and reportedly opened a channel of communication on the presidential polls. A meeting is likely next week.




</TEXT>
</DOC>