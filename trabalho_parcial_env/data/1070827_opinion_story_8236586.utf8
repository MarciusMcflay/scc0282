<DOC>
<DOCNO>1070827_opinion_story_8236586.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 Letters to Editor

 Killers at large

 Sir As one reads the hair-raising tales of schoolchildren being mowed down by wild vehicles, one is more and more convinced that the police of Calcutta are trying to live up to their image of a lazy, unhelpful, monumentally inefficient and bribe-happy force (Bus kills boy who shunned pool car, Aug 23). However, on the morning of August 23, the city woke up to find them in a different mode, teeming the streets and going about their work like men possessed. The reason: the prime minister of Japan was visiting Calcutta. The streets had to appear clear, navigable and motorist-friendly.

 An authoritative officer in his patrolling van was seen arguing with the mother of a six-year-old girl who had driven her daughter to the pick-up point of the school bus. This efficient representative of the law forced her to park miles away from the point, taking no notice of the fact that a child had to stand alone on the pavement while the police van followed the car to ensure that it did not force its unwanted presence on a foreign dignitary.

 Could it have been the same cops who generally do the rounds of Salt Lake in the wee hours of the morning, accosting a software engineer returning after a late shift and making him cough up five hundred rupees for supposed suspicious activities, or pouncing on a young couple out for morning walk and slapping a hefty receipt-less fine for supposed public display of affection and this while apartments are looted a stones throw away?

 Our socialist government, though not unduly bothered about mundane things like the safety of children or the taming of killer buses, is surely up there with the best when it comes to fawning over foreign dignitaries.

 Yours faithfully,

Arunabha Sengupta, 

 Calcutta

 Sir Every accident is unfortunate. The question is not whether public transport is safer than pool cars, but whether those who drive vehicles on Calcuttas streets are fit to do so. It is the skill of the person behind the wheels that makes the difference between safety and danger. According to the law, this skill is to be certified by an authority which then issues the driving licence. A person possessing the driving licence is supposed to have qualified the requisite tests devised by the Public Vehicles Department. But this is hardly ever the case. The system works like this: the driving schools and touts make deals with the motor vehicle inspectors. The inspectors are supposed to look at each application, and conduct and supervise driving tests, check the road-worthiness of all public vehicles, and so on. Instead, they simply put their initials on the hundreds of certificates forwarded to them against a pre-negotiated amount, and the public is at the mercy of a new breed of reckless drivers. At the PVD offices in Alipore and Beltola, even talking to an official requires a tout and some money. 

 The traffic police are no better, letting off errant drivers for a bribe. It is because of them that drowsy drivers with questionable experience drive school buses and pool cars in the morning, killing unfortunate travellers like the young Majumdar couple recently, and dashing against trees and dividers. Pool cars are still the safest bet for schoolchildren, because they will not start running before a child can get off. But parents and school administrators can put pressure on their drivers to prevent accidents. There will still remain the problem of the lawless public buses and auto-rickshaws that only the police and public transport authorities can solve. 

 Yours faithfully,

Manoj Banerjee, 

 Calcutta

 Sir The fact that children are regularly crushed to death under the wheels of monster buses in Calcutta speaks volumes of our insensitivity as a city. It is not that buses turn into such killers only because of the system of commission in private bus syndicates; the reasons go deeper than that. Indiscriminate issue of driving licence is the hallmark of the citys transport authorities. The police, who are supposed to discipline the reckless traffic, never seem to be up to their task. The school authorities are no less at fault, since it is their responsibility to ensure that the children return home safely. And finally, the government has not shown any sign of concern even after the series of mishaps involving schoolchildren. It ought to be hanging its head in shame.

 Yours faithfully,

Surajit Das, 

 Calcutta

 Sir The most disturbing thing that I see in Calcutta is the mad race among bus drivers to overtake each other, often resulting in serious accidents. Since the police alone cannot curb this trend, the people must start behaving responsibly. The passengers of rogue buses, instead of being mute spectators, should object, and if necessary, apply force to bring the errant drivers in line. 

 Yours faithfully,

Tuli Banerjee, 

 Cambridge, US

 Sir While the spotlight is on road accidents in Calcutta, it might be well worthwhile to take a look at the inhuman working conditions of transport workers here. The average bus or minibus driver drives 13 hours a day in Calcutta traffic, and is rewarded for picking up more passengers and fined for arriving late. Given this, accidents are just waiting to happen.

 Yours faithfully,

Dhurjati Das Gupta, 

 Calcutta

 Sir While the authorities sleep, we, the passengers of Calcuttas buses, can do our bit to prevent accidents. We must stop urging bus drivers to speed up, break lane, or overtake from the wrong side to make up for our own lateness in getting out of home. But are we willing to change?

 Yours faithfully,

Kalyan Kumar Ghose, 

 Calcutta




</TEXT>
</DOC>