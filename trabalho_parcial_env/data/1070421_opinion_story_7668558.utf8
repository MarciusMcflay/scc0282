<DOC>
<DOCNO>1070421_opinion_story_7668558.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 TWO ADAPTATIONS

 Ananda Lal

 Theatre

 More and more Bengali groups are leaving behind the ideological credo of socialist realism and exploring other avenues for sources, even commercial British genres. Two current productions have adapted a hit farce and a murder drama respectively from mid-20th century London theatre.

 Ekush Shatak presents Loknath Bandyopadhyays Krishnaranga, from Peter Shaffers successful Black Comedy (1965), one of the funniest plays you can presently view. Shaffer built it around the simple but ingenious device of a power cut, during which the stage lights turn stark white so the audience can clearly see the cast behaving as if in pitch dark.

 The plot itself is a typical romp: a starving artist must impress both the father of his new love and a prospective patron scheduled to visit his spartan digs. He decides on the ploy of temporarily borrowing a vacationing neighbours furniture. Suddenly a fuse blows, followed by the successive entries in darkness of the would-be father-in-law, neighbour, former girlfriend, electrician and potential buyer.

 Unlike most Bengali adapters, Bandyopadhyay keeps the script short and tight, and directs with dexterous comic pacing. The crucial lighting obviously demands split-second timing too, in which Badal Das does not fail. As the sculptor, Apurba Roy takes enough pratfalls (picture) to prove his physical conditioning, while Pradip Chakraborty lifts the clueless electrician into the best supporting role.

 For Sanglap Kolkatas Astarag, the inspiration is Agatha Christies The Verdict (1958), possibly her only play without a corresponding fictional edition. Unluckily, it flopped in the West End, which should have given the adapter-director, Kuntal Mukhopadhyay, something to think about, even though Christie herself liked it a lot.

 In the original, a German professor (here, Krishnendu) escapes Nazism and settles in England with his invalid wife and her devoted cousin, evidently in love with him. A rich industrialist offers to pay for the wifes treatment if Krishnendu accepts his frivolous daughter (infatuated by Krishnendu) as a pupil. The professor takes the bait for his wifes sake.

 The problem is that Astarag/Verdict does not qualify as a thriller, suspense drama, detective story or murder mystery; there is no mystery about the murder, which occurs in front of our eyes. Also, the culprit later dies accidentally quite a cop-out. No wonder the audience at the London premiere booed!

 So, even the best acting cannot rescue this moral lesson on compromising ethics. But Mukhopadhyay stereotypes his own performance of the absent-minded idealistic professor, just as he does that of the other extreme, the spoilt-brat tutee, as a bad girl, down to her costume. And, unlike Gere vis--vis Shetty, he also holds back from the passion of his clinch with his wifes cousin in the source. 




</TEXT>
</DOC>