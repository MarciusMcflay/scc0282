<DOC>
<DOCNO>1070721_calcutta_story_8080848.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Mall-eria, strictly by design

 - Retail addresses must marry diverse elements and experiences

 SUBHRO SAHA

 City Centre, in Salt Lake, has defined spaces for visitors to relax and rest. A Telegraph picture

 Ten years ago, India didnt have a single shopping mall. Now, nearly 50 million sq ft of mall space is in various stages of completion, across seven major cities. Calcutta and its fringes alone will have 35-40 new retail centres in the next 36 months.

 Will all these properties do well? Or will some of them be forced to down shutters and maybe put to adaptive reuse? 

 The right expertise in conceptualisation, design, development and management of malls is absolutely necessary to optimise long-term sustainability and profitability, experts concur.

 A shopping mall should be able to bring together diverse elements and experiences under one roof. The design must be geared to stimulate shopping, or else you have lost the plot, no matter how spectacular the property is, feels Nicolas Kyriacos, director of Bentel Associates International of South Africa, pioneers in commercial architecture.

 Kyriacos, who coordinates development of new business in India for Bentel, and oversees concept, development and design direction, adds: Our plan is based on symbiosis between big and small retailers to create a microclimate that encourages visitors to shop. Bentel has designed the South City Mall on Prince Anwar Shah Road and is also doing the Acropolis mall on the Rashbehari connector.

 City-based architect J.P. Agrawal, who created Forum on Elgin Road and is now doing Mani Square on the Bypass, agrees. A mall might have both the right tenant mix and healthy footfall. But if you cant offer an environment conducive to shopping, it wont translate into retail success, he warns.

 Agrawal says good retail design should address both the target audience and the right retailers who could cater to that buyer group, doing the scale keeping in mind the hinterland. For instance, you cant replicate the Forum box format on the Bypass or in Narendrapur. Each location will have its specific needs and character.

 How important is location in the context of sustainability? In the absence of a land-use control plan from CMDA, shopping malls can be constructed anywhere, provided they conform to the local building rules. As a result, malls are mushrooming everywhere around town, irrespective of the capacity of the roads and infrastructure.

 There should be a healthy distance between malls so that traffic flow and car parking do not affect vehicular movement. Overcrowding at any location will affect all the shopping centres in any area, feels Partha Ranjan Das, architect and urban designer.

 Das, who was involved with Charles Correa in the design of City Centre, in Salt Lake, feels most of the upcoming malls are simply colourful glass boxes with shops inside. There are no defined spaces for the shoppers to relax and rest for a while. These malls are designed only for maximum profit from sale of commercial space, he laments.

 Free space is at a premium in most of the existing and proposed shopping centres, concurs Harsh Sanon, another city architect who has designed the Haute Street mall coming up at the Topsia intersection.

 This is primarily because most malls lease out stores on the basis of super built-up space and theres hardly enough room for the shoppers to move around. This outlook has to change, Sanon points out.




</TEXT>
</DOC>