<DOC>
<DOCNO>1070505_foreign_story_7737182.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 Bangla army new ball game

 ASHIS CHAKRABARTI

 Soldiers on guard at a railway station in Dhaka

 Dhaka, May 4: There are no prizes for guessing who is ruling Bangladesh. But the countrys army, that has a chequered history of staging coups and imposing martial laws, is into a different ball game this time. 

 And this despite the fact that it is the prime mover behind the caretaker government comprising civilian advisers.

 The most obvious evidence of this new role of the military is the virtual absence of army chief Lt Gen. Moeen U. Ahmed from public life. He is rarely seen or heard in public fora a dramatic change from the days of the countrys 15-year tryst with military rule under Gen. Zia-ur Rahman and H.M. Ershad between 1975 and 1990.

 The other significant evidence is the relative freedom that the media has even during emergency. There are curbs, threats and the advice from the government and the army, especially at the district level, admits a senior journalist, but all these are a far cry from the censorship and harassment during Ershads military regime.

 Ahmed, though, did sound a familiar note last month. In a rare public speech after the state of emergency was declared in the country on January 11, he hinted that the country had been let down by the leadership of both Khaleda Zia and Sheikh Hasina. 

 Bangladesh, he said, needed a new political party system. Given the governments attempts at that time to force the two women leaders out of the country, his remarks were interpreted as a precursor to a possible army takeover.

 Political leaders, his former army colleagues and other observers were somewhat intrigued by Ahmeds comments. 

 They agree that Ahmed is no Rahman or Ershad. He is not the sort of general who would re-visit the days of army coups and martial law administrations. But, given the history of the countrys army, they kept their fingers crossed. 

 The air was cleared in a few days by Ahmed himself. At a party in a western diplomats house, he told two senior journalists that he had no intention of taking over the administration. The army will go back to the barracks once an elected government takes charge.

 The army could have taken over the government on January 11 if it so wanted. And, if it had done so at that time, the people would have approved of it because of the complete lawlessness that prevailed in the past five years, says Subid Ali Bhuina, a retired general, who was Khaledas principal staff officer when she was Prime Minister between June 2001 and October 2006.

 Moeen did the wisest thing by not directly taking over and its unlikely that hell do so now, he argues. 

 That Bhuina is now with the Awami League speaks of a special trait of the Bangladesh army. Over the years, retired army generals and other officers have joined politics. Their first preference has always been the Bangladesh Nationalist Party (BNP), founded by Rahman, Khaledas assassinated husband, in order to legitimise his coup. 

 Although the BNP is known to be a pro-army party and the top army brass has been known to be pro-BNP, Hasinas Bangladesh Awami League, too, now boasts nearly a dozen retired army officers. 

 Politicians and other people were, therefore, surprised at the armys tough action against many senior BNP leaders, most notable of them being Khaledas son and heir-apparent, Tarique Rahman. 

 Although the army has not openly involved itself in politics since 1991, it played a role in politics, mostly with the consent of a section of the political class. Law and information adviser to the government, Moinul Hosein, defends the armys role in the government. It isnt like in the old days when the armys role meant waging wars and conquering countries. A modern army has and should have a role in running the country, he argues. 

 So why did the army not take over the government at the start of the declaration of the emergency? Analysts here offer two basic reasons. 

 First, when Rahman and Ershad imposed martial law administrations in 1975 and 1981, respectively, Bangladesh had a presidential form of government. The 1991 constitution changed it to a parliamentary democracy. 

 A martial law now would have to completely suspend the constitution. Besides, neither the army nor politics in Bangladesh can go back to the pre-1991 days. But then, any army coup anywhere does not happen to uphold the constitution. 

 The second reason sounds more realistic. An army coup now would have caused international reactions that might have the interests of the army itself. A coup could have made Bangladesh ineligible for the UNs peacekeeping missions abroad. The UN assignments have become a major source of income for both officers and men of the Bangladesh army.

 The income from just a two-year UN term is sometimes more than the salaries of a soldiers years of service. There has to be an extraordinary situation in which a Bangladeshi army officer or soldier will agree to lose this income.

 Others talk of a third important reason. The old stereotype of the Bangladeshi army being a political, rather than a professional, army is no longer true, another retired general explains.

 Army bosses have political preferences, like any other people. But, they would leave politics to politicians, except in extraordinary circumstances. But then, I really dont know what really is in the top generals minds.

 That is the refrain one hears in most conversations here. Ahmeds assurance has not quite silenced whispers about the armys plans. The situation is too fluid to draw conclusions. Hasinas return home next week may lift the curtain on the next act of the armys play.




</TEXT>
</DOC>