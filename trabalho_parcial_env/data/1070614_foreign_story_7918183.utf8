<DOC>
<DOCNO>1070614_foreign_story_7918183.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 New arthritis drug hope

 NIGEL HAWKES

 London, June 13: Tens of thousands of patients crippled by rheumatoid arthritis can expect dramatic improvements in their treatment with the arrival of a new class of smart drugs, scientists said today. 

 A study of three medications has shown that they can reduce the symptoms of rheumatoid arthritis, the debilitating joint disease, by about 50 per cent. Experts say the drugs will help liberate many sufferers with severe disease from pain and allow them to lead a near-normal life. However, doubts remain over patients chances of getting the new drugs, which have yet to be approved by the National Institute for Health and Clinical Excellence (NICE), the government watchdog, and could add around 250 million a year to the NHS drugs bill. 

 Britain lags behind other European countries and the US in introducing new medicines. Drugs launched in the past five years, including this new class, make up 27 per cent of the bill for medicines in the US, 24 per cent in Spain, 22 per cent in France, but 17 per cent in the UK. Trials have shown that the three drugs MabThera (rituximab), Orencia (abatacept), and tocilizumab can have a marked impact on symptoms of rheumatoid arthritis, which include joint pain, stiffness and swelling. The disease, which occurs when the immune system attacks the joints, affects an estimated 400,000 people in the UK, 4,000 seriously. Each new drug consists of molecules that target different parts of the immune system. 

 MabThera and Orencia are licensed in the UK; the latter was launched this month, while tocilizumab is undergoing later-stage clinical trials. Professor Paul Emery, a leading British specialist and co-author of the review in todays online edition of The Lancet, said: They are strikingly effective and they work on different targets from the existing drugs, thats the joy of it . 

 The research showed that all three slowed progression of the disease and reduced its symptoms. All achieved the best results when used in combination with the standard treatment, methotrexate. Not all patients respond, and there can be serious side-effects in some, but 30 to 40 per cent of patients do see big improvements. 

 THE DAILY TELEGRAPH




</TEXT>
</DOC>