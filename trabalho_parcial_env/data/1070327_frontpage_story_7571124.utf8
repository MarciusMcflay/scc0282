<DOC>
<DOCNO>1070327_frontpage_story_7571124.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Nuke talks amid negotiator drama

 K.P. NAYAR

 Washington, March 26: At the end of two days of negotiations with the Americans in New Delhi on the nuclear deal, the Indian delegation was bogged down in housekeeping issues, making it unlikely that the so-called 123 agreement to take the deal forward would be finalised by the end of the talks tomorrow.

 Sources attending the talks said today that the gap between New Delhi and Washington on substantive issues was still wide, but a big road block was confusion over the composition of the Indian side in the negotiations.

 American diplomats said their delegation, led by Richard J.K. Stratford, director in the office of nuclear energy, safety and security in the state departments Bureau of International Security and Non-proliferation, were told before they left Washington that they would be talking to an Indian delegation led by Raminder Singh Jassal, deputy chief of Mission at the Indian embassy in Washington. 

 This arrangement was changed while Jassal was en route to India from the US and it was decided that Subrahmanyam Jaishankar, Indias high commissioner in Singapore, would head the Indian team.

 To add to the confusion, Jaishankar had barely landed in Singapore after a trip to New Delhi when he was asked to repack his bags and fly back to India.

 The Indian delegation, which is composed of officials from several ministries and agencies, such as the department of atomic energy, is patently unhappy with the arrangements, not because they doubt the competence of either Jassal or Jaishankar. 

 Veterans of Indian negotiations since Independence cannot recall a single instance where officials serving abroad were called to lead an inter-ministerial team meeting in New Delhi. 

 During informal discussions among members of the Indian team before they sat down with the Americans on Sunday, several officials are said to have expressed the view that an arrangement under which officials serving abroad head the key Indian delegation will not work.

 Who will do the follow-up at the end of talks? one participant from outside the ministry of external affairs asked. There is so much of inter-ministerial co-ordination to be done after each round. 

 But the confusion surrounding housekeeping within the Indian team points to other, more serious problems, which do not augur well for the nuclear deal with the Americans.

 It is understood that arrangements about leadership of the delegation and similar key issues were made by the Prime Ministers Office and not the MEA. 

 This is being interpreted as a desire on the part of the MEA, especially its cautious minister Pranab Mukherjee, to distance his ministry from the increasingly unpopular nuclear deal as the Indian political scene slowly drifts into election mode. 

 That trend was barely beneath the surface during the recent visit of foreign secretary Shiv Shankar Menon to Washington, when he sought to focus on issues other than the nuclear deal. Menon gave the clear impression that the nuclear deal was the baby of Shyam Saran, the special envoy for the deal, who is part of the PMO. 

 Anticipating problems because arrangements for the latest round of 123 talks have been patently mismanaged, Ronen Sen, Indias ambassador to the US and an architect of the deal, flew to New Delhi on Friday to be around if some fire-fighting was required.

 The official explanation, however, was that Sens trip was planned long ago and that he was in India to discuss whether he should continue in Washington after his current term expires in August.

 Sadly, the state of the 123 negotiations is exasperating Indias biggest supporter in the Bush administration, Secretary of State Condoleezza Rice.

 Rice told a group of reporters on Friday about the imperative of the 123 agreement. It is the only way they are going to be able to do it with the Nuclear Suppliers Group. It is the only way they are going to be able to do it with anyone that can help them on the civil nuclear side.




</TEXT>
</DOC>