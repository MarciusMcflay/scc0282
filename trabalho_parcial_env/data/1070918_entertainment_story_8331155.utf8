<DOC>
<DOCNO>1070918_entertainment_story_8331155.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Fan Club Hero Worship is hard work

 Just how star-struck are you? Would you put up giant posters, join boisterous parades and participate in general mayhem for your hero? In that case, you would be a perfect candidate for honorary fan club membership. But there is more to being a devout follower than making noise on the city streets, or pledging allegiance to a celluloid demigod. Writing cerebral editorials, social work, role-plays and travelling are some of the other activities that Calcuttas self-proclaimed fans dedicate themselves to.And while fan club members of old were satisfied corresponding with large corporations promoting their products, such as the Maggi Club or the Barbie Fan Club, todays groups are by, of and for the fans, with some special benefits for the objects of their affections .

Film frenzy

 Any Shah Rukh Khan first day, first show spells smashing of coconuts, dancing on the streets and raucous revelry for the SRK Fan Club, based in Kali Banerjee Lane in north Calcutta. It came together a decade ago and has more than 300 members, some of whom can rattle off impressively long dialogues voiced by the King Khan.

 Arnab Roy, secretary, basks in the memory of meeting the superstar. I met Shah Rukh in 1992 when he came down to perform at the Netaji Indoor Stadium. He approved of our fan club and we felicitated him with a life-size statue and a golden crown. The fan club unveiled an SRK archive on the stars birthday last year. Titled SRK The God in Many Moods, the archive also includes a 5.10-ft statue of the star made of fiberglass, documentary films on him by Munni Kabir and recently-released copies of Mushtaq Sheikhs biography Still Reading Khan, and sells memorabilia. The club also owns some of the costumes Shah Rukh had donned in Main Hoon Na and Paheli. We are expecting few more from Om Shanti Om, reveals Roy.

 Sometimes fan frenzy can get out of hand too. The city has been home to a fan club temple dedicated to Amitabh Bachchan, although it faded into obscurity after S.P. Kamat, secretary of the Amitabh Bachchans Fans Association ran into murky waters (he was charged with murder). His attempt to deify his favourite idol, holding a yagna for the construction of a temple for their hero in Dum Dum, also earned him the contempt of Big B himself. He has been our inspiration in whatever we do, organising blood donations and eye camps, opening a thalassaemia detection centre. He is our god, Kamat had claimed then.

 Homegrown stars too are the objects of very vocal adulation. Tollywood heartthrob Jeets fan club was started by 26-year-old Tuhina Sarkar. Saathi Tomar is named after Jeets first Bengali film Saathi, after she met him in person almost three years ago. Were great fans of Dadabhai (Jeet) and want to do some good work in his name. Our aim is to be of social help and were concentrating on helping needy children with educational and medical support, says Tuhina who is planning a formal launch of the fan club on Childrens Day this year. The club also ensures a gala first day, first show of any Jeet film for club members and their families. The club plans to go official with special membership cards for its 200-plus members. Many of the fans look forward to meeting Dadabhai so we try and invite him for our special fan club screenings. Even if they can catch a glimpse of him, it is good enough for that year, explains Tuhina. 

 The monumental Mithunda has many fan clubs, of course. One particularly enthusiastic bunch of followers has set up three sites dedicated to their icon: http://mithunfb.tripod.com, http://mfbtv.tripod.com and http://bangalibabu.tripod.com, which discuss Mithun Chakrabortys films, upcoming projects, review his work, display movie clips, showcase his latest work and even provide free tickets for his latest releases. Mithunda has been supportive of our endeavour and in turn we try to help his social work projects by setting them up online and inviting donations from fans from all over, says Tuhin Purkayastha, president of the fan club that is now trying to move beyond the virtual realm.

 Sporting stars

 Football which governs the food (chingri vs ilish), clothes (maroon and green vs red and yellow), cultural climate (Bangal vs Ghoti) and mood (delight vs despair) of the city and its famous rivals Mohun Bagan and East Bengal have fans and fan clubs aplenty. 

 The East Bengal Fan Club was a rather late entrant into the circuit when it was set up in August 2006, but it already has three branches in Barasat, Jalpaiguri and Dum Dum. Supporters in each locality can enrol and bear the responsibility of hoisting flags and screening matches. Members stand a better chance of getting tickets. East Bengal merchandise is also distributed through the fan clubs which have a pool of nearly 1,000 members. We plan on introducing privilege cards for our members that will get them discounts on retail purchases, says Shantiranjan Dasgupta, assistant general secretary, Kingfisher East Bengal Club. Apart from organising para football matches on holidays, the fan club also initiates medical check-ups and blood donations. And after every win, they rush to the market to buy up all the ilish for a grand celebratory feast. 

 There are a number of Mohun Bagan fan clubs, including those in Behala, Shyambazar and Salt Lake. This year the Shyambazar Fan Club organised a programme on Mohun Bagans foundation day where they took fans and ex-players to visit all the spots associated with the early days of the club on horse-drawn carriage, says Asoke Chatterjee, an old timer who played with both clubs in the 60s. Not only did the fan clubs provide a great support base for the players, they also helped them out on a number of occasions from putting up banners in the stadium and paras to helping Sailen Mannas family through the difficult times when he had a heart attack, stresses Chatterjee. Tiger prawn is their symbol. From fans with head gear with emblems of prawns to bringing live chingri on to the field as offerings to the players, we have seen fans do some crazy things, concludes Chatterjee.

 Dada, of course, has a string of fan clubs in town. Souravs Fans Club in Kalighat came together when Ganguly was dropped from the team in 2006. Around 30 members burnt effigies and held demonstrations against the BCCI, Kiran More and Greg Chappell. The fan club can also let loose when their Dada hits a high. Whenever hes declared man of the match or series we go around the streets ringing bells and distributing sweets, says Abhijit Mukherjee, president. 

 But Gangulys fan club in Baguiati underplays its adoration. The club publishes a bi-annual magazine that talks not only about Ganguly, but other achievers in any sport from Bengal. Contributors include sports columnists, editors and cricketers. 

 Musics maestro 

 Mohammed Rafi still commands quite a large fan base in the city. Started by Ruhi Khan in 1997, Rafi Lovers Circle started small amongst friends and family. Over the years, it has grown to more than 125 members who meet once a month. We are very fond of his timeless songs and decided to forge a friends circle, says Khan. The club is choosy about granting membership. Entrants need to fill up a form and meet us face to face before we can decide on admission. Four shows held annually at Gyan Manch allow fans a platform to perform their favourite Rafi songs. On the singers 25th death anniversary, the club introduced the Mohammed Rafi Memorial Award to felicitate a person who has been closely associated with Rafi.

 Literary loyalty 

 The P.G. Wodehouse fan club Blithe Spirits got together in 1998 as one of the very few registered fan societies in the city. What started as an initiative of four Wodehouse enthusiasts has grown to a group of 50 members. The core group is selective; wannabe fans need to fill up a questionnaire and get half the answers right for a place in the club. Subrata Chatterjee, the general secretary and vice president, joined the group in its initial stages and continues to host regular meetings where members spend a few merry hours discussing interesting characters, situations and future programmes. There was a time when I was going through a depressing phase and reading Wodehouse acted as a balm. This club is an extension of that, says Chatterjee. The club organises quizzes, play-reading sessions and fancy dress parties, where the 20-70 year-old members come dressed as Wodehouse characters. 

 Pottermania has naturally given rise to a number of fan sites. Mugglenet.com and Darkmark.com bind together fans, ensuring Pottermania outlasts the books and films. I visit Mugglenet, which is one of the best Harry Potter sites, to gather interesting bits of information about the characters. I can find out the smallest details like birthdays, favourite colours/food of my favourite character as well as download customised wallpapers, says Ananya Dutta, a Class IX student. Darkmark.com allows members to create characters based on the Harry Potter world and then create stories that are continued via the message boards. I have five new characters that I have created and inserted into the world and the story is taken forward through a collaborative effort between the various fans. This has helped me hone my writing skills and also keep the excitement of Harry Potter alive, says t2 Quill Club Member Soham Bhaduri.

 Travel junkies 

 The Indian Railways Fan Club or IRFCA is one rather eccentric fan club with a bunch of members dedicated to preserving the heritage of Indian Railways, celebrate its technology and track the latest events. Their website http://irfca.org has a collection of travelogues as the members travel off the beaten track into small stations and towns and write about their experiences. Anyone who is interested in the railways, its history and with a little bit of the travel bug in them can join, says Samit Roychoudhury, one of the designers of the website who has recently published The Great Indian Railway Atlas, based on information from IRFCA members.

 Mohua Das and Diya Kohli

 What fan club would you like to join/set up? Tell t2abpmail.com




</TEXT>
</DOC>