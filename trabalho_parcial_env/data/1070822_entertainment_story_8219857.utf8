<DOC>
<DOCNO>1070822_entertainment_story_8219857.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Non-fantasy fantasy

 Why Stardust has the fun and pacing of Midnight Run with the veneer of Princess Bride. Also, what links Brad Pitt to Cate Blanchett

 Neil Gaimans novel Stardust, the source for the new movie of the same name, is a bit of an anomaly among the works that have made him a legend in the comics world. To begin with, though it was originally published in four instalments by DC Comics in 1997, Stardust was never a comic book series with panels and dialogue, like, say, Gaimans famous Sandman. Its a prose novel with Arthur Rackham-like illustrations by Charles Vess, and came out in a comics format only because traditional book publishers didnt want to spring for so much colour printing. Stardust was subsequently reissued as a hardback and trade paperback, and in 1999 was published in a text-only version.

 Set in two parallel worlds, a quaint Victorian village named Wall and the fantastical kingdom of Stormhold, and with a plot that involves both a witch and a posse of murderous princes chasing a fallen meteorite that is really a young woman, Stardust is also written in a consciously old-fashioned manner. Gaiman composed it in longhand, using a fountain pen and a leather-covered notebook, he said in New York recently, and the result was that he eliminated a lot of computery bloat. His aim was to evoke the manner of early-20th-century writers like Lord Dunsany and Hope Mirrlees, who wrote fantasy stories of a sort that was sometimes called faerie.

 Stardust was intended to be pre-Tolkien, a fantasy novel that didnt read like one, and the movies creative team the director, Matthew Vaughn, and the screenwriter, Jane Goldman have attempted much the same thing: a fantasy film that can be watched not just by the Lord of the Rings crowd, or even by Gaimans worshipful following, but also by people who wouldnt be caught dead at a fantasy film.

 In many ways Jane Goldman and Matthew Vaughn were unlikely choices for the movie which stars Claire Danes, Sienna Miller, Charlie Cox, Michelle Pfeiffer and Robert De Niro especially considering that Gaiman is famously fussy about turning his work into film. His usual way of working plotting a graphic novel frame by frame and then turning his instructions over to an artist is as close, he has said, as you can get to moviemaking without actually making a movie. What I do is like being the screenwriter, director and editor. The artist gets to be the cameraman and the actors.

 Goldman had never written a movie before and is best known for an X Files guidebook and as the host of a British television series investigating the paranormal. Vaughn had directed only one other movie, Layer Cake, and is best known for producing Guy Ritchies heist films, Snatch and Lock, Stock and Two Smoking Barrels. Gaiman settled on Vaughn mostly just because he found him trustworthy. To say that Hollywood producers are not trustworthy is like saying that lions are not vegetarians. But Matthew has always stuck to his word, and I do trust him.

 On the phone at least Vaughn sometimes sounds like one of Ritchies brash, fast-talking card sharks. I wrote it, directed, produced it, he said of Stardust, explaining that he raised the money independently. I put the whole thing together. Im quite good at doing things my way. When he spoke to Gaiman, he added, he already had the whole movie in his head. I think a good idea is a good idea, he went on. If there are too many problems with something, then just dont make it. But we had a very good idea for this.

 The idea, in a nutshell, was to make the fantastical scenes seem in some ways more realistic than the Victorian ones. Basically I love fantasy novels, but I wanted to make a non-fantasy fantasy movie, Vaughn said. I kept telling everyone: Just play it normal. Youre not wearing a costume, youre wearing clothes.

 He added that he deliberately injected the Stormhold scenes with contemporary touches. The whole movie is really Midnight Run, Vaughn added, alluding to the 1988 chase movie in which a bounty hunter, played by De Niro, travels cross-country with a white-collar criminal, Charles Grodin.

 Yvaine and Tristran, he explained, referring to the fallen star and her half-mortal companion (Danes and Charlie Cox), are Charles Grodin and De Niro. The F.B.I. the princes. And the witches are the mob. That was my inspiration. I thought, Im going to make a movie that has the fun and pacing of Midnight Run with the veneer of Princess Bride.

 Gaiman suggested Goldman, whom he had known for years and encouraged during the writing of her first novel, Dreamland. According to Vaughn and Goldman, the two of them hit it off immediately and shared a view of how the movie needed to be structured.

 That stars of the magnitude of Pfeiffer and De Niro would turn up in his story, which is all about star power of another but not entirely dissimilar kind, is something Gaiman never anticipated. When I imagined casting, I dreamed of Alec Guinness playing all the roles, he said. I was in awe when I saw it. There were things I argued about and lost, and it turns out Matthew was right about them. When you look at it, all the subliminal cues that tell you youre in a fantasy movie arent there. Its like watching a slightly skewed historical film.

 Charles McGrath

 (New York Times News Service) 




</TEXT>
</DOC>