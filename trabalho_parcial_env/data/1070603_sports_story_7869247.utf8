<DOC>
<DOCNO>1070603_sports_story_7869247.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 McClaren denied a famous victory 

HENRY WINTER

 Steven Gerrard and Ronaldinho in action at Wembley on Friday. (AP) 

The kings of the carnival spoiled the Wembley homecoming party for England on Friday night. Just when Steve McClaren was celebrating a sweet victory over the masters of the Beautiful Game he was given the sourest of awakenings. England have endured pain by South Americans called Diego before, but never a Brazilian one. 

Sporting a smile triggered by John Terrys 68th-minute goal, McClaren was happily contemplating the final whistle, and England embarking on next weeks Euro 2008 qualifier in Estonia in a buoyant mood. But poor concentration proved costly in injury time when Diego headed in Gilberto Silvas chipped pass. 

The disappointment should soon fade. The positives far outweighed the negatives. Terrys towering header emanated from a free-kick struck perfectly by David Beckham, who inevitably impressed most at dead-ball situations, although one delighted dinked pass did create a headed chance for Michael Owen. The real test for 32-year-old Beckham will come when the pace of the game increases. 

Of more significance to Englands long-term future was Owens diligent 82-minute shift. Returning to the full fold for the first time since his dreadful World Cup injury, Owen showed glimpses of his old class, but he needs a more influential partner than Alan Smith. Peter Crouch, who seems to have run over McClarens cat as well as Rafa Benitezs, eventually loped on, and surely offers a better option against Estonia (in the absence of the suspended Wayne Rooney). 

McClaren was also said that Steven Gerrard remains central to Englands good fortunes, the Liverpool captain being voted man of the match for an evening of hard graft closing down Kaka. McClaren also learned that Gerrard and Frank Lampard will never dovetail. Shamefully, many England supporters booed the Chelsea midfielder, which clearly unsettled him and he had a poor night. 

Until Diego frustrated England, Wembley had reverberated to noisy acclaim after seven years of the sound of builders. A thunderous minutes applause for the late, great Alan Ball was so moving a tribute that even the German official Markus Merk quickly joined in. 

Brazil settled quicker. Ronaldinho and Kaka were enjoying an evening at the flicks, looking to work the ball forward to Vagner Love while Robinho buzzed around in the hole. Even in second gear, the boys from Ipanema brought some sunshine to north London and were unfairly denied a goal when a linesmans flag ruled out Gilberto Silvas header. 

Gerrard ran about, tackling Kaka and Ronaldinho, putting out fires, and trying to roam upfield. Galloping towards the edge of Brazils box after 28 minutes, he was tripped by Mineiro, and Wembley stood in expectation of some Beckham fireworks. As a thousand camera flashes captured the fans obsession with Beckham, the Real Madrid midfielder curled the ball narrowly wide. Like a lovelorn teenager, Wembley sighed in disappointment. 

All eyes were on the No 7s. Moments after Beckhams chance, Ronaldinho clipped a free-kick wide. It encapsulated the first half: touches of class but little to set pulses racing. 

For all the excellence of Gerrard and Shorey, England lacked a cutting edge. Owen and Smith kept running into the excellent Naldo. Owen was typically busy, though, making runs down the channels, waiting for a ball that never came in the first half. 

Brazil fashioned a fine opportunity early in the second half. Robinho and Kaka cunningly set up Ronaldinho, whose 20-yard drive caught Kings foot and deflected wickedly goalwards. Robinson reacted brilliantly to save. 

Back came England, Beckham delivering a superb ball into the box for Owen, climbing well, to head fractionally over. McClaren then switched tactics, flooding midfield, and the 65th-minute arrival of Stewart Downing almost paid spectacular dividends when the Middlesbrough winger let fly from 25 yards, forcing Helton to tip over. 

 England were showing more pace 

 and cohesion, and Beckham delivered an outstanding reverse 

 pass towards Owen, whose header just failed to trouble Helton. 

 Terry appeared to have won it, but then came Diego. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>