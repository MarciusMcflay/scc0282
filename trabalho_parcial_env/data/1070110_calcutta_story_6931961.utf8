<DOC>
<DOCNO>1070110_calcutta_story_6931961.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

Life

 To be the host with the most

 PLENTY OF PLANNING AND 

 SOME COOL CREATIVITY... ANUMITA GHOSH TELLS YOU HOW 

 TO GET IT RIGHT

 The guests will be here any moment. The house is clean, the mood set. The beer is chilled, the chicken crackling in the barbecue. You are all dressed up and raring to go. The party begins it is a roaring success. Next-day calls find guests showering praise. All credit, to you, the perfect host...

 What may seem so smooth the morning after is, of course, the result of a whole lot of sweat and often tears. Playing the perfect host has a lot to do with how you deal with those testing, taxing and tiresome moments.

 GoodLife tells you how lots of planning and a bit of creativity can spare you the party blushes...

 Plan ahead

 Be organised. First and foremost, chalk out a to-do list. Says Mumbai-based grooming expert Chhaya Momaya: Draw up the guest list carefully. It is important that the crowd gels well. For a formal party, keep casual friends at bay. Talking loudly, untimely jokes do not suit the ambience, Chhaya adds. 

 The ambience is essential and so is the right choice of music. Music selection depends on the crowd profile. For example, a formal party demands soft music. Stock some recent chartbusters. A few warm-up drinks and the requests will follow, explains Chhaya.

 After music, comes munch. Plan the menu carefully. If the not-so-young crowd rules, ensure dinner is more on the traditional side and served early. If its a youngsters party, stress on finger food. Shop for non-perishable items a few days ahead of party time. Perishables can be fetched the day before or in the morning. Make sure you have food ingredients available or add it immediately to your shopping list. It prevents buying duplicates. 

 Last but not the least, keep enough time out to dress up. (Its been a long day, so taking care of body odour is a must.)

 Beverage basics

 As bar guru Irfan Ahmed puts it: The key to a successful party is drinks personalised. Finding out the guests likes and dislikes works to your advantage. Do not end up as the bartender. Stir it up sensibly. Start off with an alcohol punch prepared beforehand. An open bar helps the host relax and mingle. The guests are at their creative best, too smiles the mixologist. 

 If it is a big group, avoid too many cocktails. The timing and the day is important. If a working day follows or it is a lunch party, keep the drinks light.

 For a private party, it is always better to be over-stocked. Irfan prescribes loads of ice (half a kilo per person), glasses (four glasses per person), fresh fruit juice, vodka (neutral and flavoured), a single malt whisky, a blended Scotch and energy drinks as essentials. Avoid one-litre cold drink bottles. They tend to get flat when left opened.

 Do not be over-conscious about glass guidelines. Get the closest you can. Be creative, ensure your guest is comfortable handling it, shares Irfan.

 Food facts

 Guests are the stars for the evening, not the food. So do not let kitchen work eat into your time. The food is only a part of the whole get-together. You do not have to have the best of wine and a lavish restaurant spread to let the crowd enjoy to the fullest. Your guests expect you to be a part of the company, says Anirban Simlai, F amp;B director, The Park. 

 Go in for an easy-to-make cuisine like Italian. The market is filled with a variety of pastas and exotic vegetables. All you need is to whip up some yummy sauces, prescribes Simlai. 

 For a guest list exceeding 20, 

 engage a private catering service or restaurant that suits 

 your pocket. Says Chhaya: To add the personal touch, 

 some exotic salads can be prepared at home.

 The biggest food faux pas happens when you experiment. Apologising for a recipe that did not turn out the way the chef in yesterdays cookery show had demonstrated is not going to help. Never serve a dish you arent confident of, cautions Simlai.

 Strike a balance between vegetarian and non-vegetarian dishes. In case of a buffet spread, ensure the food is hot till the end.

 Do not fish for compliments. Theres no harm in tossing a grandma recipe around, but badgering guests about your culinary skills and boasting of the prawns price tag is a no-no, smiles Simlai.

 Long intervals between courses leave guests fidgety and gossipy. Serving wine in between courses leaves you with enough time to serve the next.

 Table tricks 

 Cutlery is arranged in the order of use. Forks, bread plate, bread knife and serviette are placed on the left while knives, spoons and glassware are to the right. 

 For a formal setting, ensure that cutlery is placed about one inch from the edge of the table. Each one should line up at the base with the one next to it. The blade of the knife faces the plate and the glasses are positioned about an inch from the knife. The glasses should be in order of use white wine, red wine, dessert wine and water goblet.

 Knives to the right and forks to the left should never be more than three. The dessert spoon, to the right, and dessert fork, to the left, are placed on dessert plates when brought to the table.

 Make sure the centrepiece is low enough, not to obstruct the guests view of each other or their conversations.

 For a buffet spread, spoons, forks, quarter plates, dinner plates, dessert plates and spoons will be required. Says Simlai: It is preferable to lay vegetarian and non-vegetarian dishes on separate tables. To be at your hospitality best, one could label the dishes! 

 Code of conduct

 Be warm and cordial from the word go. A personal invitation or call is a must. Leaving a message on the mobile looks half-hearted, says Chhaya. Dress for the occasion. If it is a business party, let not outfits and accessories scream out a fashion statement, she adds.

 Introduce people with correct names and designations. Give each guest 15 to 20 minutes of your time. Mingle slowly, theres no need to be a social butterfly. React to impolite comments intelligently, Chhaya cautions. 

 Introduce family members humbly. Do not exaggerate incidents. If your precious vase has been tipped over, be gentle. You can cry your heart out after the guests leave, prescribes the grooming expert. 

 For family parties, Chhaya suggests interactive and fun games like Antakshari or Dumb Charades.

 Bid a warm goodbye to one and 

 all after accompanying them to the door.

 Do

 Introduce your personal 

 touch. Remember the guests are attending a party at home 

 and not some restaurant

 Brief your helping hands

 Keep extra dinner plates handy

 Whip up light appetisers; you do not want guests losing 

 out on the main course

 Dont

 Crowd your guests. Check on seating and walking patterns 

 beforehand 

 Interrupt others, be a good listener

 Bother planning conversations ahead.Be yourself




</TEXT>
</DOC>