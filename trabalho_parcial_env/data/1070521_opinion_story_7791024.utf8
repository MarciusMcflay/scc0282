<DOC>
<DOCNO>1070521_opinion_story_7791024.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 INTO THE BLACK 

 BOX 

 FIFTH COLUMN

 GWYNNE DYER

 The 

 vast majority is with me, said Pakistans president, General 

 Pervez Musharraf, a year ago. The day I come to know Im 

 not popular, Ill quit. But more than that, theyll be out 

 in the streets, and I would not be allowed to stay. Well, 

 theyve been out in the streets for two months now, and 

 its a good question how long the general will be able to 

 stay in power. Its an even better question what comes next.

 Of the nine nuclear powers in 

 the world, seven are predictable countries that basically 

 support the status quo: the United States of America, 

 Russia, China, India, Britain, France and Israel. The eighth, 

 North Korea, may have one or two working nuclear weapons, 

 or maybe not. Then there is Pakistan, a one-bullet regime 

 with Islamist radicals lurking in the wings and around fifty 

 nuclear weapons plus delivery vehicles.

 A year or so after Pakistan first 

 tested its nuclear weapons in 1998, I asked an American 

 defence analyst what he thought would happen if officers, 

 who were seen as extremists, took power in Pakistan. He 

 said that there would be a traffic jam over Kahuta (then 

 the main Pakistani nuclear centre), as American, Indian 

 and Iranian aircraft launched simultaneous, uncoordinated 

 strikes aimed at eliminating Pakistans nuclear capabilities. 

 Its too late for that now: Pakistans 

 nuclear weapons are widely-dispersed and well- protected. 

 But it does give a measure of how horrified some other countries 

 would be if Musharraf` were replaced by a regime drawn from 

 some of the more extreme elements in the Pakistani military. 

 Instead, the current agitation suggests an eventual transition 

 back to civilian rule, but then there are no rules in Pakistani 

 politics. 

 When General Musharraf seized 

 power in a bloodless coup eight years ago, popular disgust 

 with the corruption of Pakistans civilian politicians was 

 so deep that he had real support for some years. Generals 

 have run Pakistan for almost half the time since independence, 

 sixty years ago, and, on average, the military regimes have 

 been slightly less corrupt. But Musharrafs life got much 

 more difficult after the 9/11 terrorist attacks on the US.

 Riding it out

 Pakistani governments, both civilian 

 and military, depend on appeals to nationalism and religious 

 sentiment to keep the impoverished majority quiet, but this 

 has worked much less well for Musharraf since he was compelled 

 to side with America in the war on terror. The surprise 

 is that it has taken this long for a crisis to erupt, but 

 now it has arrived.

 The trigger was Musharrafs attempt, 

 two months ago, to dismiss Chief Justice Iftikhar Chaudhry, 

 to make way for a more malleable judge who would not challenge 

 his intention to run for president again while remaining 

 commander-in-chief of the army. (This is unconstitutional 

 under Pakistani law, but Musharraf got away with it in the 

 rigged election of 2002.)

 It was the straw that broke the 

 camels back. All the groups that felt abused or insulted 

 by Musharrafs policies finally went out into the streets, 

 and the protests continue. There are rumours of a deal between 

 Musharraf and former prime minister, Benazir Bhutto, leader 

 of the Pakistan Peoples Party, the biggest in the country. 

 She has been living in exile, but he would amnesty her and 

 she would come home to be prime minister again, leaving 

 him the presidency. Nawaz Sharif, the ex-prime minister 

 whom Musharraf overthrew in 1999, denies this, insisting 

 that (Bhutto) said to me she will not enter into any deal 

 with Musharraf, but stranger things have happened in Pakistani 

 politics. 

 Musharraf may be able to tough 

 it out for a while, but the civilian politicians will probably 

 be back in the end. There is, however, another deeply worrisome 

 possibility. The Pakistani army is a black box, and no one 

 knows what is going to come out of it.




</TEXT>
</DOC>