<DOC>
<DOCNO>1070906_foreign_story_8282604.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : International

 Not surprising Cate found role scary, she plays Dylan

 Challenging

 Venice, Sept. 5 (Reuters): That Cate Blanchett found her latest screen role very scary comes as no great surprise. She was playing Bob Dylan.

 The unusual casting was made by US director Todd Haynes, whose movie Im Not There is a complex portrayal of the singer-songwriter using six performers to play Dylan, including Australian-born Blanchett, a young black actor and Richard Gere.

 In competition at the Venice film festival, the biopic seeks to avoid reducing Dylan to an easily definable type, and gives a sense of how difficult the ever-changing musician is to categorise.

 Cate was scared. She told me many times that this was a very scary challenge for her, Haynes told reporters after a press screening of the two-and-a-quarter hour film. Blanchett, 38, was not at the briefing.

 I think it took her a long time to commit to the role and shes a very busy actor and had to balance it with her schedule, but mostly I think it was due to fear, which is completely understandable.

 Blanchett, who won an Oscar for her portrayal of Katharine Hepburn in The Aviator, plays Dylan at a time when he shocked folk followers by embracing amplified rock and struggled with the media which sought to define him as a folk protest singer.

 In her black-and-white sequences, Blanchetts hair is dark and frizzy, and she adopts some of the mannerisms of Dylan, although the performance is not meant as a direct mimic.

 The open-ended nature of Im Not There meant the film was the first dramatic portrayal of his life Dylan had ever approved, Haynes said.

 I do think it was because of this open structure, something that would keep expanding who he is and what hes about and not reducing it, which I think is the tendency in the traditional biopic to do.

 Also playing Dylan are Gere, young black actor Marcus Carl Franklin, Christian Bale, Heath Ledger and Ben Whishaw.

 Old-style, black-and-white footage is mixed with colour sequences for Gere and Ledger and with real news footage of US protests in the 1960s and scenes from the Vietnam War.

 In production notes for the film, Haynes said these were his way of channelling anger he felt over the US invasion of Iraq.

 The relatively obscure Dylan track Im Not There was used for the title to portray the singer retreating from public life in the 1960s.

 Gere plays Dylan as the fabled outlaw Billy the Kid, who after finding refuge in the town of Riddle is forced to abandon his sanctuary and move on. 




</TEXT>
</DOC>