<DOC>
<DOCNO>1070522_sports_story_7812148.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Bentez: We need to be pro-active

OLIVER KAY

 Rafael Bentez 

Rafael Bentez has a simple answer when he is asked what he learnt from the most critical team-talk of his life, with Liverpool 0-3 down at half-time against AC Milan in the Champions League final in Istanbul two years ago. English, he said. Now I can express a little better the things I want to say. 

With no danger these days that his thoughts and instructions will be lost in translation, there seems little prospect of a similar scenario when the teams renew hostilities in this years final at the Olympic Stadium in Athens on Wednesday. Liverpool, though, have an unwelcome habit of doing things the hard way in cup finals. 

They were 0-3 down before they started playing in earnest in Istanbul and even trailed West Ham United 0-2 in the FA Cup final last May, eventually winning in a penalty shoot-out on both occasions. Not so long ago they won a crazy Uefa Cup final 5-4 against Alavs and an FA Cup Final 2-1 against Arsenal thanks to two late goals from Michael Owen. 

In this final, I would like to see the perfect team, Bentez said. The perfect team must be pro-active. Sometimes you need to react, but if you want to have the perfect team, you need to train the players to be pro-active. Thats something we can improve. Last time we gave away a free-kick and conceded a goal from the free-kick in the first minute, so this time I will say: Dont concede an early goal. Then we can start working as a team, as normally we do here at Anfield, for example. 

Liverpool departed for Athens Monday, but Bentezs game plan for the final is still not entirely clear. There were indications that the problem position on the left-hand side of mid-field would be filled by Boudewijn Zenden, as in both legs of the semi-final against Chelsea, but then he suffered an ankle injury in last weeks training camp in La Manga. 

If Zenden does not recover, the possibilities in that position will include Harry Kewell, fit again after missing almost the entire season with a foot problem, or John Arne Riise, presumably with lvaro Arbeloa operating behind him as a left back. 

Whatever his choice, Bentez will not allow his team to leave Milan the kind of space that Kak, Clarence Seedorf and others were afforded in both legs of the semi-final against Manchester United. 

Carlo Ancelotti, the Milan coach, said immediately after that game that he expected a far more difficult game in the final because of Liverpools greater tactical and physical strength and Bentez, while upset by recent criticism of his teams lack of style, agrees that his will be a very different approach from Uniteds, with Javier Mascherano likely to be asked to trail Kak. 

I think its true that United and us are different teams, Bentez said. We cannot approach the game as United do. They have different kind of players and we will do different things. When you approach this kind of game, the first thing is to analyse your team and to see which players are fitter and then say: Okay, this could be my team. Then you look at the other team and say, This could be their team. 

And then if you can find the weaknesses of the other team and the strengths of your team, perfect. Sometimes you need to change during the game, but you need to have a plan. I always say Plan A, Plan B and sometimes in a game you have to have a Plan C and Plan D. 

This time, language should not be a barrier. If you need to express something in 10 or 15 minutes and youre losing 0-3 with all the supporters behind you and it is not your language, its not easy, he said. Maybe that was the hardest thing for me in my time here. But I am sure that this time wont be like last time. THE TIMES, LONDON 




</TEXT>
</DOC>