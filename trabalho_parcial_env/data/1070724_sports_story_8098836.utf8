<DOC>
<DOCNO>1070724_sports_story_8098836.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Im sure my dad is looking down

 - Its been a long road to Harringtons first major title 

THE DAILY TELEGRAPH AMP; AGENCIES

 Padraig Harrington in Carnoustie on Sunday. (AP) 

It was entirely appropriate that the first Irishman to win an Open championship since Fred Daly, in 1947, should want to enjoy the moment. In fact, even had he been the 50th Irishman to achieve that feat, Padraig Harrington would have waxed lyrical on Sunday. After he had finished blubbing. 

Initially struggling for words, it took a moment for his achievement to sink in. Then the emotions took over. 

Padraig Harrington, Open champion, how does that sound, he was asked. It sounds like a lot to take in, he stumbled. I had so much going through my head most of it genuine shock and it will take a long time to sink in, but it was amazing, incredible, to see that putt drop. 

And on Monday, he was still in a state of disbelief. 

Its hard to describe, its unreal, especially in those quiet reflective moments when I am on my own, he said. I was standing in the shower and I thought: I won the Open championship. 

The likeable Dubliner, whose victory lifted him to sixth in the world rankings, said he did not overdo the celebrations. 

The first drink I had was a beer straight out of the claret jug, said Harrington. There was champagne spraying and celebrations and we moved into a party mode. I only had a couple of drinks, though. I was flying anyway and there was no need for any outside help. 

Harrington finally went to bed at 4am, but did not sleep for long. I was wide awake at 6am and woke my wife up. The claret jug was at the end of the bed; we both looked at it and we were still in a state of disbelief. But then she said: Can we go back to sleep now? 

After the win on Sunday, he paid tribute to his father, Paddy, a former policeman, who died from cancer days before the 2005 Open at St Andrews. Id like to thank my mum and dad. My dad is not here but Im sure he is looking down, he said. 

Still clearly raw, he said that losing would have been devastating, especially if it had come as a result of his double-bogey six at the 18th first time around. 

I think it would have been very hard to take if I had lost it there because I still had a chance. I stayed positive and convinced myself that in a play-off Id do the business. If Id lost, Id be wondering about being competitive on the golf course in the future, he added. It has been a long road for the Irishman, who is one of the hardest-working professionals around. This first triumph came in his 37th major tournament, his 11th Open. 

I did begin to doubt that I would ever win a major, he admitted. When I started as a professional I would have settled for being a good journeyman. Being named as a player who could win a major is great but it brings its own pressure. Ive come a long way. 

My goal is to win more than one major so that this doesnt become the pinnacle of my career I am determined to fulfil other targets and move on. 




</TEXT>
</DOC>