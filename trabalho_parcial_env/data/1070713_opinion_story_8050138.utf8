<DOC>
<DOCNO>1070713_opinion_story_8050138.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FIRST LIGHT

 The Bengal Renaissance: Identity and 

Creativity from 

Rammohun Roy to 

Rabindranath Tagore 

 By Subrata Das Gupta, 

Permanent Black, Rs 595

 This book is a refreshing addition to the literature on the Bengal Renaissance. More importantly, it offers a new way to examine this particular epoch. The books subtitle refers to the new approach of exploring the Bengal Renaissance from a cognitive model of identity and creativity. In this book, the author, Subrata Das Gupta, combines the attributes of a cognitive scientist and a historian to study and interpret minds as well as historical material. Das Gupta also lends to the book a broad, humanistic outlook. His findings may not satisfy the sceptics or the radicals but one cannot ignore the important premise that the didactics of the Bengal Renaissance must be examined in the context of the times to which it belongs. The subject matter cannot be judged or deduced only from post-Saidian, postmodernist and deconstructionist derivatives. It is possible to let each of the separate approaches in understanding the past speak in their own voices. Questions will be asked and more than one answer will be on offer. 

 The technical aspect of the new approach is presented lucidly in the first chapter of the book. It is not left merely to jargon. The technical terms drawn from the discipline of cognitive science have been explained clearly to explicate the cognitive processes of creative mentality and cross-cultural mentality in studying the Bengal Renaissance. So far, as the events or the parlance of the Bengal Renaissance are concerned, there is hardly anything in the book that one has not read or known earlier.

 The second foundational chapter deals with the Orientalist cognitive identity as distinct from Edward Saids theory of Orientalism with regard to the East-West historical encounter. Again, one does not replace nor cancel the other as is persuasively shown from time to time by both Saidian commentators as well as historians such as Das Gupta or David Kopf. Das Guptas book reminds the reader how a select group of individuals from another culture discovered and adored Indian art, literature and history. It comprised British Orientalist scholars and administrators such as William Jones, Henry Colebrook, Charles Wilkins, Warren Hastings and many others. Das Gupta goes into the cognitive processes of this phenomenon by perceiving in it a schema of an Indian golden age and by placing it in the cognitive identities of the individual scholars respective need/goal spaces. He also studies James Mill to show that there was a counter-schema in Mills case. It is a well-known fact that Mill could not ascertain anything utilitarian in Indias history and attainments. Thus, Mills History of British India fulfilled his need/goal space by trumpeting the countrys failure which, in turn, had a negative impact on British policy in India.

 The other chapters discuss the role of the Brahmo Samaj in constructing a cross-cultural mentality, the creative mentality of Bankim Chandra Chattopadhyaya and Rabindranath Tagore, the scientific consciousness and cognitive identities of Mahendra Lal Sircar, Jagadish Chandra Bose and Prafulla Chandra Ray. There is also an interesting chapter on how Ramakrishnas religion became the fount of Swami Vivekanandas creativity. The strength of this book lies in its treatment of the subject, and the lucid manner in which it explains what went into the construction of the ideology of the Bengal Renaissance. 

 UMA DAS GUPTA




</TEXT>
</DOC>