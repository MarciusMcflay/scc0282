<DOC>
<DOCNO>1070915_opinion_story_8312746.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 GREED AROUND THE WORLD

 - A new book, American unilateralism and a look beyond 

 Politics and play - Ramachandra Guha

 Reasoned disinterest 

 Nayan Chanda is that altogether rare specimen a modest Bengali intellectual. But he has much to be immodest about. An outstanding student at Presidency College and Jadavpur University, he later went to the Sorbonne to do a Ph D in modern history. He had written large chunks of his thesis on Cambodian foreign policy, when he decided to leave Paris to take up a reporters job in Southeast Asia. He reached in time to cover the fall of Saigon. Chanda spent the next two decades in the region, working for and eventually becoming editor of the Far Eastern Economic Review.

 In the late Nineties, Chanda moved to the United States of America to help set up the Yale Center for Globalization. He is still there, editing and directing YaleGlobal, a wide-ranging and influential e-magazine which is read by opinion-makers around the world. Chanda was arguably the first Indian to make a mark in the competitive world of international journalism, and to do so both in the old medium of print and the new medium of the web. 

 Nayan Chanda left the world of scholarship long ago, but there remains a scholar within him. In his years in the East, he published a classic account of the conflicts in Indo-China, entitled Brother Enemy. Now he has written Bound Together, a masterful, long-range history whose subtitle explains the books emphases: How Traders, Preachers, Adventurers and Warriors Shaped Globalization.

 Bound Together is a mature mans book. It could not have been written in the authors twenties or thirties (or even forties). It distils all of Chandas learning and experience, acquired over several decades and in several continents. The very long time-span (it starts with the migration of early man out of Africa) and the keen spatial sense come from his training in the great, and interdisciplinary, French tradition of historical research. His understanding of political expansion is owed in part to his being raised in a former colony and his working in other former colonies. And the focus on commerce, particularly commerce on the high seas, is surely not unrelated to his life in the trading regions of Southeast Asia and, more recently, North America.

 Nayan Chandas new book says a great deal, and says it well. But this is not a book review, so let me turn now to what Bound Together leaves unsaid, or perhaps half-said. A long chapter called The Imperial Weave starts with the Roman Empire and ends with the Pax Brittanica. It stops short of calling the US an imperial power. When, in a conversation with Chanda in Bangalore, I asked whether America was indeed an empire, he answered that two standard attributes of an empire were that it taxed its subjects and conscripted them for their armies. By those criteria the US was not, strictly speaking, an empire.

 In the very last pages of his book Chanda writes of the worlds sole superpower, the United States, which many view as the new Rome, has enormous, near-imperial power without an obvious empire. Then he adds: Even if they may not be sympathetic to Islam, a vast majority of the worlds population shares the Muslim worlds antipathy towards a unilateralist American foreign policy. While many people in Asia and Europe felt deep sympathy with the US after 9/11, this has dissipated among the daily images of devastation, carnage, torture following the American-led invasion of Iraq.

 The status of the worlds sole superpower is not one the US may enjoy for ever. Militarily it looks unassailable, but its economic preeminence is being increasingly challenged by China. And the soft power it once enjoyed has been rapidly eroded by its recent actions. The future of the globe, and of globalization itself, depends hugely on how the Americans adjust to their declining status in the world. Will they respond in the same benign way as that earlier imperial power, Great Britain? 

 The omens are not propitious. For the belief that America was, is, and always will be the greatest nation on earth is widely held across the political spectrum, shared by Republicans and Democrats, neo-conservatives as well as liberals. With this belief in their greatness comes the belief in their rightness. In any dispute with the US it is always the other country that is wrong. That Americans could ever be mistaken or be in error is inconceivable. Consider the shocking response of Democrats like Hillary Clinton to the mess in Iraq. Having destroyed that country (Clinton actually voted in favour of the invasion) they now claim that the Iraqis are incompetent to run it. This is a classic case of blaming the victims. 

 As a regular visitor to Britain, I am struck by the powerful absence of nostalgia there. Most British people do not mourn the loss of empire. Whether Americans will take so easily to not being number one is another question. In fact, some analysts argue that the real reason for the invasion of Iraq was to preempt Chinese access to the oil resources of the Middle East. However, to the case of Iraq, we must counterpose the case of Darfur, where the displacement of several million people and the killing of more than half-a-million has not been adequately addressed by the world community largely because of Chinese interests in Sudanese minerals. 

 My sense is that in the years to come, the rivalry between China and the US will promote discord and violence across the globe. The competing ambitions of nation-states are thus one threat to harmonious and successful globalization. The spread of mass consumerism is another. In a lyrical passage, Nayan Chanda describes the individual components of an iPod he bought for his son. While the machine was designed by that quintessential American company, Apple, the microdrive that was the heart of the machine was made by Hitachi in Japan, the controller chip was made in South Korea, the Sony battery was assembled in China, the stereo digital-to-analog converter was made by a company in Edinburgh, Scotland, the flash memory chip came from Japan, and the software on a chip that allows one to search for and play ten thousand songs was designed by programmers at PortalPlayer in India.

 The iPod is a small device which uses little energy. Its use may indeed be successfully globalized. What however of that other object deeply desired by young men and women around the world, the automobile? To approach the norm set by the US where two citizens in three own a car 700 million Chinese and 600 million Indians will have to drive around in their own, enclosed, private vehicles. And where will the steel and aluminium to make them come from? And the oil to power them? And the roads to drive them on? The mind boggles at the number of square miles of earth that shall have to be devoted exclusively to mining. Or at the number of oil wells that would be required to move the minerals to the smelting plants to the car factories and eventually to the showrooms. Or at the number of peasants who will be displaced to build the highways these cars make necessary. Or, finally, at the number of tonnes of greenhouse gases that this mining and manufacturing and driving will cumulatively release into the atmosphere.

 The evidence presented by Nayan Chanda and other writers demonstrates that globalization has, so far, and on the whole, increased prosperity and lessened misery within nations. Critics argue that some groups, such as tribal people living on land under which lie rich veins of iron ore, have suffered disproportionately as a consequence of global capitalism. But it is time we more seriously considered that other caveat, so far. In the long run, it is not American unilateralism but the greed of all humans that will put in peril the prospects for peace and happiness on earth.

 ramguhavsnl.com




</TEXT>
</DOC>