<DOC>
<DOCNO>1070411_sports_story_7633396.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Vaughan laid low by captaincy: Fletcher

 - He just puts pressure on himself knowing that, as the leader, hes got to go and get runs 

DEREK PRINGLE

Sundays World Cup defeat to Australia brought up a landmark for Michael Vaughan, who has now led England more times in one-day Internationals than anyone else. So far, the record does not appear to be connected to his form with the bat, which, if modest for most of his career, has become poor enough that most teams would consider dropping him. 

Not England. With the same confidence in statistical probability that says a monkey left with a keyboard will one day produce Twelfth Night, Englands selectors and coach believe he will come good. 

Until that moment arrives, Duncan Fletchers defence of Vaughan seems to revolve around how leadership is affecting his batting, a curious logic given that he has just broken Nasser Hussains record of 56 matches as one-day captain. 

Hed probably get a lot more runs if he wasnt captain, Fletcher admitted on Monday before the team flew to Barbados. 

Hes probably just putting himself under pressure, with a few bad knocks and the captaincy getting to him. 

Having chatted to him, he looks very confident. Hes still got full belief in his captaincy. He just puts pressure on himself knowing that, as captain, hes got to go and get runs out there. 

Leading an international cricket team is tough, and being captain of England even tougher. But with a one-day average of 26.5 and a strike rate of 67.9, Vaughans poor form in the shorter game has been overlooked in return for his leadership skills. To suggest they are now becoming a burden to his batting surely changes the ticket upon which he was selected. 

Captains play as allrounders and its important that we have all-rounders in one-day cricket, Fletcher said. He captains for us, which is a huge job on its own, especially for England. And hes a very good batsman. To think of dropping him is like saying, Do we drop Andrew Flintoff? Hes not making runs at the moment but hes bowling outstandingly well for us. Thats how I look at it. 

England have faced three Test-playing countries in this World Cup and lost to each of them. The most positive spin is that the three New Zealand, Sri Lanka and Australia are probably the three best sides in the conditions. But it is hard to massage Vaughan or Flintoffs batting returns against these three, which over three knocks read 31 and six, respectively. 

If you look at the way Vaughan and Flintoff approach an innings, a pattern emerges. Both men look to hit a cricket ball with the full face of the bat, without much give in the wrist. It is a technique that serves them well in the longer game, but is almost incompatible with the shorter one, especially on such slow pitches. 

Opponents just set a straight field with mid-on and mid-off a few yards deeper than normal, bowl a normal line and length, and let the pressure build until something gives. 

Interestingly, Australia captain Ricky Ponting suffered the same shortcomings as a one-day batsman, but has overcome them by running well between the wickets and learning to sheath his big cut and replace it with a defter one. As Englands batsmen did with Brad Hodge, Ponting picked out Englands slowest fielders and ran whenever that ball was hit to them. 

Mindful of his shortcomings on the sluggish pitches, and the reduction in boundaries that has brought, Flintoff is working on picking up more ones and twos. 

With England not taking advantage of the opening powerplay when the ball is hard, there have been suggestions that he should open the innings and his shoulders, but Fletcher appeared to scotch that suggestion. 

There isnt a game goes by when we dont think of changing things, but we need to look at the ramifications of that, Fletcher said. You can change one player and he slots in, you change another, and four players have to shift position in the batting order. Is one man worth changing the whole balance of the side for? 

England have three Super Eight matches left. Their next game, against Bangladesh in Bridgetown Wednesday, is a contest made tougher by Bangladeshs burgeoning confidence following their win over South Africa. A fresh pitch at a fresh venue should suit England bowling attack, but there is mounting pressure on all concerned not least the teams embattled coach. 

Ive been under pressure since we were four for two in Johannesburg back in 1999, Fletcher said, recalling the nightmare start to his tenure as England coach. And Ive been aware of it ever since. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>