<DOC>
<DOCNO>1070602_opinion_story_7862777.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 NORTH AND SOUTH

 Memory lanes evoke misty, winding ways, smelling of early autumn mornings, with a slight chill in the air. But the chief minister took the straight road while reminiscing about his past. When inaugurating a new sports complex in north Calcutta recently, he talked of the need to develop the northern part of the city to match its development in the south. There is, of course, a civic authority to look at these things, an authority which should not be relying on the chief ministers bursts of nostalgia to do its job. But perhaps its members, like all Calcuttans, are addicted to binaries. North versus the south, tradition versus modernity and so on till that particular lemon has been squeezed dry and work postponed one more time. 

 Only the issue is hardly one of preserving tradition. Planners and promoters do not even catch their breaths before demolishing the most beautiful old buildings in order to put up office blocks and malls. To them, restoration, conservation and transformation are unreadable collections of letters. Thus, for them, tradition and aesthetics have nothing to do with lucrative modernization projects. The problem with north Calcutta is that cleaning it up is too much work, with little observable profit at the end of it. Taxpayers have always paid their taxes, what does that have to do with improvement? Old, sprawling and unplanned, the northern part of the city with its vast markets, amazing houses, its tiny shops with their incredible wares, its imposing places of worship, its river and canals, and its teeming thousands, could have been the citys unique selling point if a team of experts, engineers and planners had put in a bit of thought. 

 The myopia behind the neglect of the north is staggering. Cleaning up and repairing the roads, de-silting the waterways, beautifying water bodies, restoring heritage areas while keeping them functional or even changing their uses, redoing the sewerage all this would make life for the inhabitants easier. This would also ease traffic congestion, reduce water-logging and ease movement between the northern and southern parts of the city, thus giving a keener edge to trade. Modernization need not necessarily be identified with blind towers of granite, steel and black glass or with concrete cobwebs of flyovers. Neither is retaining the unique character of a place a sin. Making that unique character saleable, a generator of revenue, means the bringing of technology and modern amenities that would automatically ease the lives of the people who live in the area. Just the re-laying of tram tracks has made a difference, as the coming of the metro had. The work has now piled up. There is no dearth of people with vision. What is lacking is thought, good sense, foresight and will on the part of the administration. And pressure on the part of the residents.




</TEXT>
</DOC>