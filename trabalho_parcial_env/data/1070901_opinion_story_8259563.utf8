<DOC>
<DOCNO>1070901_opinion_story_8259563.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 NUKED BY BAD TASTE

 - The nuclear deal has shown up how low political debate has sunk

 Politics and Play - Ramachandra Guha

 ramguhavsnl.com

 Without bad taste

 In recent years, there has been a sharp decline in standards of political debate in India. In and out of the parliament, issues concerning the public good are rarely discussed logically or dispassionately. The arguments more often reflect ideological prejudice or personal hostility rather than rational thought. The degradation has been palpable for some time now; but it is in the debate on Indias nuclear deal with the United States of America that it has reached its very nadir.

 I claim no expertise in energy policy or the law. I do not pretend to have grasped the nuances of the 123 agreement. Although, as a citizen, I remain worried that nuclear reactors are (to paraphrase the American Green Ralph Nader) unsafe at any price, I can understand why, with the growing threat to human life posed by global warming, they might find their promoters. 

 This column, then, is not about the rights and wrongs of the nuclear deal. It is about the ways in which it has been discussed and debated. In this (as yet unconcluded) debate, three varieties of indecorous behaviour have been put on display. Let us call them hypocrisy, bad taste, and rank careerism.

 In their statements on and arguments about the Indo-US nuclear agreement, all the major political formations have been hypocritical. The Bharatiya Janata Party now warns of the danger that we will become too subservient to the US. This is odd; for the Hindu right has in the past consistently stood for closer ties with Uncle Sam. After China invaded Tibet in 1950, it argued that Indias salvation lay in an open alliance with the US. The Jana Sangh, forerunner to todays BJP, opposed Jawaharlal Nehrus policy of non-alignment on the ground that America was infinitely preferable to the Soviet Union. As foreign minister between 1977 and 1979, Atal Bihari Vajpayee worked hard to dispel the image in Washington that we were clients of the Soviets. As foreign minister between 1998 and 2002, Jaswant Singh acted as if India fully intended to become a client of the US. He equated himself with a mere deputy secretary of State, and even offered to send Indian troops to help in the illegal invasion of Iran. 

 Given this history, with what face now can the BJP warn of the dangers of American expansionism?

 At least with regard to the US the communists have been more consistent. A visceral anti-Americanism has been their stock-in-trade since the beginnings of the Cold War. But their newfound concern for our national sovereignty is more hard to swallow. In 1942, they put the interests of the Russian people above that of Mahatma Gandhis national movement. In 1962, many of them supported China when it invaded India. We can be certain, even now, that if India had been buying reactors from China or Russia this would have met with their enthusiastic support, even if those deals came, on the Chinese or Russian side, with all kinds of strings attached.

 Finally, the Congresss rather open cultivation of the US is somewhat at odds with its previous, and mostly principled, equidistance from the worlds Great Powers. 

 Likewise, the bad taste has also emanated from both the ruling party and the Opposition. George Fernandess calling the prime minister a serial bluffer and suggesting that in another country he would have been shot has embarrassed even his most loyal friends. J. Jayalalithaas insinuation that the prime minister was so much in favour of the deal because his daughter and son-in-law were American citizens was cheaper still. (One can find fault with Manmohan Singh on many counts, but never on the counts of his integrity and patriotism. In fact, it is more likely that the hysterical anti-Americanism of some Opposition leaders is not unconnected to the fact that their own children are American residents or citizens.) Still, the Oppositions excesses in this regard were equalled or perhaps even exceeded by the comments made about them by the serving Indian ambassador to the US, Ronen Sen. To call opponents of the deal headless chicken[s] (later denied by him) was petty and vain in equal measure. And Sens fawning praise of George Bush might have made even Jaswant Singh squirm in embarrassment.

 In the matter of bad taste, it is the communists who come out least badly. Their arguments and claims about the nuclear deal might have been ill-considered, but they were not, so far as I can tell, vulgar.

 I come, finally, to the rank careerism displayed in connection with this most contentious deal. On August 12, a Delhi newspaper published a leading article with the title Trust the Treaty. The caption gave away the line of argument. But what was significant was not what was being said but who was saying it. The article was written by S.K. Singh, at the time the governor of the Indian state of Arunachal Pradesh. Seeing his byline, I rang a friend in Delhi. Surely it is unconstitutional for a serving governor to go out and bat for the government like this? I asked. Treat the article not as a violation of constitutional propriety but as a job application, answered my friend. He wants a bigger state.

 Oddly enough, with the media and the political parties so obsessively focussed on the rift between the Congress and the Left, no one else seems to have noticed S.K. Singhs transgression. I have seen no comment on it in the press. By contrast, every word uttered by Prakash Karat has been minutely examined. The comments made by George Fernandes and Jayalalithaa have been reported and deplored. The remarks, on or off the record, of the ambassador, Sen, have been debated ad nauseam. One newspaper has called for his immediate recall; another has run a long article defending him.

 As a leader of a major political party, Karat is fully entitled to express his views on the nuclear deal, in private or in public. So are Fernandes and Jayalalithaa, although one would wish they used more decorous language. Sen was certainly foolish in saying what he did. But he said what he did on the spur of the moment. In contrast, his former foreign service colleague and fellow political appointee, S.K. Singh, devoted a good deal of thought to the piece he published in the Hindustan Times. I have no doubt that he believed in what he said, and no doubt either that he knew that, given the position he held, he should not be saying it in public. He might have calculated that, in the present climate of unconcern for norms and propriety, anything goes.

 He was right. It was not just that his transgression was not noticed; it was actually rewarded. In the reshuffle of governors announced last week, S.K. Singh was shifted from Arunachal Pradesh to Rajasthan a more populous state, a much more important one, and closer (in all respects) to Delhi. 

 On the whole, and from all sides, the debate around the nuclear deal has been largely lacking in decorum. Its spirit is perhaps best captured in the Bengali word abhadra. Even in this crowded field, it should not be too hard to choose the most abhadra of the acts on display namely, S.K. Singhs job application. That it has been so quickly acted upon, and in his favour, is merely a marker of the times we live in.




</TEXT>
</DOC>