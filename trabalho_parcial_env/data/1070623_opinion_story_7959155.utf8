<DOC>
<DOCNO>1070623_opinion_story_7959155.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 REFLECTIONS ON A VIOLENT PAST AND PRESENT

 Uddalak Mukherjee

 Photography

 The past is often linked to the present through violence. And such violence, on some occasions, is engineered by the State. The Times: Then and Now (Samay Takhan, Samay Ekhan), a photographic exhibition at Boi Chitra that ended on June 20, exposed this linkage by bringing together a collection of photographs of the historic Tebhaga movement and the recent carnage in Nandigram. The exhibition was divided into two sections. The first included the Tebhaga pictures, in black-and-white, that were shot by the late Ananda Pal, staff photographer of Swadhinata, the mouthpiece of the undivided Communist Party. The second comprised a selection of photographs of the violence in Nandigram that took place in March.

 Though separated by time and space, the story in these pictures, curiously, has remained unchanged. Pals lens, in particular, unveils the brutal nature of State power, as well as the courage of ordinary people poor peasants, women, even children who chose to rise and fight for their rights. The ominous silence in some of Pals photographs has a denseness that could be felt by touch. In the other pictures, Pals depiction of the bloodshed is more subtle. Take for instance, the long shot of gun-toting policemen moving in a single file to a village. There is a pond that lies next to their path, with two ducks floating on its calm waters. Here, Pal has cleverly used the supposed tranquillity as a ruse to allude to the gathering darkness. Another remarkable image (picture) is that of a group of villagers, including women. Some of the women have their heads covered with the end of their saris. The men and women are carrying some sort of weapon sticks, sickles, even a broom. They have angry eyes, but their brave faces are strangely still in the face of the onslaught.

 The coloured photographs of the slaughter in Nandigram include that of a corpse tied to poles, a half-covered face wielding a matchet, a bleeding, injured child and so on. Unfortunately, they lack the sensitivity and the depth of Pals work. The pictures are grisly, and indulge in a kind of sensationalism that is typical of photojournalism today. 




</TEXT>
</DOC>