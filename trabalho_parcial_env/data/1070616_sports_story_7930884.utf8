<DOC>
<DOCNO>1070616_sports_story_7930884.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Beckham revels in dramatic life 

 - Whenever criticism is thrown at me, I find a way to come back stronger 

GEORGE CAULKIN IN MADRID 

A Spanish television station is billing its football coverage with the tagline Vive La Liga Loca, which David Beckham must feel is underselling it. His final, traumatic season at Real Madrid has coincided with his most difficult time in public prominence, but after receiving assurances that his England place is once more secure, a title shimmers in front of him. Crazy does not do it justice. 

Banished by Steve McClaren, ignored by Fabio Capello and then ostracised by his club when stalled contract negotiations led to him agreeing to uproot his family to California, Beckham responded as he always has. Whenever criticism gets thrown at me, I find a way to come back stronger, he said on Thursday; informed in January that he would never play for los meringues again, he has been integral to their renaissance. 

The same applies to England unbeaten in the two matches since his return and providing his standards do not waver, there is no longer any reason why his move to the Los Angeles Galaxy should entail his international ambitions dissolving in a Hollywood sunset. 

He is determined to play on. Without a doubt, he said. The manager has already made me aware that as long as my fitness is right and Im playing like I have done over the last few months I can stay in the squad and in the team. I think my fitness will be the same, if not better. 

The 32-year-old must have permitted himself a wry chuckle this week when RamCalderspoke fancifully of exploiting a get-out clause in his $250 million (about 127 million) deal with the Galaxy. After all, it was not too long ago that the Real president dismissed Beckham as an average cinema actor. There will be no volte-face. 

The contract was signed a few months back, there is no clause in the contract saying it can be changed, so it will be my last game on Sunday, Beckham said. I have played here for four years, Ive had an incredible time and I hope it will have the best possible ending in a championship, but this is it. 

Barcelona and Seville may have a say in that, though a Real victory in their home game against Mallorca will confirm them as champions regardless of their rivals results. 

His fitness has been suspect since suffering a recurrence of his ankle injury in the 2-2 draw away to Real Zaragoza last Saturday and Beckham on Thursday jogged gingerly alongside Jos Luis San Mart, Madrids recovery expert, at their training ground in Valdebebas. It would take more to threaten his participation against Mallorca, which also represents Roberto Carloss swansong. 

Im sad to be leaving and I didnt expect it hindsight is a strange thing and I thought I would finish my career here but everybody knows what happened, Beckham said. The 1998 World Cup was tough (after his dismissal against Argentina), but this has been the most difficult time in my career. I wasnt involved in training and matches and things were said about me that werent true. 

He leaves with most Madrid fans wanting more. Some people think Im going into semi-retirement, others that I can keep playing at the top, Beckham said. But its important to go to America and be an ambassador there when I can still perform. In some ways its painful, because Ive had 15 years playing top-flight football for Manchester United and Madrid and the level of football is different in America. 

THE TIMES, LONDON 




</TEXT>
</DOC>