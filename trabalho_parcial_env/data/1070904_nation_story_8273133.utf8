<DOC>
<DOCNO>1070904_nation_story_8273133.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Cocktail defence against diabetes

 OUR SPECIAL CORRESPONDENT

 Actor Farooq Shaikh at an anti-diabetes rally. File picture

 New Delhi, Sept. 3: Patients with diabetes who take a two-drug cocktail of blood pressure-lowering drugs are less likely to die from complications of the disease than those who dont receive this treatment, a new study has shown.

 The international study involving 11,140 diabetes patients, including nearly 500 in India, has shown that a fixed-dose combination of perindopril and indapamide reduces the risk of heart and kidney ailments.

 These findings show how a simple and relatively inexpensive additional treatment in diabetes could save thousands of lives each year, said Nikhil Tandon, professor of endocrinology at the All India Institute of Medical Sciences and one of the investigators.

 A fixed-dose combination of these two blood pressure-lowering drugs costs less than Rs 12 a day, he said. 

 The patients who received the drugs either had a previous history of heart trouble, or have been suffering from diabetes for at least 10 years, or had high blood pressure. But the benefits were also seen in patients without a blood pressure problem. 

 The study, presented at the European Congress of Cardiology in Vienna on Sunday by Tandons international collaborators, has shown that the two-drug combination reduces the risk of death from heart disease by 18 per cent and the risk of death from any cause by 14 per cent. The risk of new or worsening kidney disease was reduced by 21 per cent. 

 This translates into 30,000 lives saved in India each year with a conservative estimate of 30 million patients with diabetes in India and half of them receiving this treatment, Tandon told The Telegraph. 

 Medical surveys over the past five years have indicated that the number of patients with diabetes in India is between 30 million and 40 million and is growing. Heart disease and kidney failure are the key causes of deaths from diabetes in India. 

 This treatment reduced the likelihood of dying from complications of diabetes with virtually no side-effects, said Stephen MacMahon, a professor at The George Institute for International Health and a key investigator in the study, which was conducted with support from a manufacturer of perindopril and indapamide and Australias National Health and Medical Research Council. 

 The benefit from the two drugs stems from their blood pressure-lowering effect as well as additional effects, Tandon said. They may reduce the load on the heart and pressure in vessels of the kidneys.




</TEXT>
</DOC>