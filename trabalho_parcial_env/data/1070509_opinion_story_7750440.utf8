<DOC>
<DOCNO>1070509_opinion_story_7750440.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 TAKE REAL CARE

 To govern a country as populous and as desperately poor as Bangladesh is no easy job. The task becomes almost impossible if its politicians are bent on making it ungovernable. If the army-backed caretaker government in Dhaka has so far received popular support for many of its moves, it is because it has restored a semblance of governance in the country. Sheikh Hasina Wajeds return home poses a new challenge to the interim administration. True, domestic and international pressure forced the government to retract its earlier decision not to allow her to come back to Dhaka. But it would be unfortunate if Bangladesh returns to destructive politics and anarchy. Both Ms Wajed and her rival, Begum Khaleda Zia, have to come to terms with the new reality in the country. They have to realize that the people do not want the politics of street battles and shutdowns to haunt their lives any more. Of course, Ms Zia, who ruled the country for the past five years, has to take the larger blame for the collapse of governance in the country. The caretaker government has been trying to rescue it from the brink of disaster. The more worrying question for Bangladeshis now is not what Ms Wajed will do but whether Fakruddin Ahmeds government can carry on with its clean-up operations. 

 The tasks that the government has set itself are far more important to Bangladeshs society and politics than to the fortunes of the political leaders. The electoral reforms that are being put in place are only a part of the larger political reforms that the country needs in order to become a functional democracy. Bangladeshs experiences with electoral democracy have been no better than its encounters with martial law administrations. The fault lay not with any democratic institution but with the politicians who abused them. Corruption may not be unique to Bangladeshi politicians. But the arrests of corrupt politicians and the exposure of the stories of their ill-gotten wealth tell their own tale. They show why the country has topped the Transparency Internationals chart of most corrupt countries year and after year. Also, they offer a sad commentary on why the country remains so hopelessly poor. It would be a sadder story if the interim government leaves its job half-done.




</TEXT>
</DOC>