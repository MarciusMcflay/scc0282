<DOC>
<DOCNO>1070326_opinion_story_7538842.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 POLITICS BY COMIC BOOKS

 Gwynne Dyer

 Being cultural advisor to Irans president, Mahmoud Ahmadinejad, must be one of the more thankless jobs on the planet, but Javad Shamghadri manages to keep busy. His latest foray is into the cultural space occupied by the teenage bloodlust demographic. 

 What bothers Shamghadri is the new Hollywood hit 300, an animated comic book of a film that shows impossibly buffed and noble Greeks seeing off an attempt by evil Persians to strangle Western civilization in its cradle, 2,487 years ago. They think its psychological warfare against present-day Iranians, thinly disguised as a story about their wicked Persian ancestors. 

 Shamghadri is so clueless about the workings of Hollywood that you really want to take him gently by the hand and walk him through it. Following the Islamic Revolution in Iran (in 1979), he says, Hollywood and cultural authorities in the US initiated studies to figure out how to attack Iranian culture. Certainly, the recent movie is a product of such studies. After pausing for a moment to savour the notion of cultural authorities in the US, let us pass on to the Tehran paper Ayandeh-No, which is quite close to the regime. Under a headline screaming Hollywood declares war on Iranians, it complains that The film depicts Iranians as demons, without culture, feeling or humanity, who think of nothing except attacking other nations and killing people. It is a new effort to slander the Iranian people and civilization before world public opinion at a time of increasing American threats against Iran.

 The underdogs

 So can we all just laugh at those stupid, paranoid Iranians for getting their knickers into a twist about a dumb, harmless splatter-film cleverly disguised as art? Im afraid not. It really is war propaganda of the crudest, nastiest kind, even though the people who made the movie have probably never had a consciously political thought in their money-grubbing lives.

 The film-makers had a great story to work with: the battle of Thermopylae in 480 BC really did save Greece from conquest by Persia. They had the extraordinary images from Frank Millers comic-book retelling of the story. And they knew that Iran is next on the US hit-list.

 For several decades now, the bad guys in American action films with an international setting have mostly been Middle Easterners. Iranians actually do live in the Middle East, so lay it on with a trowel. And as for the stiff, super-patriotic, over-the-top macho dialogue, most of it comes straight from Millers comic book, and he presumably just picked it up from the general culture in the US, which has been deeply infected by that sort of thing for the past few years. So no plot, nobody to blame, and yet the film is everything the Iranians say it is. The Persians are depicted as ugly, dumb, murderous savages who want to conquer the free people of the world, while the Spartans are clearly Americans, spouting the same slogans about liberty that are sprinkled on all political discourse in the US. 

 Whats more, the Spartans are underdogs. In almost all US-made action films with an international setting, the American heroes are underdogs fighting against enormous odds, even though they actually come from the most powerful country in the history of the world. However, you know that they are in the right, because in the movies the underdogs are always in the right, and they always win in the end.

 So the gallant Greek-Americans triumph over the evil Persians, and let that be a lesson to evil-doers everywhere. But our Iranian friends should not worry that this film is juicing American youth up for an invasion of their country, because the kids just wont get it. Down in the teenage bloodlust demographic, practically nobody knows that the Persians of ancient times and the Iranians of today are the same people.




</TEXT>
</DOC>