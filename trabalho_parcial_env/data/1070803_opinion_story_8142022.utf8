<DOC>
<DOCNO>1070803_opinion_story_8142022.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SKATING ON THIN ICE

 - Foreign treaties and the Indian Constitution

 CUTTING CORNERS - 

ASHOK MITRA

 Why not admit it, it is all over, but for some shouting in the two houses of parliament. Once the Union cabinet approved the text of the so-called 123 Agreement as finally negotiated by this countrys national security adviser with his counterpart in Washington DC, protesters over here might as well accept the reality. For even if the final version of the nuclear deal does not assuage the doubts entertained by large sections in the country, including the Left, fait accompli is fait accompli. Under our Constitution, as currently interpreted, no scope exists for parliament to sit on judgement on a treaty signed by the government of India with a foreign regime. The Bharatiya Janata Party the original sinner according to many, responsible for the nuclear sell-out could, of course, move a vote of no-confidence against the government. The Left is, however, committed to vote with the United Progressive Alliance; the government would survive; so would the clauses and conditionalities agreed upon by representatives of the two governments, including perhaps the Hyde Act provision that the United State of America expects India to be its active ally when it decides to launch its military expedition against Iran.

 New Delhi is nonetheless skating on very thin ice. The claim posted by our national security adviser that the 123 Agreement has been so drafted that even if India were to resume nuclear tests, it would not need to return to the US the technology and fuel lent to it has been immediately contradicted by the US under-secretary of state overseeing the negotiations. Clearly, there is some dissembling going on in some quarters. In any event, in terms of the American Constitution, a foreign treaty is not validated till as long as it does not receive the imprimatur of approval of the two wings of the Congress. 

 The US Congress might in its wisdom decide that the commitments on the part of the government of India with respect to the so-called safeguard provisions and return of technology and fissionable material, should India resume nuclear tests, do not go far enough. It could then choose to refer the matter back to the administration. The agreement, reached under a cloak of such fearsome secrecy, would consequently have to be reckoned as still-born.

 The nature of the anomaly is most disturbing. The national legislature of the US has the right to decide on the contents of a treaty that the countrys government signs with India. The Indian parliament, however, has no corresponding right; the only prerogative it has is to vote out a government which signs a treaty not to its liking. The issue for debate would be not the merits and demerits of the treaty signed, but the degree of confidence parliament reposes on the government.

 In the view of a significant number of scientists, jurists and experts in international relations, the Indo-US nuclear treaty, as it stands after the final round of negotiations, diminishes the nations sovereignty, and thereby adversely affects the preamble itself of the Constitution, which proclaims India to be a sovereign, socialist, secular democracy. Given the other provisions of the Constitution, parliament is helpless to defend the countrys sovereignty by vetoing the treaty. The US Congress can dictate to that countrys administration in the matter of signing treaties with foreign parties; our parliament cannot.

 But this is only the latest instance where the Union governments unfettered prerogative to sign foreign treaties gives rise to contentious issues. In the not very distant past, New Delhi signed the Marrakesh treaty, which heralded this countrys entry into the World Trade Organisation, or, as some others put it, the WTOs right of intrusion into our domestic affairs. The conditions associated with membership of the WTO entail acceptance of conditions which make this countrys agriculture subject to regulations enforced by this international agency. Agriculture and land relations are a State subject according to our Constitution. A query can be raised whether the Union government has the right to sign a treaty with an external body that impinges on an area which, under the Constitution, does not belong to the Union list, but to the State list. Nobody has, till now, taken the trouble to take the matter before the nations highest judiciary and seek its verdict on it.

 An analogous issue concerns the ambit of the WTOs precisions regarding Intellectual Property Rights. For example, Indian pharmaceutical companies complain bitterly that they are being inhibited from manufacturing and distributing cheap drugs for HIV+ patients because of patentary restrictions enforced by American drug companies quoting WTO directives. Should the WTOs rules override our Constitution? Article 38 of the Constitution, while setting forth the directive principles of state policy, states as follows: The State shall strive to promote the welfare of the people by securing and protecting effectively as it may a social order in which justice, social, economic and political, shall inform all the institutions of the national life. Cannot some citizens avail themselves of this article and demand of New Delhi that, for the sake of securing the welfare of the people, it should allow Indian firms to defy the WTO fiat and produce drugs that are within the means of HIV+ patients? What about seeking a ruling from the Supreme Court of India that any treaty that infringes a directive of state policy is beyond the competence of the Union government to sign?

 There are two ways for the Supreme Court of India to examine the juridical aspects involved in the government of Indias entering into treaties with foreign parties that seemingly cut athwart the rights and prerogatives of state governments as well as of the general public: either PILs are allowed to be placed before it for its consideration, or the court takes up some or all of these issues on its own.

 No precedent reportedly exists of the Supreme Court choosing, at its own initiative, to review such grey areas in the nations Constitution. Nor have any of the state governments bestirred themselves to formally challenge the Union governments right to sign treaties with foreign parties, including agencies of the United Nations, on subjects which encroach on the jurisdiction of the states.

 In the matter of the Indo-US nuclear agreement, though, a private citizen filed, on June 2 last, a PIL challenging the governments prerogative to sign such a treaty without prior approval of the nations parliament. The Supreme Court has obviously found prima facie merit in the plaint and has admitted it for consideration. Unfortunately, it has posted the case for hearing only on October 8 next. In case the government of India meanwhile wins a vote of confidence in parliament on the issue, whatever the nations highest judiciary ordains would conceivably turn out to be an academic exercise; it is most improbable that, whatever the verdict of the Supreme Court, a retroactive clause would be attached to it.

 The situation could still be saved if either of the two alternatives is entertained. First, a prayer could be posted with the Supreme Court to advance the date of hearing of the case. Or the court might be appealed to to kindly issue a directive to the Union government that a decision on the draft nuclear agreement be stayed till as long as the court has not disposed of the matter.

 Is all this crying in the wilderness? For, like other institutions, the nations highest judiciary too has its own sense of priorities.




</TEXT>
</DOC>