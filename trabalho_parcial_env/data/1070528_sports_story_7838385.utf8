<DOC>
<DOCNO>1070528_sports_story_7838385.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Amelie: Big W is everything

CLIVE WHITE 

Choking. Among the sporting vernacular, its one of the more distasteful words. For Amelie Mauresmo its such a personally distressing one she cannot even bring herself to repeat it in French. Historically, it has stood between her and the French Open title on numerous occasions, even though she has never made it to a semi-final at Roland Garros let alone a final. 

Once a sportsman or woman wins a major title, theyre supposed to be no longer troubled by this terrible affliction; thereafter when they lose from a leading position they just, well, lose and last year in Paris after Mauresmo had won the Australian Open she certainly seemed to be breathing more easily. 

Since then, of course, she has also won Wimbledon so presumably she should now be able to wolf down a five-course Parisian meal without so much as a glass of water beside her. 

She is a complex character but one of the most charming in the sport, who is finally winning the trophies her tennis deserves. 

Last year was the first time in 12 attempts that the 27-year-old had actually enjoyed her own championships and for her sake hopefully it will be the same this year. 

Unfortunately, as usual, her fellow countrymen have been cranking up the pressure for a few weeks now on our Amelie. She said she cant walk down a street in Paris without someone calling out, Come on, Roland Garros is yours this year, are you going to get it? I would be so happy. 

A couple of months ago she had another nasty feeling in the pit of her stomach, but that turned out to be appendicitis. It means she arrives at Roland Garros short of match play and unsure of exactly where her game is. 

Even so, the world No. 5 shouldnt be too weighed down by her own expectations. If it were to be 13th time lucky it would make her career complete and perhaps she can take succour from the fact that Britains Virginia Wade won Wimbledon at the 16th attempt only after securing the Australian and US Open titles. Mauresmo had always thought that if she were to win a Slam, Wimbledon would be the first, even though the French, obviously, has been her long-time goal. She would happily swap her Wimbledon title for it, wouldnt she? 

Not sure, she said. If my career were to end today I would not exchange my Wimbledon title for the French definitely. Theres something about Wimbledon and it hurts me a little bit to say it thats different. There is a history, there is an atmosphere, there is something there that is different to anywhere else. 

 THE SUNDAY TELEGRAPH 




</TEXT>
</DOC>