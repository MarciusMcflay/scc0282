<DOC>
<DOCNO>1070311_opinion_story_7499158.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 WOOL SACK, ERMINE ROBES

 There is something strangely anomalous with the idea of elections in the House of Lords. It may well be argued that there is something equally anomalous with the very idea of a House of Lords in a democracy. The origins of the House of Lords go back to feudal times, when the barons of England and the monarch devised a system to balance baronial power and the sway of royalty. Today, it is an institution that recognizes the vestiges of hereditary and aristocratic privileges in a society that is governed by the rules of the market and of merit. The hereditary aspect of the House of Lords began to be diluted with the introduction of the Life Peerage Act of 1958. The hereditary element was further diluted in 1999, when 90 per cent of the hereditary peers were removed. The hereditary peers constitute only 12 per cent of the House of Lords as it stands today. This radically altered the character of the House of Lords and also introduced in a big way the politics of patronage and corruption in the manner in which people were elevated to a peerage. It is not uncommon for those making huge contributions to the Labour Party to find themselves in the privileged chamber presided over by the Lord Chancellor sitting on a wool sack. The vote in the House of Commons, earlier this week, was in favour of making the upper house a wholly or a largely elected chamber. One intention of this reform, which can be stalled by the lords for one year, is to stop the entry of people who pay to become lords. It is significant that as many as 224 members of the House of Commons voted against the reform while 337 voted in favour. The introduction of elections in the House of Lords obviously does not have overwhelming support among the members of parliament.

 It is easy to argue that the House of Lords is a useless body. Most British politicians would tend to agree with this. Winston Churchill, who was proud of his aristocratic and ducal provenance, steadfastly refused to accept peerage because he believed that it would mean the end of his political power and influence. Anthony Wedgwood Benn, at the opposite end of the political spectrum from Churchill, chose not to be known as Lord Stansgate, which would have been his hereditary title. Both Quentin Hogg and Alec Douglas Home gave up their hereditary titles when they were active in politics. They became Lord Hailsham and Lord Home respectively as life peers. Yet most observers agree that the standard of debate in the House of Lords is higher than the proceedings of the elected house.

 There is, of course, an intellectual and a constitutional case for an upper chamber, which can act as a potential check on the excesses of the lower house. A chamber based on aristocratic privilege may not perhaps be best placed to perform such a function. The introduction of elections will destroy the traditions of the House of Lords, whose members owe allegiance only to the queen. The raucousness of modern politics will further diminish the magic of her majesty and her lords.




</TEXT>
</DOC>