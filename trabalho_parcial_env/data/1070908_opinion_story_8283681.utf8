<DOC>
<DOCNO>1070908_opinion_story_8283681.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 THEATRE OF THE GUTS

 Aveek Sen

 Visual Arts

 After her 1923 show, a critic Modernist and male had described Georgia OKeeffe as rendering in her picture of things her bodys subconscious knowledge of itself. Though immediately accepted into the New York avant garde, the painter soon began to tire of her work being talked about solely in terms of the body and of the subconscious. She was being seen as a star, but only as the best among her female contemporaries, and as never quite managing to go beyond the confines of her body and her subconscious in the paintings. It did not take OKeeffe long, after this, to threaten to quit painting altogether if Freudian interpretations continued to be the only way of looking at her work. 

 The complicated and dramatically self-exposing corporeality of Jaya Gangulys paintings (CIMA Gallery, until September 15) also risks provoking such reductive readings. The catalogue essay, for instance, never lets her art escape the stranglehold of biology and biography: she was born as a girl-child into an orthodox Brahmin family. Though anxious to distinguish her rebelliousness from that of a woman-libber (sic), it presents Ganguly as displaying her femininity as a natural consequence. Such a view will find in her work very little apart from obvious auto-projection, over-stressed emotions and metaphors of procreation. But the grave, wise, yet not unfunny eyes that look out of her paintings seem to be staring doggedly back at precisely such a simplifying gaze. The viewer who expects to see a Woman Artist endlessly rehearsing her self-absorption is confronted, instead, with a grotesque theatricality that transforms its lugubrious preoccupation with the labyrinths of flesh into something inward, strange and disconcertingly impersonal. 

 This impersonality allows Ganguly to be not only a woman fascinated with the drama of human innards, a theatre of the face, limbs, guts and organs, depicted in all their bizarre minutiae, but also an artist playing allusively with her own creative inheritance, Indian as well as Western. Everywhere in this exhibition and particularly in the later works one notices the presence of other artists who have opened up or reconstituted the human body according to their own sense of how the mind lives in it. The dominant presences are Picasso, Jogen Chowdhury, Rabindranath and perhaps Francis Bacon, and less directly, Dal, the early Klee and a bit of Schiele. The postures and grouping of the bodies are often unmistakably Jogen, as are the long, narrow eyes, shaped like eels, with their covert, sidelong glances. Rabindranath is in the layering of darkness upon darkness, with luminous blues and reds shimmering between the layers. He is also in the long-necked profiles that make the faces look like sinister flowers (the faces illustrating Shey), and in the arabesques and curlicues that whimsically decorate the borders and backgrounds of the main tableaux. 

 But Ganguly takes on her Indian masters and subjects them to a process of compulsive evisceration that becomes her own. The human figures and faces out of Rabindranath and Jogen are elaborately disembowelled, and the dismembered faces, fingers, flaps, folds and follicles are then allowed to reach out for one another in a kind of lumbering, surreal slow motion to create other bodies, single and grouped. These reconstituted bodies often blur the polarities of male and female, human and animal, wild and tame, tragic and comic. Hence, the only titled piece in the exhibition is called Similitude Reconciled. It is a depiction of Durga and Mahishasur, in which the goddess, her antagonist and the beasts that bear them become a single corporeal entity, with two faces and interlocking limbs. This reconciliation lends to Gangulys large, multi-figure compositions a mysterious stillness and poise. The faces with their multiple labial folds, fish-eyes and curious head-dresses, or sometimes wizened like that of a mummy, with the teeth showing look unillusioned about the complicated bodies they are attached to. Yet, even when they look directly at the viewer, they are inscrutable. They seem reconciled to the necessary humiliation of having the inward mystery of their bodies exposed to the gaze of the world.

 Parallel to the larger work, three beautiful series of faces done on paper in the mid-Nineties, in 2005 and in 2006 trace the unfolding of another idiosyncratic subjectivity. Those of the mid-Nineties are the bleakest and most violently fragmented (in a European-Modernist sense) a heap of broken images, precariously held together to form the semblance of a face. The 2005 faces are in a vivid, deepening indigo and glints of red. In profile and with fantastically embellished head-dresses, they are full of a sly, sharp drollery. Finally, in 2006, they grow out of Venetian masks and recall the copiously vegetal human faces by Arcimboldi, although Gangulys faces are made of tiny human bodies and of the stippled and stubbly textures of skin and hair. Their reds, blues and flesh-tones are more opulent than before. 

 If the larger paintings are reconciled to the humiliation of being on show, these stylized, decadent faces have learnt to play with devious concealment and masquerade. It is as if these exquisite denizens of a jardin exotique are trying to waylay the viewer from the darker and more austere compulsions of the theatre of the guts. 




</TEXT>
</DOC>