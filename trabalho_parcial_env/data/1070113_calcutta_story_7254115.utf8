<DOC>
<DOCNO>1070113_calcutta_story_7254115.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Quiz twist, band beats

 It was celebration time once again 

 at Presidency College just a week after New Years Day. 

 Milieu, the annual college festival organised in association 

 with Unish Kuri, kicked off on January 6 with off-stage 

 events like carrom and table tennis competitions.

 The weekend extravaganza turned out to be a grand affair. We had a high number of registrations for the table tennis competition this year, with representatives from almost all the reputed colleges, said Sayantan Saha Roy, general secretary, Presidency College Students Union.

 The Eastern Nite saw a performance by city-based folk band Dohar, which had the audience dancing to traditional rural folk tunes. Up next was a performance by Usha Gangulys theatre group Rangakarmi, which performed Maiyyat, a drama based on caste issues in northern India.

 Over the next three days, Milieu saw more than 20 colleges participating in various on and off-stage events like antakshari, eastern solo competition, western dance, quiz and medley, the last being a mix of a number of events like just-a-minute (jam), spin-a-yarn, dumb charade and extempore.

 One of the most popular events was the western band competition that was held on January 9. Twenty two bands came to register for the competition but we had time only for six. We had to increase the number to 10 because of the immense rush. Even then 12 bands had to be turned away, said Sayantan. Later in the evening, Bangla band 4WD performed at Milieu Dinner.

 The film festival held on January 10, which showcased movies like Jana Aranya, by Satyajit Ray, Run Lola Run by Tom Tykwer and Pulp Fiction by Quentin Tarantino, was also a big hit.

 One of the main attractions of Milieu, as always, was Chao Quiz, held at the canteen where the participants had to crack questions with a twist. Sample question: Which city has the maximum number of taboos? Answer: Ban-galore.

 The main attractions, however, were the performances by bands in the evening each day. Western Nite was reserved for a spot of headbanging with city-based bands like Krosswindz and Hobos taking the centre stage.

 The fest wrapped up on January 

 10 with a performance by the popular band Cactus (picture 

 above by Bishwarup Dutta) which regaled the audience 

 with hits like Saridon and Holud pakhi.

 Chandreyee Chatterjee

 Pointers to the past

 Trust South Point School to raise the bar yet again in a matter of numbers. The schools reunion at Nalban, in Salt Lake, brought together 1,200-plus former students. 

 This is a new record. Last year the footfall was around 900, Krishna Damani, president of South Point Ex-Students Association, was ecstatic. Perhaps in anticipation of a record crowd, Meghnad Roychowdhury of the class of 1990 was posted at the gate with a wireless set.

 The next person who walked in was not a South Pointer. Bollywood singer Udit Narayan had decided to make the most of the sizeable gathering to promote his Bengali album Kolkata O Kolkata. Chaperoning him was music company Saregamas business head S.F. Karim, a South Pointer of the 1971 batch.

 If the romantic lines in Bengali were greeted with catcalls, the crowd broke into cheers as soon as Udit turned to Bollywood and his latest hit Khaike paan Banaraswala. That set the tune for the rest of the night as Lakkhichhara took over with drum beats that played on the eardrums. The music is so loud, Class II student Tamoghno Chattopadyay, one of the few current Pointers around, sat with a martyred look next to his mother.

 The teachers had taken shelter 

 from the night chill under a far-away shade. Our presence 

 would restrict their fun, junior school principal Madhu 

 Kohli chuckled. Deputy commissioner of police, traffic, 

 Jawed Shamim, and actresses Papia Adhikari and Rimjhim Mitra 

 walked in as the night rolled on leading to a fireworks 

 display (picture by Sanat Kumar Sinha).

 At one side of Nalban, former Pointers were being called over to pose for the camera batch by batch. Ananda Sengupta was not bothered. We passed out in 1974, he said, as students of the 1995 batch scrambled on to the dais. His daughter Urmi, now in Class XII, would join the rush next year. 

 Sudeshna Banerjee

 Only Connect

 Abhijit Gupta

Spaced out

 Last evening, I saw a news clip 

 of astronaut Sunita Williams talking to students in Punjab 

 from space, and marvelled not at the ingenuity of it, but 

 how commonplace all this has become. In three or four years 

 time, we may well see Stephen Hawking in space, lecturing 

 on the world, the universe and everything from a spaceship 

 spinning 100 km over the earth. And if you, gentle reader, 

 had enough money say $200,000 then you could 

 also find yourself a passenger on Richard Bransons 

 Virgin Galactic, which plans to offer sub-orbital spaceflights 

 to the paying public from 2009.

 But later that night, a memory 

 waved a tentacle from the murky depths of my subconscious: 

 what would Kartik Chandra Paul have thought of all this?

 If you claim to live in Calcutta 

 and do not know who K.C. Paul is, then I am afraid you are 

 not the real article. For the past three decades, Paul has 

 been the most tirelessly dedicated graffitist in this fair 

 city, leaving his mark from Tala to Tollygunge without fair 

 and favour. Working with nothing more fancy than whitewash 

 and black paint, Paul has been proclaiming that it is the 

 sun which goes round the earth, and not the other way round: 

 Galileo, Copernicus, Aristarchus of Samos had all got it 

 wrong.

 I met Kartik Chandra Paul 18 years 

 ago when a friend and I had gone to interview him for a 

 now-defunct magazine called Calcutta Skyline. We had sought 

 him in deepest Howrah and had met a shy unassuming man, 

 who lived in a two-roomed house by a pond covered with the 

 ubiquitous water hyacinth. Amidst grazing cattle and waddling 

 ducks, the man had stood gesticulating wildly as he talked 

 of Ptolemy and cosmology. Behind him, the whitewashed walls 

 of his house were covered with astronomical diagrams.

 Paul had told us that he was in 

 the armed forces when he studied the movement of the evening 

 stars and became suspicious about the position of Venus. 

 After a course of self-study he came to the conclusion that 

 while the heliocentrists were right about rotation, they 

 had got it wrong on revolution. In 1974, he concluded that 

 the earth stood still and the sun travelled around it with 

 its orbit titled at an angle of 231/2 deg;. Among the other 

 things he said was that Mercury and Venus orbited the sun, 

 and the three of them were orbiting the earth as a system. 

 The outer planets orbited the sun but were not moving along 

 with it. Paul had even corresponded with NASA on the subject 

 but the space body had politely told him that they were 

 satisfied with the heliocentric system.

 Unfortunately for Paul, almost 

 all of recent astronomy contradicts his theory. But I still 

 remember how both my friend and I were charmed with the 

 quiet assurance of the man, and his unshakeable belief that 

 one day the astronomical textbooks would have to be rewritten. 

 If Richard Branson has another free seat going on his ship, 

 I would strongly recommend Kartik Chandra Paul for it.




</TEXT>
</DOC>