<DOC>
<DOCNO>1070815_opinion_story_8192380.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 MAN AND MACHINE

 Abhijit Bhattacharyya

 Although much has been said about the length of runways and the slippery surface caused by rain, what remains unsaid is that even at the best of times and with the best of technology at hand what matters most in any critical stage of the flight is the capability of the man behind the machine. Flying is a job in which even the slightest of mistakes can result in fatalities. This is borne out by numerous air disasters around the world.

 Way back in June, 1995, a plane carrying Mexican fans for a soccer match between Mexico and Norway crashed in the woods in heavy fog while approaching the Dulles airport outside Washington DC. Two weeks later, on July 2, a US Air DC-9 jetliner, with 57 persons aboard, crashed during a thunderstorm. And on December 29, 1995, near Van, Turkey, an aircraft, whose pilot had aborted two landings, crashed during a third attempt in blinding snowstorm. On July 9, 2006, a S7 Airlines Airbus-310 arriving in Siberia from Moscow crashed into a concrete wall killing at least 122 aboard.

 Flying high

 In all these air crashes, the weather was thought to be the main factor responsible. Nevertheless, a single factor can never be the cause of repeated air disasters. There is usually a combination of factors. Take-off too can be as risky as landing. This apart, irresponsible and reckless pilots have often shown the hazards of flying high and taking off for the high altitude.

 It was the reckless act of a China Airlines pilot that ensured the crash-landing of an Airbus-300 and its explosion on the tarmac of the Nagoya airport. The inexperienced co-pilot at the controls struggled to land the craft as its computerized controls tried to abort the landing and gain altitude. The explosion killed 264.

 Nearer home, two cases deserve mention. The Bangalore crash of Airbus-320 on February 14, 1990 exposed the inexperience and incompetence of the pilots, who could not use the state-of-art technology. In Patna, a Boeing 737-200 crashed into houses adjacent to the dangerously small and short runway during its second landing attempt on July 17, 2000.

 Lessons to learn

 There are some important points that have emerged from Sao Paulo and other crashes. No accident can occur on account of a single factor. Every accident has its actors and factors working overtime. Most accidents occur during or immediately after take-off and landing. Bad weather inevitably complicates the man-machine interface. Management in the cockpit always plays the most crucial role. 

 Although the Sao Paulo accident is now history, it bears important lessons for our airfields. The four major Indian airports Palam, Dum Dum, Santa Cruz and Chennai have congested roads adjacent to the runways. The approach to the airports are densely populated. This steadily reduces the approach funnel and sterile ground, thereby enhancing the vulnerability of the aircraft in an emergency. All the four Metro airports have 11,000 feet plus runways, but the smaller airports have less facilities. The burgeoning population and the land mafia constitute perennial danger.




</TEXT>
</DOC>