<DOC>
<DOCNO>1070918_opinion_story_8327378.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SMALL TROUBLE

 One of the markers of changed times is the degree to which passions are inspired by machines. Mobile phones, for example, are blessed and cursed with equal vigour, often by the same people, if at different times. The government of Karnataka has forbidden schoolchildren up to the age of 16 to use mobiles in classrooms or within the school premises. The reasoning behind the ban may be well intentioned, but it is nonetheless peculiar. The concern here is about the childrens health, since it is believed that the exposure to radio frequency fields hurts the brain, the central nervous system and the ears. The idea is not new; mobile users have grown used to strident alarms followed by cavalier dismissals of danger pouring out of scientific and technological institutions. Without going into the intricate relationship of scientific and technological findings with the corporate funding that makes tests possible, it may be relevant to clear the confusions in attitude to one of the most handy little machines of today. 

 The Karnataka governments reasoning is peculiar because banning the mobile in the class or on the school grounds will not protect the health of children up to 16. What it might do, though, is to compel pupils to pay attention in class and develop a modicum of good manners. Compulsively playing games and exchanging messages during studies, games and in social situations cannot help a healthy growth of personality. But if the ban is only within the school since a government clearly cannot dictate behaviour outside the institution forbidding the sale of accessories or cards to users under 16 seems a bit under-the-belt. Besides and this is the problem with mobiles taking them away from children may result in exposing them to terrible dangers minus the means to get help. Few parents would deny the usefulness of the mobile even when exasperated with the use children make of it. It is too early to say what effects in the long term a mobile will have on its users health. What is needed now is to develop a culture of best use. This is especially important in India, where the cellphone has found a huge market, where it has provided villages with easy and cheap means of communication and therefore a kind of empowerment, where the rich and the poor both depend on it so heavily. Banning it is pointless; teaching children certain basic principles of good manners and good health is the most that can be done at the moment.




</TEXT>
</DOC>