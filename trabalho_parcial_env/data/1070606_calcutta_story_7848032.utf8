<DOC>
<DOCNO>1070606_calcutta_story_7848032.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Proud tales from the paddocks

 SUBHRO SAHA

 BK Seniors Good Selection, trained by Paddy Rylands, won the Taltola Plate at the Calcutta Races in 1962

 Not a Saturday siesta for them, but the intoxicating hammering of hooves by those splendid sprinters and stayers at the race course in appropriate finery. Chocolates and gifts galore in the evening with even a champagne party thrown in if a Filter or a Fonseca from the family stable galloped home ahead of the pack.

 Meet the Poddars, rare racehorse owners since the era of the homebred tonga tatts putting up a brave front against the Arab thoroughbreds at the Royal Calcutta Turf Club (RCTC), Barrackpore and Tollygunge.

 Going against the grain of the conservative Marwari clan, the family, which was primarily responsible for bringing Burmah Shell into India, has carved out a novel niche for itself on the race tracks.

 My late father Balkrishanlal (BK) Poddar owned racehorses in the 1920s when most of the Marwari households in Calcutta scarcely knew anything about racing and there was a strong stigma of gambling which stuck with the sport, recalls Brijendra Kumar, at 63, representing the second generation of the pedigreed Poddars.

 BK Senior was the frontrunner among the Brothers Poddar with six to eight horses under his leash at any given time, while Raghunath Prasad, Hanuman Prasad, Janki Prasad and Laxman Prasad, all contributed to the stable count. The races were often the only place you could catch hold of all five together.

 Dad really had a yen for horses, and won the Derby, the Queen Elizabeth Cup, the Governor-General Cup and the Viceroy Cup, with his fleet of horses trained by British trainers like Goswell, Fownes and Galstaun. He also meticulously maintained a register with information on all horses, says BK Junior.

 He inherited five of his fathers horses after his fathers death in 1962, and continued to train them by Paddy Rylands, entering them in the RCTC events.

 After three-four years, however, I gave up owning, even though I still go to the races, says Brijendra.

 His cousin Nandkumar (NK), who remains as passionate about the sport at 65 as he was three decades back, won the 1000 Guineas and the May Fowl Cup with his favourite horse Citibank in 1977.

 However, in those days, the British trainers were partial to the Maharajas and often we had to pay a price for this colonial legacy, smiles NK.

 His brother, the late Lalit Prasad and cousin, the late Bimal Prasad, also owned horses. Son Arvind kept the racing flag flying for the family for a while, winning the Bangalore and Calcutta derbies and the May Fowl Cup.

 Owning horses has run in the blood and with it, a progressive streak. Our men folk used to frequent the 300 Club, a hip nightclub on Theatre Road, and also Firpos and Flurys. Even the ladies were encouraged to go to the races and participate in celebrations which would often continue till Monday, remembers BK Junior.

 His son Yashwardhan has broken the tradition and has nothing to do with horses at least at the moment. This could well be the trot trend for the new generation as priorities change, but the fond memories and proud tales from the paddocks will stay with the canter clan.

 For instance, on Rakshabandhan, Dad used to take my sisters Gyanwati and Pushpawati to the stables to tie rakhi to his favourite horse Filter. From three-four days ahead of race day, we had a steady stream of visitors seeking the right tip from Dad at our 51 Southern Avenue residence. Those who couldnt meet him, cornered his driver Ghosh, coaxing and bribing him for the winning tip, remembers BK Junior.




</TEXT>
</DOC>