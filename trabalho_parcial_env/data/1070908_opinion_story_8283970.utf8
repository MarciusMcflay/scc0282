<DOC>
<DOCNO>1070908_opinion_story_8283970.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SOME ACTION AND LOTS OF FUN

 Ananda Lal

 Theatre

 The indomitable The Action Players (TAP) are back with a new production, Our Time (picture), consisting of fourteen brief sketches in the mime style they have patented. Unlike some of their larger-scale, more ambitious works in the recent past, it returns to the Marceau-esque technique of bare stage, black outfits and short vignettes of everyday life, except that TAP rely on group dynamics rather than solo enactment and eschew the white-face makeup associated with classic mime. 

 Some may think that this regresses to an earlier phase in TAPs development, forgetting that the troupe does not have a constant population its young members grow up and get jobs, after which most of them do not find the time to persevere in such activities. Like university theatre, in which a director must start afresh with raw hands once the seasoned talents graduate, the founder, Zarin Chaudhuri, has had to go back to the drawing board, recruiting five debutants straight out of Class X at the Oral School for Deaf Children. The remaining seven have had prior experience under her. This dozen should provide TAP with a core team for the next few years. 

 Our Time begins with comic flair on Love is Blind, where a mans premeditated attempts at wooing fall flat three times before the real thing hits him out of the blue. Sadder themes also arise, most touchingly in Beggars, whose eponymous characters imagine caring for a child after picking up a small frock from the garbage. The obligatory farce achieves its best effects in ATM, when two local boys try to draw out cash without a card; Home Alone Dad, in which a father looking after his baby collapses from the strain, comes a close second. Some other skits, such as On a Crowded Bus, show that co-directors Shreyashi Ghosh and Irfan Ahmed (himself a fine actor) need to pay more attention to realistic details for the sake of plausibility. 

 Another young black-garbed unit of college students, Miraje, recently reworked Pinters one-act The Lover into their inaugural endeavour, The Lovers Trapped. Three women and two men play the husband and wife whom Pinter entwined in an elaborate charade of adulterous fantasies and British stereotypes. Jhelum Ghoshs directorial method of fluidly interchanging one performer with another physicalized Pinters idea impressively, presenting several combinations of couples. Yet one could not help feeling that by deviating frequently from Pinters brilliant and often ridiculously funny dialogues, she simplified the verbal ironies a bit too much. Nevertheless, her inventive approach could take her far. 




</TEXT>
</DOC>