<DOC>
<DOCNO>1070623_sports_story_7962418.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Federer fresh and relaxed after holiday at home

 - First meeting with Borg was very surreal, recalls Swiss ace 

Mark Hodgkinson 

 Roger Federer 

When Bjorn Borg took his fifth successive Wimbledon title, in 1980, Roger Federer was not even a twinkle in his mothers eye. And when Borgs Wimbledon run ended in that 1981 defeat by John McEnroe, the Swiss player was just a month old. But Federer is someone who knows his tennis history as well as he knows how to caress a forehand pass. 

So, if he lifts the golden Challenge Cup for the fifth summer in a row, to share the record with the Viking god of the All England Club, it would be something special. 

I wasnt even there when he won his fifth, and I had only just been born when he lost for the last time at Wimbledon, but I have seen the re-runs of those matches, a super-relaxed Federer said three days before his first match on Monday. 

It takes a lot to faze Federer, but when the world No. 1 had his first proper meeting with Borg late last year, on a Dubai practice court, he was so thrilled that he found himself laughing inside as he looked over the net at his hitting partner. 

I was in Dubai for a couple of days and I had this fantastic idea of calling up Bjorn, who was there for a seniors match, and saying, do you want a hit? It would be a dream come true for me. 

He said, fine, 10 am tomorrow. That was the first time that I got to speak to him at length. It was lovely to meet him properly and play against him. It was very surreal. I was warming up and I was laughing on the inside. I thought to myself, yes, hes playing just the same as he used to, Federer said, glorying in the memory and dispelling the notion that you should never meet your idols. 

I think everybody would like to know why he retired at such a young age, at 26. In a way it was a pity that he didnt play on longer, but at the same time it makes him unique for having quit when he was so young. 

During the Wimbledon fortnight, Borg will be at the All England Club for only the second time since that 1981 defeat. He has told this paper that he wants to sit on Centre Court on the second Sunday of the fortnight and watch Federer equal his SW19 Big Five. 

There has been talk that should Federer win, Borg will present him with the trophy. But Federer, a traditionalist, said that wouldnt be a good idea, if I won. It should be Royalty presenting the champion with the trophy. 

After losing to Rafael Nadal in the French Open final, Federer withdrew from last weeks Halle meet, though he had prepared for his last four Wimbledon victories by winning that event in Germany. 

Clay is very energy-consuming, he said, and once the French Open is over you kind of fall into a hole for a couple of days, so for me it was good to go home for a change. I got five days in Switzerland before Wimbledon and I feel good now, relaxed and mentally fresh. To me, thats more important than having played some matches on grass beforehand. 

I played cards with my Dad, my friend and my godfather. I invited them over to my new apartment in Zurich. I also invited some friends over to dinner. It was nice to invite people because I never used to do that. I didnt have the time, or I was living at my parents and they were the ones who invited people over. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>