<DOC>
<DOCNO>1070414_opinion_story_7644965.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SHUFFLE AND LISTEN

 So natural and so inevitable and so simple. What could this be a description of? Love? Death? No, this is how Apples industrial designer describes the iPod. The first one was sold five and a half years ago, just a couple of months after 9/11. Apple announced earlier this week that it had sold the 100 millionth iPod, making it the fastest-selling music-player in history. Since November 2001, there have been 10 new models, including the Mini, the Nano and the Shuffle, some of these already in their fifth generation. The iPod is seamlessly integrated with iTunes, the worlds most popular online music, TV and film store. It offers five million songs, 350 TV shows and over 400 films. And once Apples iPhone is launched in June, it would be possible not only to carry around 10,000 musical items everywhere, but also to use the same chic little thing to access the internet and make phone-calls.

 What has this gadget-turned-icon come to stand for in such a short time? Most obviously, it is changing the way in which music and other forms of entertainment are becoming a part of everyday life. Work, play, leisure and travel, among other things, can now all take place inside a personally designed and privately enjoyed musical space, which can be refurbished endlessly and more-or-less legally. To carry ones music (and entertainment) about without disturbing anybody, to shuffle items around and create random patterns of listening, to have compactly about ones person all that one loves and needs, and yet feel light and bright and sparkling: such are the pleasures on offer from this triumph of minimalism. It is a curious mix of being in control of, and being controlled by, something one possesses almost intimately. And unlike other, more human, intimate possessions, one remains delightfully mobile with, and unencumbered by, this one. With the iPod, pleasure becomes wonderfully integrated with the person, something that can be carried about in ones pocket and in ones head. So it is the coolest new way of being an individual.

 Yet, the paradox of this form of individualism is that it makes one feel self-contained in ones pleasures as well as bonded with other listeners of music and buyers of iPods. And this feeling of commonality is unique to modern, globalized consumerism. Sitting in the Metro with earphones plugged in, or chatting on the Net on ones laptop, or driving alone on a busy highway at night, one is both solitary and connected, peculiarly local and potentially global. The iPod, like so many of this centurys commodities, makes its owner stand out as well as belong. Yet, quite apart from what Beethoven or Metallica might do to the eardrums if heard too intimately, there remains something strangely chilling about being able to carry about ones music in absolute silence. To inhabit public spaces perfectly insulated in ones private world of pleasure is one of the oddest luxuries of these times.




</TEXT>
</DOC>