<DOC>
<DOCNO>1070211_calcutta_story_7376052.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Kundu Special travels in time

 The Bengali tour institution changes to cater to a new market, but retains its core of personalised care and food

 A toy train on its way to Darjeeling. Kundu Special stopped conducting tours to the hill station due to several services there catering to Bengalis. (PTI)

The Bengali tour institution changes to cater to a new market, but retains its core of personalised care and food

 The irate wife threatening to go off with a lota kombol on a Kundu Special trip is an old Bengali staple. Kundu Special, the original Bengali conducted tour organiser, holds a special place in the Bangali heart. There has never been a bigger hope for the single Bengali woman. 

 The brand connotes safe, organised holiday tours, particularly for the elderly, undertaken by an unmistakably Bengali tour operator, who would not only have his own Bangali randhuni along with porters and managers who all spoke Bangla, but also the Ganesh marka mustard oil, hing and Dunlopillow. To ensure home comfort even in the heart of Rajasthan sands or on the chilly climbs of Amarkantak.

 The Kundu magic still works. If a tourist needed Horlicks and chicken stew because he was feeling under the weather, Kundu would serve just that and only for him, insists Soumitra Kundu, from the family that runs the business. 

 It has been working since 1933 when Sripati Kundu reserved a full train and undertook a 56-day all-India tour. He was the pioneer of tourism in India, says Soumitra, who belongs to the third generation. While Sripati had several other businesses, a true Bengali, he was passionate about travel, and in home comfort. That led him to a new thriving business. 

 From reserving a full train in the 30s, Kundus took to reserving bogies in the 60s. They would carry the Dunlopillows to fit into the train seats for comfort. Instead of checking into hotels (there werent that many in those days), the tourists would stay on in the bogey that would be stationed on the side tracks, somewhere in a railway station in India. The mornings would start with hot ghee-soaked luchis and dhonkar dalna along with a steaming cup of tea. That would lead to khichudi and begun bhaja in the afternoon. 

 The khichudi may have given way to fried rice in the 80s and to chowmien and biryani in the 21st Century, and the stationed bogey to hotel rooms, but even in the days of Cox and Kings and fancy packages and low-cost airfares, Kundu Special holds its own as a traditional Bengali bastion.

 At Kundu, each tourist group is still accompanied by two tour experts, two cooks and four service boys, all of who are Bangali and 99 per cent of the tourists remain Bengali, too. Their social profile remains unchanged too: they come from the middle-class and upper middle-class. Many clients have been touring with them over generations. There are also the rich who often fly and put up at star restaurants but join the group for sightseeing.

 Over the years, their itinerary has changed a bit. Its no longer viable to have all-India tours. So my father first broke the tours into north India, south India, west India and so on. Then there were 22-day trips. But now who has the time for a three-week vacation? People are now on shorter 10 or 12-day vacations and are willing to travel more frequently than before, explains Soumitra. So while one goes to Himachal to do Spiti Valley in the summer, he or she could come back next year to do the Lahaul area later. 

 Kundus have also added North Sikkim and revived Andaman tours in the last two years. But what is heartbreaking is that Kundus no longer do Puri or Darjeeling! Its no use for us to do Puri. There are so many hotels and restaurants there that serve Bengalis that we really need not be there.

 But business thrives. Kundus had 470 people queuing up on the eve of the first day of Puja bookings in 2006. We have to turn down many, simply because we cant accommodate them, says Soumitra. 

 And still Bengali single women threaten to run off with Kundu Special, though they have more options now.

 ANASUYA BASU




</TEXT>
</DOC>