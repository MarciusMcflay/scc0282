<DOC>
<DOCNO>1070910_nation_story_8298484.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Problem of plenty but low-wage guests

 Port Blair, Sept. 9 (Reuters): A flood of low-budget tourists since the 2004 tsunami is hurting the ecologically fragile Andaman and Nicobar Islands and ruining plans to make it a top global destination, industry officials said.

 The influx has drained scarce resources such as water, sparked excessive demand for airline tickets, hit hotel revenues and created a service culture which is insensitive to the needs of wealthier travellers, they said.

 The problem, officials said, is a decision to allow all levels of government and state-run firms employees to use their paid family and home leave travel allowances, awarded every two years, to fly if they want to visit the Andamans.

 Low-wage employees are now taking up the chance to fly for the first time and visit the islands. Such trips previously were mostly restricted to senior employees.

 The move to attract more visitors and boost the economy, badly hit by the tsunami, has backfired as it has overburdened the isles without raising earnings correspondingly, tourism officials said.

 For these tourists, the destination does not matter. They come because they get to fly for the first time in their lives, said Mohamed H. Jadwet, the head of the Andaman Chamber of Commerce and Industry. We also pushed for this after the tsunami but we never thought it would come to this.

 Tourism, farming and fishing are the main sources of income for the islands, which can be reached only by air or a three-day ship journey from the mainland. The tsunami hit the isles badly, killing about 3,500 people and displacing 40,000.

 It also hurt tourism, with the number of visitors in 2005 plummeting to 32,000 from an annual average of 100,000. But the government move saw it surge to over 125,000 in 2006 and it is expected to cross 150,000 this year, officials said.

 The influx, however, has not seen an increase in revenues as an estimated 80 percent are low-wage employees.

 They are drawn by travel firms on the mainland known to corner cheap airline tickets and offer them to state employees who get full fare leave travel allowance, allowing them to pocket the difference, tourism officials said.

 These people will not spend more than Rs 500 a day on rooms, food and sight-seeing, said G. Bhasker, who owns a middle-level hotel and runs a travel firm here.

 We are not against any strata of society coming here but we also have to see what it is doing to the islands resources and the industry, he said.

 Foreigners, often the most high-paying of visitors to India, are few and far between.

 The Andamans have faced a severe water shortage this year and the rising number of tourists meant that Port Blair got 30 minutes of tap water supply once every five days this summer, residents and activists said.

 Garbage disposal has become a huge problem as the sprawling town has no modern waste management system.

 While an airline boom has seen fares fall, they remain artificially high for Port Blair, preventing island residents from flying and putting off regular tourists.

 We have limited tourism-carrying capacity because of limited resources and that has been overstretched, said Samir Acharya, of the Society for Andaman and Nicobar Ecology. 

 Now the Andamans are badmouthed so much that genuine tourists dont want to come, he said.




</TEXT>
</DOC>