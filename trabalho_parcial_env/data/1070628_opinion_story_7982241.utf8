<DOC>
<DOCNO>1070628_opinion_story_7982241.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 ALARMING NOTES FROM THE UNDERGROUND

 The use of maximum force in dealing with the Naxalite menace is destined to fail unless it is backed by constructive development that involves the local population, writes Anuradha Chenoy

 The author is professor, School of International Studies, JNU

 Parallel force 

 The districts of Jharkhand and Chattisgarh, known as the Naxal-affected belts, are areas where the scheduled tribes and castes make up more than 60 per cent of the population. Poverty is endemic in this region. The government is carrying out two types of development. The first is based on industries, mining and commercialization, and the second is linked with the National Rural Employment Guarantee Act, the mid-day meal scheme and primary education. As far as the Naxal problem is concerned, the policy is to use maximum force. Which of these development models and policies is working is a critical question for the future of these states and their people.

 The first developmental policy regarding the increase of private investment and ownership in mining, forestry, and so on is not new. This type of development was the initial reason behind the alienation of tribals since they saw their communal methods of ownership and freedom being curtailed. As large areas are cordoned off to make mines, large dams and special economic zones, tribals are displaced and turned into migrant labour. Tribal customs, like the making of local brew from Mahua trees, have been banned and foreign liquor shops have come up. The Naxalites have thrived in such an iniquitous environment.

 The second developmental model, connected with social and economic schemes, is becoming increasingly popular, although it is using only 25-30 per cent of its capacity. Recent surveys by the Right to Food Group have revealed many problems with these schemes which need correction to make them effective and beneficial to more people. Yet, these schemes work in the Naxal-affected areas and because of their popularity even the Naxals support these programmes, testifying to their importance. The government argues that Naxals impede development. But when development is positive and supported at the ground level, anyone wanting political legitimacy is forced to support it.

 The Naxals work on small-time development issues like running some schools, health centres, dams, foodgrain banks, and so on. This gives them local level support, without which they would not be able to survive. The Maoists levy taxes and extort money from contractors and the locals for such work and for procuring the wide range of weapons that they possess. The level of support to Naxals in Jharkhand, where they are fast spreading, however varies. 

 In areas where the local population sees that significant efforts are being made by the government for improvement, the Naxals are not popular. Who would want to go to a Naxal school if the government school functioned? But in most places people are fed up with the police. Villagers say that if the Naxals come at night and want to be fed, the police invariably turn up next morning and want to be bribed. The choice then is between the Maowadi and Khaowadi. 

 Anyone interested in these areas, from the local member of parliament or that of the state legislature, to contractors and businessmen, has to have some alliance with the Maoists. How else would elections be held? And how else would contracts be completed? The Naxals argue, In our zones, anyone can pass through if their identity is clear. Maoists, in fact, no longer believe in liberated zones but in zones of influence, where they co-exist with others and where they have parallel judicial and executive structures the jan adalat (peoples court) and their militia that executes. The smallest unit is the two-man village unit; then there is the area secretary and the area commander. Area decisions are taken together by the area commander and secretary. The sub-zonal committee is overseen by the zonal committee and the zonal commander. They are assisted by a local guerilla squad and a special guerilla squad. Leaders and guerilla squads do not comprise all locals. They can be from any other region. The entire party is underground.

 It is known that women have functioned as supporters, couriers and leaders, but very few come up for the risky work. The womens organization, the Nari Mukti Sangh, functions at all levels, including in the armed squad, where women get full military training. Most women join this movement because of poverty and some because of ideology. The major work of politicization is undertaken by them.

 The police have little knowledge of the functioning, except when Naxals are caught and then named commander, whatever their real status. Thus the local people often suffer police brutalities as there is little to distinguish between them and the Maoists. This is especially so in Jharkhand, where the Naxals are more local. 

 In the meantime, the police have killed hundreds of alleged Naxalites in encounters. They do not allow first information reports to be registered and give no compensation to families. The fear of the contesting militia has divided villages and caused fear and internal displacement, forcing villagers to evacuate their houses and camps, leading to unending personal tragedies.

 Like the special security forces created earlier to deal with insurgency in the North-east and in Kashmir, the Salwa Judam was created in Chattisgarh. This government-sponsored force of well-armed local volunteers comprises former insurgents and the local youth. This state-armed unofficial militia has caused much harm and turned more people towards insurgency. It has helped militarize the society, where children now dream of guns, and the use of force is the accepted method of negotiation. This militia is unable to distinguish between ordinary civilians and insurgents. They see the entire community as enemy, similar to the bounty killers who are used in all local disputes.

 Many human rights groups have recorded the excesses of this militia. Such reports, however, have been ignored. Instead, journalists and activists have been branded as sympathizers. Meanwhile, the Salwa Judam model is being copied in other areas like Jharkhand, where the Nagrik Rakshak Samiti or Narsu has been working along the same lines and all local sources testify to its unpopularity and criminality.

 Maximum force has been officially justified because of the killing and looting by the Naxals. Local officials say that once Naxals are caught, torture is essential to extract information. Figures, however, show that the number of Naxal-related incidents has not decreased, rather the number of human rights violations by both sides have significantly increased. Further, if the incidents and violations decrease in one area they simultaneously increase in another. For example, incidents of Naxalite strikes have gone down in Andhra Pradesh, but if nine out of 16 districts were affected in Chattisgarh, 18 out of 22 districts are affected in Jharkhand today.

 In these circumstances, the schemes like the NREGA are all the more important. Yet they are still to be fully implemented. The Right to Food group witnessed that while there was increasing awareness of the act, the staff to implement it was still inadequate. There were delays in wage payments, there was lack of institutional arrangements (for example, Jharkhand has no panchayat elections), a monitoring system and accountability.

 The outcome is thus already quite clear. People support ideas that benefit them and involve them. The idea of development based on human rights has become rooted in the minds of the people. To deny this is to lead to more conflict on all sides. 




</TEXT>
</DOC>