<DOC>
<DOCNO>1070426_opinion_story_7695915.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FRENCH TASTE AND BRAVURA

 The first round of the French presidential elections reflects the eagerness of the electorate and its sharply divided opinion on what would bring change, writes Jay Bhattacharjee

 Taking the long and winding road

 The official results of the first round of the French elections were released late on Monday by the interior ministry of France. The verdict narrowed the field from 12 candidates to two, and thereby brought the curtain down on a nail-biting campaign.

 By choosing the UMP candidate, Nicolas Sarkozy, and the Socialist contender, Sgolne Royal, the electorate put an end to the ambitions of the rank outsider, the centrist, Franois Bayrou, who had emerged out of the blue as a strong contender for the second round. The virulent rightist, Jean-Marie Le Pen, was relegated to a distant fourth position. The voting percentages were 31.2 per cent for Sarkozy, 25.9 per cent for Royal, 18.6 per cent for Bayrou, while the former paratrooper and Nazi-admirer, Le Pen, managed to get barely 10.4 per cent. The remaining 8 candidates, from the fringe leftist and ecological parties, together managed to get approximately the same level of support from the electorate as Le Pen.

 The high turnout on Sunday also belied snide comments in the Anglo-Saxon media. In the last elections in 2002, the participation was around 71.6 per cent, while this year, 83.8 per cent of the electorate turned out to cast their votes. This is a record last equalled in 1965, when the French voted for the first time to elect a president through universal suffrage. The enthusiastic and eager response of the countrys citizens is itself a tribute to the strong democratic traditions that the French have preserved over the last two centuries.

 However, the results of the first round and the period of waiting till the second round will also enable the voters and socio-political analysts to ask probing questions about the countrys basic structure and value systems. Is the country suffering from a 14- year-old malaise, as some commentators suggest? Or, is the French model basically effective, although minor repairs are always welcome?

 In both these cases, opinions vary sharply. In the first debate, the lack of consensus pertains not only to the identification of the malady, but also to the prescriptions. The discussion and the polemics have become quite intense and heated in this arena. In the second case, the debate is more focused but hardly bereft of controversy. The problem gets aggravated because the core of the discussion keeps shifting from economics to sociology, philosophy and history.

 Let us start with the economics. Undoubtedly, France has slipped from being 7th in the global ranking in the international per capita GDP table, 25 years ago, to 17th in 2006. Even after adjustments for purchasing power parity, France has not fared too well, with non-starters like Austria and Ireland having overtaken the hexagon recently. 

 For commentators across the Channel, this is, clearly, good news. However, the English (it is always the English that they talk about in France when they discuss the old rivalry) are not yet ready to uncork the bubbly as the French and UK per capita GDP figures are running almost neck to neck.

 The other undisputed fact is that the French worker, according to all available data from the World Bank and the Organization for Economic Cooperation and Development, is the most productive in the world, based on hourly output figures. This single parameter in the French economic scenario produces an enormous impact on the entire social and economic fabric of the country. The working classes in France keep on asking where the fruits of their labour are disappearing. There is also a bloated corporate sector, where the management and shareholders corner a disproportionately large segment of the stake- holders pie, leaving little for the workers.

 The largest French companies featuring in the CAC-40 (the French equivalent of Indias Sensex) on the Euronext stock exchange, have been earning record profits in the last five years. The problem is that many of them have substantial overseas shareholding, mainly from the United States of America and British sources. France has cutting-edge technology in many spheres but the large corporations (be it in the public or the private sector) have made a complete hash of commercializing this invaluable asset, especially in the global market. Corporate corruption is widespread and is rarely punished. On this core, the French business tycoons can teach a trick or two to our business mafia.

 Against this backdrop, the 35- hour working week introduced by the previous socialist government has become the nucleus of the debate on the economy before and during the elections. For the Right and the business groups, the picture is crystal clear the French do not work hard enough. For the Left, this is a bagatelle collectively, the country places (or should place) more emphasis on the quality of life and leisure. The Anglo-Saxons, as usual, jumped into the fray and muddied the waters. It needed the authoritative and weighty voice of Paul Krugman of the MIT to introduce a modicum of sanity among the commentators across the Channel and the pond.

 In a seminal article in 2005, Krugman observed that a comparison between the economies of the United States and France, in particular, shows that the big difference is in priorities, not performance. Were talking about two highly productive societies that have made a different trade-off between work and family time. And theres a lot to be said for the French choice. Driving the nail in the coffin, Krugman goes on to observe that the French have less income and less personal consumption than the Americans because it is a social choice they have made. The flip side of the coin is that French schools are good across the country. Nor does the French family, with guaranteed access to excellent health care, have to worry about losing health insurance or being driven into bankruptcy by medical bills. Fully employed French workers average about seven weeks of paid vacation a year, compared to less than four for their American counterparts.

 This brings us to the other main debate that of excessive government spending in France, together with a high level of taxes and social security charges. For Sarkozy and company, these elements have contributed to Frances high level of unemployment and other associated maladies. 

 Admittedly, the problem of unemployment, particularly among the youth, is a major blight on the countrys socio-economic framework. It is also a phenomenon that seems to have become a structural feature; it has defied all sorts of treatment in the last 20 years. More disturbingly, it has acquired a colour-oriented trait that has had disastrous effects in recent times. The riots in the French suburbs in late 2005 were sparked off by this explosive combination of social and economic deprivation.

 The notion of a strong government that promotes social welfare and invests in vital infrastructure is a cherished one in France. The very revival of the war-ravaged country from the mid-Forties was based on this model and very few French politicians have had the nerve to question its raision detre.

 All said and done, this model has done the country proud. Even the usually derisive British say that public services work in France, unlike in Britain. Simon Jenkins, the English columnist, remarks that the public realm in France has taste and bravura, whereas the British one is grotty. The perceptive author, Julian Barnes, says that the huge number of British settlers in France in recent years go there in search of a bucolic fantasy that is unattainable in their country.

 This is what Royal would like to retain, with some major corrections to the defective elements in the model. She wants a rupture with the past, and a country bereft of racism, xenophobia, elitism and corporate greed. In this quest, she may get the support of a majority of the Bayrou camp. However, Sarkozy is not about to give up. The battle is on, and only time will tell who emerges the winner.




</TEXT>
</DOC>