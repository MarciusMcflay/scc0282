<DOC>
<DOCNO>1070917_entertainment_story_8326872.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Superman not so super after all

 The pioneer is a sad 17th. The topper is powered by a kinetic computer-driven style. From pages to screen how the homo superior has fared

 17 SUPERHEROES 

 With Heroes one of the most talked-about shows on TV, an enthusiasm for super-powered heroes no longer carries the social stigma it once did. The year 2008 promises yet more comic-book characters migrating from the page to the screen, with big-budget debuts for the long awaited Watchmen, Iron Man, The Flash, and half-a-dozen lesser characters in production.

 We thought it might be timely to review the biggest players in the superhero movie franchise business and assess their future prospects. Weve scored them according to a range of substantially arbitrary criteria, focusing on their longevity both in comics and on film, and concocted a box-office score based on an average performance of all movie appearances by the character to date.

 Having an iconic costume is a key part of establishing a major superhero brand, so weve added a rather subjective costume category too. This is based as much on the potential of the outfit to transfer from the printed page to the silver screen as much as it does on sheer elan.

 1) Spider-Man

 After a risible attempt at a big-screen transfer in the 1970s, Spider-Man finally made a successful leap onto celluloid in 2002, with Evil Dead director Sam Raimi establishing a kinetic, computer-driven style that connected with comic-book geeks and regular cinemagoers alike. The first movie made over $100 million in its first three days and it remains in the top 20 highest-grossing films to this day. Theres no doubt that the studio money men will push for a fourth film in the series (theres talk of up to three more films) even if the director and star of the hit films do, as has been predicted, depart now that the record-breaking trilogy is complete. Tobey Maguire is currently planning to at least take a break from playing the misunderstood wall-crawling teenager and has reputedly signed to star in, and produce, the live action version of a vintage Japanese Transformers-style giant robot cartoon saga called Robotech.

 Costume: 10

 Coolness: 9

 Longevity: 3

 Box office: $2,495,718,076 

 2) The Incredibles

 Essentially the film that Fantastic 4 should have been, The Incredibles is an excellently crafted story about superheroes that combines knowing nods to the geek fraternity (Metaman, express elevator! Dynaguy, snagged on takeoff! Splashdown, sucked into a vortex! No capes!) while still delivering a believable family dynamic and a plot that draws in the casual viewer. And all this with entirely computer-created actors. To his undying credit Brad Bird, the director (and voice of Edna Mode), resisted pressure to create a sequel because, in his own words, he couldnt think of a story that was good enough. If only all film-makers had such integrity.

 Costumes: 10

 Coolness: 10

 Longevity: 1

 Box office: $631,442,092 

 3) 300

 Not comic-book characters in the classic mould, but certainly rooted more strongly in Frank Millers graphic novel than in any history book, the improbably-toned and barely dressed hoplites were presented in a virtual world which although not entirely novel (Sin City, another Frank Miller adaptation, used broadly similar techniques) showed the way forward for directors seeking to translate the extravagant vistas of the comic book into cinematic reality. Unsurprisingly, director Zack Snyder has now been commissioned to make sense of the highly influential, but long considered unfilmable, Alan Moore masterpiece Watchmen. 

 Costume: 2

 Coolness: 10

 Longevity: 1

 Box office: $454,592,590 

 4) X-Men 

 Marvels ever-changing lineup of mutant heroes has probably the most self-consistent explanation for its super powers of any comic book characters. Indeed, its worked so well that Marvel has expanded is homo superior backstory to embrace virtually all of its characters. Less successful were some of the costumes, with both Wolverines brown-and-yellow coveralls and Storms revealing negligee being transliterated into speedway riders leathers for their cinematic outings. Although roundly castigated by the fans on release, the third X-Men movie has been the best performer at the box office and there is no reason to believe, especially given the rotating cast of characters, that the movie franchise cannot endure almost indefinitely. Halle Berry has publicly expressed her desire to revisit Storm in a fourth X-Men film but she may be disappointed; rumours have surfaced recently of an X-Men prequel movie. Spin-off vehicles for favourite characters Wolverine and Magneto are already in pre-production.

 Costume: 4

 Coolness: 10

 Longevity: 3

 Box office: $1,163,063,674 

 5) The Fantastic 4

 One of those films that irritated the fanboys and critics alike, but still did pretty creditable business. The first Fantastic 4 film is by no means as bad as some people might have you believe, being a fairly faithful rendering of the extended family of heroes as they were depicted in their Silver Age heyday. The Fantastic 4 movies are one of the few examples of a comic book property being successfully true to the original form, rather than being adulterated with notions of dark or adult themes. One significant obstacle to their future success is the fancifully cosmic nature of their rogues gallery, typified by gigantic humanoid Galactus the planet eater who was, to widespread disappointment, reduced to an amorphous special effect in the recent Rise of the Silver Surfer sequel. 

 Costume: 6

 Coolness: 6

 Longevity: 2

 Box office: $607,290,873

 6) Batman 

 The most-filmed superhero property, not least because his main powers are a good deal of determination and a huge amount of disposable wealth, both of which are comparatively easy to fake on film. The treatments range from the deliberately campy 1966 Adam West effort to Christian Bales gritty American Psycho model which returns the Dark Knight to his gothic roots. The future of the franchise looks safe with another duel with The Joker in The Dark Knight already in production and the bubbling rumours of a Worlds Finest team-up with Superman or even, probably a dream too far, a movie featuring all of DCs major heroes together. 

 Costume: 9

 Coolness: 11

 Longevity: 7

 Box office: $1,570,772,639 

 7) Unbreakable 

 The film that took the fashionable what if superheroes were real notion to its extreme, M. Night Shyamalans dark fantasy is a clear antecedent of NBCs current TV hit Heroes. Bruce Williss unwitting superman is pitted against an adversary who lives and breathes comic books and knows how the story is supposed to develop. Rumours of a sequel abound, but they seem based more on wishful thinking. 

 Costume: 0

 Coolness: 7

 Longevity: 1

 Box office: $248,118,121

 8) The Hulk

 Its difficult to say how it all went wrong for The Hulk although wrong is a relative term when the film is still in IMDbs all-time Top 100 blockbusters. Certainly the trailer, with the jolly green giant playing swingball with tanks, looked like the stuff of movie legend and the world-class director and cast seemed set to deliver a roaring success. Something about the excessive tinkering with The Hulks origins, with the addition of a superfluous genetic engineering element, weakened the character in a way that the minor modification of his stable-mate Spider-mans powers did not. A sequel/reboot is in the works and it may well be that Marvel can yet add The Hulk to its long list of successful page-to-screen transfers. 

 Costume: 0

 Coolness: 5

 Longevity: 1

 Box office: $245,360,480

 9) Constantine 

 Bearing only the loosest relationship to its source material, a comic called Hellblazer from DCs adult-oriented Vertigo imprint, Constantine is part of a long and noble Hollywood tradition of filleting all of the subversive quirkiness out of an Alan Moore property and turning it into something palatable for popcorn-throwing US preview audiences. Of all of the comics characters most suited to star Keanu Reevess likeable brand of dim laconic cool, this isnt the one. 

 Costume: 2

 Coolness: 7

 Longevity: 1

 Box office: $230,884,728 

 10) Ghost Rider 

 On paper, this looked like being a disaster. A comparatively minor comic book character portrayed by one of the most renowned hams in Hollywood. Where did it all go right? Ghost Rider benefited from a well-timed release, and excellent promotional campaign, and an endearingly silly story that anyone could understand. As a bonus, it featured a blazing skeleton in a leather jacket who rode an enormous motorbike. Whats not to love? 

 Costume: 10

 Coolness: 6

 Longevity: 1

 Box office: $228,738,393 

 11) Daredevil 

 Considered by fans to be rare dud for Marvel, its hard to see where it lost the publics attention. The characters costume was tinkered with, but no more than those of Batman or The X-Men. Perhaps, when it comes down to it, if the story isnt up to scratch then no amount of special powers or martial arts skill will win. Despite a better response to the extended retail DVD, there seems little chance that Ben Affleck will be required to wedge his legendary chin into hornheads cowl for a second attempt. 

 Costume: 7

 Coolness: 5

 Longevity: 1

 Box office: $179,179,718

 12) Superm an 

 Christopher Reeves ability to switch between bumbling everyman Clark Kent and saintly bermensch Kal-El is what made the first two episodes of the 1980s Superman franchise the best-loved superhero movies of all time. The character was undone though by his own omnipotence, with expensive affects and a paucity of credible opposition driving the Reeve incarnation into a creative cul-de-sac. The recent Brandon Routh reboot has taken a reverent approach to Reeves iconic characterisation but the addition of a Superbaby bodes ill for the integration of The Blue Boy Scout into the new pantheon of serious comic book movie stars. 

 Costume: 8

 Coolness: 10

 Longevity: 5

 Box office: $875,116,559 

 13) V for Vendetta

 Along with League of Extraordinary Gentlemen, this rather clumsy adaptation of an Alan Moore graphic novel was instrumental in making the comic genius turn his back on the film world. The multilayered plot of a drably totalitarian Britain was simplified to something closer to a standard superhero tale. We hope the eagerly-anticipated Watchmen movie can redeem Moores faith in the Hollywoood machine. 

 Costume: 6

 Coolness: 6

 Longevity: 1

 Box office: $131,411,035 

 14) Blade 

 One of the advantages, for a screenwriter, of a less well-known comic character is the extent to which artistic liberties can be taken with their personality and capabilities. Certainly Wesley Snipess Blade is a good deal more taciturn, and more powerful, than the character as originally presented in Marvel Comics. The first film seemed to be released with comparatively low expectations but the lure of the trilogy was too strong for Marvel once the box-office figures came in. A projected TV spin-off was considered but it looks now as if Blade may, like many of his quarries, be destined for the grave.

 Costume: 4

 Coolness: 6

 Longevity: 3

 Box office: $338,605,468 

 15) Teenage Mutant Ninja Turtles 

 One of the most effective comic book-movie-TV-toy synergies ever, the four tortoises named after Renaissance painters began life as a parody of several Marvel Comics characters in a comic published by industry minnows Mirage Studios in 1984. Through a combination of savvy business dealings and sheer good fortune, creators Peter Laird and Kevin Eastman built a merchandising empire peaking with a trilogy of puppet/live-action movies. A recent sequel looks to have revived the fortunes of the martial arts reptiles among a new generation of action figure collectors. 

 Costume: 3 

 Coolness: 3

 Longevity: 4

 Box office: $416,381,410 

 16) Hellboy 

 Adapted from Mike Mignolas blackly witty books for Dark Horse comics, Hellboy came from apparently nowhere (or, specifically, Hell) to be one of the major genre successes in 2004. Hellboy is a demon adopted by the US military and employed to combat an endless parade of Lovecraftian menaces. Like many of his fans, Hellboy enjoys sugary treats and mild profanity. A perfectly-cast Ron Perlman looks set to return in a Hellboy movie next year, as summer 2008 shapes up to be dominated by a slew of comic book movie franchises new and old. 

 Costume: 4 (+6 for horns)

 Coolness: 9

 Longevity: 1

 Box office: $99,318,987 

 17) The Phantom 

 Because he was in many ways the original comic book superhero (pioneering the form-fitting costume that has since become the standard) and was played on screen by one of Hollywoods most handsome leading men, its something of a mystery why The Phantom was such a disappointment as a movie. Perhaps the period setting was partly to blame (although that did no harm to the equally pulpy Indiana Jones franchise). There is, nevertheless, rumoured to be a franchise reboot in the works for The Phantom, demonstrating the studios inexhaustible enthusiasm for fit young men in tights.

 Costume: 6

 Coolness: 5

 Longevity: 1

 Box office: $17,300,000 

 Michael Moran 

(The Times, London) 

Which is your favourite superhero movie? Tell t2abpmail.com




</TEXT>
</DOC>