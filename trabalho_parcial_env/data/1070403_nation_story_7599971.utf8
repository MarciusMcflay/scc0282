<DOC>
<DOCNO>1070403_nation_story_7599971.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Comrades in alms

 - In a world swept by huge changes, a lifestyle held hostage to small change

The Red star has faded, and the dream of revolution it inspired now seems like a quaint idea from a worn book.

 New currents have swept away many of the cobwebs in the Indian communists mind and turned some old enemies into new allies. Yet for the Marxist, his dues to history are more easily paid than his personal bills.

 So, while BJP and Congress MPs can be seen chilling out at Barista cafes or dining at five-star hotels, their colleagues from the Left can at best afford the Kakeda or the National at Delhis Connaught Place.

 For, one thing hasnt changed since the time when the Soviet Union was a superpower and university students carried the Little Red Book rather than iPods. The partys tight leash on members wallets shows no sign of withering.

 In the good old days, many Indian communists, their whole life dedicated to building a classless society, lived in Spartan communes and ate at a common mess. Most of their elected leaders, too, wore their austerity like a badge.

 The high tide of consumerism has made that radical lifestyle unfashionable. The communists and their leaders are no longer squeamish about being spotted at fast food joints or in cars other than Ambassadors or Fiats.

 Or are they? A few years ago, a CPM member of Parliament caught eating at Nirulas near the party headquarters in Delhi had launched into a lengthy explanation about how he had been starving since that morning.

 His embarrassment was hardly surprising. The CPM stays ever alert against its MPs or whole-timers being seduced by the lure of the bourgeois lifestyle.

 So, when MPs salaries rose from Rs 26,000 to Rs 42,000 a few months ago, the party decided to shield them from the luxury of having money to spare the root of all temptation.

 It laid claim to the entire Rs 16,000 hike, raising MPs stipulated donation to the party fund from Rs 19,000 a month to Rs 35,000.

 This leaves the MPs with Rs 7,000 like before to buy their groceries, pay their childrens school fees, run a second establishment in their constituencies and foot their travel bills.

 The parliamentarians are not complaining, at least in public. When you join the communist party, you know you cannot live a life of luxury, a party MP said.

 So how do they get by?

 Its the daily allowance of Rs 1,000 they are entitled to during Parliament sessions, plus what they get for attending House committee meetings, that tide them over.

 In all, it comes to Rs 20,000-25,000 a month, an MP said. Also, dont forget that the wives of most MPs work.

 Life would still have been difficult had it not been for the highly subsidised food and travel they are entitled to.

 We pay a nominal charge for accommodation and electricity. In Delhi, we can make one lakh free phone calls, which is more than enough, the MP said.

 But once they are in their constituency, the MPs must pay for their travel and phone calls.

 The Marxists choice of tipple, therefore, tiptoes cagily between Old Monk and Royal Challenge, generally giving wines a wide berth. The preferred puff, however, ranges from the bidi to Wills/Gold Flake to even Dunhill/Marlboro, being easier on the pocket.

 There is no single kind of school where the Left MP sends his children. Hannan Mollah and Yechury picked Sardar Patel Vidyalaya, a reputable public school. Many others settle for central schools.

 Mohammad Salims children, though, studied at Calcuttas Patha Bhavan till Class X. They did their Higher Secondary from a village school in Udaynarayanpur, Howrah, because Salim wanted his children to get a taste of rural Bengal. The MPs eldest son is now at Bengal Engineering and Science University, Shibpur.

 Yet its not just the need to count his pennies that keeps the Leftist away from the Page 3 lifestyles of many of his peers from other parties and from flashy cars, five-stars and Fab India kurtas (Sitaram Yechury is an exception). The aam admis champion is touchy about image.

 So last year, when CPM boss Prakash Karat and his wife Brinda, also a politburo member, travelled to Europe on a holiday the party simply clammed up on the destinations they were expected to visit.

 Yet the MPs are far better off than the whole-timers. They are the tribe of tireless workers who often braved official persecution, left their families and worked from the underground to make the CPM what it is today.

 There is neither subsidy nor travel concession for them. A whole-timer in even a good (CPM-ruled) state like Kerala or Bengal cannot hope to earn more than Rs 4,000.

 Last September, Karat had announced that the full-time activists will be paid the minimum wage of an unskilled worker. According to the National Rural Employment Guarantee Act, this is either Rs 60 or the state minimum wage, whichever is higher. Kerala and Bengal have minimum wages of Rs 130 and Rs 79.

 But in states like Rajasthan and Bihar, the party whole-timers are worse off than unskilled workers on the minimum wage. They are paid less than Rs 60 and often not at all.

 Which, perhaps, is why the full-time workers the backbone of any communist party are in short supply just when the CPM is making its presence felt in national politics and its overall membership is rising. The partys last political organisational report expressed concern at this.

 Many would see in the trend a sign of the times when the romantic call to rebellion can no longer inspire the youth to a life of sacrifice.

 The party report, however, skirted the big picture as it laid down the dos and donts for the cadre with an apparatchiks obsession for ritual.

 They should lead a communist life, it said. They must not accept gifts and compliments from others.

 MONOBINA GUPTA IN NEW DELHI




</TEXT>
</DOC>