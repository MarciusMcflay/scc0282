<DOC>
<DOCNO>1070907_opinion_story_8279288.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 Paperback Pickings

 Blood and the Bible

The last testament (Harper, Rs 295) by Sam Bourne is the latest addition to the religious conspiracy sub-genre. In April 2003, when the Baghdad Museum of Antiquities is looted, a boy runs away with an ancient clay tablet. Some years later, an Israeli scholar is shot dead by security agents as he pushes through the crowd towards the Israeli prime minister, on the eve of a historic deal with the Palestinians. But he holds a blood-stained note in his hand, not a gun. After a series of retaliatory killings, Maggie Costello, the American peace negotiator, discovers that the slain are historians and archaeologists, the only people with a key to the past. A mystery located in the last unsolved riddle of the Bible can end all hostilities in west Asia or plunge the world into an apocalyptic war. Despite the suspense, drama, realistic setting and scholarly games, the prose and the motifs are often trite, which is rather disappointing, since Bournes first novel, The Righteous Men, was good enough to be translated into 29 languages.

 Why Muslims Hate America...And WHat The West Can Do ABout It (Magna, Rs 225) by Arjun Das seeks to enlighten the West, and Americans in particular, about the myriad strands of Muslim disaffection with them. Das, a retired air force group captain, says nothing that has not already been said, and by greater scholars at that. In any case, Das should have used a different publisher and editor, given the grammatical errors and infelicities of style. The backcover blurb is unreadable, as is the very first sentence of Chapter 1: A moving map is not easy to draw but it is possible to draw a stretching outline of the Muslim emotionally dynamic spread of hatred for Americans.

 Urban Voice: Views amp; Visuals from a New World (Frog, Rs 195) boasts Ramachandra Guha and Shashi Tharoor among the contributors and offers an interesting assortment of essays, short fiction, poetry and analyses, as well as a graphic novel and a photo feature. Billed as an Orwell special, this thin volume carries three pieces by George Orwell, including the classic Politics and the English Language.

 The Waiting Room (Penguin, Rs 195) by Anupa Mehta is the story of betrayed and paranoid Maya and her unrequited admirer, Aniket. The key to Mayas state of mind lies in her psychoanalysts clinic, a place that left many others traumatized too. The narrative unfolds in the form of Mayas diary entries. In trying to make sense of Mayas scarred and short life, Aniket finds her love for Mir, the solace she found in Sufi thought and his own devotion to her, redemptive in themselves. Mehta does well to keep the tale short and focused and to avoid the stylistic monotony of many Indian writers in English.

Cultural History of Medieval India (Social Science Press, Rs 230) edited by Meenakshi Khanna belongs to the series Readings in History. The ten essays in this volume explore regional sub-cultures in the Indian subcontinent. Essays such as Popular Jokes and Political History and Popular Shiism, along with the rest, argue against dividing culture into binaries of great and little, of classic and folk. Elements within a culture not only interact with each other but also assimilate other elements across religious and cultural boundaries. Such intercourse defines concepts such as the self and the other. An educative collection, the book nevertheless could have done with a detailed note on contributors since its target readership is primarily students.




</TEXT>
</DOC>