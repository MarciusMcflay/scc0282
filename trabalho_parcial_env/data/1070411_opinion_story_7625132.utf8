<DOC>
<DOCNO>1070411_opinion_story_7625132.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 OUT OF PLAY

 Sreyashi Dastidar

 Subhash Chandra does not like being compared to Kerry Packer because Packer carried out a coup and when the Australian board agreed to give him the rights he abandoned the idea. We are not going to do that, the Zee TV chief has said. Chandra cannot be taken up on that yet, but he can certainly be congratulated for taking cricket away from the anglophone world. 

 This has been coming, ever since the headquarters of the ICC moved out of the Long Room at the Lords to Dubai, and an Indian or a Bangladeshi as ICC chief started looking normal. In the countries of its origin and initial spread, cricket had already lost out to other games, or was doing so rapidly. Nowhere was it more pathetic than in England. In the summer of 2004, members of an Ashes-winning English team were asked by British journalists to comment on the chances of Beckhams boys in the European Cup. It goes without saying that Beckham would never be asked a similar question before an Ashes series. 

 In Australia too, football had a similar effect on the following of cricket. In New Zealand, rugby has always been bigger than cricket. The West Indians too have embraced football and basketball in a big way (the islands can maintain their federal existence in these sports, unlike in cricket). 

 But what about the ICCs much-flaunted High-Performance programme and those preceding it, which are supposed to have swelled the ranks of cricket-playing nations? The truth is, the only success story of these programmes has been Bangladesh, a country that got test status after 1992. Kenya had begun with promise reaching the World Cup semi-final in 2003 but seems to have lost its direction since. Ireland has caused at least one major upset in the World Cup, but it must be admitted that the participation of the minnows has not exactly enhanced the level of competition. (The tournament is more of a platform where a few amateurs get to play with the pros.) 

 Small following

 It is important to note that the only new team that seems to be going anywhere belongs to the subcontinent, since a few decades from now, cricket may cease to be played outside the region. The World Cup will then be no more than a Saarc championship. If this sounds too radical, then a look at market indicators will suffice. The fact that Australian and West Indian cricketers endorse Indian products can only mean one thing that they are commercially more wanted in India than in their own countries. The low turn-out in West Indies, particularly after the exit of India and Pakistan, could be another nail in the coffin. Toying around with the format of the game, which has resulted in innovations like Twenty20, may be successful in the short- term, but their long-term life will depend on the response from India and Pakistan. 

 Chandra has, thus, entered a scene in which all the stakes are heavily loaded in his favour. His noble thoughts of grooming young cricketers and handing them over to the BCCI need not be taken seriously: having thrown the gauntlet at the BCCI, he will not be in any mood to help the board in its job. It is too early to conjecture about the Indian Cricket League, but overseas cricketers may be relieved to know that the subcontinent will be there to give them employment and endorsement contracts, when the game has ceased to be played in their countries. 

 A strange twist has been added to the tale by the BCCIs meeting last week. If the Packer series has any lesson for todays cricket boards, then it is about the foolishness of taking challenges from commercial players lightly. But instead of trying to keep the cricketers in good humour, the BCCI has done exactly the opposite. Surely the board realizes that ruffling the feathers of its immensely popular stars at this juncture would be to play into Chandras hands? Then why has the BCCI done it? Is there a more complex and sinister pattern to the events that hasnt met our eyes yet?




</TEXT>
</DOC>