<DOC>
<DOCNO>1070817_opinion_story_8200801.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 DADDY KNOWS BEST

 - SEZs and other iffy situations

 Cutting Corners - Ashok Mitra

 It is an ill wind that blows nobody any good. Viewed from that angle, the current controversy over so-called Special Economic Zones has at least brought to the fore a number of issues with respect to citizens rights and the rights of the State.

 We are supposed to be guided by the rule of law. The law, as it exists in the country at this moment, allows the State to acquire land belonging to private citizens for a public purpose. The act of acquisition is not supposed to be justiciable. However, the State has to offer adequate compensation for acquiring the property concerned; the amount of compensation is justiciable, and subject to the verdict of courts.

 A grey area nonetheless exists. Can the government step in and acquire anyones land and hand it over to any other person or entity and claim the act to be in consonance with public purpose? An extreme, but not altogether far-fetched, example will illustrate the kind of problems that may arise. Suppose there is a lovely bungalow with sprawling lawns on all its sides in a posh neighbourhood which belongs to X. His sworn enemy, Y, casts a covetous eye on this property. He has friends in influential positions in government and inveigles them to acquire the property by invoking the provision of the public acquisition legislation, eject X, and hand it over to Y. Can the entire process be described as reflecting a public purpose?

 Because of incidents at Singur and Nandigram in West Bengal and elsewhere the issue has now come up before the judiciary. One of these days, the nations highest judiciary can be expected to throw some light on the legality of the matter. Should the Supreme Court decide that the acquisition of 997 acres of land at Singur by the government of West Bengal to facilitate the establishment of a car manufacturing plant by the Tatas satisfies the criterion of public purpose, the judicial process would come to a surcease. In that event, even those who persist with their reluctance to surrender their holdings, and have refused the cheques offered by government representatives, would be left with no legal alternative; the government would have the legal sanction to apply force and remove them from their hearth and home.

 Let us, nonetheless, put our hand on our heart and further pursue the issue. Is there not life even beyond the ruling of the nations highest judiciary? In terms of our Constitution, the legislature has the right to change the law; within certain limits, our parliamentarians have the prerogative to introduce amendments to the legal framework, including the statute that currently governs the acquisition of land. Legislators are elected representatives of the people; they cannot afford to ignore the will of the people. If, at any point of time, a substantially large number of electors want the legislators to have a second look at the law pertaining to land acquisition, the law-makers will have no option but to take heed.

 Two separate aspects would call for clarification. First, should there not be a more precise definition of public purpose? To use the mechanism of land acquisition to serve the private interests of near and dear ones of those who are at the helm of administration renders the concept of public purpose a laughing stock. It is one thing to acquire land with the object of expanding a rail system or widening a road or highway or building a bridge or flyover which would contribute substantially to the enlargement of public welfare. But it is an altogether different proposition where public purpose is sought to be defined as serving the interests of a private entrepreneur and adding to his profits. In the long haul, the project for which a private entrepreneur is seeking the land would conceivably lead to a large-scale increase in employment and income in society. This is, however, an iffy situation. The benefits that might accrue to society at large would come in the distant future, or they may not come at all. Should someone not willing to give up his land be forced to do so on the assumption of future societal gains flowing from the project concerned? What discount factor should be deployed to assess the present value of the hypothetical future stream of income and employment, and who should be in charge of this exercise?

 The other problem is equally complex. Suppose there is a fresh statute with a fairly detailed definition of public purpose, a particular project satisfies all the criteria the new legislation has laid down and even the judiciary puts the imprimatur of approval on the new statute. Despite all this, a majority of those whose land is sought to be acquired are still not agreeable to surrender their property. Would such circumstances warrant the State to go ahead and forcibly take over the land? 

 Despite the certainty of a most impressive increase in their wealth and welfare, the majority amongst those whose land is involved are refusing to cooperate. Would the State be justified to compel them to give up their property? Compulsion is not exactly the ideal means to persuade dissenters to see reason; there is also a great danger of consequential recriminations leading to outbreak of violence. How should the government proceed where both law and the criterion of optimum social welfare appear to be supportive of a decision to implement the project and yet the majority of the people in the area hold other views?

 Consider an even trickier situation. Let us assume that the judiciary decrees the purpose for which land is being acquired to be very much in order and the majority of the people whose land is affected also ready to go along. But a minority of those whose land is proposed to be acquired continue to be recalcitrant and are determined to resist the process of acquisition till the bitter end. Should this minority in the extreme case, the minority might consist of just a single individual have the right to veto the project?

 Whether we like it or not, we travel here into the terrain of pure economic theory. Is there or is there not a social welfare function? The celebrated economist, Kenneth Arrow, would deny its existence. He has his famous Impossibility Theory, which claims that preferences of individuals comprising a society are impossible to be aggregated into a system of social preference. If his theory is right and he claims to have mathematics on his side all public activities in modern society ought to come to a halt even if a single individual refuses to cooperate with the government and disagrees with the views of the majority of citizens. Other economists would argue that since, in a democratic society, the majority elect the government, the latters decision should be final on issues of public importance. They would only add one proviso: when doubt arises on an issue, which is seemingly controversial, a special referendum might be held to thrash it out.

 The Arrovians would perhaps insist that such a procedure still does not establish the existence of a social welfare function, what is being proffered is only a State-determined welfare function. Ignoramuses will surely walk out of the debate at this stage and leave it to the logicians to battle it out. And in any case, the basic issue remains what it always has been: even where the State implements a project that will shower indisputable benefits to the community at large, a decisive majority of the citizens affected must be convinced, a priori, that it is indeed going to do so.




</TEXT>
</DOC>