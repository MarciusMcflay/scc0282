<DOC>
<DOCNO>1070511_opinion_story_7755261.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 MAN BEHIND THE AURA

 Man of peace

 Dalai Lama: Man, monk, mystic, By Mayank Chhaya, 

 Mapin, Rs 595

 He claims to be just an ordinary monk a Nobel Peace Prize and unending comparisons to Mahatma Gandhi are just a few indicators to the contrary. I met the fourteenth Dalai Lama myself, several years ago, when I was a child, and let me tell you, he is not ordinary. He is soft, he is enigmatic, and he is not easily forgettable. Add to that all one knows about this great monk, and you have someone who far surpasses the ordinary. But I suppose someone in his position, with his stature and magnetism, must tell himself that he is nothing special, in order to maintain some sanity. Mayank Chhaya, the author of this particular biography of the Dalai Lama, insists that the Dalai Lamas self-effacement is genuine, that the Dalai Lama truly sees himself, in many ways, at par with others in search of answers to challenging questions, as many of us are.

 One of the main attractions of this book lies in the fact that it is an authorized biography of the Dalai Lama, and is the only authorized biography written by a non-Buddhist. Chhaya, an Indian journalist, provides a deep understanding of the Sino-Tibetan conflict that triggered the Dalai Lamas exile to India, in 1959, where he still resides. And while Chhaya admits, in his introduction, that his book is written under the definite promptings of his conscience in support of Tibet and the Tibetans, he makes a heroic effort at outlining the complex Sino-Tibetan situation in a highly objective manner. He constantly brings to life both sides of an argument within the China-Tibet context.

 Because this is an authorized biography, the writer had almost unlimited access to the Dalai Lama a man who remains traditionally shrouded in mystery. These numerous encounters went far in unravelling some of those mysteries. Chhaya was able to ask the Dalai Lama some difficult questions, and his responses, and, more importantly, the way in which they were delivered, simplifies some of the seeming complexities surrounding the man.

 In his attempts to reveal the various facets of the Dalai Lamas personality, Chhaya effectively delineates a very human and humane being, a person one can easily relate to, and it is in these simple and fundamental human traits that much of the Dalai Lamas magic can be found this, I think, is one of the key messages of Chhaya. 

 More than 45 years after fleeing Tibet, the Dalai Lama is still operating from his government-in-exile in Mcleodgunj in India. He continues to champion the cause of an autonomous Tibet through non-violent means. Over the years, he has gained global recognition as one of our greatest leaders. The world will continue to watch closely as events unfold in Tibet, mediated by China and the Dalai Lamas heroism. Had it not been for the latter, the question of Tibets autonomy would have been crushed long ago. We look forward to an updated edition of the biography, which will hopefully tell us the rest of the story about arguably the most memorable Dalai Lama in Tibets history.

 PRIYANKA JHALA




</TEXT>
</DOC>