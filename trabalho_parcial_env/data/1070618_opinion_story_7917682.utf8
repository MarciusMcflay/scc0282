<DOC>
<DOCNO>1070618_opinion_story_7917682.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 LOOKING AT AFGHANISTAN

 - India is blind to the opportunities at its doorstep

 Commentarao S.L. Rao

 The author is former director- general, National Council for Applied Economic Research

 Indian network

 Afghanistan has become bracketed with Iraq and its instabilities. In Afghanistan, the mujahedin ousted the Soviet occupiers and then lapsed into civil war. Afghans welcomed the taliban because they brought peace after protracted turmoil. They then suffered the oppressively fundamentalist and conservative rule by the illiterate and brutal taliban. The American invasion has led in the last five years to much reconstruction and economic growth. Iraq was an artificial country created by the British while Afghanistan is an old nation with no secessionist movements despite being a highly individualistic people, with strong tribal loyalties and a fairly rigid Islam. As the gateway to Asia, it has been fought over but never conquered until the Soviet occupation, with help from quislings within. The taliban evicted the Russians but left their imprint of illiteracy and fundamentalist Islam on a whole generation of young people. An economy and a country destroyed by 30 years of Russian and then taliban rule are visibly rebuilding itself and fairly well.

 Past rulers used central funds and a strong army and police to keep the tribes in line. The new constitution is centralist. The army is still untrained; the police are a rabble; there is no honest judiciary. There is little semblance of the rule of law. Warlords dominate many provinces. There is rampant red tapeism and widespread corruption. Ministers vary in quality. The president is universally acknowledged to be lacking in vision and to be very weak. This makes the imposition of central authority very difficult.

 Afghanistan has no census, with no certainty about its population (said to be almost 30 million) and its composition. Yet, a recent study shows definite improvement (in relation to the last 20 years) in human development indicators like infant mortality. The economy is similarly believed to be growing. New industries have come up to supply the occupying International Security Assistance Force (mainly American) and the local population. But security continues to be a worry, outside (and also inside) Kabul. The south, on the Pakistan border, is the refuge of the taliban and is the worst for security, hampering development there. 

 At an international conference in Kabul sponsored by the Aga Khan on creating an enabling environment for private initiative, telecommunications was rated a major success. There are other smaller ones as well. In 2003, telecom was a national monopoly with 12,000 fixed line phones in the whole country, 50,000 mobile phones and 20,000 satellite phones. The cost per call was three to four dollars per minute. For long distance calls, Afghans had to trudge hundreds of miles to a neighbouring country.

 Today, there are 2.5 million subscribers, with five operators, one of the four private companies, Roshan, having 50 per cent share. Call rates today vary around 10 cents per minute for a local call anywhere in the country and 45 cents for international calls. The market is growing at 50,000 new connections per month. Roshan is the leader and the other operators follow its lead. It employs over 1,000 people of average age 23, and indirectly (outsourcing and so on) another 20,000. Except corporate accounts, almost all subscribers are pre-paid ones. As in India now, each has to submit personal details, but apparently the police have no access to cell phone calls except through the ministry. Telecom contributes to 10 per cent of government tax revenues. Roshan provides all employees with transport to work and back, lunch, and has significant community involvement in health, education, children and rural social work. Women are 40 per cent of this work force.

 High illiteracy, desire for communication, news and information explain the demand for cell phones. It will help to bind the country since the coverage is already over 45 per cent of Afghanistan. Roshan was promoted by the Aga Khan Development Network (as it did a five-star hotel and other investments) as part of its high-risk initiatives programme. The network is ready for its investments but it will take time in becoming self-sustaining. In Afghanistan, it is now doing very well. These investments have introduced new norms for behaviour and responsibility as well as skills in a workforce unaccustomed to such. There are said to be 16,000 local community councils. These constitute a tremendous resource for decentralized development, essential for a country as individualistic and tribal as this. Afghanistan must encourage private organizations to become active. Here, the existing network of local councils is ideal and must be more fully used.

 However, the government is bent on expensive new central and large projects. In the power sector, Afghanistan is ideally placed with its harsh terrain, climate and the nature of the people to generate power locally in small quantities for local use. The fuel could be portable diesel or even gas, and biomass. As with telecom, terrorists might not hinder movement of such fuels since everyone otherwise becomes vulnerable. But the government is focussed mainly on long transmission lines from neighbouring countries and large hydroelectric projects. 

 The workforce in the twenties-to -thirties age group is uneducated, having been trained to use guns. Few are prepared for menial work, most are expensive, demanding higher wages than similar workers in China or India. Considerable help and effort are needed to give such people literacy, skills and training. India, Bangladesh and Sri Lanka have the experience and thus can induct them into the workforce. Adult literacy programmes are urgently required. Distance learning might help. The use of television as in Indias satellite instruction for television education could help and telecom operators can deliver it. Literacy, computer usage and the internet could help spread other specialized education and skills.

 The taliban is growing in strength. Pakistan is a safe haven for them. The Afghan army is unable to suppress violence. But Afghanistan is not a basket case; nor is it beyond redemption as Iraq appears to have become. Strong leadership and policies more relevant for a nation composed of tribes led by warlords are necessary. But Afghanistan is saddled with a centralized constitution and large-project mentality. The few ministers who could be effective are more concerned with positioning themselves to succeed the present president than in demonstrating capability in their present assignments.

 In these circumstances, much cannot be expected from the government. Private initiative through business and civil society and decentralization are the answer. Roshan and the telecom sector and other investments have demonstrated this. More is required. The bureaucracy must be made responsive and corruption reduced. Strong external pressure can achieve some results as it did in China where Deng Xiaoping created a China Council of high-level foreign businessmen and experts meeting annually with Deng (and his successors) along with every top minister. It identifies problems and bottlenecks, monitors implementation and suggests new directions. In Afghanistan, where many ministers were educated abroad and are not politicians, they give time (unlike in India) for such meetings and listen to advice. Such a council could therefore be a strong lobby to move Afghanistan forward.

 Afghanistan is ill-served by the Western media, which focuses only on the disturbances and not on progress. The media in south Asia, particularly in India, have neglected Afghanistan. Despite Indian movies and film songs dominating the air, Indian products are invisible. No Indian private enterprise, politician, diplomat, government official, Chamber of Commerce or media attended the Enabling Environment conference. Pakistan had a big presence, with the prime minister flying in for the closing session. Afghanistan offers great opportunities in future years with its minerals, gas, location and virgin markets. India must not let Afghanistan slip away from its orbit as it has done with Myanmar.




</TEXT>
</DOC>