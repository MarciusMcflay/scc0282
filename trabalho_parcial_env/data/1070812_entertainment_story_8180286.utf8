<DOC>
<DOCNO>1070812_entertainment_story_8180286.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Entertainment

 Me, you amp; my blog

 Falling asleep so Ill crash now. Will post on Ghajini tomorrow. Cheers.

 Thats Aamir. Yes, Aamir Khan. Telling us you, me he is going to bed.

 Am a real admirer of Federer. Love the focus and passion with which he plays. I find that when I watch him play I feel inspired to do better work

 Thats Aamir again, after watching the Wimbledon final. And if you agree with Aamir and so many of us do you can tell him that. And he will respond on his blog. Yes, Aamir Khan blogs, at www.lagaandvd.com. Its really great to be able to connect with youll directly. First hand, no mediator, no newspaper, magazine, or TV channel, he writes in one of his early posts. We agree, Aamir.

 There is a delicious excitement to this which isnt at all about being star-struck. Aamir promises he reads all the posts. He only answers some, now that the response has become so overwhelming. Why would you want to gush about him, to him, when you could use the chance to recommend your favourite book, and have him recommend his to you? When you could dialogue with him about Fanaa, and why you didnt like it? When you could better understand why he didnt like Memento but would like to act in its Hindi remake?

 This is as personal as it gets. This is the wonder of the weblog. Which everyone who is anyone and everyone who is no one wants to be a part of.

 It is the web that is growing in power and influence. The new Web 2.0. Where anyone can go take part. You know its happening when governments want to ban them. The frequent shrill yelps about Orkut from various powers-that-be are a good example, says Peter Griffin, a writer and blogger, who is associated with a number of blogs.

 A blog is different from a website, more in spirit than in form. These are usually personal endeavours, which discuss personal issues, and few hope to profit from it. Blogs are easy to set up, easy to run. A blog is more a publishing device there is a blogger (or several bloggers, in the case of a collablog) who writes (or, increasingly these days, posts pictures, or audio or video; but it all boils down to a personal voice) and people come around to read, explains Griffin. And it gets bigger and bigger. If the blog permits comments, a forum, a community of its readers, can take shape around it.

 Blogs, however, are by no means homogenous. There are collablogs (where a number of people collaborate on a single blog), journal blogs (the personalised diary), video blogs, photoblogs, art blogs, the strange sounding moblogs or blogs updated from the mobile phone, and even a splog which is a blog used for the sole purpose of spamming! But somehow, the term has come to be associated with micro-sites that are in no way associated with commerce, which initiate debate, discussion and outrage over topics based on current affairs, literature, music, art, history, politics, comics, food and even pornography.

 I see blogs as part of the same discourse which saw a very large number of women in the 19th century maintain private diaries. Such private writing was variously a project of memory, therapy, a celebration of literacy and access to the tools of writing (by no means to be taken for granted) and an escape valve, says Abhijit Gupta, professor of English at JU and a t2 columnist.

 Teleute is an avid blogger. My blog kind of serves as a reminder for me if something interesting happens, I like to post at least a line or so. Quite like a diary. Feedback from other people, though, isnt necessarily what I am looking for. Now I blog mostly for my friends to read, and use the blog as a rant space theres far more angst than fun stuff on my blog, says the Calcutta-based student. She, like many others, does not sign off with her real name, and would not like her name to be used in this article either. Not only does this maintain a certain level of anonymity, it creates an alternative persona in the virtual world.

 But all blogs arent about soul-baring in cyberspace. 

 Theres a big blogging social scene, and lots of people use their blogs like other people use sites like Facebook, explains author and blogger Samit Basu. Ive been to a couple of blogmeets. In Calcutta, as far as I can see, its mostly a student crowd, and in Mumbai and Delhi its mostly young working people.

 There is a public service component to some blogs as well. Peter Griffins blog Mumbai Help was set up for all those affected by all kinds of emergencies in the metro, providing a parallel source of information to official channels. He has also been involved with blogs on the tsunami, bird flu and other disasters. Bloggers are into self-expression. They know how to search, to organise info, to network, to crusade. And these efforts were pretty innovative tweaks of the technology that was available, explains Griffin.

 There is also much talk about a connection between blogging and the publishing industry. But whether eloquent blogs will result in multi-million dollar book deals being struck is doubtful. Samit Basu, author of The Simoquin Prophecies and The Manticores Secret, calls blogging micropublishing in its purest form. But the West is completely saturated with blogs, reducing chances of being picked up by publishers. In India, though, the scene is more hopeful. The Compulsive Confessors (another active Indian blogger) book got commissioned because of her blog. And because so many foreign publishers are coming in and everyones looking really hard for new voices, you do have editors taking a look at some of the more popular bloggers, says Basu.

 The leap from Net to page may not be desirable, either.

 A blogger who writes well need not necessarily make a good playwright or novelist. Samuel Pepys for example did not write much else other than his diaries. But what diaries! We are able to read him without hankering for Pepys the poet or Pepys the playwright, says Abhijit Gupta.

 Whatever else it has achieved, blogging has definitely succeeded in creating a bond between bloggers. Blogs do tend to help in fostering ties between a very diverse demographic. I suppose there is something about sharing thoughts with a person unknown to you, other than the fact that you read each others blogs and therefore to some extent share each others lives that creates a kind of intimacy that is difficult to define, says Teleute.

 So whether you want to impact public opinion on a burning social issue, or share your love for the latest Bollywood flick, make a cosmic connection about absolutely nothing or share your angst with your best cyber pal, the Net has room for you. All you need to do is log in, type it up and hit publish.

 And if you want to wish Aamir Khan goodnight, you know how.

 How to do it: It is easiest to host your blog on a blog-hosting service where all the required software is provided by the developer. There is a wide variety of these services to choose from and the more popular ones are Blogger, Wordpress, Myspace, Livejournal and Windows Live Spaces. You have to create an account with some dedicated blog server, choose your blog address, and get started. Most sites have basic templates. Once you master the art, you can create new templates.

 There really are all kinds of blogs: One of the award-winning blogs is Random Thoughts of a Demented Mind, started by the Great Bong in August 2004. He joins together a whole community of people who agree, disagree, squabble and argue over various strands of thought on issues like the Presidential elections, human rights abuses against minorities, David Dhawanism....

 Blogs are used for a variety of reasons: On Professor Supriya Chaudhuris blog http://novelandmodernity.blogspot.com/, class lectures are published to enable students to continue their coursework even when the professor is in another country. Then there is Abhijit Guptas The Good Loo Guide a collablog (http://thegoodlooguide.blogspot.com) which lists loos in major cities of the world along with ratings, softness of toilet paper, loo reads, loo graffiti, loo art with special emphasis on Calcuttas offerings.




</TEXT>
</DOC>