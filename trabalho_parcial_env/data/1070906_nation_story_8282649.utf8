<DOC>
<DOCNO>1070906_nation_story_8282649.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 French twist to Partition: Attlees the villain

 AMIT ROY

 Attlee

 London, Sept. 5: Indian loyalty to the Labour Party, especially to Clement Attlee, the British Prime Minister from 1945-51, could be seriously misplaced.

 In the UK, generations of Indian voters have continued to support Labour in one general election after another out of a sense of emotional bonding with the party that granted freedom to India. In fact, Labour could not have held many a key marginal seat without the Indian bloc vote.

 But historian Patrick French, who delights in challenging conventional wisdom, says the man to blame for the chaotic manner in which Partition was executed in India is not Lord Mountbatten, but the Labour Prime Minister who took the decisions back in London Clement Attlee.

 French told The Telegraph: Blaming Mountbatten is a bit like blaming Bremer for the troubles in Iraq you know the decisions have been taken by Bush in Washington.

 This is a reference to L Paul Bremer III, who was a de facto viceroy in Iraq when he was head of the Coalition Provisional Authority from May 11, 2003, until June 28, 2004 and made a mess of his responsibilities.

 My central point is that despite the disastrous legacy left by Winston Churchill in India in 1945, the blunders of 1947 were Attlees responsibility, not Mountbattens, French asserted.

 Discussing the crucial Indian electoral support Labour has been able to take for granted, French argued: I think it arose less from the virtues of the Labour Party in the 50s, 60s and 70s and more from the legacy of 1947 and the fact that until (John) Major came along, the Tories were linked to racial prejudice in the eyes of many UK Indian voters. 

 But in terms of economic policy, (Margaret) Thatcher was far closer to the thinking of most of the diaspora.

 Mountbatten 

 Lord Swraj Paul was at the centre of the storm because he had confirmed he would give whatever I can afford to Labour before the next general election because he believes in Gordon Brown, who has succeeded Tony Blair, who received three cheques for 125,00, 2 million and 2 million from fellow steel tycoon Laskhmi Mittal.

 Mittal was emphatic in his support: I am a long-term supporter of the Labour Party and the work it has done in the UK to improve the overall prosperity and prospects of the country since coming to office in 1997.

 As for Paul, he explained: I am a member of the Labour Party since 1974. I joined because I was building a plant in Michael Foots constituency (Ebbw Vale in Wales) and I was so impressed with his statesmanship, his principled life, so I liked the man and joined the party.

 French, though, says Foot did not cover himself with glory during the 1975-77 Emergency in India he gave Indira Gandhi knee-jerk support.The historian has just written a newspaper article in which Attlee has been called the unnamed guilty man of Indias slaughter.

 The line, which French is due to repeat in forthcoming discussions in India, is that on the 60th anniversary of Indian Partition, the part played by Clement Attlee in facilitating mass killing is still ignored.

 French writes: Clement Attlee, quiet and uncharismatic like John Major but without the circus background makes an unlikely villain, but he was responsible for the key decisions. 

 He goes on: Attlee hated the camera as much as his last viceroy, Lord Mountbatten, loved it. A recent biographer repeats the common claim that in India he (Attlee) achieved what virtually no one else, in any country, has achieved, before or since: to withdraw in good order from a vast slice of Empire.

 This is palpably untrue: through his action and inaction, Attlee facilitated mass slaughter but never took the blame for it. In later life, he said that giving India Independence was his greatest achievement. As Stalin observed, one death is a tragedy, the death of millions is a statistic.

 Although in old age Mountbatten liked to pretend he had been given plenipotentiary powers, it is apparent from cabinet minutes that the crucial decisions were all taken by Attlee. 

 According to French, Attlee gave Cyril Radcliffe, a British barrister, six weeks to invent a new dividing line. This uncertainty over boundaries was the proximate cause of the mass migration and ethnic cleansing.

 On the day when Radcliffes boundary was announced, Attlee held an emergency cabinet meeting over a currency crisis: the meat ration was cut, foreign holidays were banned and the convertibility of sterling was suspended. India was the last thing on his mind. 

 Today the carnage is often presented as the product of atavistic rivalries and larger historical forces, rather than the consequence of gross political incompetence. The BBC2 epic The Day India Burned: Partition has the best broadcast interviews I have ever seen with survivors and perpetrators of the 1947 massacres, but ignored Attlee altogether.




</TEXT>
</DOC>