<DOC>
<DOCNO>1070218_calcutta_story_7401490.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Write choice, celeb siblings

 The script can make or unmake a star-studded commercial 

 Shiloo Chattopadhyay

 The sister is visiting the den of his brother. While rummaging through memorabilia, she discovers the first recognition that the brother got as an actor. She wants to hang up the certificate but does not like the look of the wall and paints it afresh. The brother returns home, notices and gives the sister a hug. Sorry, no prizes for guessing the stars or the brand. They are the celebrity siblings Soha and Saif. And yes, the paint is Royale from Asian Paints. 

 Ads with a star enjoy myriad benefits. Attention-grabbing power, endorsement, rub-off of an image attribute of the star on to the brand. No wonder there have been attempts to pack in more than one celebrity into an ad. Presumably, the logic is more the merrier. Budgets may have come in the way of some such intentions. A couple of the other ads that readily come to mind feature Hema Malini and Esha Deol (water purifier) and Big B and small B (car). 

 The mother-daughter ad was pure endorsement, but thanks to the script, neither lady sounded convincing. The Versa campaign with the Bachchans, on the other hand, had a lot of comedy. But it stopped there, and did not try to go beyond and bring to fore the father-and-son bonding.

 The Royale ad is different in this aspect. Not only do we have two successful actors in the ad, there is much more that has been done with the two, thanks to the script. Let us not forget that Saif is going through one of the best phases of his career. From Dil Chahta Hai to Parineeta to Omkara, the man has displayed tremendous range and has caught the fancy of critics and consumers alike.

 Soha, post Rang De Basanti, is a loved face credited with capability to display histrionic talents. Just having two of them together in an ad may have been considered a good enough ploy. But the ad was made to do something more. It is less about Saif and Soha, and more about the unique connect that many siblings enjoy. The ad matches its quantity of star presence with the quality of emotions portrayed. True, the emotions acquire a far stronger dimension because it is between two celebrity siblings. Yet one can hardly argue that the real star is the emotion itself rather than the stars.

 This is not the first such ad that Saif has lent his presence to. It is difficult to forget his earlier guitar ad for the same brand where Sharmila Tagore, his mother, was omnipresent without a single frame being given to her. Nor can we fail to remember the Lays ad in which Saif and his father Nawab of Pataudi played pranks while watching a cricket match. 

 What does the brilliance in such ads do to brands like Royale? Paints or tyres are unfortunate product categories that are not always in the forefront of consumer mind. When you have that one puncture too many, you think of replacing your tyre. Or when after many Diwalis you feel you can afford to give your home a new look, you think about paints. How do you engage the consumer in the interim years of lean interest? 

 The Royale campaign provides a simple answer to that question. Creativity is a potent weapon against consumer inertness. 




</TEXT>
</DOC>