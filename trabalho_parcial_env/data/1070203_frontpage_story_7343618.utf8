<DOC>
<DOCNO>1070203_frontpage_story_7343618.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Earth cannot afford humans

 OUR SPECIAL CORRESPONDENT

New Delhi, Feb. 2: Global warming driven by human activities will continue for centuries despite corrective action, the latest scientific assessment of climate change released today has warned. But scientists in India said the future behaviour of the monsoon remains unclear. 

 A summary of the fourth assessment report of the Intergovernmental Panel on Climate Change (IPCC) issued in Paris today contains what scientists say is the strongest evidence yet that humans are responsible for global warming, sea level rise, and changing weather patterns. 

 Human interference with the climate system is unquestionable, said Rajendra Pachauri, chairman of the IPCC, an international agency engaged in evaluating evidence for climate change. What humans have done over the last couple of hundred years is unprecedented. It hasnt happened over tens of thousands of years, Pachauri told The Telegraph on the phone from Paris. 

 The report said global warming is unequivocal, evident from observations of rising temperatures, widespread melting of snow and ice, and rising sea levels. It said 11 of the last 12 years (1995-2006) have been the warmest years since 1850. 

 Scientists have attributed global warming to rising levels of heat-trapping gases such as carbon dioxide, methane and nitrous oxide in the atmosphere. While the burning of fossil fuels is the main source of increasing atmospheric levels of carbon dioxide, the report said, methane and nitrous oxide are primarily due to agriculture. 

 The report said the average global temperature may rise by 1.1 C to 6.4 C over the next 90 years, predicting melting Arctic and Antarctic sea ice, rising sea levels, intensification of cyclones and extreme weather events. 

 The global warming and sea level rise would continue for centuries even if carbon dioxide levels were stabilised because it takes very long to remove the gas from the atmosphere, the report said. 

 Some components of climate change have become inevitable, said Pachauri. 

 Indian scientists who were part of the IPCC exercise said most computer-based climate models predict an increase in monsoon rainfall when the rise in only atmospheric carbon dioxide is taken into account. 

 But, in reality, we also have to factor in the effect of dust particles in the atmosphere soot and sulfates resulting from industrial activity, and natural dust from the land and salt dust from the oceans, Jayaraman Srinivasan, a senior atmospheric scientist at the Indian Institute of Science in Bangalore and one of the 450 lead authors of the IPCC report said. 

 Until the effect of dust is put into the models, its going to be difficult to predict exactly how the monsoon will behave, said Srinivasan who had evaluated the climate models used to predict the future of the planet. 




</TEXT>
</DOC>