<DOC>
<DOCNO>1070703_frontpage_story_8007038.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Not cricket but a cracker of a game

 - Grenade-throwing contest set to make India debut in October 

 JAYDEEP BASU

 New Delhi, July 2: India has seen Shoaib Akhtar. Now it can see more explosive stuff, bowled off a Bishan Bedi run-up.

 Grenade-throwing as a sport will come to India for the first time when the Fourth World Military Games are held in Hyderabad and Mumbai in October.

 Apart from ordinary events like athletics, swimming and wrestling, the Games - with 5,000 contestants from about a hundred countries - will have grenade-throwing as part of an event called the military pentathlon, said Air Commodore S.C. Joshi, secretary-general of the organising committee.

 The spectators neednt fear for their lives the specially made hand grenades will set off low-grade explosions and will contain no splinters.

 Watching from a safe distance at the Hyderabad artillery base, where the pentathlon will be held instead of the Gachibouli Sports Complex, cricket fans may be tempted to ponder if the national genius isnt better suited to this alien sport.

 For one thing, unlike batsmen facing the Shoaibs and Brett Lees, theres no question of being at the receiving end. This should be a relief, although Bollywood heroes have been known to expertly lob back grenades hurled at them on screen.

 That the grenade isnt expected to bounce is ideal for Indian wickets. But Indians should choose their sport carefully.

 A member of the Games technical committee said the event would be split into two categories: distance and accuracy.

 In the first category, he who throws the farthest, wins. Some contestants can throw a grenade more than 100 metres. They are not Indians, the official shrugged. The throwers from China, Germany and Austria are much better we stand no chance.

 In the accuracy category, an iron ring will be placed on the ground 25 metres from the starting line, the official said. You have to land the grenade right over a given spot inside the ring.

 That should be easier than landing a cricket ball on a tea leaf, a legendary ability said to have been shared by an army of Indian spinners from the magnificent Bedi to the miserly Bapu Nadkarni.

 The parallels with spin dont end there. In either form of grenade-throwing, the run-up is kept short in keeping with battlefield constraints and also because the throw must be rather flat. And once the thrower has pulled the safety pin out, theres no time for the fast bowlers 20-stride approach, anyway.

 Like cricket, the right grip and a free-flowing action are the key.

 The importance of properly gripping the hand grenade cannot be overemphasised, says the Garys US Infantry Weapons Reference Guide before explaining the nuances.

 Hold the grenade upright with the pin away from the palm so that it can be easily removed by the index or middle finger of the free hand.

 Garys says a thrower mustnt stifle his natural style by being too bookish. As long as, that is, his body is facing sideways, towards the enemys position, and he throws basically overhand.

 And no bending the elbow, please.

 The technique could be a little different for women, who will be given lighter grenades (460gm against the mens 675gm) to throw at the Games. But one thing is common: the grenade must describe an arc.

 The non-throwing hand is held at a 45-degree angle with the fingers and thumb extended, joined, and pointing toward the intended target.

 Allow the motion of your throwing arm to continue naturally once you release the grenade, Garys advises. This follow-through improves distance and accuracy and lessens the strain on your throwing arm.

 The venerable gentlemen in the Long Room at Lords would nod approvingly.




</TEXT>
</DOC>