<DOC>
<DOCNO>1070611_opinion_story_7842024.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 WASTE NOT, WANT NOT

 - To make the reduction in the cost of education meaningful

 TAPAS MAJUMDAR

For more than forty years now actually ever since the Kothari commission (1964-66) broached the idea people in India have been living with a contradiction of their own making. On the one hand we, at every level of our political life, including that of even prime ministers and ministers of education, have always expressed our simply unshakeable determination to devote to public spending on education precisely six per cent of our national income. To tell the whole truth, nowadays an even more important-sounding number is being canvassed at the higher echelons of government it is six per cent of the GDP. In the case of a large domestic economy and only a small foreign transactions sector like Indias, the distinction, of course, is still important though it makes no difference, as one of Shakespeares characters wisely commented on stage in a somewhat dissimilar scene.

 On the other hand, our Central and state governments, particularly the Planning Commission and the down-to-earth finance ministries, no matter which party ruled the roost and where, always and equally steadfastly pleaded that there was no money with the government anywhere near six per cent of the GDP to spend on education. Fortunately, I hear, the Planning Commission is now also telling other contenders that after having to allocate such fantastic amounts to the social sector and for very good reasons, they will soon have no money left for the others.

 Let me first place my problem in its proper and current context. Thanks to India shining and the GDP growing much faster than what sensible economists had expected, the current share of public spending on education that never rose to even 4 per cent in its history keeps steadily falling, and has now fallen to around probably 2.7 per cent of the GDP. By the time you digest this piece of information, the percentage might have gone further down. I hear that people at the very highest level are now openly wondering why anyone had to think up that entirely imaginary six per cent in the first place. An interesting question. A long answer to the question will make an essay that would not interest you. But the short answer should be sufficient.

 There were two government of India committees on the cost of education that I had the opportunity of chairing in recent years. The first was the 1999 expert group on the cost of universal elementary education implicit in the impending constitutional amendment to make the right to education a fundamental right. The second committee was the one of 2005 to discuss alternative cost scenarios, following the national common minimum programmes commitment of six per cent of the GDP to education. In both, we actually tried not to concentrate on the percentage of the GDP needed but on the countrys minimum requirements spelt out in real terms, given the parameters of the objectives in each case.

 We had tried to list the minimum requirements that we saw were still not available such as at least two classrooms in every elementary school, with two full-time teachers, and the provision of such schools for all children with similar basic facilities. In the case of higher education, we stressed on facilities such as good laboratories and libraries in college or university departments along with competent and reasonably well-paid faculty, comparable to the minimum provided at any good university (not necessarily the best) in other parts of the world.

 Our findings were, briefly, the following. The states existing commitment of six per cent of the GDP for the entire education sector, and thus the allocation of roughly one-third of that to elementary education, looked sufficient since the economy as a whole was growing. For the higher education sector, however, especially for subjects in which our students were, or could be, internationally competitive, more was needed. But, in both cases, good housekeeping and husbanding of resources were imperative and we tried to indicate a few ways and a few ways out. 

 I may point out here that the target of six per cent was thought of in the Indian context forty years ago, after looking around the table and noting what the state spent on education in each of the worlds educationally-advanced countries. Analogy is not logic. Nevertheless, let me complete this particular analogy. Six per cent of the GDP is many times larger per capita in a rich country with a small population than in a poor country thinking of educating a billion people. The case for Indias education sector, thanks to past neglect, has therefore become manifold stronger today.

 But I should also add the statutory warning. In any sane scenario, the share of allocation for education goes up steadily year after year, not all at once, for this process cannot be rushed without inviting horrendous scandals. Investment in education builds human capital. Like Rome, the human capital of any country was not built in a day.

 I was reviewing all this in my mind as I listened to the experts on yet another advisory committee, called recently to find ways of reducing our projected costs of education. There was some relief expressed because of an unexpected, and still little understood, demographic projection. Simplifying it a bit, for the year 2004-05, Indias children in the age group 6 to 13 years is estimated by the Census commissioner to be 194.6 million. But for 2014-15, this estimate unexpectedly falls to 189.0 million. That will be 5.6 million school children less to care for! If this is a natural trend reflecting rational parental choice, then it may well be good news at last. But could it be the growing gender disparity that was affecting child birth rates? Not being a demographer, I didnt know what to think.

 I would, however, like you to think of not just this but of a more meaningful reduction in the cost of education. Remember the Central bill to activate the fundamental right to education that had to be shelved? Suppose it were revived? Suppose also the revision allowed a fast-tracking of students that let the most talented or the most hardworking to skip a couple of years on their way up in the educational ladder? Any successful fast-tracking would automatically cut costs all the way.

 The types fit for fast-tracking that come to mind immediately are: (a) brilliant academic achievers who could reach their chosen professions faster if they wanted to; (b) serious students who might be glad to work harder to go through school (and college) and pass out as fast as they can, and join the work-force securing early openings and seniority in the administrative or other civil, military and management services; and (c) the equally serious students of average merit who might be induced to put in extra hard work for joining the workforce at the middle levels quickly to meet an economic constraint.

 If pursued fairly, the provision of fast-tracking could not only encourage quality and hard work in teaching and learning, but also help India to be more competitive in the global knowledge market. Funnily, this would not be such a new thing for us either. Those of my students who graduated in the early Fifties made their mark in all fields years earlier than their equivalents coming up fifty years later, although for no fault or deficiency of the latter. All our students start now at Class I as five-plus or six-year olds and would be routinely disqualified, however talented, if, as under-age, they tried for a degree or admission at any point higher up unless an imaginative high court cries foul, as Delhi thankfully has done recently, invoking the 86th Amendment and the new Article 21(A) of the Constitution.




</TEXT>
</DOC>