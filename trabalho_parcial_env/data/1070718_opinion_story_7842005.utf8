<DOC>
<DOCNO>1070718_opinion_story_7842005.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SELLING POINT 

 - Professionalized military procurement is still elusive in 

 India 

 Brijesh D. Jayal

 IAFs Su-30 aircraft: 

 show time

 The latest report of the parliamentary 

 standing committee on defence, tabled in April, has expressed 

 concern over the depleting combat fleet of the Indian air 

 force, which stands at 33 squadrons against the authorized 

 39.5. It has also noted the delayed progress on the proposed 

 acquisition of 126 multi-role combat aircraft. Considering 

 the time taken for such inductions, it appears inevitable 

 that IAF force levels will decline further to 28 squadrons. 

 Vayu Bhawans cup of woes does not end here. Recently, the 

 Russians, who are learning the tricks of the globalized 

 arms trade faster than others, have dropped a bombshell. 

 They want to renegotiate contract terms not only for the 

 recent top-up Su-30 aircraft order, which was intended to 

 shore up the depleting force levels, but even the earlier 

 sealed contract. In the fiercely competitive arms marketplace, 

 it appears that even the sanctity of contracts is becoming 

 a casualty, leave alone sentiments of long-standing friendships 

 and procurement relationships. 

 It is ironic that on the day there 

 were celebrations marking three years of the present government 

 in office, the findings of the comptroller and auditor general 

 on an audit of capital acquisitions by the ministry of defence 

 for the period 2003-2006, were reported. The audit looked 

 into the reasons leading to the defence ministrys surrender 

 of Rs 3,500 crore out of the allocated capital budget. The 

 ministrys justifications cut no ice and the CAG concluded 

 that the structure of the defence acquisition organization 

 is inefficient and ineffective and it is unable to spend 

 what it is allotted. 

 These strong observations merit 

 deep introspection. Soon after the present government took 

 office, this writer had, in this column, reflected optimism 

 as the manifesto of the leading party had claimed that it 

 would ensure that all delays in the modernization of our 

 armed forces are eliminated and that funds budgeted for 

 modernization are...spent to the fullest. Never, to ones 

 knowledge, had any party manifesto been so sensitive to 

 and specific about the armed forces modernization needs. 

 There was indeed hope.

 These reports, therefore, come 

 as a huge disappointment. Whilst it must be said that the 

 present CAG audit also covers a part of the period of the 

 previous dispensation, the issues stretch beyond specific 

 governments. They reflect a frozen bureaucratic mindset, 

 one impervious to the crying needs of the fighting services. 

 That there is awareness among the highest echelons of the 

 government is not in doubt. It is the failure to understand 

 the complexity and dynamics of the international arms trade 

 and to introduce changes to leverage participation to national 

 advantage that is disappointing.

 Pursuant to the recommendations 

 of the group of ministers on reforming the national security 

 system, a dedicated structure for defence procurement was 

 set up with the specific goal of achieving better time-cost 

 management in the acquisition process, building up institutional 

 memory and helping obtain better value for money. In this 

 revised organization, the defence minister chairs the defence 

 acquisition council with respective secretaries chairing 

 the acquisition, production and research and development 

 boards. 

 This was followed in 2006 by the 

 release of the defence procurement procedure for capital 

 acquisitions by the defence minister, which introduced, 

 among other things, the provision of offsets in contracts 

 exceeding Rs 300 crore, and an integrity clause to eliminate 

 middlemen. The defence minister even expressed the hope 

 that during the Eleventh Plan period, Rs 45,000 crore would 

 become available by way of offsets. Clearly, the offset 

 clause appeared the high point of this procedure. If reports 

 are to be believed, there are already discordant voices 

 from the industry.

 In theory, we have instituted 

 a comprehensive procedure. We now expect things to fall 

 in place. In the international arms marketplace, things 

 dont quite work out that way. We already have a case in 

 court where, notwithstanding the variant of the integrity 

 commitment, warring vendors have in court confirmed that 

 commissions were paid on a helicopter contract. As for offsets, 

 these are noble in intent but highly complex in content 

 and execution, and in their benefits. In a researched paper 

 titled, Arms Trade Offsets amp; Development, Jurgen Brauer 

 and Paul Dunne, find virtually no case where offset arrangements 

 have yielded unambiguous net benefits for a countrys economic 

 development. This issue needs to be meticulously studied, 

 taking into account larger national interests and the merits 

 related to each individual case. It certainly cannot be 

 a blanket requirement. 

 Initial optimism, born out of 

 realization at the highest levels of government of the need 

 for modernization and commitment towards its achievement, 

 is giving way to disappointment. Recently, the parliamentary 

 standing committee on defence was critical of the functioning 

 and performance of DRDO. Based on this, an independent performance 

 audit committee has recently been set up. Here was a clear 

 pointer that even the reorganization was not delivering 

 on expectations. The latest standing committee report further 

 strengthens the argument that there are deep fissures within 

 the system that do not yield to superficial tinkering. If 

 our armed forces are to graduate to a brave new world of 

 efficient military procurement and modernization, then drastic 

 surgery is the answer.

 Thanks to Bofors and Tehelka syndromes, 

 few of those in transitional seats of authority really have 

 their hearts in soiling their reputations in the procurement 

 business. They would prefer to complete their tenures keeping 

 their reputations intact rather than risk taking decisions 

 and being hounded thereafter. A change of mindset is needed 

 in every echelon of decision-making. But that is easier 

 said than done.

 Another issue relates to the complexities 

 of defence modernization in general and defence procurement 

 in particular. The CAG report has highlighted that defence 

 acquisition is a cross disciplinary activity requiring 

 expertise in technology, military, finance, quality assurance, 

 market research, contract management, project management, 

 administration and policy making. In spite of this, our 

 defence acquisition process is managed by people who are 

 serving a tenure posting for two to three years in the course 

 of which they will need to deal with industry professionals. 

 Also, the system remains confined only to the defence ministry. 

 A start can best be made by professionalizing the entire 

 organization through educating and well-trained cadre dedicated 

 to this profession.

 If these are the challenges, then 

 every superficial action that is taken supposedly to streamline 

 and document procedures towards efficient procurement will 

 become a hurdle towards achieving our objective. The reorganized 

 high-level structures can only be as good as the staff inputs 

 they receive. Similarly, the defence procurement procedures, 

 while being perfect in intent, will take flexibility out 

 of the hands of professionals who, when indulging in complex 

 technical and commercial negotiations, must be as agile 

 as their counterparts across the table. Often in these hard 

 negotiations, fleeting opportunities and trade-offs have 

 to be grabbed. How does one negotiate when every initiative 

 taken may be construed as motivated and when every small 

 point must be referred to the highest levels for endorsement?

 Arms trade in the modern world 

 is capital- and technology-intensive and cruelly competitive. 

 It casts a shadow over every facet of national activity, 

 be it defence research and development, trade, commerce, 

 economy, security or diplomacy. It must meet clearly defined 

 national strategic objectives. It is a task for professionals 

 in this field. The American department of defence runs a 

 Defence Acquisition University, whose stated mission is 

 to provide practitioners training, career management and 

 services to enable the acquisition, technology and logistics 

 community to make smart business decisions and deliver timely 

 and affordable capabilities to the war fighter. Until we 

 recognize this fundamental need, and move rapidly towards 

 professionalizing defence procurement, we will continue 

 to stumble from one crisis to another.

 Considering that the nation still 

 awaits a national defence university, a proposal which the 

 cabinet has accepted, another defence university might be 

 a distant dream. If so, the nation should be prepared for 

 continued ad hocism in military modernization and 

 forget the objective of delivering timely and affordable 

 capabilities to our war fighters.




</TEXT>
</DOC>