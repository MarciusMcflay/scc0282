<DOC>
<DOCNO>1070416_business_story_7652553.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Business

 Twilight blues

 Why are insurers punishing our elders, asks Srikumar Bondyopadhyay

Elderly people are liabilities in a country where everybody is counting on demographic dividend for future prosperity. Seventy-two year old Nikhil Mukherjee has been made to believe so by his insurer. 

 Insurance companies are raising their premium rates for senior citizens by almost 100 per cent, forcing them to either discontinue their medical insurance plans or reduce the sum assured.

 Mukherjee bought a mediclaim of Rs 3 lakh for himself and a Rs 2.5-lakh policy for his wife (61) from a public sector insurer eight years ago. 

 In these eight years, we made no claims against our mediclaim policies and last December, we paid a total premium of Rs 13,500, Mukherjee said. I heard that insurers were planning to increase the premium rate for their medical insurance plans. On inquiry, I learnt that I would have to pay over Rs 26,000 as premium this December. But I simply cannot pay that hefty premium. I dont get a pension my only income is from interests on my retirement benefits, he added. 

 So the option is either I reduce the sum assured or quit the plan altogether. Why cant the government stop insurers from penalising old people? he wonders.

 Troubled times

 The Mukherjees and all those who are above 65 years of age might see a 100 per cent rise in their medical insurance premium. And if you think you are lucky not being that old, think again. The medical insurance premium is going up progressively from 5 per cent to 80 per cent for those who are in the lower age group. 

 Only people who are below 25 years can really take heart from the latest move by the 12 general insurance companies in the country. The insurers are actually reducing the premium rates for this age group by 20 per cent. Insurers last raised their premium rates by 6 per cent in 2003.

 High rates

 The four public sector insurers National Insurance, Oriental Insurance, New India Assurance and United India Assurance are mulling a steep hike in their rates, particularly for people above 60 years. Private sector insurers already charge higher rates. 

 Oriental Insurance had also revised its rates in October last year. 

 For the Varistha Mediclaim launched in December 2006, National Insurance had factored in the rate hike in the premium. The other two public sector insurers, New India Assurance and United India Insurance, have also filed their revised rate proposals with the insurance regulator. 

 Age bar

 Insurers are also reducing the maximum age for buying a fresh medical insurance policy. Following the launch of the Varistha Mediclaim, National Insurance Company is issuing fresh mediclaim policy only to people up to 59 years of age compared with 65 years earlier. While existing policyholders can renew their policies till 80 years of age, people who are above 59 years and seeking a mediclaim policy for the first time will have to take a Varishtha Mediclaim, said Dilip Burman, director and general manager, National Insurance Company.

 Similarly, United India Insurance has reduced the maximum age at entry for its mediclaim plans to 55 years from 60 years earlier. 

 Most private insurance companies dont offer fresh medical insurance to people above 60 years. Only Bajaj Allianz offers Silver Health to people in the age group of 46-75 years. Sorry, we cant offer an insurance to your 72-year-old father. But your mother, who is 62 years, can get one. For her, the premium for a Rs 2-lakh mediclaim policy will be Rs 11,203, said a Bajaj Allianz executive to Dipankar Banerjee, who wanted to buy medical insurance for his parents. The premium which Bajaj Allianz quoted was Rs 3,000 more than what National Insurance is asking for the same sum assured even after its rate hikes, he said. 

 The message is clear. Older people will now have to pay more for their medical insurance. 

 Why?

 The insurers, however, have their reasons. For the last several years, we havent increased the premium rates though healthcare costs have gone up, said Burman. Moreover, against every Rs 100 that we get as premium, we have to pay Rs 130 as claim. Such a high claim ratio is unsustainable for any company. 

 I know a lady who claimed and got Rs 70,000 just to get her tooth plucked. This is unethical, agreed Mukherjee.

 Any way out?

 Check out group mediclaim being offered at a lower premium than individual mediclaims. Some public sector banks are offering low-cost medical insurance to their account holders. But again, the maximum entry age in most of these group mediclaim policies is 60 years. 

 Senior citizens like the Mukherjees, who have not made any claims against their policies, can take advantage of the accrued non-claim bonus and reduce the level of sum assured to keep their premium outgo at the previous level. Insurance companies increase your sum assured by 5 per cent for every no-claim year. Mukherjee himself is now entitled to a sum assured of Rs 4.2 lakh (Rs 3 lakh + Rs 1.2 lakh no-claim bonus). On renewal of his policy, he can opt for reducing his original sum assured to Rs 1.5 lakh (the sum assured he would be entitled to is Rs 2.1 lakh) and pay a premium of Rs 8,323.




</TEXT>
</DOC>