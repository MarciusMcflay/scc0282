<DOC>
<DOCNO>1070622_frontpage_story_7957559.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 Cinema paradiso

 Flashback

 SOUMITRA DAS

 The facade of New 

 Empire. A file picture 

 One of the most cherished memories 

 of my childhood is a vision of the great magician PC Sorcar 

 (Sr) turning the entire three-storeyed auditorium of New 

 Empire into an underwater kingdom of Nile blue. I was totally 

 mesmerised by that beautiful watercolour vision. 

 In another act, the entire hall 

 would be filled with chattering and jiving skeletons, and 

 as the show reached its climax Sorcar (Sr) would suddenly 

 disappear from the stage with the speed of a whirlwind and 

 reappear in a jiffy on the third floor balcony. From there 

 he would wave out at the cheering audience.

 I had always wondered if this 

 was merely a figment of my lurid imagination. But when I 

 cross-checked with Saoli Mitra, who had begun to tread the 

 boards along with her parents and the Bohurupee group from 

 early childhood, her memories concurred with mine.

 For further nostalgia deep-diving 

 she asked me to get in touch with Debtosh Ghosh, a veteran 

 actor now with Pancham Vaidic, who, by his own admission, 

 was mad about movies particularly Hollywood and European 

 films.

 The peculiarity about New Empire 

 is that its main auditorium is on the first floor where 

 the front and rear seats are situated. The balcony and dress 

 circles are on the second floor, and on the third floor 

 are the galleries. In the past the seats were covered with 

 thick red corduroy. Once the walls were dressed from third 

 floor downwards with beautiful red textile. Looking glasses 

 hung strategically on the walls afforded a good view of 

 the entire hall, says the 74-year-old Ghosh who appeared 

 in City of Joy.

 PC Sorcar (Sr) performing 

 the famous act of slicing a woman 

 into two at New Empire. A file picture

 Resident Europeans and Anglo-Indians, 

 who had formed the Dramatic Club of Theatre, staged contemporary 

 hits such as Look Back in Anger and Dial M for 

 Murder. The expensive and heavy sets for these productions 

 were made in the backyard and lowered onstage by means of 

 a pulley. If the set depicted a room the doors used to be 

 so heavy they never quaked when slammed.

 Apart from concerts of Western 

 music by international stars, this was the hall where Bohurupee 

 and Uday Shankar used to hold their performances. Marcel 

 Marceau held his first performance in Calcutta here.

 This hall is connected by a gangway 

 to Lighthouse which has turned into a clothes market. Both 

 halls belonged to Humayun Theatre. In Lighthouse, the floor 

 sloped downwards from the screen. Nandan imitated this architectural 

 feature.

 Jon Lang in his book, A Concise 

 History of Modern Architecture in India, wrote that 

 Willem Marinus Dudok (1884-1974) had designed Lighthouse 

 (1936-38). The theatre/cinema, now much altered, still 

 recalls the De Stijl work of the Dutch architects of the 

 period. The buildings bold contrasts between solid surfaces 

 and voids, between vertical and horizontal lines and its 

 use of balconies and circular windows (now largely gone) 

 are clearly within the De Stijl mould.

 Stage performances used to be 

 held at Globe, too, which was once an opera house. If one 

 looks at the rear of the hall from Sudder Street one can 

 still catch sight of a legend to that effect in giant letters.

 Parsi theatre used to be held 

 at Elite. Then in 1948 it was reconstructed. Renoirs River 

 was screened here and giant arc lamps were installed. The 

 Scandinavian ice revue dazzled us, says Ghosh.

 Continental films were screened 

 at Regal and Hindi hits such as Anarkali and Baiju 

 Bawra were shown at Society. Ghosh never mentioned it, 

 but I distinctly remember Tiger Rag, the signature 

 tune of Tiger cinema, now a clothes bazaar. Theatre personality 

 Tarun Roy had tried to revive it by staging plays but in 

 vain.

 It was not for nothing that once 

 Metro on Chowringhee was known as The home of the stars, 

 as the print media advertisements went. The finest of cinemas 

 anywhere, it was owned by Metro Goldwyn Mayer or MGM till 

 the early 1970s.

 According to the website cinematreasures.org: 

 It was the first of two cinemas to be designed in India 

 by the noted New York-based (Scottish born) architect Thomas 

 Lamb, the other being the Metro Cinema in Mumbai in 1938, 

 again for MGM. Lamb drew up plans for this Metro Cinema 

 in 1934 and it opened in 1935. It is one of the finest 

 examples of art deco in India. 

 Thomas Lamb also designed the 

 deco styled Metro in Cairo, Egypt. The Metro cinema, Calcutta, 

 is almost identical to the Metro Theatre, Adelaide, South 

 Australia.

 With its thick, red pile carpet, 

 huge art deco lamps hanging from the ceiling and bar it 

 became a byword for luxury. The walls were covered with 

 fresco in Metro. Before the hall became dark, the lights 

 dimmed gradually and the effect was enchanting, says Ghosh. 

 During the screening of King 

 Solomons Mine the six-ft-plus doorman, Sarasi Bhushan 

 Chattopadhyay, had to dress up as a Zulu chief in complete 

 regalia.

 Ghosh mentioned a theatre in Dharamtala 

 opposite Sacred Heart Church, which few have even heard 

 of. I saw Kurosawa films here. Film societies used to hire 

 it. Actor Chhabi Biswas had staged a play here, says Ghosh. 

 Many pearls did Ghosh extract from memory. 

 soumitradasabpmail.com




</TEXT>
</DOC>