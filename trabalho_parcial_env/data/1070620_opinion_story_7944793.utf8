<DOC>
<DOCNO>1070620_opinion_story_7944793.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 THE RISE AND THE FALL 

 Dipankar Bose

 Fifth Column

 Experts in trade and industry 

 as well as in the media feel that the present interest rate 

 is too high to sustain the growth momentum of the country. 

 They have been emboldened by the fact that inflation in 

 wholesale prices has fallen from 6.73 per cent in mid-February 

 to 4.85 per cent by end-May. Nevertheless, a high base during 

 the corresponding week in the last year and expectations 

 of normal monsoon contributed significantly towards the 

 fall, apart from a marginal decline in inflation for the 

 primary articles.

 So the interest rate may be lowered 

 under pressure from the most powerful lobby of the nation, 

 ostensibly to maintain growth, as if the rate of interest 

 is its main determinant. However, a lower interest rate 

 and high inflation in retail sales can generate speculative 

 demand in a country where banking supervision is weak. The 

 more-than-a-year-long inflation that has been going on is 

 largely speculative in character, thanks to the huge circulation 

 of black money and the low interest rate along with easy 

 availability of credit.

 The real estate sector provides 

 a good example. The demand for housing, fuelled by low interest 

 rates and the overenthusiasm of the banks for disbursing 

 housing loans, rose steeply since 2004. The speculators, 

 sensing their opportunity, jumped into the fray and soon 

 turned the situation into one of severe excess demand, availability 

 of urban land being limited and that of black money being 

 unlimited. The result was a blistering growth in the housing 

 sector with prices reaching an all-time high.

 Soaring high

 The Reserve Bank of India did 

 caution the banks by raising the risk weightage of real 

 estate loans. Later, it raised the lending rates thrice 

 during 2006. But, even at 9 per cent, up from 6 per cent, 

 the demand for housing loans was unflagging and the loans 

 soared from Rs 16,000 crore in 2001 to Rs 1,86,000 crore 

 in 2006.

 The major factors behind this 

 phenomenal growth were the low default rate of less than 

 1 per cent that made housing loans the safest lending option 

 for banks, the availability of black money and inflow of 

 foreign funds. The situation is reversing now and the non-performing 

 housing loans of the banks have crossed 3 per cent.

 The problem surfaced in early 

 2006 when some banks discovered that they had financed the 

 same property. In October 2006, the Indian Banks Association 

 asked its members to provide information on the frauds they 

 had encountered. The frauds were committed by fabricating 

 income documents, encashment of loan cheques by third parties, 

 forged title documents, overvaluation of property to secure 

 larger loans and multiple financing.

 Recently, the finance ministry 

 has banned external commercial borrowings for integrated 

 townships of 100 acres and above, the only area for which 

 ECBs were allowed for the real estate sector. This is a 

 welcome step, considering the alarming rise in ECBs, but 

 it has come too late in the day. Net medium-term and long-term 

 external borrowings increased from $1 billion in 2005 to 

 $13 billion in 2006. A mere fraction of it could spell disaster 

 for the housing market.

 Financial liberalization has been 

 introduced in a country, which had already been flooded 

 with black money. The sharp rise in housing loans only added 

 to the speculation. The inflow of foreign funds and a low 

 interest rate led to a property boom which pushed up prices 

 in urban areas. 

 However, under the present circumstances, 

 a downtrend is inevitable. The oversupply and the present 

 high rate of interest regime may lead to the creation of 

 half finished real estates projects and even defaults. The 

 authorities should have thought twice before introducing 

 financial liberalization at such a large scale. Nevertheless, 

 rising income levels will hopefully put a stop to this process.




</TEXT>
</DOC>