<DOC>
<DOCNO>1070902_sports_story_8265917.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Collingwood: I like to challenge myself

 - Well see in four years at the World Cup whether Im successful, says England ODI captain 

Sue Mott

 Paul Collingwood 

Paul Collingwood is a thoroughly modest man. He had an appointment and he kept it. Lesser mortals would have taken on board the fact that their 11-month-old baby had passed on a horrible virus, that the England team doctor had ordered him to bed and that he was in grim, stomach-clutching pain. They would have thrown a wobbly of some dimensions, ordered a wheelchair and eye-mask and refused all contact with outside, especially journalistic, life. But the captain of the England one-day cricket team is made of sterner stuff. 

They are 3-1 up against India. One more victory and they have the series won. The side looks young, nerveless and exciting. It is far too early to judge, but spineless surrender and scatterbrain implosions seem to be featuring far less than of yore. 

Yet Collingwood had never been a captain before, and there are many more yets than that: Yet he had been mocked by Shane Warne for receiving his Ashes MBE after a bit-part in only the fifth Test against Australia in 2005 Yet he had seemed to be more of yeoman stock than a hugely talented star. . . Yet after he made his Test debut in 2003 against Sri Lanka, he was neither a fixture nor a fitting in the side. He was in and out like the drinks trolley. The selectors seemed to view him as putty, someone to shove in a gap where necessary. Matthew Hoggard used to say he was selected solely on the grounds that his iPod had speakers. 

But the slow-burn, no-nonsense work ethic of Collingwoods County Durham upbringing finally wore down all resistance. The ODI captaincy became vacant, due to Michael Vaughans inconsistency, and it looked like a straight fight between Colly and Kevin Pietersen, a contrast of fairly momentous proportions. In the event, KP withdrew. 

My appointment, I guess, was a little bit of a surprise. Even to me, said Collingwood, manfully keeping grimaces of his discomfort at bay. I had no aspirations to be England captain. I have always been a just-happy-to-be-in-the-team sort of player. 

Im not successful yet. Well see in four years time at the World Cup whether Im successful. Hopefully, Ill be captain that long. 

Were making inroads on our approach. Peter Moores has been brilliant since he came in. We do a lot of skill work. Its the teams with skill that win. Look at Brazil in football. You want team spirit and determination as well. You play your best one-day cricket when you go in there with a fearless attitude. But I think when youve got a fear of failing, youre holding yourself back at least 20 per cent. 

Ive felt that fear of failure massively. Its in every professional sportsman somewhere. I always realised I didnt have the best technique. Im not Brian Lara or Sachin Tendulkar. The crowd doesnt say, What an amazing shot that was by Collingwood! when Im batting. I score my runs in my own manner. But in the mental side of the game I can make up a bit on the talented players. 

I most felt that fear of failure in my one-day series debut. It was the biggest test of my career. You spend all your life trying to play for England. Then I actually got the chance against Australia and Pakistan in 2001. I was very, very nervous. I was there with Alec Stewart, Darren Gough, Nasser Hussain, Mark Ramprakash, all these names who were my heroes growing up. I probably didnt think I belonged there, to be honest with you. I didnt do very well (he was out lbw for two, and 18 runs were scored off his two overs of bowling) but its how you bounce back that matters. 

Ive always believed you have to see the depths of despair before you get to the top. Then your achievements mean more to you. Its more satisfying. Above all, I like to challenge myself. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>