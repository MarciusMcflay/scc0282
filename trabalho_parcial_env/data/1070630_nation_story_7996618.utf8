<DOC>
<DOCNO>1070630_nation_story_7996618.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Gandhi sale on track, Delhi looks to law

 - Question of copyright

 AMIT ROY

 Members of the Gandhi Sena and students of Mahatma Gandhi School during a silent protest at Gandhi Ashram in Ahmedabad on Friday. (PTI)

 London, June 29: On Tuesday when the documents and letters collected by the late Austrian banker Albin Schram go on sale at Christies, there will be a little more excitement than might have been anticipated about the fate of Lot number 379.

 As things stand today, the auction house in London is adamant that the sale of a draft article by Mahatma Gandhi, written only 19 days before his assassination and in which he had pleaded for greater tolerance for Muslims in India after Partition, will go ahead as planned.

 A spokesman for Christies said: There is no reason in law to stop the auction. Its provenance is known and legal. This is a public auction. Anybody is welcome to come along and bid for it.

 The auction will start at 10am but Lot 379 the seven-page draft of an article for the Urdu Harijan, containing amendments and corrections in Gandhis own hand may not come up until 2pm.

 In India, various Gandhians have woken up to the reality of the sale rather late in the day and have appealed to Manmohan Singh to ensure the document returns to India. 

 Instructed by the culture ministry in Delhi, the Indian High Commission in London has gone through the motion of enquiring whether the sale can be stopped or the document bought privately before the auction but diplomats have been told neither option is now possible.

 The Christies spokesman instead revealed an interesting piece of information to The Telegraph. This document has been bought in auction twice before in London once in 1984 at Phillips and then in 2002 at Christies, he said. Both were public auctions.

 The implication is that the Indians have not been particularly bothered in the past about acquiring what they are now desperate to get their hands on.

 On the second occasion, Gandhis draft article (which has been wrongly referred to as a letter in some reports in India) went into the collection of Schram, who lived in Switzerland and had gathered handwritten letters and documents over a 30-year period.

 Tuesdays auction will include 570 letters and documents linked to many of the most notable figures of European history from the 13th to the 20th centuries, including Lord Byron, Winston Churchill, Charles Dickens, Elizabeth I, Sigmund Freud, Napoleon, Sir Isaac Newton, Oliver Cromwell, Claude Monet, Oscar Wilde and Charlotte Bronte.

 The auction of The Albin Schram Collection of Autograph Letters is expected to raise 2m for the bankers estate, though his family members were surprised to discover the valuable papers tucked away casually in a drawer.

 The spokesman said: In the last week, there has been more publicity about the Gandhi document than anything else.

 Before the controversy, the Gandhi article was estimated to fetch between 9,000 and 12,000. But now the price may be forced up by the extra publicity, especially if representatives of the Indian government enter the bidding.

 If two people bid, the auction will continue until one stops, the spokesperson pointed out. He added: Some people have said this was Gandhis last letter it wasnt.

 On the larger question of preserving national heritage, it is fair to say that Britain today is committed to ensuring that objects of art and value are not plundered from poor nations, especially in war-affected countries, and smuggled out for sale to wealthy art collectors. 

 However, Britain, which is a store house of treasures from all over the world built up in the 18th and 19th centuries, draws the line at actually having to return them. 

 The question of copyright on Gandhis article is trickier, lawyers say. Although Gandhi has willed all his property to the Navjivan Trust in India, a document that was not illegally obtained by a westerner may now belong to those who have inherited his estate to do much as they please.

 A great many Indian objects of value and sentiment come up on a regular basis in the London auction market, without stirring much official Indian interest. Some may well have been smuggled out post-Partition but most are sourced from private collections in Europe. 

 The only way to recover Indias lost treasures may be for Indian institutions and collectors to buy them back on the open market and this seems the only practical solution to getting back what is undoubtedly a Gandhi article of great emotional and historic value.




</TEXT>
</DOC>