<DOC>
<DOCNO>1070913_opinion_story_8307599.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 Letters to Editor

 Long days journey

 Sir As soon as rumours were floated about Sachin Tendulkars possible retirement after the one-day series against England, they sent shockwaves through the country and its cricketing fraternity (Sachin vows to stay on, Sept 7). Tendulkars wife was promptly hounded by journalists, though she, as expected, denied that her husband is considering quitting. It is interesting that several former cricketers have commented on the post-match exhaustion and symptoms of general burnout that Tendulkar finds increasingly difficult to hide. Whether Tendulkar quits soon or not, the media will continue to speculate on his fitness, his skills and the probable time of his departure. The reason is that Tendulkar is not merely a great cricketer, he is an icon, a cult figure and a marketing brand. While there is no denying that Tendulkars batting is past its prime, his records will not be broken for a long time to come, especially in the shorter version of the game. Besides, Tendulkar has been a great unifier of Indians, who are wont to take differences in class, caste and creed a little too seriously. Perhaps the British media spread the rumour of his retirement because he was tormenting the English bowlers. 

 Yours faithfully,

Tapan Das Gupta, Digha

 Sir Sachin Tendulkar apparently did not talk of retirement at all, but it is true nonetheless that he was finding it difficult to recover from one-day international matches. Former cricketers of great calibre, such as Bishan Singh Bedi and Sandeep Patil have agreed that, despite his performance in England this season, Tendulkar these days has to make an effort. The implication of this is obvious that the little master is not going to be around for long. Anybody who has followed his career closely will have reached that conclusion already. It is, however, unfair to blame the British press for spreading the rumour of Tendulkars retirement. That was the handiwork of a Marathi daily. 

 Yours faithfully,

Gagan Roy, Howrah

 Sir All that Sachin Tendulkar is guilty of were some innocuous comments about exhaustion and his admission that he is not as energetic as he used to be. The resultant panic seemed strange, since those who panicked are the same people who were baying for his blood after Indias poor show at the World Cup last year. Even former cricketers, who were talking of comic heroes while referring to Tendulkar then, are now singing his praises. Some of them, like Bishan Singh Bedi, who are prone to making unsolicited noises, will still complain about the lack of effortlessness in Tendulkars recent performances. But how do these people criticize Tendulkar at every turn and yet expect him to be as fluent as he was in his younger days? Perhaps they have forgotten that he is human after all. 

 Whenever Tendulkar goes through a bad patch, his detractors, and sometimes even his fans, jump up to suggest that he should quit. After all these years, every time Tendulkar goes out to bat, he still shoulders the expectations of a billion people, who still spare no opportunity to criticize him. One day, sooner rather than later, Tendulkar will indeed stop playing. The least we can do now is allow him the space and time to make that decision for himself. 

 Yours faithfully,

Priyanka Aich, Calcutta 

 Sir The hullaballoo over Sachin Tendulkars rumoured retirement made me realize that the best Indian batsmen are all on the verge of retiring. What will happen to the famed Indian batting line-up once Tendulkar, Sourav Ganguly and Rahul Dravid retire?

 Yours faithfully,

Shama Parveen, Calcutta

 Children of men

 Sir The logic behind compelling children, through legislative action, to take care of their parents is difficult to understand (Right to home for parents, Sept 9). As someone past the age of 50, I find the exercise derogatory and humiliating. Are parents of my generation parasites that they need to survive on others charity? Or did we make babies as a long-term investment, hoping to make, in taxmens jargon, long-term capital gains? First, having a child is the parents decision. The unborn childs consent is not sought in the process. The baby does not beg for a womb, nor does it seek the assurance of lifelong square meals. Most important, it does not choose its parents. Therefore, the child cannot be held responsible for a decision to which it was not a party. 

 Second, these political and social diktats target the middle class. The super-rich and the very poor always remain beyond such guidelines. An objective assessment would show that middle-class child rearing is heavily subsidized in India working mothers get paid maternity leave. When the mother goes to work, domestic helps, who are paid peanuts and are often children themselves, look after the child. Childrens education is often state-sponsored or employer-reimbursed, and there is an income-tax rebate on childrens tuition fees. Parents end up contributing only a tiny fraction of the cost of child-rearing while society foots most of the bill. But parents get a lot of benefits. The birth of the baby boosts their prestige in a society that looks at childlessness almost as a flaw in ones character. Then the parents brag about their childs achievements, academic or otherwise. If todays children were made to repay the costs of their upbringing, then paying society at large, by helping philanthropic organizations would be more appropriate.

 Besides, a childs life is his own, and not an extension of his parents. When parents disapprove of their children having separate households, they are encroaching on their childrens basic rights. Why cant parents accept responsibility for making monstrous kids who torture and deprive them, instead of going to the judiciary to ask for free meals? It is unfortunate that the parliamentary committee is holding their brief. I would rather starve to death than legally compel my son to pay for my meals and boarding. Besides, dont we hear of mothers jettisoning their kids for careers in show business, or alcoholic fathers raping their daughters? 

 Yours faithfully, 

Tapan Pal, Batanagar




</TEXT>
</DOC>