<DOC>
<DOCNO>1070706_opinion_story_8021066.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 SINKING FEELING

 It never rains, but pours. And it floods and drowns. Calcuttans are never required to recall this fact of city life more than once a year, when a sustained spell of heavy rain over a few hours or days during the monsoon turns the city into a gigantic drain. The weather gods have been more demanding this time. Two bouts of heavy rain in less than a month have impressed upon citizens the need to jog their memories more frequently, and inflict the same exercise on the civic authorities who determine the quality of their lives. Unlike last month, the deluge this time has not only left large parts of the city submerged in waist-deep water, but also left fears of a health epidemic unless the water is flushed out and the drinking water cleansed of refuse. The mayor of Calcutta, Bikash Ranjan Bhattacharyya, treats this as a normal occurrence in Indian cities, and derives immense satisfaction from citing Mumbai and Bangalore as examples. Practical difficulties in flushing out water into a river during high tide, the incapacity of the colonial sewerage or the technical snags that delayed the pumping out of the water are supposed to somehow temper the evidence of inefficiency on the part of the institution he heads. But it is doubtful if the city will forget in a hurry the fact that the impact of the prolonged showers could have been lessened significantly if Mr Bhattacharyyas men had used the crucial winter months and the imported machinery to desilt the sewer arteries and got ready to prevent more water-logging.

 The mayor would have us believe that the city would be ready to take on such weather challenges by 2009, by which time the drainage channels would be unclogged. Given the civic authorities remarkable respect for schedule, Calcutta is unlikely to get over its nightmare soon. Besides, there is a lot more to achieve than just a sprucing up of the drainage system. To keep away solid waste and plastic from the sewers or to prevent encroachment of surface drains, the administration must show less tolerance to civic irresponsibility. And since even the most stringent of laws cannot keep the city dry and clean unless there is greater awareness and compliance on the part of the public, it is necessary that citizens pay more attention to their habits and habitat even when it is not pouring.




</TEXT>
</DOC>