<DOC>
<DOCNO>1070617_calcutta_story_7933754.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Quite contrary

 Increased visibility has not increased tolerance for the gay community

 Films are right: (From top) 

 stills from My Brother Nikhil, Brokeback Mountain and Happy Together, which portray gay relationships in a humane way 

The bride is Manoj Shah, a model. The groom is Amit Pandey, a fashion choreographer. They have announced that they are going to be married. We wanted to give our relationship a name and pledge our loyalty to each other, says Amit. 

 The two are worried that they will not have the law on their side. Section 377 of the Indian Penal Code criminalises sex between two people of the same gender as an unnatural offence. If needed, we will move out of the country to legalise our relationship, says a nervous Amit. 

 Pawan Dhall of Sathii, an NGO working to prevent AIDS, describes the couple as brave. A lot of gay couples have had private ceremonies and live together as married, but this is the first time that a gay couple is going public about their decision to marry. Law may not recognise it but it cannot stop two consenting adults from living together.

 The gay community is far more visible in the city, compared to a decade, or even five years ago. Immediate circles, especially friends, seem to be more tolerant. Amit and Manoj have been open about their relationship with friends and family. All my model friends were happy for us, though our parents dont approve. They have not accepted our decision to marry, but are okay with our sexual orientation, says Amit. 

 Awareness boom 

 Has greater awareness ensured greater acceptance? Has coming out made life better for the members of the gay and alternative sexual communities? Amit and Manoj are very lucky. 

 Increased visibility has led to new problems. There has been no let-up in the violence against the lesbian, gay, bisexual and transgender (LGBT) communities -- some feel it has increased. Two kotis (koti is a term to describe an effeminate man; originally derogatory) were murdered in the last two months. There have been several reports of abuse. 

 There is greater resistance to ones attempt to claim space. In schools and colleges, harassment or ragging based on a persons orientation has shot up. Earlier, a boy who did not seem manly enough was called meyeli or sakhi in jest, but today one is immediately branded as homo on a serious note, says Anindya Hajra, a member of NGO Sanhita, who also runs a resource centre called Pratyay.

 Safe places 

 Its important to be in a safe place. But how many safe places have opened up for a person who can be identified as gay? 

 The creative professions showbiz, fashion designing, film-making, the arts have always been tolerant. Several well-known personalities from these fields dont need to deny their sexuality.

 Entertainment zones shopping malls, multiplexes and nightclubs provide a freer feel. More or less. But they can be hostile. I was recently stopped from entering a new resto-pub with a friend. The reason was my long hair. Other men with long hair, supposedly in fashion, were allowed in. Probably another gay man who isnt identifiable would have gained easy entry, says Anindya.

 Being in a liberal, supportive workplace or home environment is safe. Says Bishan Samadder, who works as a programme co-ordinator for an international NGO: Coming out has not been an issue with supportive friends and family members. At work, Im around people educated in a western model with an open outlook.

 Yet such a place can be hostile, too. There have been times when being frowned upon for being effeminate or the lack of conventional masculinity has been a cause of insecurity rather than my desire for a man, says Bishan. 

 Hostile zones 

 And the government job or a corporate one (not very high up on the ladder)? A couple of months ago, my friend, a software professional, came out when he went public with a sexual harassment case in his office. His workplace has turned very hostile since and colleagues often gang up and ridicule him, says Anindya.

 A person I know lost his government job two months ago after telling a colleague that he is gay. The colleague went and complained against him to the boss. He suffered a nervous breakdown, says Dhall. 

 Public transport? When dealing with taxi drivers or using public transport I adopt a more straight and harsh attitude, says Pronoy, a 25-year-old media professional.

 Many of the participants in the Rainbow Pride Walk organised every year in Calcutta to mark Stonewall Riots Day the 1969 riots that galvanised the gay rights movement in the US have later faced discrimination at workplace or among friends and relatives.

 Platforms galore

 Yet alternative platforms are multiplying. The Rainbow Pride Week and Siddhartha Gautam Film Festival on AIDS and sexuality organised by Saathii have witnessed a growing participation from members of the LGBT community.

 The first Rainbow Pride Walk organised in 1999 had 15 participants, while this year the organisers are expecting nearly 400 people to join the parade, including many from the districts. About 150 people attended the Siddhartha Gautam Film Festival in 2003. This year, were holding it in four cities and expect around 800 people to turn up, says Dhall.

 Consciousness of identities is widespread. NGOs hand out lists that define the different identities: LGBT, Queer, Transsexual(ity), Transvestite (Crossdresser), Hijra, Koti. While some feel these labels are Western hand-me-downs, others feel that naming oneself, ones own community, is a major first step towards empowerment. This year will also witness the first LGBT film and video festival in the city, which is being organised by two support groups. 

 Mainstream films, quite perceptibly, have opened up. Films like My Brother Nikhil and Rules Pyaar Ka Superhit Formula make a statement instead of showing homosexuality in a poor or jocular light, says Dhall. It is essential to feature alternative sexuality in mainstream films to depict gays as regular people. Honeymoon Travels in recent times has done that in a smart and subtle way, says Bhaskar.

 Paradox 

 A year ago when some members of the gay community were putting up posters for World AIDS Day, they were stopped by a group of local boys. Both parties ended up in the nearby police station to file a complaint. The boys who were identifiably gay were harassed, their clothes were torn and their mobile phones taken away, says Anindya.Humiliation and mockery are a part and parcel of our life. 

 Some of it may have to do with Calcutta. I used to work in Bangalore before I shifted to Calcutta two years ago. The more cosmopolitan a city is, the less it tends to be bothered about a persons private life or sexual orientation. In Calcutta people are always a little too inquisitive about my orientation, says Sumit, a software professional in an MNC. 

 But the communitys experience now also mirrors any movement trying to claim space, says Anindya. Its like sounding the death knell for the traditional Indian family. There is panic that the world will turn queer and a large part of homophobic violence arises out of this anxiety and fear. Its one right pitted against another. 

 And Bengali has still resisted a popular word that is an equivalent of gay vernacular Bengali would dismiss a gay person as a homo, rather than refer to as a somokami. 

 (Mail your feedback to sunday.metroabp.in)

 MOHUA DAS




</TEXT>
</DOC>