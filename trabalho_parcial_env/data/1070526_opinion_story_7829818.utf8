<DOC>
<DOCNO>1070526_opinion_story_7829818.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 UPON THAT MOUNTAIN

 What mountaineers call the death zone above 8,000 metres on the Himalayas engenders its own code of ethics. This code is obviously based on a set of assumptions different from those that determine codes of behaviour under normal circumstances. Notions of kindness and comradeship acquire radically different dimensions in conditions in which the brain is deprived of oxygen and in which even the smallest gesture and the most natural of activities demand enormous effort. Reactions under such extreme circumstances are different, and responses are often very delayed. It is not fair to expect normal behaviour from people at over 8,000 metres. Nothing is normal in such abnormal conditions. Thus the rescue of Usha, a Nepalese woman who had been found 500 metres below the summit of Everest, can only be described as heroic. David Hahn, a veteran mountaineer, and Phinjo Dorje, a Sherpa, decided to rescue Ms Usha. By performing a duty they have made themselves heroes because they did it high on the slopes of the worlds highest mountain.

 But this act should not lead to a condemnation of those who follow a different code of ethics in the death zone. Such a condemnation would be too facile a shift, and too easy a judgement to make. In a very real sense, above 8,000 metres with or without oxygen a human being is completely alone. He is alone because he has chosen to be alone in an elemental battle against the naked forces of nature. Climbing in the death zone is considered the ultimate test in courage and endurance precisely because in that height a man is pitted against the mountain. Success in the death zone ascending the summit is a triumph of human will. Those who choose to go for this battle know in their innermost soul that up on the mountain one is alone. Thus the ethic of helping another person in trouble is somewhat irrelevant. Very few mountaineers expect to be helped. The risk of trying to help is death. Often the exhaustion and the responses of a brain starved of oxygen go against the good intentions of lending a helping hand. These are not factors to be scoffed at from a high moral ground. Reinhold Messner did not have the physical and mental resources to go and look for his brother when both of them were caught in a blizzard on their way down from Nanga Parvat. Above 8,000 metres, life is not black and white: the ethics there are thus different and beyond ordinary judgment.

 Some mountaineers think of their own survival first. Others cannot help not reaching out to those who are in danger. There are no absolutes above 8,000 metres. It is significant that such ethical issues have acquired an urgency only after high-altitude climbing has become commercialized. Too many non-mountaineers are on the slopes every season and they do not understand that ethics are fluid in the death zone. Masters of the ice craft ponder ethical questions if at all only when they are back in the security of the base camp.




</TEXT>
</DOC>