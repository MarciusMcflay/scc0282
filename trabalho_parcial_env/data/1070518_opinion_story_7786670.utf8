<DOC>
<DOCNO>1070518_opinion_story_7786670.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FOR A SCHOLAR OF REPUTE

 Bookwise

 The big book

Take four relatively new books published late in 2006 or early in 2007: Amartya Sens Identity and Violence: The Illusion of Destiny; William Dalrymples The Last Mughal: The Fall of a Dynasty, Delhi 1857; Will Huttons The Writing on the Wall: China and The West in the 21st Century and Ramachandra Guhas India after Gandhi: The History of the Worlds Largest Democracy. In publishers parlance, these are big books, that is, they would sell in large numbers and go into several reprints. These books prominently display extracts from advance reviews by leading scholars in the respective fields. In other words, these books are endorsed for their scholarship and relevance before being formally released. 

 However, are endorsements a new phenomenon, and do they help sales? Are the reviewers approached by publishers in collaboration with the authors? 

 Endorsements have long been an essential marketing tool. But earlier, they meant extracts either from the authors earlier books, or, in the second and subsequent editions, extracts from reviews of the first edition. For instance, Graham Greenes The best spy story I have ever read ... on the cover of John Le Carrs The Spy Who Came in from the Cold was an extract from his review of the first edition; it was not a pre-publication endorsement. Today, reviewers are sent either a proof-copy or the manuscript for their comments, as advised by the author. The relevant extracts are then published in the prelim pages or the covers. They are also used as publicity material in advertisements. 

 Endorsements are now an integral part of marketing for a simple reason. The flood of new books leaves most readers little time to browse and choose; they need word-of-mouth recommendations or endorsements from scholars who would read the book and then write what they honestly felt about it. 

 The value of an endorsement rests on the name and scholarly reputation of an endorser. However, in India, where networking is an insidious game, the danger that slipshod work would get through is always present. Thus, publishers have to collaborate with authors in scrutinizing a list of potential reviewers and offer the review to the most reputed. 

 But, are these reviewers paid to write what they do? The answer is both yes and no. The publisher and the author would like a favourable review, preferably with quotable phrases. A fee is certainly paid to the reviewer for the time and care given to the review. How much or how little that amount is can never be known but, in some cases, pretty generous fees have been paid not as much as the advances we hear of, but not too far behind either. 

 RAVI VYAS




</TEXT>
</DOC>