<DOC>
<DOCNO>1070525_opinion_story_7814386.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 FROM THE HEART OF DARKNESS

 White mans burden

 Red River By Lalita Tademy, 

 Headline, pound;11.99

 Lalita Tademy takes us on an unforgettable journey into the American Deep South, set in the Reconstruction era. Her novel starts with, and builds from, the cruel and traumatic Colfax Massacre of 1873 in Louisiana, and takes us through four generations of two families the Tademys and the Smiths as they struggle to survive, even prosper, in a racially- hostile environment. The author interlaces history with ancestral anecdotes to produce an extraordinary piece of writing. 

 The amendments to the US Constitution, following the end of the Civil War in 1865, included abolition of slavery, federal legal protection and voting rights for African-Americans. Unfortunately, even after the Civil War, during the Reconstruction period, African-Americans were far from enjoying the benefits of these amendments. The Colfax Massacre revealed the inefficacy of those policies. African-American citizens of Colfax, Louisiana, in an effort to defend the newly-elected Republican courthouse appointees, experienced what has been called the bloodiest single instance of racial carnage in the Reconstruction era. In this period, it was the Republican Party that supported the abolition of slavery and granting civil rights to African-Americans. 

 This novel is filled with countless painful encounters, and Tademys eloquent descriptions are haunting. There is the systematic mutilation of the African-American men, who managed to survive the annihilation at the courthouse, only to encounter agonizing torture and death at the hands of the White League members. Or the description of an old mans ribs being cracked as he is repeatedly kicked, all because he refused to give up his queue to a white man, after waiting for two hours in the scorching heat. Lalita Tademy brings these accounts to life with her poignant language. 

 As the great-great-granddaughter of Sam Tademy one of the few who survived the Colfax Massacre the author has obviously put together years of research into both her family tree as well as the history of the Deep South to relate the horrors of racial repression that came before and after the Colfax carnage, along with the hope and optimism that were somehow held on to and passed on by her ancestors to the next generations. Tademy also provides illustrations and photographs which remind us that the people she describes are real and the unjust situations they were often confronted with perhaps fictionalized in varying degrees reveal the extraordinary cruelty inherent in human beings. Since the novel covers the life of the Tademys and the Smiths from 1873 to 1937, a more detailed political history post-1873 would have helped. 

 Despite its inescapable darkness, the novel isnt all sad. There is an underlying theme of courage in it, not just the courage to speak your mind or stand up for yourself but the courage to endure despair. Sam Tademy had dreams, honourable and admirable dreams, to build a school for African-American children, to own land and provide for his family. His faith, in the face of considerable adversity remains truly remarkable. Even as Lalita Tademy describes a particular historical period, she makes the hardships of the individuals real with touching stories of hope, courage and the determination to survive and pursue happiness even in challenging times. 

 PRIYANKA JHALA




</TEXT>
</DOC>