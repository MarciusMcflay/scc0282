<DOC>
<DOCNO>1070430_business_story_7716692.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Business

 Going places

 Some mutual fund schemes have brought overseas stock markets closer, says Srikumar Bondyopadhyay

 You go places, what about your investments? Does this caption, promoting a new mutual fund scheme, make you wonder whether your investments need an overseas work experience too? 

 Yes, we are talking of investing in foreign markets. How about making your money work abroad and bring home the big bucks?

 Take this: the sensex appreciated 18 per cent in the last 12 months, while Chinas Shanghai Composite index shot up 150 per cent during the same period. Many other emerging markets have provided a much better return compared with the Indian index. 

 Therefore, investment in overseas stocks could surely earn you a higher return compared with domestic shares.

 However, you cannot directly buy stocks of companies listed on overseas bourses because no local brokerages offer this facility. This is where a new breed of mutual funds can be useful. 

 Global opportunities

 Principal Global Opportunities Fund and Templeton India Equity Income have invested almost 35 per cent of their corpus in overseas stocks and earned a return of around 30 per cent. But schemes that have invested only in domestic stocks have provided a return of only 10-12 per cent over the past 12 months.

 Fidelity has also launched its International Opportunities Fund, while SBI, UTI, Kotak and Sundaram BNP Paribas have filed their offer documents for similar schemes. 

 Lean start

 The Principal Global Opportunities Fund was launched in March 2004 but failed to attract investors because of restrictions on investments in overseas stocks. 

 The restrictions were relaxed later and fund houses can now invest up to $4 billion in a year. However, there is a sub-limit each fund house can invest only up to 10 per cent of its asset under management or $150 million, whichever is lower, in overseas stocks in a given year. 

 We expect this restriction to go soon. The Reserve Bank will also come out with clarifications on the $100,000 foreign remittance limit for individuals, said a spokesperson of Fidelity Mutual Fund. If that happens, schemes that invest in overseas stocks will get a boost, fund managers feel. 

 Till 2005, mutual funds were allowed to invest only $50 million and that, too, in an overseas company with a subsidiary in India or a 10 per cent stake in an Indian firm. This restricted us to a few companies from a few countries, said a fund manager with UTI Mutual Fund. 

 Emerging markets

 However, domestic mutual funds can now invest in any stock listed on any recognised foreign stock exchange. This will help us invest in emerging markets in the Asia Pacific region such as Malaysia, Korea and Thailand, said the Fidelity official. Several emerging markets have performed better than ours, she added.

 Follow the parent

 The Principal Global Opportunities Fund re-evaluated its portfolio after the rule change. Its entire corpus is now invested in Principal Global Investors Emerging Markets Equity, a market fund managed by the foreign parent. 

 While investing in overseas stocks, mutual funds mostly depend on the expertise of their foreign partners. Foreign fund houses have a good understanding of global markets. When we launch an overseas fund, we will also follow the same fund management model, said Nilesh Shah, chief investment officer of ICICI Prudential Mutual Fund.

 Diversify and rule

 Global markets do not move in tandem. A study of historical data of stock markets around the world shows that if in one year one market is surging, another is struggling. 

 The laggards may overtake the winners in the very next year by a decent margin. For example, the Singapore market gave a 12.81 per cent return in 2005. In 2006, stocks on the exchange surged 31.02 per cent.

 Diversifying beyond the domestic turf will expose you to markets that are growing. It could be an avenue to book profit when the home market is dull. However, it also entails newer risks involving geo-political issues and foreign exchange fluctuations. 




</TEXT>
</DOC>