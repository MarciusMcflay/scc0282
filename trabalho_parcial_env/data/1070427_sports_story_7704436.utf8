<DOC>
<DOCNO>1070427_sports_story_7704436.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Confused SA no match for rampant Aussies

Mark Nicholas

 MoM Glenn McGrath celebrates one of his three wickets in the semi-final against South Africa on Wednesday 

The early-morning word from the Beausejour Stadium here was that the toss could decide the second semi-final of the World Cup. It did so, but not in the way anyone imagined. By using the new ball all but perfectly after Graeme Smith had chosen to bat, Australia blew away an inexplicably tentative South African top order. 

Swarming over their opponents like crazed warriors, Ricky Pontings team fed on poor shot selection, nervous hearts and frayed minds. Australian cricketers do this off pat and it is the South Africans who hate it most. 

The seven-wicket margin of victory (as reported in Thursdays Late City edition) suitably reflected the gulf between the sides on the day. The dynamic Andrew Symonds hit the winning runs and the young pup, Michael Clarke, was with him at the denouement. 

The Sri Lankans will know that their hands are full for Saturdays final in Bridgetown but they, and only they, have the quality, variety and attitude to break the Australians. Its ages since the defending champions have been beaten in a World Cup match and right now their cricket is close to flawless. 

South Africa have made a hash of this tournament too often for coincidence and Wednesdays performance was to type. The monkey grips their back like a vice and not for a moment did it look like letting go. It is as if their collective brain goes AWOL (absence without official leave) when so much is at stake. 

Smith and Jacques Kallis were bowled, running run down the pitch to the brand new ball. It is one thing to challenge Australia with a positive intent, quite another to lose your marbles. Kallis is a measured cricketer who just went mad. 

First he shimmied at Glenn McGrath and flayed him through cover for four. Then, when he tried the same the next ball, he lost his off stump to a yorker. This was the first over he faced from the finest bowler of the age. It was only the sixth of the match. What was he thinking about? 

The pitch was dry, almost subcontinental in look and pace. A score of 240 would have taken some chasing but South Africas two senior batsmen appeared hell-bent on something nearer 400. Following this example, AB de Villiers pushed loosely forward, Ashwell Prince wafted at a ball so wide he did well to reach it and Mark Boucher found himself drawn hypnotically to the line and length that have given McGrath his fortune. 

The score was 27 for five in the 10th over. It might have been 27 for six had umpire Steve Bucknor heard the woody sound of Herschelle Gibbs bat upon a fast inswinger by Shaun Tait. Gibbs fell when he flailed outside off stump and Adam Gilchrist accepted another easy chance. The vaunted lower order fared little better and only Justin Kemp salvaged some self-respect. 

The Australians offered no respite. Bracken made the white ball swing, while McGrath cut and seamed it, Tait bullied his way into the South African psyche with wild bursts of short and full-length bowling at high speed and Brad Hogg fooled them with his wrong un as often as he baffled them with his Chinamen. 

McGraths performance belied the fact that his career is on one final lap. He is a thoroughbred among fast and fast-medium bowlers, a man who never loses sight of the simple thing and who has remained a step ahead of his opposition. 

His record is exceptional in every format and level of the game, and he has become the leading World Cup bowler of all time. Even now, with the slippers ready by the fire, he usurps the young pretenders. His figures on Wednesday merely rubber-stamped genuine greatness. 

Not seriously threatened by the target of 150, the Australian chase did, however, begin with a hiccup or two. Gilchrist lost his middle and off stumps to an inswinger from Charl Langeveldt and Ponting was missed by the airborne Prince at midwicket. Normal service was resumed by an exquisite Ponting straight drive and some meaty leg-side hits by Hayden. 

The Australia captain had broken free, but maybe complacency got the better of him for he played across a straight ball and paid the price. Hayden steadied the jitters until his patience finally gave way. By then, the real craft had been done and dreams of a third consecutive World Cup trophy could begin. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>