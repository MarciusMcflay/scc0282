<DOC>
<DOCNO>1070825_sports_story_8235193.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 McLaren victims of the F1 paradox 

 Kevin Garside

 Lewis Hamilton in Istanbul on Friday 

The McLaren brand centre formerly the motorhome in the Istanbul Park paddock resembled the Marie Celeste. The teams management and drivers, who ordinarily would have been tucking into tea and canapes with the rest of us, were locked in febrile talks 30 miles away in a city hotel. 

Six races remain. McLaren cannot afford a repeat of the internecine spat that cost them important constructors points in Hungary. The planned rapprochement between feuding teammates Lewis Hamilton and Fernando Alonso did not happen during Formula Ones two-week recess according to one source, because they were enjoying their holidays too much. Surely not. 

Pictures of Hamilton cavorting on the Cote DAzur with the bosss daughter, Sara Ojeh, whose father Mansour owns a share of the team with Ron Dennis, would support the view that Hamilton was not losing sleep over the qualifying dust-up in Budapest, which revealed the deepening rift between the championship rivals and teammates. 

Dennis cancelled all media commitments at the circuit, which lies an hours drive to the east on the Asian side of the Bosphorus, to bang heads together. Sympathy rests with him. When the testosterone was at its height in Budapest, he was let down by his drivers, who allowed their grievances to spiral out of control and into the hands of the stewards. Not the brightest of moves. 

Left with an implausible tale to tell in explanation of how Alonso sat stationary for 10 seconds at the end of qualifying before darting out for his final run, thus denying Hamilton one last punt at pole, Dennis talked himself into a hole that ultimately cost McLaren 15 constructors points. 

The tension Dennis is required to negotiate arises from a paradox inherent in F1; the requirement to be a teammate as well as a rival. While Alonso and Hamilton might agree on paper to play by the rules, when the dander is up, courtesy falls by the way. 

They are in a championship dogfight, separated by a mere seven points. It is a state of affairs neither could have anticipated when the lights went out at Melbourne in March. Alonso believed he would be at the vanguard of the McLaren challenge. That is the position he envisaged when he signed in November 2005 to replace Kimi Raikkonen. 

The advent of Hamilton as a championship challenger changed everything. Alonso contends that without his input it would not have been possible for Hamilton to run at the front. He is aggrieved that McLaren have failed to recognise this by not throwing the full weight of the teams resources behind him. 

While it may be the case that Alonsos experience has added winning detail to the McLaren proposition, it does not explain Hamiltons superiority. Even if he has the benefit of Alonsos experience in terms of strategy and set-up, Hamilton does not have more at his disposal than Alonso. In short, they have equal resources. The only unique element they have to work with is their respective talent. 

Most observers agree, there is little between them in that regard. The problem for Dennis is managing a champion who expects greater respect and an irreverent rookie who refuses to give it, on the track at least. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>