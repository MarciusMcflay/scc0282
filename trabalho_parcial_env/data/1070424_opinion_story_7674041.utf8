<DOC>
<DOCNO>1070424_opinion_story_7674041.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 RACE AGAINST TIME

 In spite of being Americas model minority, Asians in the US feel threatened again after the Virginia killings, writes Cyril Arijit Ghosh

 Fear factor

 The International Herald Tribune reported on Thursday, April 19, 2007, that about 93,000 South Koreans are enrolled in colleges and educational institutions across the United States of America, including 460 at Virginia Tech the site of last weeks gruesome rampage carried out by Seung-Hui Cho, a Korean-born, legal, permanent resident in the US, and a final year undergraduate student at Virginia Tech. The IHT article goes on to document fears in the Korean community about a racist backlash in the US similar in spirit to the one that Arab-Americans (and those perceived to be Muslim) faced in the aftermath of the September 11, 2001 attacks on the World Trade Center and the Pentagon. 

 This is not a sign of unnecessary paranoia. The fear of backlash is clear and present. An unidentified man recently called into a show on Radio Korea, Los Angeles, to announce that his son had been spat at in school. The Washington Post reported, on April 18, 2007, that across the Washington areas 52,000-strong community of Korean-born residents one of the most educated and established immigrant groups in the country the dominant sentiment in the aftermath of the killings was one of apology. Many Koreans across the nation appear to be apologizing for the fact that Seung-Hui Cho was of South Korean descent. Media reports indicate that when the news first broke out that the murderer was Asian, and no name had been given out yet, many Koreans prayed that he would not turn out to be Korean. 

 Facebook, the online networking site for college students, already lists several hate groups dedicated to Sueng-Hui Cho, and, although the target of the attacks is seemingly the killer alone and not the entire Korean community, some of the talk on these groups makes explicit references to Asian-Americans in general and border on defamatory speech. The Raw Story, an increasingly popular online site for investigative reports, has published a screen capture that reveals the names of some of the first student groups to develop right after the rampage. Examples include F**k Cho Seung-Hui; Kill F***ers like Cho Seung-Hui; and S**ew Cho Seung-Hui.

 Meanwhile, the day after the shootings, a close friend and co-worker of mine sent me an irate email message reporting an absurd conversation she had had with her ageing landlord in Astoria, a predominantly Greek neighbourhood in the borough of Queens, New York. Here is an excerpt: 

 Landlord: I heard he was an Asian...

 Friend: Well he was born in South Korea, but was basically raised in the US

 L: Yes, yes, but those people it does not matter if they are brought up here or not, they are fanatics...their religion...you know...these Muslims are all fanatics and dangerous...

 F: Well, I am pretty sure that the shooter was not Muslim chances are that he was Christian...

 L: Okay, yes, yes, but we are not talking about that, we are talking about those fanatical Muslims...I mean they come here and wear those dresses like a woman and those hats. 

 And so on. One could brush this off as random, isolated babble from an unreasonable person. But then again, given the history of xenophobia in the US, one might be well-advised not to. 

 The US has always maintained within its liberal-democratic set-up various forms of marginalization on the basis of ascriptive identities such as race and ethnicity. Successive waves of immigrant groups have been stereotyped and vilified in the US before being accommodated within the folds of the white mainstream. 

 The Irish, the Italians, the Jews have all been for long periods, one might add systematically excluded from mainstream American Anglo-Protestant culture. Various Asian-American groups had been the object of severe racial discrimination up until the Fifties. During several decades of the 20th century, strict restrictions were placed to prohibit mass immigration from Asia, and it resumed only after the liberalization of immigration laws through the Immigration and Nationality Act, 1965. 

 These Asian immigrants (particularly southeast and east Asians) are regularly presented, both in satire and in dead earnest, as the model minorities and their achievements in educational institutions, standardized test scores, spelling contests and suchlike are ubiquitously cited. Clearly Asian immigrants are the next in line to be assimilated into mainstream American society. Most available data show their high levels of immigrant incorporation in terms of education, careers, earnings, low crime rate, and family stability.

 What does the massacre at Virginia Tech mean for this model minority, particularly for Koreans and Korean-Americans? Will they become the new Arabs in the US? Chances are that this will not happen. Most people who are in touch with the news media regularly realize this was an isolated incident involving a mentally unstable person. Nor will the status of the Asian-Americans as the model minority rapidly decline. However, one thing can be predicted: xenophobes will attempt to ride the crest of this tragedy and in the coming weeks we will hear of sporadic incidents of Asian-bashing.




</TEXT>
</DOC>