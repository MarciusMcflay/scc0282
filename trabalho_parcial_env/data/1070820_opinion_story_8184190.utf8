<DOC>
<DOCNO>1070820_opinion_story_8184190.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 OVER COLD WATERS

 GWYNNE DYER

 I n the beginning of August, remote-controlled mini-submarines planted a Russian flag on the Arctic seabed at the North Pole, symbolically claiming the Lomonosov Ridge, an underwater mountain range, as part of the countrys continental shelf. If the claim were accepted, it would expand the Exclusive Economic Zone in which only Russians can exploit minerals and other seabed resources all the way to the North Pole, but it wasnt immediately obvious how planting a titanium-encased Russian flag on the sea-floor advanced Russias case.

 Days later, Danish scientists headed for the Arctic to gather evidence for their claim that the Lomonosov ridge is actually an extension of Greenlands continental shelf, and therefore belongs to Denmark. We will be collecting data for a possible demand, explained Christian Marcussen of the Geological Survey of Denmark and Greenland. And then, last Friday, Canadian prime minister, Stephen Harper, flew to Resolute Bay in the territory of Nunavut for the photo-op of a lifetime.

 Canadas new government understands that the first principle of Arctic sovereignty is: use it or lose it, said Harper, for the second time in a week, trotting out a phrase that was originally coined to describe one of the uglier realities of nuclear strategy. Nunavut is one of the coldest human settlements, but Harper was having the time of his life. For once, there was some sort of threat to Canadas sovereignty, or at least it could be made to look as if there was, and he was the staunch patriot standing up for Canadas rights. What politician could ask for more?

 Arctic scramble

 Its actually the Canadian government that has led this round of Arctic posturing, beginning with its declaration in April that the Northwest Passage a series of channels between Canadian-owned Arctic Islands that would connect the Atlantic and Pacific Oceans if they werent choked with ice most of the year would no longer be classified as territorial waters. In future these will be Canadian internal waters, over which Canada exercises complete control.

 It was a crowd-pleasing gesture in Canada, especially since the United States of America has long denied that the Northwest Passage is even Canadian territorial waters, insisting instead that it is international waters over which Canada has no control. Washington has even sent warships through from time to time, deliberately not asking permission, which greatly annoyed Canadian nationalists. And global warming means that by 2015 or 2020 the Northwest Passage might even be open to commercial shipping for five or six months a year, so Harper had a plausible pretext for getting excited.

 But it was a pretext, not a reason, since there is actually no danger that the US is going to steal the Northwest Passage from Canada, or blockade it, or even attack Canadian ships. Yet Harper has announced that Canada will spend $7 billion on six new armed ice-breakers to assert its sovereignty in Arctic waters, build a new deep-water port at Nanisivik on the northern tip of Baffin Island for both military and civilian use, and even open a new army training centre for cold-weather warfare at Resolute Bay.

 There is a scramble for the Arctic, but it is not military. Its about laying claim to valuable resources on the basis of geographical and geological data, within the framework laid down by the United Nations Convention on Law of the Sea. This 1982 treaty sets out the rules for claiming seabed rights, which is the only issue of real economic importance to the various Arctic players. Its all about mapping the seabed, doing the seismic work, and registering your claims within ten years of ratifying the UNCLOS treaty. For Canada, that means by 2013, and it would do better to concentrate on that task, like the Russians and the Danes, rather than make meaningless military gestures.




</TEXT>
</DOC>