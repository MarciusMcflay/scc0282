<DOC>
<DOCNO>1070930_calcutta_story_8375890.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Metro

 Life under water

 Rania Mauza 

 Bangur 

 Many places in the city, including Behala Bank Colony, Shibtala, Sapgachhia, Martinpara, Kalagachhia, Garia and Rabindra Palli, are still waterlogged. Metro profiles a few families living with the filthy water in different parts of the city since the rains last week. 

 The Saha family, Lake Town

 What would it be like, being a guest in your own house? The Sahas of Lake Town came close to that, living as guests at a hotel a couple of kilometres away from home since Wednesday. 

 Water entered our Block B apartment (in Ward 29) on Sunday. Some neighbours said over the phone that the place is still inundated, says Debashis Saha, a senior consultant with Apollo Hospitals.

 His wife, Suchandra, also a doctor, is attached with Bankura Medical College. The couple moved to Hotel Host in Baguiati with their two daughters and Suchandras mother after putting up with the waterlogging for three days. Both my cars, a Santro and a Maruti 800, were fully immersed. There was no drinking water and our stock of food was going down, says Debashis. The electricity would be cut off for hours and I spotted a snake in the garage. That did it for me, he adds. 

 The family spent a fortune to get to the hotel. The rickshaws charged Rs 50 each for a ride to the Lake Town-VIP Road crossing. A taxi to Baguiati at 5pm charged an exorbitant amount. The family, which has taken two non-AC rooms, is shelling out Rs 1,560 per room per day. 

 The Bahadurs, Rania Mauza 

 The Sahus, Rania Mauza 

 The Deys, Doctor Bagan

 The Sahas at the Baguiati hotel 

 The younger of the Saha siblings, Megha, is giving school a miss as the family could not bring her books along. Megha, however, says that she is missing her computer the most. Her sister Pritha, a first-year computer engineering student at BP Poddar Institute of Engineering, is more annoyed. My exams are going on. Its difficult preparing for an exam in a hotel room. Moving to the hotel was a major hassle and a lot of money has to be spent to fix the cars.

 Officialspeak: Anup Mitra, chairman-in-council, South Dum Dum Municipality, says: We will operate more pumps. We expect the water to subside soon. The problem was caused by the dredging of canals.

 The Dey family, Doctor Bagan 

 The downpour this week only added to the misery of the Dey family, as it raised the level of the water that had accumulated here after showers in June and did not recede in three months. 

 The family has 12 members. We have forgotten what it is like to walk on dry land. Just as the water from the June rains was drying came this downpour. It will take at least three months to dry out completely, says Dipali Dey, in her late 50s. The pump in our house had to be repaired thrice in the last two months because of the waterlogging, she adds.

 The house in Doctor Bagan (Ward 122) in Behala is built at a height of three feet from the ground. Water was standing at two feet in the courtyard and the street outside on Friday. 

 Septuagenarian businessman Swapan Kumar Dey, Dipalis husband, had moved here in 1990. Waterlogging has always been a problem in these parts, but over the last four years it has become unbearable. The situation worsened during construction of a water-treatment plant in Haridevpur, Keorapukur, a kilometre from Doctor Bagan, he says.

 The civic tap in the building compound was submerged during the worst spell of rain last week and the family had to fall back on a local tubewell. The tubewell water was red.We had to make do with it after treating it with Zeoline. All the shops in the area were closed and we could not get mineral water, adds Swapan Kumar. His two sons, one of whom works at a private firm, could not go to work for three days. Swapan Kumar was not able to reach his shop in Behala Sodepur till Friday. 

 The family is worried about Tanmoy, the eldest of the children in the Dey household, who will appear at the Madhyamik. Not only is he having to give school and tutorials a miss, he is not able to study after sunset because there is no power, says Dipali. 

 She is also worried about someone falling ill, especially at night. No ambulance can come up to this place through the water after dark. Carrying people out would also be difficult because the road leading up to our house is slippery with three-month-old moss, says Swapan Kumar. 

 Officialspeak: Mayor Bikash Ranjan Bhattacharyya says the situation is unavoidable. There was no drainage system in the area that we could improve. Work is going on under the Rs 1,800-crore Asian Development Bank scheme for the Calcutta Environment Improvement Project. It will take until 2009 or 2010 for the project to have an impact.

 The Bahadur family, Rania Mauza

 At Rania Mauza near Bansdroni, partly under the Calcutta Municipal Corporation and partly under the Rajpur-Sonarpur municipality, water stands not only in the streets but also inside houses. Schools are closed and residents most men from the area are small shop-owners had to stay indoors the entire week. Water entered the single-storeyed houses on Sunday, the second day of the deluge. Occasional bursts midweek did not help. 

 Thirty-year-old Thakurdas Bahadur lives in Ward 33 with his wife Anima and an eight-month-old son. Thakurdas owns a covered cycle-van from which he sells clothes. The wheels of the van, which is parked outside his thatched house made of bamboo poles, are well under water. For seven days, he has not been able to pedal out to Naktala, where he usually sells the clothes. If theres no business one day, it affects us. I have been sitting at home for an entire week. If the water doesnt go down soon, we will have to go without food, he says. A downpour two months back had led to a months waterlogging in the area. 

 He complains that the water has not gone down more than three inches though the rain has stopped. The area gets flooded whenever it rains. In the four years I have been living here, this is the worst, he adds. Originally from Taldi in Canning, Thakurdas came to Rania Mauza after his marriage in 2003. 

 The couple are apprehensive that the stagnant water can cause an epidemic. I am worried about my little son, says Anima. Drinking water is a huge problem. I have to wade 100 m through waist-deep water to reach the only tubewell in the vicinity. Sometimes dirty water gets into the bucket. The tubewell water is reddish in colour, she says. 

 Thakurdas and Anima are disillusioned with the authorities. No one has dropped in to provide even moral support, says Thakurdas.

 The Sahu family, Rania Mauza

 Chintu Sahu, a Class IV student of the local Bikash Bharti school, the eldest of four children, proudly says that he and his siblings have braved it to school throughout the week. The school declared a rainy day on Monday but regular classes resumed on Tuesday. The houses of many other children are not flooded. So school is open, he says. 

 His mother Laxmi is not sure that the children should venture out in the water. After all, it reaches up to the shoulders of my youngest where it is deep. You hear stories of people getting electrocuted in the water. There might also be snakes, says the 34-year-old. Her husband Durgesh, a driver, stays elsewhere in the city. I never allow my children to bunk school. But now I am in a fix, says Laxmi. 

 Water entered our house on Sunday. We began heaping everything on the bed, from food items to utensils to suitcases full of clothes. Some of my books got wet, says Chintu, whose favourite subject is arithmetic. Sanitation is problem, especially for the children. The dwindling rations are yet another worry. We came to live in this rented place two years ago. We are planning to move out as soon as possible, says Laxmi. 

 Locals say water from Bansdroni, Chakdah, Rainagar, Bramhapur and Nischintapur Mauzas drains into Rania Mauza. 

 Officialspeak: Kamal Ganguly, the chairman of Rajpur-Sonarpur municipality, says: Rania was a paddy field. Water from wards 111, 112 and 113 flow here before being drained out through Kudghat canal. We expect the water to dry in seven days. 

 RITH BASU




</TEXT>
</DOC>