<DOC>
<DOCNO>1070914_opinion_story_8309670.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 HISTORY WITHOUT PEDAGOGY

 The Mughal world: Life in Indias Last Golden Age By Abraham Eraly, Penguin, Rs 495

 The relationship between the historian and his facts has always been problematic. A history is not a catalogue of facts, nor a framework of interpretation into which every piece of available information must necessarily fit. The historian is neither the humble slave, nor the tyrannical master, of his facts. The relation between the historian and his facts is one of equality, of give-and-take, writes E.H. Carr in What is History?. If Carrs verdict is read alongside Fernand Braudels notion of la longue dure which insists that, in writing history, primacy should be given to certain enduring structures over events in a relatively long historical time-segment we realize that the historians handling of facts is never neutral, nor purely judgmental, but is informed by a certain vision or philosophy.

 What today has come to be known as popular history has certain characteristic ways of dealing with facts. Abraham Eraly, in The Mughal World, demonstrates one standard method of fact-management. He reconstructs the Mughal period, scouring accounts of foreign traders and ambassadors like William Finch, Ralph Filch, William Norris, Sir Thomas Roe et al and delving into the memoirs of Mughal courtiers like Badauni and Bakhtawar Khan. Eralys extensive quotes from his sources are interspersed with his connecting comments. This lends his narrative the feel of a reportage. Eralys jargon-free language and his shunning of endnotes and footnotes are his ways of freeing history from pedagogy.

 The conditional qualification of historical facts is determined largely by the narrativity of historical accounts. A historical narrative, which F.R. Ankersmit calls narratio, is different from other narratives in its ability to accommodate contradictions without being self-contradictory. Eralys narratio, despite its journalistic bent, is flexible enough to incorporate contesting visions. While hailing the Mughal age as Indias last golden age, Eraly does not hesitate to discuss the huge paradoxes of the period. He depicts the poor rich country and the peasants lot as faithfully as he portrays the luxury of amirs and the flourishing Mughal trade.

 In keeping with the convention of popular history, Eraly interprets his facts rather than factualizing his interpretation. This approach, although acceptable, has both its strengths and weaknesses. Eraly steers clear of toeing a pre-set grand design. But he occasionally resorts to paraphrasing his source material and passing it off as interpretation.

 ARNAB BHATTACHARYA




</TEXT>
</DOC>