<DOC>
<DOCNO>1070507_nation_story_7744186.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Nation

 Winston, sarong has won the wardrobe war

 AMIT ROY 

 London, May 6: Travel, runs the clich, broadens the mind but it also seems to be making the British more Indian in their choice of food and clothes, according to a survey published today. 

 That the Indian curry has established itself as the favourite food of the British is no surprise. 

 In fact, the British do not have to go abroad to taste Indian cuisine since there are an estimated 8,000-10,000 restaurants in the UK, mostly run by Sylhettis from Bangladesh, it has to be said, serving everything from dal to onion bhaji, matar paneer, tandoori chicken and poppadom (no wonder this was Jade Goodys nickname for Shilpa Shetty). 

 What is perhaps a little more surprising is that the Indian sarong comes number one among the favourite items of apparel from abroad, ahead of even French lingerie, which is ranked second, according to the survey conducted by Thomas Cook, the travel company.

 Brits have also been heavily influenced by foreign shores in their choice of clothes according to the poll, the company said. Fifty-four per cent reckon foreign styles have had an impact on their wardrobe, with the sarong named as nations favourite fashion item from abroad. 

 The man who could claim some of the credit for the change is the former England football captain David Beckham, who was ridiculed for wearing a sarong not on the beach as most people do but to a restaurant in London. 

 But he had the support at the time of the Times columnist Matthew Parris, who applauded Beckham for being a male role model in touch with his feminine side. 

 For a footballer to wear a sarong and pink nail varnish (Beckham apparently did that, too) took courage, noted Parris. 

 Heres a modest three cheers from a columnist with no interest in football, wrote Parris. I have never met David Beckham and dont expect to, but I admire him. Beyond his footballing, I think he has shown real moral courage as a role model. 

 Parris added: Anyone who had suggested ten years ago that there would be a natural place in our sporting pantheon for a fellow who wore a sarong would have been laughed to scorn.

 Im not suggesting that Beckham sees himself as a one-man mission to civilise sporting culture: he and his wife had self-interested reasons to establish a distinctive brand. But the brand he chose says something to the world to his world in particular about his own tolerant instincts and his openness to difference, to experiment, to beauty and to style. It took guts to present this version of himself to his natural supporters. 

 That said, the ghosts of colonial sahibs, who ruled India sternly in khaki and topees, must be collectively turning in their graves. It would have been unthinkable for Winston Churchill, who scorned Gandhi for wearing the dhoti, to have declared: We shall fight on the beaches in our sarongs 

 The Thomas Cook survey, released to coincide with the May Bank holiday in the UK and the start of the holiday season, polled 2,000 people, of whom almost 80 per cent said they preferred going abroad for their holidays partly because this is often cheaper than taking a break in Britain. 

 Over half agreed that travelling overseas had influenced their lives in some way. 

 In return, although the survey did not say so, corporate India has adopted the dark pinstripe suit. 

 The survey confirmed just how much our lives have been influenced by travel. 

 A staggering 87 per cent of Brits reckon they eat more foreign food than British grub these days, with curries ranking as the nations number one foreign food, the survey also said. 

 The line-up of the top 10 foods from abroad was as follows: Indian curries; pasta; pizza; fajitas; Thai curries; French cheeses; tapas; barbecues; stir fries; and olive oil. 

 Although Thomas Cook, which was set up in 1841, offers tours to all parts of India, it has an extensive programme to popular areas such as Goa and Kerala, a spokesperson pointed out. 

 Food from Kerala is now catching on, with some up-market restaurants such as Quilon in London offering speciality cuisine from the area to a discriminating clientele. 

 When Thomas Cook invented the package holiday in the 19th century, he had no idea just how much travel would change British life, the company said. 

 Perhaps even more than other countries, we have embraced the foods and lifestyles encountered on our travels, and made them a staple part of our lives.




</TEXT>
</DOC>