<DOC>
<DOCNO>1070831_opinion_story_8251474.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 GLAMOUR AND STORIES

 Osman Ali Khan, the last Nizam

 THE LAST NIZAM: THE RISE AND FALL OF INDIAS GREATEST PRINCELY STATE By John Zubrzycki, Picador, Rs 395

 John Zubrzyckis book is about the last ruler to use the title of Nizam-ul-Mulk, who left the wealth and splendour of one of the richest princely states of India to settle in the Australian outback. Zubrzycki is a journalist from Sydney, and has travelled and worked in different parts of India for a long time. 

 Despite the title The Last Nizam, the book actually chronicles the history of the extraordinary dynasty that ruled the Deccan till 1948. Between Aurangzebs attempts to conquer the Deccan and the Hyderabadi states merger with the Indian Union, Zubrzycki does not miss the struggle between European powers to take control of India and the political intrigues that helped the English become masters of their own fate in India. 

 It is not that the Nizams, who remained loyal to the Mughals, did not achieve much during their long rule. Ironically, they are remembered only for their riches, their wine and women and their dirty palace intrigues that were common to even European feudal rulers. Zubrzycki seems to be a victim of such a false notion as well, and tends to focus more on these trifles than on serious matters of the state. 

 His book has little on the economy, administration and the lives of the people under the Nizams. Zubrzycki gives a short history of the dynasty and begins with the first Nizam, a man of colossal stature who ruled from 1724 to 1748 and established the Asaf Jahi dynasty. The title of Asaf Jah was given by the Mughal Emperor Mohammad Shah. The seventh and last Nizam ruler, Osman Ali Khan (1911-1948), was more interested in composing ghazals than everything else.

 From the conquest of Golconda, Zubrzycki moves to the seven Nizams and their times in a way that offers us a glimpse of the magnificent palace life. In the background, there is also the history of the rapid English ascendancy in India. 

 The author seems to use a lot of material to write with such ease. Nevertheless, he is more interested in penning stories of the Nizams their power and splendour, the palace intrigues, the harems and even anecdotes. Perhaps, Zubrzycki knows only too well that these tickle curiosity and make books sell. Even the eighth Nizam has more words devoted to his lavish lifestyle than to the serious issues of his life. 

 The author makes his carefully-crafted narrative merge with the chronicle with such subtlety that we may not be certain as to whether he is depicting the past or denigrating the times and its people. Even the quotations used are carefully picked in order to prove his point of view. If we prepared ourselves to not expect much substance, then we would relish the snippets offered on the pages of the book. 

 After all, The Last Nizam is a gripping tale that is as interesting as the stories of The Arabian Nights, written in an easy and flowing narrative that is diffi- cult to put down till the last page. Although the title promises a book on the last Nizam, the book is ultimately not about him. It is not a biography, nor a history of India. Nor will it add much to the tomes already written about various rulers of the Deccan. It is a chronicle of one of the most fabulous dynasties of India, written by a foreigner who has ultimately failed to understand the country and its people.

 SHAMS AFIF SIDDIQI




</TEXT>
</DOC>