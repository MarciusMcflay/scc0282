<DOC>
<DOCNO>1070417_sports_story_7656874.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Success at the highest level hangs by a mere thread

Mark Nicholas

 Paul Collingwood has turned out to be Englands miracle 

Two weeks from now, we shall at last know who has won the World Cup. It is glib to say we no longer care, for Englands painful progress continues to fill column inches. Until Tuesday night at least, Michael Vaughans insecure team are among the candidates. This is incredible. 

At no stage of the tournament have England appeared to have a clue. It is as if modern one-day cricket is an alien a threat to overcome rather than a friend to embrace. Only Kevin Pietersen with the bat, Andrew Flintoff with the ball, and Paul Collingwood would rate a mention in Australian, Sri Lankan or New Zealand ranks. 

The remaining talent floats around aimlessly. There is no wristwork from the batsmen, little variety or subtlety in the bowling and few predators in the field. 

Thankfully, South Africa have proved equally one-dimensional, so we may well have a contest on Tuesday, if not a spectacle. This lively Barbados pitch is tricky to bat on at 9.30 am and those who start the game best may well have a place in the semi-final as their reward. 

Resisting the new ball will take technique and courage more than flair. For this reason, Andrew Strauss should return to the top of the order, giving an experienced left/right combination for the South Africans to consider. The courage needed is less physical than mental, for limited batsmen must bide their time, choose the attacking options carefully and leave something for the middle-order to feed upon. 

News that Marcus Trescothick is making spring runs cannot be easy for Vaughan. Pleased as he will surely be by his friends rehabilitation, it must niggle that Trescothick is doing so in a south-west corner of England instead of the Caribbean. Perhaps an Englishman should feign injury and an SOS be sent to Somerset. 

Sportsmen need fresh minds and vitality. It is impossible to remain at fever pitch for months on end, which is why Australia lost the Commonwealth Bank Series and then in New Zealand, too. Even Ricky Ponting was weary. No team is safe from over-exposure, success at the highest level hangs by a mere thread. Flintoffs batting has a jaded look while Pietersens remember, he had a rib related sabbatical is alive. 

The miracle is Collingwood, who has been superb. A time out during the middle period of the Australian series allowed him to take stock and adopt a back-to-basics approach that would have been applauded by government pretenders. 

He is collecting runs in the manner of a craftsman, never hitting the ball too hard, but invariably into a gap. Brilliant in the field and a skilful and calm bowler, he is proof that to become exceptionally good at something does not take talent alone. 

This, of course, is the frustration about Englands ongoing one-day tribulations. The team should be so much better than they are. 

Part of the reason is inheritance. Administrators see Test cricket as morally superior. Dermott Reeves Warwickshire team was not taken seriously because of their irreverence; Mark Alleynes Gloucestershire were considered a freak in which the sum of the parts was much greater than the whole. 

None of those players, bar Reeve himself, albeit briefly, played for their country at a time the 1996 and 1999 World Cups when Englands one-day cricket was lamentable. Now, with Sussex and Lancashire pre-eminent, only Flintoff is regularly called upon. 

Were Lasith Malinga or Shaun Tait English, their unusual actions would have been coached towards the more orthodox. Why is there still no wrist spinner or modern-style finger-spinner anywhere in English cricket? If James Kirtley was from another land, would anyone care less about his action? 

It cannot be a coincidence that Englands best batsman perhaps best ever one-day batsman is not English. The gift of the unorthodox is as much in the mind as in the eye, hands or feet. While Vaughan and Ian Bell play good-length, off-stump balls to mid-off, Pietersen whips them through mid-wicket. The unorthodox sends a shiver through the spine of any opponent and immediately changes the dynamic between bat and ball. 

The first relevant one-day innovations came from Martin Crowes New Zealand in the 1992 World Cup. Mark Greatbatch was given his head at the top of the order and Dipak Patel, a modest off-spinner, was given the new ball. Reverse swing emerged as a genuine threat, particularly at pace, and then, in 1996, Arjuna Ranatungas Sri Lanka took the Greatbatch thing to the limit with Sanath Jayasuriya and Romesh Kaluwitharana. 

The 1999 World Cup illustrated the need for a more orthodox approach in English conditions but England themselves were no better, lurching out of the tournament as witlessly as West Indies will almost certainly do here. Like it or not, England has no feel or sympathy for the one-day game at international level. At first-class level there is so much of it that it is no longer special overkill challenges the once golden goose. 

Though Australia are playing the most powerful cricket in the Caribbean, some of the most thoughtful and attractive has once again come from the Sri Lankans. Mahela Jayawardene uses the power plays craftily and each of his bowlers offer something different. They appear to be enjoying themselves and see Pontings team as more vulnerable than they see themselves. 

As all the matches to come have real meaning about time, too the pitches, and how the players adapt, may well decide their outcome. 

 THE DAILY TELEGRAPH 




</TEXT>
</DOC>