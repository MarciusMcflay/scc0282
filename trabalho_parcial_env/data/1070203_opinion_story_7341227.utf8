<DOC>
<DOCNO>1070203_opinion_story_7341227.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 GONE WITH THE WINDIES

 Brian Lara, among the all-time 

cricket greats, is looking at 

retirement. He played his last one-day match in India and may retire after the World Cup

 Genius, they say, is unpredictable. Cricketing geniuses are no exception. Few sporting sights can match the opulent stance of a batsman with his bat raised high in the air, weight poised on a bent front knee, eyes low and level. Then the bat slashes down like a magic wand and the ball hits the fence in the blink of an eye. 

 If there is one left-handed batsman who has built massive scores with equally massive pomp, it is Brian Charles Lara. And his pomp has often brought him at loggerheads with the cricket authorities of his country. Yet, Lara has always emerged triumphant. He has had his share of injuries and dry runs. There have been times when the torrent of runs has become an occasional spurt. But ultimately his bat has always had the final say. Which is why, as he played his last one-day innings in India, the image of a man with his bat held high continued to wield its magic. 

 Lara is looking at retirement. For most sports lovers, it is difficult to believe that the 37-year-old may not be around after the World Cup. After all, Lara seemed destined for a career in sports. His father Bunty Lara and his older sister Agnes Cyrus were quick to spot his talent when he was only six years old and had him enrolled in a local coaching centre for weekly sessions. He was a natural athlete and we had absolutely no doubt that he would excel in games, Bunty recalled in an interview. 

 But cricket was not his only interest in those formative years. He became a member of the under-14 Trinidad and Tobago soccer team and tried his hand at table tennis too. But it was his move to Fatima College in Port of Spain that convinced him cricket was really his forte. 

 Records seemed to tumble in front of Laras prowess. In 1987, he broke the West Indies youth batting record and made his first class debut for Trinidad and Tobago in 1988 in the Red Stripe Cup against Barbados. Facing a formidable bowling attack from legendary pacers Joel Garner and Malcolm Marshall, Lara cracked 92. He was selected for the West Indies team later in the year. 

 But not everything moves like a fairy tale. That was when Lara lost his father, and was compelled to withdraw from the team. However, it soon became apparent that he was a man who had greatness thrust upon him. At the age of 20, he became Trinidad and Tobagos youngest captain, leading them to victory in the one day Geddes Grant Shield. In 1990, he made his international debut for West Indies against Pakistan, scoring 44 and 6. 

 And then there was no stopping Lara. Sambaran Banerjee, then a national Indian selector, was among those who admired his mettle more than a decade ago. I realised the first time I saw him play that he was someone who would leave his mark for a long time to come, recalls Banerjee. Indeed, Lara has scored 20 per cent of the total runs of his team during his career. 

 Agrees former India captain Dilip Vengsarkar, He has been a fantastic batsman and has borne the burden of batting for a weak West Indies team. He adds that Brian Laras overall outstanding performance is not affected in any way by the fact that he has not delivered his best in India. 

 And indeed, Lara has weathered many a storm. Throughout the Nineties, the West Indies cricket team went through a recovery process after the retirement of their fabled pace battery and feared batsmen. It was during this period that Brian Lara brought out his best performance. He won and lost the captaincy, but reiterated genius time and time again. 

 One of his predecessors, the legendary Gary Sobers, had set a world record of the highest Test score of 365. For decades, the record was untouched and many a pundit thought that Sir Gary would never be overtaken, most definitely not by a member of the still struggling West Indies. But geniuses do not perform according to predictions or expert opinion. In 1994, Brian Lara made cricketing history by scoring 375 against England in Antigua. Sir Gary is reported to have said that his record was broken by someone who was very deserving. 

 But still there were sceptics who wanted to brush him off as a onetime wonder. As if to prove them wrong, he scored an incredible 501 for Warwickshire against Durham the same year. Brian Lara became a true celebrity. Yet, when he declined selection for the West Indies team over a dispute over his personal Cable and Wireless sponsorship deal many thought it was the end of the road for him. However, the issue was resolved and he was reappointed captain of West Indies for the third time in April, 2006. 

 Brian Lara as an individual has been scintillating on and off the field. He has dated model Leasal Rovedas and has named his daughter Sydney in honour of the ground where he scored a highly acclaimed double century. 

 Throughout his career there have been comparisons with the Indian batting maestro Sachin Tendulkar. It is not quite fair to compare the two since they come from different countries and different backgrounds, but there can be no denying that both are great players, avers former India Test bowler Karsan Ghavri. Ghavri says that Lara is among the best cricketers the world has ever seen. He is among the all time greats and his runs speak for him, notes Ghavri. Former Indian cricketer Pranab Roy points out that Lara recovered his highest test score from Mathew Hayden after scoring a mammoth 400 against England. Out of 1000 cricket players, only one manages to score the amount of runs that he has, he adds. 

 Indeed, the records held by Brian Lara would need an almanac to fill in. But his enthusiasm still remains the same as when he started as a youngster. I want to talk of cricket as I did as a teenager, he said recently, adding that he wants to see the West Indies team move on after he has hung up his pads. When he does retire, spectators and team members will miss the bat that acted like a guillotine. Bowlers, though, will heave a sigh of relief.




</TEXT>
</DOC>