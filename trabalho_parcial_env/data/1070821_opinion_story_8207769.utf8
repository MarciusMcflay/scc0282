<DOC>
<DOCNO>1070821_opinion_story_8207769.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Opinion

 PRICE DISCOVERY

 Indias cellphone craze has not only filled all empty spaces with mutters; it has caused a massive spectrum crunch. It did not take the wizards of the department of telecommunications to predict it; even a child could have foreseen it. For all these years it did nothing but hope: it kept hoping that the army would vacate the swathes of spectrum it was not using. The army, however, saw no reason to do so. Especially after the DoT hinted that some money may be available to persuade the armed forces to vacate spectrum, their appetite was whetted. All they had to do was to wait till the situation got desperate; they could then extract an exorbitant price. That moment would seem to have arrived, but the forces are still in no hurry to give up spectrum.

 Spectrum shortage has had predictable consequences. For one thing, the DoT cannot license any more operators; so whilst only two years ago the industry was intensely competitive, today we have oligopoly in many circles. Far from having to fear competition, operators know that their competitors are going to find it difficult to sign on more customers. And they themselves are often suffering from spectrum shortage. So there is no incentive any more to compete on price. On the contrary, the incentive is for increasing prices. Hutch and Airtel did raise prices, but the others hesitated, for the new minister wants them to reduce charges and make him popular. So they tried something more covert. They increased charges for short message service. These little love-calls take a second or two to transmit as against conversations, which go on and on; and SMSs can be queued up and transmitted when spectrum is vacant. The operators thought they would make another penny or two on these dainty messages. But they were wrong. They have displeased TRAI. 

 So the shortage continues. As any youth learns in his first economics lesson, the first thing to do in the circumstances is to raise the price of the scarce resource spectrum in this case. The operators are against it, since it would only transfer money to the DoT, which they see as a useless nuisance. They would like to trade spectrum amongst themselves, but the DoT is against it since it would get no share of the extra money some of them would make. But a market in spectrum has to come sooner or later. The DoT should take its head out of the sand and ask itself what, if any, rules should govern the market. The most important point is that the market should function openly and provide a transparent price discovery process. That would be to the DoTs advantage; it can then put a small tax on market transactions. So, far from banning it, the DoT should set up the market under its own aegis.




</TEXT>
</DOC>