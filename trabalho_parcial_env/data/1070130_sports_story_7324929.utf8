<DOC>
<DOCNO>1070130_sports_story_7324929.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Sports

 Now were all even: Woods

Denver: They crisscross the globe, dominating on almost every continent, one whacking a small, white ball better than anyone in the world, the other a fuzzy green one. 

Tiger Woods and Roger Federer are making history in their respective sports, owning golf and tennis the way very few ever have. 

Over the weekend, they wrote new chapters in their march toward sports history. 

No. 1 in golf, Woods won his seventh straight PGA Tour event on Sunday in San Diego, a record eclipsed only by Byron Nelson back in the 1940s when the competition wasnt as tough. 

No. 1 in tennis, Federer won the Australian Open a half a world away, marking his 10th Grand Slam victory and furthering his quest to become the best tennis player ever. 

Their excellence has united them over the years. They have become friends, and neither lets his own accomplishment go unnoticed by the other. 

Hell text me and say he won one there, Woods said in a TV interview Sunday. Now, Ive got to text him and say were all even. 

Its a friendly rivalry between two men who never have to play each other the 31-year-old golf star and a 25-year-old counterpart on the tennis circuit. 

They are athletes who dominate in individual, sometimes lonely, sportsmen who recognise the commitment and sacrifice that must be made, even if the games they play may seem foreign to the other. 

Last year, Woods was in Federers box to watch his US Open victory. A few months later, Federer walked the course with Woods at a golf tournament in China. 

Though many wonder how the joys of fatherhood his wife, Elin, is due with their first child next summer might affect him, Woods conceded his charge toward the top of golfs record book might be a more realistic quest than Federers. 

The only thing going for me, is Ive got longevity in my corner, Woods said, acknowledging the reality that golfers can play on a top level into their 40s, while tennis players fade out much earlier. 

Not so long ago, Nelsons record of 11 victories seemed untouchable. Nelson racked up those wins under very different circumstances, before golf reached the heights its at today. 

 (AP) 




</TEXT>
</DOC>