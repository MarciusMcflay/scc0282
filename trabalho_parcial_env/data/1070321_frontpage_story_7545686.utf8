<DOC>
<DOCNO>1070321_frontpage_story_7545686.utf8</DOCNO>
<TEXT>


The Telegraph - Calcutta : Frontpage

 American malaria-buster with Indian breeding power

 G.S. MUDUR

 New Delhi, March 20: A bunch of Indian-origin mosquitoes bred in an American laboratory may herald a new and potentially controversial way to control malaria. 

 US scientists today announced that mosquitoes genetically engineered to resist the malaria parasite can breed better than wild mosquitoes. 

 The disclosure raises the possibility that the genetically modified mosquitoes could eventually replace the ones that can carry the disease.

 But the technology is also likely to stir a debate on the ethics and safety of releasing genetically modified mosquitoes into the environment.

 A research team at the Johns Hopkins University in Baltimore has genetically modified a group of Anopheles stephensi mosquitoes with origins in India to prevent them from becoming infected with a malaria parasite.

 Marcelo Jacobs-Lorena and his colleagues had five years ago shown that inserting a special gene into mosquitoes can prevent a malaria parasite from infecting the insects gut.

 The new study, reported in the Proceedings of the National Academy of Sciences, is the first to show that genetically modified mosquitoes have a reproductive advantage when fed with malaria infected blood.

 These mosquitoes dont pick up the parasite, but live longer and have more offspring than wild mosquitoes, said Jason Rasgon, a team member at the Johns Hopkins University Malaria Research Institute.

 Rasgon cautioned that the studies only demonstrate proof of principle of a possible future strategy. This is a very new technology and we need to be cautious, Rasgon told The Telegraph over phone. The social and ethical issues will be more important than the science. 

 In their experiments, the Johns Hopkins researchers combined equal numbers of transgenic and wild mosquitoes and gave them meals of blood infected with Plasmodium bergii, a malaria parasite that infects mice. 

 After nine generations, 70 per cent of the mosquitoes were genetically modified, compared to only 50 per cent at the beginning of the experiment a signal that the modified mosquitoes breed better than wild mosquitoes.

 Other scientists described the finding as an important step, but pointed out that the technology has yet to be demonstrated against Plasmodium falciparum or Plasmodium vivax, which cause malaria in humans.

 The Anopheles stephensi mosquitoes altered through genetic engineering are believed to be the descendants of mosquitoes collected from India during the seventies, and continuously bred at the US National Institutes of Health. 




</TEXT>
</DOC>